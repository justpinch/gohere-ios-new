module Fastlane
  module Actions
    module SharedValues
      TARGET_ITC_TEAM_ID = :TARGET_ITC_TEAM_ID
    end

    class GetProjectTargetAction < Action
      def self.run(params)        
        project = Fastlane::Actions::GetProjectAction.run({})
        development_team = project["development_team"]
        targets = project["targets"]

        if !targets.has_key?(params[:name])
          UI.user_error!("Target #{name} not found")
        end
        target = targets[params[:name]]

        # Get development team ID
        development_team = target.dig("settings", "configs", "Release", "DEVELOPMENT_TEAM") ||
          target.dig("settings", "base", "DEVELOPMENT_TEAM") || development_team

        itc_team_id_mapping = {
          "YR8R85LBZP" => "118671930",  # AppFocus B.V.
          "T2S5C783US" => "122201747",  # GoHere B.V.
          "CSAB5R6X22" => "1088761",    # Transavia Airlines C.V.
          "B474F5HMC6" => "121549015",  # VVV Terschelling
          "UTRGT3B9RM" => "827811",     # Insiders Online B.V.
          "33U6953L5Q" => "122406635",  # Buitengewoon Reizen B.V. (Freely)
        }

        {
          :bundle_id => target.dig('templateAttributes', 'bundleId'),
          :bundle_version => target.dig('templateAttributes', 'marketing_version'),
          :development_team => development_team,
          :itc_team_id => itc_team_id_mapping[development_team],
		  :firebase_app_id => target.dig('templateAttributes', 'firebase_app_id')
        }
      end

      #####################################################
      # @!group Documentation
      #####################################################

      def self.description
        "Get iOS target settings"
      end

      def self.available_options
        [
          FastlaneCore::ConfigItem.new(key: :name,
                                      #  env_name: "FL_GET_PROJECT_TARGET_API_TOKEN", # The name of the environment variable
                                       description: "Target name",
                                       verify_block: proc do |value|
                                          UI.user_error!("No target name for get_project_target action given, pass using `name: 'TargetName'`") unless (value and not value.empty?)
                                       end),
        ]
      end

      def self.output
        []
      end

      def self.return_value
        "Hash with target settings"
      end

      def self.authors
        ["ivasic"]
      end

      def self.is_supported?(platform)
        platform == :ios
      end
    end
  end
end
