module Fastlane
  module Actions
    module SharedValues
    end

    class GetProjectAction < Action
      def self.run(params)
        require 'yaml'
        proj = YAML.load_file('project.yml')

        development_team = proj["settings"]["base"]["DEVELOPMENT_TEAM"]
        targets = proj["targets"]
        project_version = proj.dig("settings", "base", "CURRENT_PROJECT_VERSION")
        build_version = proj.dig("settings", "base", "PRODUCT_BUILD_VERSION")

        {
          "development_team" => development_team,
          "targets" => targets,
          "project_version" => project_version,
          "build_version" => build_version
        }
      end

      #####################################################
      # @!group Documentation
      #####################################################

      def self.description
        "Read project.yml and return a hashmap with project settings"
      end

      def self.available_options
        []
      end

      def self.output
        []
      end

      def self.return_value
        "Hash with project settings"
      end

      def self.authors
        ["ivasic"]
      end

      def self.is_supported?(platform)
        platform == :ios
      end
    end
  end
end
