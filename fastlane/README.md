fastlane documentation
----

# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```sh
xcode-select --install
```

For _fastlane_ installation instructions, see [Installing _fastlane_](https://docs.fastlane.tools/#installing-fastlane)

# Available Actions

## iOS

### ios deploy

```sh
[bundle exec] fastlane ios deploy
```

Submit a build of an app to the AppStore

 * **`app`**: The app target name as defined in Xcode project

 * **`EN`**: The release notes in English (optional)

 * **`NL`**: The release notes in Dutch (optional)

### ios add_tag

```sh
[bundle exec] fastlane ios add_tag
```

Creates a git tag for the submitted build

### ios deploy_all

```sh
[bundle exec] fastlane ios deploy_all
```

Submit builds of all apps to the AppStore

### ios deployStaging

```sh
[bundle exec] fastlane ios deployStaging
```



----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.

More information about _fastlane_ can be found on [fastlane.tools](https://fastlane.tools).

The documentation of _fastlane_ can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
