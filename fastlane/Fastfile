# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins
#

# Uncomment the line if you want fastlane to automatically update itself
# update_fastlane

default_platform(:ios)

platform :ios do

	desc "Submit a build of an app to the AppStore"
	desc " * **`app`**: The app target name as defined in Xcode project"
	desc " * **`EN`**: The release notes in English (optional)"
	desc " * **`NL`**: The release notes in Dutch (optional)"
	lane :deploy do |options|
		app = options[:app]
		if !app
			UI.user_error!("You need to specify app name as a parameter. E.g. `app:GoHere`")
		end
		if !options[:EN]
			UI.important("No english release notes provided")
		end
		if !options[:NL]
			UI.important("No dutch release notes provided")
		end

		if !options[:skip_xcodegen]
			xcodegen(regenerate: true)
		end

		if !options[:project]
			options[:project] = get_project()
		end

		target = get_project_target(name: app)

		team_id(target[:development_team])
		cert(output_path: "fastlane/certificates")
		sigh(app_identifier: target[:bundle_id], output_path: "fastlane/profiles")

		build_app(
			project: "GoHere.xcodeproj",
			scheme: app,
			clean: true,
			export_xcargs: "-allowProvisioningUpdates",
			configuration: "Release",
			include_bitcode: false,
			output_directory: "fastlane/builds",
			buildlog_path: "fastlane/builds",
		)

		sentry_upload_dsym(
			auth_token: '156eae1f465b4c9f93cddaed93d7d032a464ce4af7fc4d3490807e4ed447ee85',
			org_slug: 'appfocus',
			project_slug: 'gohere-ios',
		)

		ENV['FASTLANE_ITC_TEAM_ID'] = target[:itc_team_id]
		upload_to_app_store(
			app_identifier: target[:bundle_id],
			skip_screenshots: true,
			skip_metadata: true,
			skip_app_version_update: false,
			submit_for_review: false,
			release_notes: {
				'en-US' => options[:EN],
				'nl-NL' => options[:NL]
			}
		)

		# add git tag
		add_tag(options)
	end

	desc "Creates a git tag for the submitted build"
	lane :add_tag do |options|
		app = options[:app]
		target = options.dig(:project, "targets", app)
		project_version = options.dig(:project, "project_version")
		build_version = options.dig(:project, "build_version")

		marketing_version = target.dig('templateAttributes', 'marketing_version')
		tag = "builds/#{app}/#{marketing_version}-(#{project_version}.#{build_version})"
		add_git_tag(tag: tag, force: true)
	end

	desc "Submit builds of all apps to the AppStore"
	lane :deploy_all do |options|
		# always regenerate Xcode project
		xcodegen(regenerate:true)

		project = get_project()
		targets = project["targets"]
		targets.each do |name, target|
			if name == "UITests" or name == "Tests"
				next
			end
			opts = options
			opts[:app] = name
			opts[:skip_xcodegen] = true
			opts[:project] = project
			deploy(opts)
		end
	end

	desc "Run make clean"
	private_lane :clean do |options|
		Dir.chdir("..") do
			sh("make", "clean")
		end
	end

	desc "(Re)Generate Xcode project"
	private_lane :xcodegen do |options|
		if options[:regenerate]
			clean()
		end
		Dir.chdir("..") do
			# Make sure Cocoapods is in the same Ruby env as Fastlane!
			# Otherwise installing pods will fail below
			sh("make", "project")
		end
	end
	
	lane :deployStaging do |options|
		app = options[:app]
		if !app
			UI.user_error!("You need to specify app name as a parameter. E.g. `app:GoHere`")
		end
		if !options[:EN]
			UI.important("No english release notes provided")
		end
		if !options[:NL]
			UI.important("No dutch release notes provided")
		end

		if !options[:skip_xcodegen]
			xcodegen(regenerate: true)
		end

		if !options[:project]
			options[:project] = get_project()
		end

		target = get_project_target(name: app)
		app_id = target[:bundle_id]

		team_id(target[:development_team])
		cert(output_path: "fastlane/certificates")
		sigh(app_identifier: target[:bundle_id] + ".debug", output_path: "fastlane/profiles", adhoc: true)

		build_app(
			project: "GoHere.xcodeproj",
			scheme: app,
			export_method: "ad-hoc",
			clean: true,
			export_xcargs: "-allowProvisioningUpdates",
			configuration: "Release",
			include_bitcode: false,
			output_directory: "fastlane/builds",
			buildlog_path: "fastlane/builds",
			xcargs: "GOHERE_ENVIRONMENT=\"STAGING\"",
		)

		ENV['FASTLANE_ITC_TEAM_ID'] = target[:itc_team_id]
		firebase_app_distribution(
            app: target[:firebase_app_id],
            service_credentials_file: Configuration/api-6015683645739740254-799449-c0175dbedc08.json,
            testers: "joris.favier@pinch.nl",
            release_notes: "Test staging"
        )

		
	end
end
