#!/bin/sh
set -e

die () {
    tput setaf 1 && echo >&2 "$@" && tput sgr0
    exit 1
}

success () {
    tput setaf 2 && echo "Success" && tput sgr0
}

[ "$#" -eq 5 ] || die "5 arguments required, $# provided. Expecting app target name, display name, bundle identifier suffix, app marketing version and firebase project ID."

echo "Copying folder skeleton..."
mkdir Targets/$1
cp -r Whitelabels/TargetTemplate/ Targets/$1 || die
success

echo "Modifying config"
sed -i '' -e "s/#APPNAME#/$1/g" Targets/$1/Source/Config.swift
success

echo "Adding target to project.yml..."

target="$(cat <<-EOF

  ${1}:
    templates:
      - Whitelabel
    templateAttributes:
      bundleId: app.gohere.${3}
      app_display_name: ${2}
      marketing_version: ${4}
      fb_url_scheme_suffix: ${3}
      firebase_project_id: ${5}
EOF
)"

echo "$target" >> project.yml
printf "$target\n"

printf "Regenerate the Xcode project for the target to appear"
success
