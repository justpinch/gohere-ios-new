//
//  KvIntroView.swift
//  KiliVilla
//
//  Created by jorisfavier on 21/10/2022.
//
import SwiftUI

struct KvIntroView: View {
	@State var text: String = ""
	let introCompleted: () -> Void

	var segments: [LottieIntroView.AnimationSegment] {
		[
			.init(fromMarker: "StartCopy-1", toMarker: "EndCopy-1", text: "onboarding_animation_copy1".localizedFromTarget()),
			.init(fromMarker: "StartCopy-2", toMarker: "EndCopy-2", text: "onboarding_animation_copy2".localizedFromTarget()),
			.init(fromMarker: "StartCopy-3", toMarker: "EndCopy-3", text: "onboarding_animation_copy3".localizedFromTarget()),
		]
	}

	var body: some View {
		VStack {
			Spacer()
			ZStack {
				LottieIntroView(name: "onboarding-animation", animationSegments: segments, segmentText: $text)
					.opacity(0)
				Image("logo-mark-rounded")
					.resizable()
					.aspectRatio(contentMode: .fit)
					.frame(width: 100, height: 100)
					.padding()
					.background(Color.gh(.appIcon))
					.clipShape(Circle())
					.background(Color.gh(.backgroundPrimary))
			}

			Text(text)
				.font(.gh(.title2))
				.foregroundColor(.gh(.labelPrimary))
				.multilineTextAlignment(.center)
				.padding([.leading, .trailing], 48)
				.frame(minHeight: 100)
				.padding(.top, 35)

			Spacer()

			Button("start", action: introCompleted)
				.primaryButtonStyle()
				.padding([.leading, .trailing, .bottom])
		}
	}
}

