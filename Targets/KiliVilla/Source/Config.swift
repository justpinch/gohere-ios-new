import Foundation
import SwiftUI

struct Config: TargetConfig {
	static let appName = "Kili Villa"
	static let appRealmForMixpanel = "Kilivilla"

	static let appRealm = "kili-villa"
	static var apiKey: String {
		switch API.Environment.current {
			case .local:
				return ""
			case .staging:
				return "bd6e9b79c85a47ec8a5194bf87f8bc2a"
			case .production:
				return "e242788b5cdb4c55a5affbbfc9a4346b"
		}
	}
	static func onboardingIntroView(introCompleted: @escaping () -> Void) -> AnyView {
		AnyView(KvIntroView(introCompleted: introCompleted))
	}
}
