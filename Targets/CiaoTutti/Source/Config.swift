import Foundation

struct Config: TargetConfig {
	static let appName = "CiaoTutti"
	static let appRealmForMixpanel = "CiaoTutti"

	static let appRealm = "ciaotutti"
	static var apiKey: String {
		switch API.Environment.current {
			case .local:
				return ""
			case .staging:
				return "3fad771e537a45709e6af377f53d6315"
			case .production:
				return "3b2ce894fb174e209256f713e5d67199"
		}
	}
}
