import Foundation
import UIKit
import SwiftUI


extension Theme.ColorScheme {
	// Tint
	static let tintPrimary				= color(light: 0xE83634, dark: 0xCF2F2D)
	static let tintSecondary			= color(light: 0xE83634, dark: 0xCF2F2D)
	static let tintTertiary				= color(light: 0x000000, dark: 0x5C5C5C)
	static let tintQuaternary			= color(light: 0xF4F4F4, dark: 0x262626)

	// On Tint
	static let onTintPrimary			= color(light: 0xFFFFFF, dark: 0xFFFFFF)
	static let onTintSecondary			= color(light: 0xFFFFFF, dark: 0xFFFFFF, ad: 0.8)
	static let onTintTertiary			= color(light: 0xFFFFFF, dark: 0xFFFFFF)
	static let onTintQuaternary			= color(light: 0x343434, dark: 0xFFFFFF)

	// Backgrounds
	static let backgroundPrimary		= color(light: 0xFFFFFF, dark: 0x000000)
	static let backgroundSecondary		= color(light: 0xE8E8E8, dark: 0x111111)
	static let backgroundTertiary		= color(light: 0xF4F4F4, dark: 0x353A4B)

	// On Background
	static let labelPrimary				= color(light: 0x343434, dark: 0xC8C8C8)
	static let labelSecondary			= color(light: 0x65656B, dark: 0x8E8E93)
	static let labelTertiary			= color(light: 0xAEAEB2, dark: 0x636366)
	static let labelQuaternary			= color(light: 0xC7C7CC, dark: 0x48484A)

	// Bars
	static let navbar					= color(light: 0xE83634, dark: 0xCF2F2D)
	static let tabbar					= color(light: 0xFFFFFF, dark: 0x2E2E2E)

	// On Bars
	static let onNavbar					= color(light: 0xFFFFFF, dark: 0xC8C8C8)
	static let onTabbarDefault			= color(light: 0x343434, dark: 0xC8C8C8, al: 0.5, ad: 0.5)
	static let onTabbarSelected			= color(light: 0xE83634, dark: 0xCF2F2D)

	// Other
	static let notification				= color(light: 0xE83634, dark: 0xE83634)
	static let placeholderText			= color(light: 0xD1D1D6, dark: 0x3A3A3C)
	static let separator				= color(light: 0x8E8E93, dark: 0xB5B5BA, al: 0.3, ad: 0.3)
	static let separatorOpaque			= color(light: 0xE5E5EA, dark: 0x373737)
	static let link						= color(light: 0xE83634, dark: 0xE83634)

	// On Other
	static let onNotification			= color(light: 0xFFFFFF, dark: 0xFFFFFF)

	// Utility
	static let utilityPositive			= color(light: 0x64C38A, dark: 0x45A16A)
	static let utilityError				= color(light: 0xC90000, dark: 0xA90000)
	static let utilityNeutral			= color(light: 0x7C88B4, dark: 0x586491)

	// On Utility
	static let onUtilityPositive		= color(light: 0xFFFFFF, dark: 0xFFFFFF)
	static let onUtilityError			= color(light: 0xFFFFFF, dark: 0xFFFFFF)
	static let onUtilityNeutral			= color(light: 0xFFFFFF, dark: 0xFFFFFF)

	// Affiliate
	static let affiliate				= color(light: 0xE83634, dark: 0xCF2F2D)

	// On Affiliate
	static let onAffiliate				= color(light: 0xFFFFFF, dark: 0xFFFFFF)

	// Pin
	static let pin						= color(light: 0xFFFFFF, dark: 0x000000)

	// On Pin
	static let onPin					= color(light: 0xE83634, dark: 0xCF2F2D)

	// Rating
	static let rating					= color(light: 0xE8C500, dark: 0xE8C500)

	// App Icon
	static let appIcon					= color(light: 0xFFFFFF, dark: 0x000000)
	static let onAppIcon				= color(light: 0x000000, dark: 0xFFFFFF)
	static let filterIcon				= tintPrimary
}



struct ThemeColor_Previews: PreviewProvider {
	static var previews: some View {
		ThemePreviewProvider.previews
	}
}
