import Foundation

struct Config: TargetConfig {
	static let appName = "Freely"
	static let appRealmForMixpanel = "Freely"

	static let appRealm = "freely"
	static var apiKey: String {
		switch API.Environment.current {
			case .local:
				return ""
			case .staging:
				return "ccd4bad170b34bde81be21035f04ef23"
			case .production:
				return "3e9bb0380aa54ecf89dcc42b2f34af14"
		}
	}
}
