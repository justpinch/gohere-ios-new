import Foundation

struct Config: TargetConfig {
	static let appName = "BONLU"
	static let appRealmForMixpanel = "BONLU"

	static let appRealm = "bonlu"
	static var apiKey: String {
		switch API.Environment.current {
			case .local:
				return ""
			case .staging:
				return "0024354521664e0f8e92c93eb0c2e886"
			case .production:
				return "077b5463c6e2415f813286c6a58434b3"
		}
	}
}
