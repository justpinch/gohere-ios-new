import Foundation

struct Config: TargetConfig {
	static let appName = "EuropeanLife Luxury Week"
	static let appRealmForMixpanel = "ELM Spain"

	static let appRealm = "elm-spain"
	static var apiKey: String {
		switch API.Environment.current {
			case .local:
				return ""
			case .staging:
				return "fac021d40ceb42879f099c80d37306e2"
			case .production:
				return "e00f61de3f5b4bb0ad5132521842baf9"
		}
	}
}
