import Foundation

struct Config: TargetConfig {
	static let appName = "Honeyguide"
	static let appRealmForMixpanel = "Honeyguide"

	static let appRealm = "honeyguide"
	static var apiKey: String {
		switch API.Environment.current {
			case .local:
				return ""
			case .staging:
				return "d75ef80077264b7c80ec63502a297517"
			case .production:
				return "b39b48922d9c4879927d7b1996bba137"
		}
	}
}
