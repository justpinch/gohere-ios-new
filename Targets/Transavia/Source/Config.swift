import Foundation

struct Config: TargetConfig {
	static let appName = "Transavia Tips"
	static let appRealmForMixpanel = "Transavia"

	static let appRealm = "transavia"
	static var apiKey: String {
		switch API.Environment.current {
			case .local:
				return "2317c3b10e184691a4b6d781c62e4741"
			case .staging:
				return "2317c3b10e184691a4b6d781c62e4741"
			case .production:
				return "37d21cbd9fab4587bcf832d0f29c8294"
		}
	}
}
