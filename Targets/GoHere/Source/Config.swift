import Foundation

struct Config: TargetConfig {
	static let appName = "GoHere"
	static let appRealmForMixpanel = "GoHere"

	static let appRealm = "gohere"
	static let apiKey: String = "66f5d9699a6b494588f3140997b3c99a"
}
