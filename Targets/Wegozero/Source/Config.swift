import Foundation

struct Config: TargetConfig {
	static let appName = "Wegozero"
	static let appRealmForMixpanel = "Wegozero"

	static let appRealm = "wegozero"
	static var apiKey: String {
		switch API.Environment.current {
			case .local:
				return ""
			case .staging:
				return "4ea05e294fc7452d8380589974645f95"
			case .production:
				return "4b11512cc3a84d9b9a979f168aa278b0"
		}
	}
}
