import Foundation

struct Config: TargetConfig {
	static let appName = "Tritt"
	static let appRealmForMixpanel = "Tritt"

	static let appRealm = "tritt-case-in-toscana"
	static var apiKey: String {
		switch API.Environment.current {
			case .local:
				return ""
			case .staging:
				return "60dfcc5813374c3f8f9f59b37e45e097"
			case .production:
				return "9d383b98615a4d168ce5065368a5922c"
		}
	}
}
