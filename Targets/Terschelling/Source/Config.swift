import Foundation

struct Config: TargetConfig {
	static let appName = "Terschelling"
	static let appRealmForMixpanel = "Terschelling"

	static let appRealm = "terschelling"
	static var apiKey: String {
		switch API.Environment.current {
			case .local:
				return "37c90499763f40e6a840bbbdf6d508f4"
			case .staging:
				return "37c90499763f40e6a840bbbdf6d508f4"
			case .production:
				return "37c90499763f40e6a840bbbdf6d508f4"
		}
	}
}
