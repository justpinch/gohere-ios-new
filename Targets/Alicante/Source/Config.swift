import Foundation

struct Config: TargetConfig {
	static let appName = "Alicante"
	static let appRealmForMixpanel = "Alicante"

	static let appRealm = "alicante-like-a-local"
	static var apiKey: String {
		switch API.Environment.current {
			case .local:
				return "#TODO#"
			case .staging:
				return "5b054998a3d9416abdacb55c7e0eec95"
			case .production:
				return "863170647e404985ae5ab430280be00a"
		}
	}
}
