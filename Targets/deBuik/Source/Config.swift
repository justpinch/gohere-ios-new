import Foundation

struct Config: TargetConfig {
	static let appName = "de Buik"
	static let appRealmForMixpanel = "deBuik"

	static let appRealm = "de-buik"
	static var apiKey: String {
		switch API.Environment.current {
			case .local:
				return ""
			case .staging:
				return "5e9f61eed10a472ba3a57fe1c8249bd7"
			case .production:
				return "a48579147a7f4e9c9905fc56c903c85d"
		}
	}
	
	static func isRatingAllowed() -> Bool {
		return false
	}
	
	static func getReviewSorting() -> String {
		return "created,asc"
	}
}
