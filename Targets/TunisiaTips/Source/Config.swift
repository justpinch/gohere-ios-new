import Foundation

struct Config: TargetConfig {
	static let appName = "TunisiaTips"
	static let appRealmForMixpanel = "TunisiaTips"

	static let appRealm = "tunisia-tips"
	static var apiKey: String {
		switch API.Environment.current {
			case .local:
				return "#TODO#"
			case .staging:
				return "d1b27b276cd44f0ab78af561ce62eac5"
			case .production:
				return "ef494416c00f4266ac0c56092384e67c"
		}
	}
}
