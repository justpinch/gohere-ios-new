import Foundation

struct Config: TargetConfig {
	static let appName = "VisitMorocco"
	static let appRealmForMixpanel = "VisitMorocco"

	static let appRealm = "visit-morocco"
	static var apiKey: String {
		switch API.Environment.current {
			case .local:
				return ""
			case .staging:
				return "0273a3729cfd4b3383f28432e30e0d25"
			case .production:
				return "2dbe0ad46539401f976dec775c69bdf1"
		}
	}
}
