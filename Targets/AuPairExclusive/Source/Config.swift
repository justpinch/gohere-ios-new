import Foundation

struct Config: TargetConfig {
	static let appName = "Au Pair Life"
	static let appRealmForMixpanel = "AuPairExclusive"

	static let appRealm = "aupair-exclusive"
	static var apiKey: String {
		switch API.Environment.current {
			case .local:
				return ""
			case .staging:
				return "96757eff59964f9b81e8091c1acce551"
			case .production:
				return "48dc0360356045578f2b756c61fe2212"
		}
	}
}
