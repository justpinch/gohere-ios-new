import Foundation

struct Config: TargetConfig {
	static let appName = "Barcelonatips"
	static let appRealmForMixpanel = "Barcelonatips"

	static let appRealm = "barcelonatips"
	static var apiKey: String {
		switch API.Environment.current {
			case .local:
				return ""
			case .staging:
				return "eedcd2a0b3e342688a3534f5fb0acf6c"
			case .production:
				return "5c98ae59a82348d6975fa93bba92de36"
		}
	}
}
