import Foundation

struct Config: TargetConfig {
	static let appName = "Espanje"
	static let appRealmForMixpanel = "ESPANJE!"

	static let appRealm = "espanje"
	static var apiKey: String {
		switch API.Environment.current {
			case .local:
				return ""
			case .staging:
				return "71f6017598a5485ca4e49093d04d5786"
			case .production:
				return "75ffe8e4cb564376b30b6f4cf7c229a4"
		}
	}
}
