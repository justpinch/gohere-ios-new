import Foundation

struct Config: TargetConfig {
	static let appName = "#APPNAME#"
	static let appRealmForMixpanel = "#APPNAME#"

	static let appRealm = "#TODO#"
	static var apiKey: String {
		switch API.Environment.current {
			case .local:
				return "#TODO#"
			case .staging:
				return "#TODO#"
			case .production:
				return "#TODO#"
		}
	}
}
