import Foundation
import SwiftUI
import UIKit

extension UIColor {
	static func gh(_ color: Theme.Color) -> UIColor {
		color.color()
	}
}

extension Color {
	static func gh(_ color: Theme.Color) -> Color {
		Color(color.color())
	}
}

enum Theme {
	struct ColorScheme: GHColorScheme {
		// dynamic color helper
		static func color(light: Int, dark: Int, al: CGFloat = 1.0, ad: CGFloat = 1.0) -> UIColor {
			UIColor { (traitCollection: UITraitCollection) -> UIColor in
				if traitCollection.userInterfaceStyle == .dark {
					return UIColor(rgb: dark, alpha: ad)
				} else {
					return UIColor(rgb: light, alpha: al)
				}
			}
		}
		
		static var bottomSheet: UIColor {
			get {
				UIColor { (traitCollection: UITraitCollection) -> UIColor in
					if traitCollection.userInterfaceStyle == .dark {
						return UIColor(rgb: 0x2E2E2E, alpha: 1.0)
					} else {
						return backgroundPrimary
					}
				}
			}
		}
	}

	enum Color {
		// Tints
		case tintPrimary
		case tintSecondary
		case tintTertiary
		case tintQuaternary

		// On Tint
		case onTintPrimary
		case onTintSecondary
		case onTintTertiary
		case onTintQuaternary

		// Backgrounds
		case backgroundPrimary
		case backgroundSecondary
		case backgroundTertiary

		// On Background
		case labelPrimary
		case labelSecondary
		case labelTertiary
		case labelQuaternary

		// Bars
		case navbar
		case tabbar

		// On Bars
		case onNavbar
		case onTabbarDefault
		case onTabbarSelected

		// Other
		case notification
		case placeholderText
		case separator
		case separatorOpaque
		case link

		// On Other
		case onNotification

		// Utility
		case utilityPositive
		case utilityError
		case utilityNeutral

		// On Utility

		case onUtilityPositive
		case onUtilityError
		case onUtilityNeutral

		// Affiliate
		case affiliate

		// On Affiliate
		case onAffiliate

		// Pin
		case pin

		// On Pin
		case onPin

		// Rating
		case rating

		// App Icon
		case appIcon
		case onAppIcon
		
		case bottomSheet
		case filterIcon

		func color() -> UIColor {
			switch self {
				case .tintPrimary: return ColorScheme.tintPrimary
				case .tintSecondary: return ColorScheme.tintSecondary
				case .tintTertiary: return ColorScheme.tintTertiary
				case .tintQuaternary: return ColorScheme.tintQuaternary
				case .onTintPrimary: return ColorScheme.onTintPrimary
				case .onTintSecondary: return ColorScheme.onTintSecondary
				case .onTintTertiary: return ColorScheme.onTintTertiary
				case .onTintQuaternary: return ColorScheme.onTintQuaternary
				case .backgroundPrimary: return ColorScheme.backgroundPrimary
				case .backgroundSecondary: return ColorScheme.backgroundSecondary
				case .backgroundTertiary: return ColorScheme.backgroundTertiary
				case .labelPrimary: return ColorScheme.labelPrimary
				case .labelSecondary: return ColorScheme.labelSecondary
				case .labelTertiary: return ColorScheme.labelTertiary
				case .labelQuaternary: return ColorScheme.labelQuaternary
				case .navbar: return ColorScheme.navbar
				case .tabbar: return ColorScheme.tabbar
				case .onNavbar: return ColorScheme.onNavbar
				case .onTabbarDefault: return ColorScheme.onTabbarDefault
				case .onTabbarSelected: return ColorScheme.onTabbarSelected
				case .notification: return ColorScheme.notification
				case .placeholderText: return ColorScheme.placeholderText
				case .separator: return ColorScheme.separator
				case .separatorOpaque: return ColorScheme.separatorOpaque
				case .link: return ColorScheme.link
				case .onNotification: return ColorScheme.onNotification
				case .utilityPositive: return ColorScheme.utilityPositive
				case .utilityError: return ColorScheme.utilityError
				case .utilityNeutral: return ColorScheme.utilityNeutral
				case .onUtilityPositive: return ColorScheme.onUtilityPositive
				case .onUtilityError: return ColorScheme.onUtilityError
				case .onUtilityNeutral: return ColorScheme.onUtilityNeutral
				case .affiliate: return ColorScheme.affiliate
				case .onAffiliate: return ColorScheme.onAffiliate
				case .pin: return ColorScheme.pin
				case .onPin: return ColorScheme.onPin
				case .rating: return ColorScheme.rating
				case .appIcon: return ColorScheme.appIcon
				case .onAppIcon: return ColorScheme.onAppIcon
				case .bottomSheet: return ColorScheme.bottomSheet
				case .filterIcon: return ColorScheme.filterIcon
			}
		}
	}
}

protocol GHColorScheme {
	// Tints
	static var tintPrimary: UIColor { get }
	static var tintSecondary: UIColor { get }
	static var tintTertiary: UIColor { get }
	static var tintQuaternary: UIColor { get }

	// On Tint
	static var onTintPrimary: UIColor { get }
	static var onTintSecondary: UIColor { get }
	static var onTintTertiary: UIColor { get }
	static var onTintQuaternary: UIColor { get }

	// Backgrounds
	static var backgroundPrimary: UIColor { get }
	static var backgroundSecondary: UIColor { get }
	static var backgroundTertiary: UIColor { get }

	// On Background
	static var labelPrimary: UIColor { get }
	static var labelSecondary: UIColor { get }
	static var labelTertiary: UIColor { get }
	static var labelQuaternary: UIColor { get }

	// Bars
	static var navbar: UIColor { get }
	static var tabbar: UIColor { get }

	// On Bars
	static var onNavbar: UIColor { get }
	static var onTabbarDefault: UIColor { get }
	static var onTabbarSelected: UIColor { get }

	// Other
	static var notification: UIColor { get }
	static var placeholderText: UIColor { get }
	static var separator: UIColor { get }
	static var separatorOpaque: UIColor { get }
	static var link: UIColor { get }

	// On Other
	static var onNotification: UIColor { get }

	// Utility
	static var utilityPositive: UIColor { get }
	static var utilityError: UIColor { get }
	static var utilityNeutral: UIColor { get }

	// On Utility
	static var onUtilityPositive: UIColor { get }
	static var onUtilityError: UIColor { get }
	static var onUtilityNeutral: UIColor { get }

	// Affiliate
	static var affiliate: UIColor { get }

	// On Affiliate
	static var onAffiliate: UIColor { get }

	// Pin
	static var pin: UIColor { get }

	// On Pin
	static var onPin: UIColor { get }

	// Rating
	static var rating: UIColor { get }

	// App Icon
	static var appIcon: UIColor { get }
	static var onAppIcon: UIColor { get }
	
	static var filterIcon: UIColor { get }
}

struct ThemePreviewProvider: PreviewProvider {
	struct Swatch: View {
		let color: Theme.Color

		var body: some View {
			HStack {
				Color(color.color())
					.frame(width: 25, height: 25)
					.border(Color.gray, width: 0.5)

				Text(String(describing: color))
			}
		}
	}

	static var previews: some View {
		VStack {
			HStack(alignment: .top) {
				section(name: "Tints", colors: [
					.tintPrimary,
					.tintSecondary,
					.tintTertiary,
					.tintQuaternary,
				])

				Spacer()

				section(name: "On Tints", colors: [
					.onTintPrimary,
					.onTintSecondary,
					.onTintTertiary,
					.onTintQuaternary,
				])
			}

			HStack(alignment: .top) {
				section(name: "Backgrounds", colors: [
					.backgroundPrimary,
					.backgroundSecondary,
					.backgroundTertiary,
				])

				Spacer()

				section(name: "On Backgrounds", colors: [
					.labelPrimary,
					.labelSecondary,
					.labelTertiary,
					.labelQuaternary,
				])
			}

			HStack(alignment: .top) {
				section(name: "Bars", colors: [
					.navbar,
					.tabbar,
				])

				Spacer()

				section(name: "On Bars", colors: [
					.onNavbar,
					.onTabbarDefault,
					.onTabbarSelected,
				])
			}

			HStack(alignment: .top) {
				section(name: "Other", colors: [
					.notification,
					.placeholderText,
					.separatorOpaque,
					.separator,
					.link,
				])

				Spacer()

				section(name: "On Other", colors: [
					.onNotification,
				])
			}

			HStack(alignment: .top) {
				section(name: "Rating", colors: [
					.rating,
				])
				Spacer()
			}

			HStack(alignment: .top) {
				section(name: "Utility", colors: [
					.utilityPositive,
					.utilityError,
					.utilityNeutral,
				])

				Spacer()

				section(name: "On Utility", colors: [
					.onUtilityPositive,
					.onUtilityError,
					.onUtilityNeutral,
				])
			}

			HStack(alignment: .top) {
				section(name: "Affiliate", colors: [
					.affiliate,
				])

				Spacer()

				section(name: "On Affiliate", colors: [
					.onAffiliate,
				])
			}

			HStack(alignment: .top) {
				section(name: "Pin", colors: [
					.pin,
				])

				Spacer()

				section(name: "On Pin", colors: [
					.onPin,
				])
			}

			HStack(alignment: .top) {
				section(name: "App Icon", colors: [
					.appIcon,
				])

				Spacer()

				section(name: "On App Icon", colors: [
					.onAppIcon,
				])
			}
		}
		.font(.system(size: 13, weight: .regular))
		.previewLayout(.fixed(width: 400, height: 1400))
	}

	@ViewBuilder
	static func section(name: String, colors: [Theme.Color]) -> some View {
		VStack(alignment: .leading) {
			Text(name).bold()
			ForEach(colors, id: \.hashValue, content: Swatch.init)
			Spacer()
		}
		.padding([.leading, .trailing])
		.padding(9)
	}
}
