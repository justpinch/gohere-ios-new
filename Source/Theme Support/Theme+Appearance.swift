import Foundation
import SwiftUI
import UIKit

extension Theme {
	static func configureAppearance() {
		let navigationBarAppearance = UINavigationBar.appearance()
		navigationBarAppearance.isTranslucent = true
		navigationBarAppearance.tintColor = .gh(.onNavbar)
		navigationBarAppearance.backItem?.backBarButtonItem?.tintColor = .gh(.onNavbar)
		navigationBarAppearance.barTintColor = .gh(.navbar)

		let standardNavBarAppearance = UINavigationBarAppearance()
		standardNavBarAppearance.configureWithDefaultBackground()
		standardNavBarAppearance.backgroundColor = .gh(.navbar)

		// change navigation item title color
		standardNavBarAppearance.titleTextAttributes = [.font: UIFont.gh(.title3) as Any, .foregroundColor: UIColor.gh(.onNavbar)]

		let foregroundColor = UIColor.gh(.onNavbar)
		let buttonFont = UIFont.gh(.subhead) as Any
		let textAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: foregroundColor, .font: buttonFont]

		standardNavBarAppearance.backButtonAppearance.normal.titleTextAttributes = textAttributes
		standardNavBarAppearance.backButtonAppearance.highlighted.titleTextAttributes = textAttributes
		standardNavBarAppearance.backButtonAppearance.focused.titleTextAttributes = textAttributes

		standardNavBarAppearance.buttonAppearance.normal.titleTextAttributes = textAttributes
		standardNavBarAppearance.buttonAppearance.highlighted.titleTextAttributes = textAttributes
		standardNavBarAppearance.buttonAppearance.focused.titleTextAttributes = textAttributes

		standardNavBarAppearance.doneButtonAppearance.normal.titleTextAttributes = textAttributes
		standardNavBarAppearance.doneButtonAppearance.highlighted.titleTextAttributes = textAttributes
		standardNavBarAppearance.doneButtonAppearance.focused.titleTextAttributes = textAttributes

		navigationBarAppearance.standardAppearance = standardNavBarAppearance
		UINavigationBar.appearance().scrollEdgeAppearance = standardNavBarAppearance

		/// TAB BAR
		if #available(iOS 15, *) {
			let tabBarAppearance = UITabBarAppearance()
			tabBarAppearance.backgroundColor = .gh(.tabbar)
			UITabBar.appearance().standardAppearance = tabBarAppearance
			UITabBar.appearance().scrollEdgeAppearance = tabBarAppearance
		} else {
			UITabBar.appearance().barTintColor = .gh(.tabbar)
		}
		UITabBar.appearance().unselectedItemTintColor = .gh(.onTabbarDefault)

		UITabBarItem.appearance().setTitleTextAttributes([.font: UIFont.gh(.tab)!], for: .normal)
		UITabBarItem.appearance().setTitleTextAttributes([.font: UIFont.gh(.tab)!], for: .selected)
		UITabBarItem.appearance().setTitleTextAttributes([.font: UIFont.gh(.tab)!], for: .focused)
		UITabBarItem.appearance().setTitleTextAttributes([.font: UIFont.gh(.tab)!], for: .disabled)
		UITabBarItem.appearance().setTitleTextAttributes([.font: UIFont.gh(.tab)!], for: .highlighted)

		/// ⚠️ SwiftUI quirks.. 😭

		// Without this TextEditor doesn't respect .backgroundColor setting
		UITextView.appearance().backgroundColor = .clear
	}
}
