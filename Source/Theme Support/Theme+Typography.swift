import Foundation
import SwiftUI
import UIKit

extension Font {
	static func gh(_ font: Theme.Font) -> Font {
		let info = font.info()
		return .custom(info.name, size: info.size)
	}
}

extension UIFont {
	static func gh(_ font: Theme.Font) -> UIFont? {
		let info = font.info()
		let scaledSize = UIFontMetrics.default.scaledValue(for: info.size)
		return UIFont(name: info.name, size: scaledSize)
	}
}

protocol GHTypography {
	typealias FontInfo = (name: String, size: CGFloat)

	static var largeTitle: FontInfo { get }
	static var title1: FontInfo { get }
	static var title2: FontInfo { get }
	static var title3: FontInfo { get }
	static var headline: FontInfo { get }
	static var body: FontInfo { get }
	static var callout: FontInfo { get }
	static var subhead: FontInfo { get }
	static var footnote: FontInfo { get }
	static var caption1: FontInfo { get }
	static var caption2: FontInfo { get }
	static var pin: FontInfo { get }
	static var notification: FontInfo { get }
	static var tab: FontInfo { get }
}

extension Theme {
	struct Typography: GHTypography {}

	enum Font {
		case largeTitle
		case title1
		case title2
		case title3
		case headline
		case body
		case callout
		case subhead
		case footnote
		case caption1
		case caption2
		case pin
		case notification
		case tab

		func info() -> GHTypography.FontInfo {
			switch self {
				case .largeTitle: return Typography.largeTitle
				case .title1: return Typography.title1
				case .title2: return Typography.title2
				case .title3: return Typography.title3
				case .headline: return Typography.headline
				case .body: return Typography.body
				case .callout: return Typography.callout
				case .subhead: return Typography.subhead
				case .footnote: return Typography.footnote
				case .caption1: return Typography.caption1
				case .caption2: return Typography.caption2
				case .pin: return Typography.pin
				case .notification: return Typography.notification
				case .tab: return Typography.tab
			}
		}
	}
}

// DEFAULTS! Targets can override this in their Theme.swift
extension GHTypography {
	static var largeTitle: FontInfo { ("Avenir-Black", 34) }
	static var title1: FontInfo { ("Avenir-Heavy", 28) }
	static var title2: FontInfo { ("Avenir-Heavy", 22) }
	static var title3: FontInfo { ("Avenir-Heavy", 18) }
	static var headline: FontInfo { ("Avenir-Medium", 17) }
	static var body: FontInfo { ("Avenir-Book", 17) }
	static var callout: FontInfo { ("Avenir-Book", 16) }
	static var subhead: FontInfo { ("Avenir-Heavy", 14) }
	static var footnote: FontInfo { ("Avenir-Roman", 13) }
	static var caption1: FontInfo { ("Avenir-Medium", 12) }
	static var caption2: FontInfo { ("Avenir-Medium", 12) }
	static var pin: FontInfo { ("Avenir-Heavy", 11) }
	static var notification: FontInfo { ("Roboto-Bold", 9) }
	static var tab: FontInfo { ("Roboto-Medium", 12.2) }
}
