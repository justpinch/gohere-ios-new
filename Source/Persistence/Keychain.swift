import Foundation

protocol KeychainManager {
	func data(for key: String) throws -> Data
	func set(_ data: Data, for key: String) throws
	func set(_ string: String, for key: String) throws
	func deleteData(for key: String) throws
	func reset() throws
}

extension Keychain {
	/**
	 Keychain configuration structure.
	 For now, only the service can be specified and if need be, we can add shared
	 keychain by defining an accessGroup in the future.
	 */
	struct Configuration {
		let service: String

		static var userAuthentication: Configuration {
			Configuration(service: "GH.UserAuthentication")
		}
	}
}

/**
 Keychain structure for reading and storing passwords for specified user accounts
 */
struct Keychain: KeychainManager {
	struct KeychainError: Error {
		let status: OSStatus
	}

	/// The keychain configuration
	let configuration: Configuration

	/**
	 Gets the secure data for specified key..
	 - Parameter key: The key under which the data is stored.
	 - Returns: The secure data
	 */
	func data(for key: String) throws -> Data {
		let query = [
			kSecAttrService: configuration.service,
			kSecAttrAccount: Data(key.utf8),
			kSecClass: kSecClassGenericPassword,
			kSecMatchLimit: kSecMatchLimitOne,
			kSecReturnData: kCFBooleanTrue as Any,
		] as CFDictionary

		var data: AnyObject?
		let status = SecItemCopyMatching(query, &data)

		if status == errSecSuccess, let data = data as? Data {
			return data
		}

		throw KeychainError(status: status)
	}

	/**
	 Sets data for specified key.
	 - Parameter data: The secure data
	 - Parameter key: The key under which the data will be stored
	 */
	func set(_ data: Data, for key: String) throws {
		do {
			// First try inserting
			try insert(data, for: Data(key.utf8))
		} catch {
			// If it failed, try updating
			try update(data, for: Data(key.utf8))
		}
	}

	/**
	 Convenience method. Sets secure string for specified key.
	 - Parameter string: The secure string
	 - Parameter key: The key under which the data will be stored
	 */
	func set(_ string: String, for key: String) throws {
		try set(Data(string.utf8), for: key)
	}

	/**
	 Deletes data for specified key..
	 - Parameter key: The key under which the data is stored
	 */
	func deleteData(for key: String) throws {
		let query = [
			kSecAttrService: configuration.service,
			kSecAttrAccount: key,
			kSecClass: kSecClassGenericPassword,
		] as CFDictionary

		let status = SecItemDelete(query)
		if status != errSecSuccess, status != errSecItemNotFound {
			throw KeychainError(status: status)
		}
	}

	/**
	 Deletes all entries from the keychain (for the `service` specified in `configuration`).
	 */
	func reset() throws {
		let query = [kSecClass: kSecClassGenericPassword, kSecAttrService: configuration.service] as CFDictionary
		let status = SecItemDelete(query)
		if status != errSecSuccess, status != errSecItemNotFound {
			throw KeychainError(status: status)
		}
	}
}

// MARK: - Private methods

extension Keychain {
	private func insert(_ data: Data, for key: Data) throws {
		let query = [
			kSecAttrService: configuration.service,
			kSecAttrAccount: key,
			kSecClass: kSecClassGenericPassword,
			kSecValueData: data,
		] as CFDictionary

		let status = SecItemAdd(query, nil)
		if status != errSecSuccess {
			throw KeychainError(status: status)
		}
	}

	private func update(_ data: Data, for key: Data) throws {
		let query = [
			kSecAttrService: configuration.service,
			kSecAttrAccount: key,
			kSecClass: kSecClassGenericPassword,
		] as CFDictionary

		let updateDict = [kSecValueData: data] as CFDictionary

		let status = SecItemUpdate(query, updateDict)
		if status != errSecSuccess {
			throw KeychainError(status: status)
		}
	}
}
