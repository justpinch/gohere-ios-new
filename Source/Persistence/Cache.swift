import Foundation

protocol FileCacheProtocol {
	func cache<Value: Encodable>(_ value: Value, key: String) throws
	func value<Value: Decodable>(for key: String) throws -> Value?
}

protocol FileStorageManager {
	var cacheDirectoryURL: URL { get }
	func write(data: Data, to url: URL) throws
	func contents(of url: URL) throws -> Data?
}

extension FileManager: FileStorageManager {
	var cacheDirectoryURL: URL {
		urls(for: .cachesDirectory, in: .userDomainMask).first!
	}

	func write(data: Data, to url: URL) throws {
		try data.write(to: url)
	}

	func contents(of url: URL) throws -> Data? {
		do {
			return try Data(contentsOf: url)
		} catch {
			if (error as NSError).domain == NSCocoaErrorDomain, (error as NSError).code == NSFileReadNoSuchFileError {
				return nil
			}
			throw error
		}
	}
}

struct FileCache: FileCacheProtocol {
	let fileManager: FileStorageManager

	enum Keys {}

	private func url(for key: String) -> URL {
		fileManager.cacheDirectoryURL.appendingPathComponent(key)
	}

	func cache<Value>(_ value: Value, key: String) throws where Value: Encodable {
		let data = try PropertyListEncoder().encode(value)
		try fileManager.write(data: data, to: url(for: key))
	}

	func value<Value>(for key: String) throws -> Value? where Value: Decodable {
		guard let data = try fileManager.contents(of: url(for: key)) else {
			return nil
		}

		return try PropertyListDecoder().decode(Value.self, from: data)
	}
}
