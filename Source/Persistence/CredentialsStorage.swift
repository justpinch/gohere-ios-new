import Combine
import Foundation

protocol CredentialsStorage {
	var authToken: CurrentValueSubject<AuthToken?, Never> { get }
}

final class DefaultCredentialsStorage: CredentialsStorage {
	enum Keys {
		static let authToken = "UserService.Keys.authInfo"
	}

	private let keychain: KeychainManager
	let authToken = CurrentValueSubject<AuthToken?, Never>(nil)

	private var cancellable: AnyCancellable?

	/**
	 Initialize with specified keychain instance.
	 By default uses Keychain.userAuthentication
	 */
	init(keychain: KeychainManager = Keychain(configuration: .userAuthentication)) {
		self.keychain = keychain

		// read stored token, if any
		do { authToken.value = try load() }
		catch {
			if (error as? Keychain.KeychainError)?.status != errSecItemNotFound {
				error.report()
			}
		}

		// Observe changes
		cancellable = authToken
			.dropFirst()
			.removeDuplicates()
			.sink { [unowned self] token in
				do { try self.set(token: token) }
				catch { error.report() }
			}
	}

	private func set(token: AuthToken?) throws {
		// reset keychain, remove old info
		try keychain.reset()

		// set new value
		authToken.value = token

		if let token = token {
			// persist the token if we're setting a new one
			try store(token: token)
		}
	}

	private func store(token _: AuthToken) throws {
		let encoder = JSONEncoder()
		encoder.keyEncodingStrategy = .convertToSnakeCase

		let data = try encoder.encode(authToken.value)
		try keychain.set(data, for: Keys.authToken)
	}

	private func load() throws -> AuthToken {
		let decoder = JSONDecoder()
		decoder.keyDecodingStrategy = .convertFromSnakeCase

		let data = try keychain.data(for: Keys.authToken)
		return try decoder.decode(AuthToken.self, from: data)
	}
}
