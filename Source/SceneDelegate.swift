import Combine
import FBSDKCoreKit
import SwiftUI
import UIKit

enum OnboardingVersion: Int {
	case none = 0
	case v3 = 3

	static let current = OnboardingVersion.v3

	var isCurrent: Bool { self == OnboardingVersion.current }
}

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
	@Environment(\.dependencyContainer) var dependencyContainer
	@Environment(\.appSettings) var appSettings

	private let appStateConfigurator = AppStateConfigurator()

	@AppStorage("OnboardingVersion")
	private var onboardingVersion: OnboardingVersion = .none
	private var isOnboardingCompleted: Bool {
		get { onboardingVersion.isCurrent }
		set { onboardingVersion = OnboardingVersion.current }
	}

	var window: UIWindow?

	static func isRunningUnitTests() -> Bool {
		NSClassFromString("XCTest") != nil
	}

	private var environmentObjectsProvider: EnvironmentObjectsProvider {
		dependencyContainer.resolve(EnvironmentObjectsProvider.self)
	}

	func scene(_ scene: UIScene, willConnectTo _: UISceneSession, options _: UIScene.ConnectionOptions) {
		guard !SceneDelegate.isRunningUnitTests() else {
			window = UIWindow()
			return
		}

		if let windowScene = scene as? UIWindowScene {
			let window = UIWindow(windowScene: windowScene)
			self.window = window

			appStateConfigurator.configure(with: window)
			Theme.configureAppearance()

			bootstrap(window: window)
				.flatMap(onboarding)
				.map { _ in
					UIHostingController(rootView: RootView().provideEnvironmentObjects())
				}
				.subscribe(Subscribers.Sink(receiveCompletion: { _ in }, receiveValue: { viewController in
					Analytics.log(.splashScreen)
					window.rootViewController = viewController
				}))

			window.makeKeyAndVisible()
		}
	}

	func scene(_: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
		if let url = URLContexts.first?.url {
			FBSDKCoreKit.ApplicationDelegate.shared.application(
				UIApplication.shared,
				open: url,
				sourceApplication: nil,
				annotation: [UIApplication.OpenURLOptionsKey.annotation]
			)
		}
	}

	private func bootstrap(window: UIWindow) -> AnyPublisher<UIWindow, Never> {
		Future { promise in
			let client = self.dependencyContainer.resolve(API.Client.self)
			let appSettings = self.appSettings
			let bootstrapRepository = BootstrapRepository(service: client.services.bootstrap)

			let publisher = bootstrapRepository.bootstrapLoader()
				.receive(on: RunLoop.main)
				.handleEvents(receiveOutput: { bootstrap in
					appSettings.update(with: bootstrap)
				})

			if let bootstrap = bootstrapRepository.bootstrap() {
				// if we have cached bootstrap, update and proceed with launch immediately
				appSettings.update(with: bootstrap)
				// in the background, also, update to the latest bootstrap version
				publisher.subscribeIgnoringEvents()
				// continue launch without interrupting
				promise(.success(window))
			} else {
				// we need to fetch bootstrap and show loading screen
				let publisher = publisher
					.handleEvents(receiveOutput: { _ in promise(.success(window)) }) // when we receive a value, we can continue
					.ignoreOutput()
					.eraseToAnyPublisher()

				let bootstrapView = BootstrapView(publisher: publisher)
				window.rootViewController = UIHostingController(rootView: bootstrapView.provideEnvironmentObjects())
			}
		}
		.eraseToAnyPublisher()
	}

	private func onboarding(window: UIWindow) -> AnyPublisher<UIWindow, Never> {
		guard !isOnboardingCompleted else {
			return Just(window).eraseToAnyPublisher()
		}

		return Future { [weak self] promise in
			guard let self = self else { return }

			let completion = { [weak self] in
				self?.isOnboardingCompleted = true
				promise(.success(window))
			}
			let onboardingView = OnboardingView(onboardingFinished: completion)
			window.rootViewController = UIHostingController(rootView: onboardingView.provideEnvironmentObjects())
		}
		.eraseToAnyPublisher()
	}
}
