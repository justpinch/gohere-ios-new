import Foundation
import SwiftUI

protocol TargetConfig {
	static var appName: String { get }

	static var appRealm: String { get }
	static var appRealmForMixpanel: String { get }
	static var apiKey: String { get }

	static func isRatingAllowed() -> Bool
	static func getReviewSorting() -> String
	static func onboardingIntroView(introCompleted: @escaping () -> Void) -> AnyView
}

// Defaults

extension TargetConfig {
	static func isRatingAllowed() -> Bool { return true }
	
	static func getReviewSorting() -> String { return "created,desc" }
	
	static func onboardingIntroView(introCompleted: @escaping () -> Void) -> AnyView {
		AnyView(DefaultIntroView(introCompleted: introCompleted))
	}
}
