import Combine
import CoreLocation
import Foundation
import MapKit
import SwiftUI

class AppSettings: ObservableObject {
	var appName: String { Config.appName }

	var appRealm: String { Config.appRealm }
	var apiKey: String { Config.apiKey }

	var appRealmForMixpanel: String { Config.appRealmForMixpanel }
	var mixpanelToken: String? { API.Environment.current.mixpanelToken }

	@Published var mothershipId: User.ID?
	@Published var categories: [PlaceCategory] = []
	@Published var defaultRegion: MKMapRect?
	@Published var biasLocation: CLLocationCoordinate2D?
	@Published var bannerId: String?
	@Published var uberClientId: String?
	@Published var promotedPlacesSearchRadius: Int = 10000

	func update(with bootstrap: Bootstrap) {
		mothershipId = bootstrap.mothershipId
		categories = bootstrap.categories
		defaultRegion = bootstrap.defaultRegion?.union()
		biasLocation = bootstrap.biasLocation?.coordinate
		bannerId = bootstrap.bannerId
		uberClientId = bootstrap.uberClientId
		promotedPlacesSearchRadius = bootstrap.promotedPlacesSearchRadius
	}
}

extension AppSettings {
	/// Default website URL for GoHere whitelabel
	static var defaultWhiteLabelWebURL: URL {
		URL(string: "https://www.gohere.app/over-ons/?utm_source=gohereapp&utm_medium=whitelabel&utm_campaign=\(Config.appRealm)")!
	}

	/// Default privacy policy URL for GoHere whitelabel
	static var privacyPolicyURL: URL {
		API.Environment.current.baseShareURL
			.appendingPathComponent("docs")
			.appendingPathComponent("privacy-policy/")
	}

	/// Default terms of use URL for GoHere whitelabel
	static var termsOfUseURL: URL {
		API.Environment.current.baseShareURL
			.appendingPathComponent("docs")
			.appendingPathComponent("terms-of-use/")
	}

	/// Default FAQ  URL for GoHere whitelabel
	static var faqURL: URL {
		API.Environment.current.baseShareURL
			.appendingPathComponent("docs")
			.appendingPathComponent("faq/")
	}
	
	static var maxRatingValue: Int = 10
}
