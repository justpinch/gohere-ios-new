import Nuke
import SwiftUI

struct DeveloperSettings: View {
	@Environment(\.dependencyContainer) var dependencies

	var body: some View {
		NavigationView {
			Form(content: {
				Section(header: Text("Device Info")) {
					cellItem(title: "Device ID", subtitle: device?.id ?? "n/a")
					cellItem(title: "Firebase Project ID", subtitle: device?.firebaseProjectId ?? "n/a")
					cellItem(title: "FCM token", subtitle: device?.fcmToken ?? "n/a")
				}

				Section(header: Text("Actions")) {
					Button(action: crashSimple, label: {
						Text("Crash Test (simple)")
					})

					Button(action: crashNetworkCall, label: {
						Text("Crash Test (network call)")
					})

					Button(action: {
						ImageCache.shared.removeAll()
						DataLoader.sharedUrlCache.removeAllCachedResponses()
					}, label: {
						Text("Clear Images Cache")
					})
				}
			})
			.navigationTitle("Developer settings")
		}
	}

	@ViewBuilder
	func cellItem(title: String, subtitle: String) -> some View {
		VStack(alignment: .leading, spacing: 4) {
			Text(title)
				.font(.caption)
				.foregroundColor(.secondary)

			Menu {
				Button(action: {
					UIPasteboard.general.string = subtitle
				}, label: {
					Label("Copy", systemImage: "doc.on.doc")
				})
			} label: {
				Text(subtitle)
					.font(.system(.callout, design: .monospaced))
					.foregroundColor(.primary)
			}
		}
		.padding([.top, .bottom], 5)
	}

	var device: UserDevice? {
		dependencies.resolve(CloudMessaging.self).getCurrentDevice()
	}

	func crashSimple() {
		let test: Int? = nil
		print(test!)
	}

	func crashNetworkCall() {
		dependencies.resolve(API.Client.self).services.bootstrap
			.get()
			.map { _ in
				crashSimple()
			}
			.subscribeIgnoringEvents()
	}
}

struct DeveloperSettings_Previews: PreviewProvider {
	static var previews: some View {
		DeveloperSettings()
			.injectPreviewsEnvironment()
	}
}
