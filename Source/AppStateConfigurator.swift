import Combine
import Foundation
import GoogleMaps
import Sentry
import SwiftUI

final class AppStateConfigurator {
	@Environment(\.dependencyContainer) var dependencyContainer

	private var userState: UserState?

	func configure(with window: UIWindow) {
		configurationAsserts()
		initializeSDKs()
		configureDependencies(with: window)
	}

	private func configurationAsserts() {
		let app = UIApplication.shared

		guard !((Bundle.main.infoDictionary?["FirebaseProjectID"] as? String) ?? "").isEmpty else {
			assertionFailure("FirebaseProjectID is required, make sure the Info.plist contains correct value for key FirebaseProjectID")
			SentrySDK.capture(message: "Missing FirebaseProjectID for app \(Config.appRealm) \(app.fullApplicationVersion)")
			return
		}
	}

	private func initializeSDKs() {
		#if !DEBUG
			SentrySDK.start { options in
				options.dsn = "https://f89e9b57b59d4b449f65f990127e0718@o225773.ingest.sentry.io/5821783"
				options.integrations = Sentry.Options.defaultIntegrations().filter { name -> Bool in
					name != "SentryUIKitMemoryWarningIntegration" // This will disable  SentryUIKitMemoryWarningIntegration
				}
				options.enableAutoSessionTracking = true
				options.attachStacktrace = true
			}

			SentrySDK.configureScope { scope in
				scope.setEnvironment(API.Environment.current.rawValue)
				scope.setTag(value: Config.appRealm, key: "app.realm")
			}
		#endif

		GMSServices.initializeSDK()
	}

	private func configureDependencies(with window: UIWindow) {
		dependencyContainer.register(service: AppSettings())

		let credentialsStorage: CredentialsStorage = DefaultCredentialsStorage()
		dependencyContainer.register(service: credentialsStorage)

		let apiClient = API.Client(credentialsStorage: credentialsStorage)
		dependencyContainer.register(service: apiClient)

		let appleSignInCoordinator: AppleSignInCoordinator = DefaultAppleSignInCoordinator(window: window, authService: apiClient.services.auth)
		dependencyContainer.register(service: appleSignInCoordinator)

		let cloudMessaging: CloudMessaging = DefaultCloudMessaging(authToken: credentialsStorage.authToken, service: apiClient.services.userDevices)
		dependencyContainer.register(service: cloudMessaging)

		// App State
		let userState = UserState(authToken: credentialsStorage.authToken)
		self.userState = userState

		// Interactors
		let userInteractor = DefaultUserInteractor(state: userState,
		                                           authToken: credentialsStorage.authToken,
		                                           authService: apiClient.services.auth,
		                                           userService: apiClient.services.users,
		                                           placeService: apiClient.services.places,
		                                           appleSignInCoordinator: appleSignInCoordinator,
		                                           facebookAuth: FacebookManager.authenticate(with:))
		dependencyContainer.register(service: userInteractor as UserInteractor)
		
		let translatorRepository = TranslatorRepositoryImpl(service: apiClient.services.translator)
		dependencyContainer.register(service: translatorRepository as TranslatorRepository)

		// Environment Objects
		let provider = EnvironmentObjectsProvider(userState: userState)
		dependencyContainer.register(service: provider)
	}
}
