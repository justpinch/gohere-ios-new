import FBSDKCoreKit
import Foundation
import SwiftUI
import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
	@Environment(\.dependencyContainer) var dependencyContainer

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		FBSDKCoreKit.ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
		Analytics.configure()
		return true
	}

	// MARK: UISceneSession Lifecycle

	func application(_: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options _: UIScene.ConnectionOptions) -> UISceneConfiguration {
		UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
	}

	// MARK: Remote Notifications

	func application(_: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
		dependencyContainer.resolve(CloudMessaging.self).setAPNSToken(token: deviceToken)
	}
}
