//
//  Analytics.swift
//  GoHere
//
//  Created by Ivan Vasic on 30/01/2018.
//  Copyright © 2018 PINCH. All rights reserved.
//

import Contacts
import FBSDKCoreKit
import Foundation
import Mixpanel
import Sentry

private var mixpanel: MixpanelInstance?

enum Analytics {
	static func configure() {
		if let token = API.Environment.current.mixpanelToken {
			mixpanel = Mixpanel.initialize(token: token, trackAutomaticEvents: true, superProperties: ["AppRealm": Config.appRealmForMixpanel])
			// add current app realm to user's list of used realms
			mixpanel?.people.union(properties: ["AppRealm": [Config.appRealmForMixpanel]])
		}
	}
}

// MARK: - People (Mixpanel)

extension Analytics {
	enum User {
		enum Property: String {
			case privateProfile = "Private Profile"
			case followerCount = "Follower Count"
			case followingCount = "Following Count"
			case favoritesCount = "Favorites Count"
			case ownReviewCount = "Own Reviews Count"
			case facebookConnected = "Facebook Connected"
			case addressBookConnnected = "AddressBook Connected"
			case pushNotificationsEnabled = "Push Notifications Enabled"
		}

		static func set(_ property: Property, value: MixpanelType) {
			mixpanel?.people.set(property: property.rawValue, to: value)
		}

		static func increment(_ property: Property, value: Int) {
			mixpanel?.people.increment(property: property.rawValue, by: Double(value))
		}

		#if !RELEASE
			static func set(_ properties: [String: MixpanelType]) {
				mixpanel?.people.set(properties: properties)
			}
		#endif

		static func identify(id: String) {
			mixpanel?.identify(distinctId: id)

			SentrySDK.configureScope { scope in
				scope.setExtra(value: id, key: "usr.id")
			}
		}

		static func registerUserProperties() {
			let isAddressBookConnected = CNContactStore.authorizationStatus(for: .contacts) == .authorized
			let isFacebookConnected = FacebookManager().isAuthenticated

			set(.addressBookConnnected, value: isAddressBookConnected)
			set(.facebookConnected, value: isFacebookConnected)
		}

		static func reset() {
			mixpanel?.reset()

			// re-register super properties
			mixpanel?.registerSuperProperties(["AppRealm": Config.appRealmForMixpanel])

			// re-add current app realm to user's list of used realms
			// (not 100% sure this is necessary here but it doesn't hurt)
			mixpanel?.people.union(properties: ["AppRealm": [Config.appRealmForMixpanel]])

			SentrySDK.configureScope { scope in
				scope.setExtra(value: "", key: "usr.id")
			}
		}
	}
}

// MARK: - Tracking methods

extension Analytics {
	/**
	 Track initial events (e.g. on startup)
	 */
	static func trackInitial() {
		Analytics.User.set(.pushNotificationsEnabled, value: UIApplication.shared.isRegisteredForRemoteNotifications)
		Analytics.User.set(.addressBookConnnected, value: CNContactStore.authorizationStatus(for: .contacts) == .authorized)
		Analytics.User.set(.facebookConnected, value: FBSDKCoreKit.AccessToken.isCurrentAccessTokenActive)
	}

	/**
	 Track screen view
	 */
	static func trackScreen(name: String, screenClass _: String? = "UIViewController") {
		// not tracking atm, only Sentry breadcrumbs

		let crumb = Breadcrumb(level: .info, category: "ui.lifecycle")
		crumb.type = "navigation"
		crumb.data = ["screen": name]
		SentrySDK.addBreadcrumb(crumb: crumb)
	}

	/**
	 Log custom event
	 */
	static func log(_ event: Event) {
		// Mixpanel
		if let name = event.mixpanelEvent {
			mixpanel?.track(event: name, properties: event.mixpanelParams)
		}

		// additional mixpanel action
		event.mixpanelAction?()

		// Sentry
		if let eventString = event.mixpanelEvent {
			let crumb = Breadcrumb(level: .info, category: "user.event")
			crumb.data = ["event": eventString]
			SentrySDK.addBreadcrumb(crumb: crumb)
		}
	}

	/**
	 Start measuring time for an event.
	 Timing will be stopped automatically after this event has been logged
	 */
	static func time(_ event: Event) {
		if let name = event.mixpanelEvent {
			mixpanel?.time(event: name)
		}
	}
}

// MARK: - Events

extension Analytics {
	enum LoginKind: String {
		case credentials
		case phone
		case facebook
		case apple
	}

	enum Event {
		// Login event
		case login(kind: LoginKind)

		// Account register event
		case register(kind: LoginKind)

		// Review posted
		case reviewPosted(review: Review, shared: Bool)

		// Place added to favorites
		case favoritePlaceAdded(place: Place)

		// Affiliate call to action
		case affiliateCTA(type: String?, link: URL)

		// Advertorial call to action
		case advertorialCTA(slug: String?, link: URL)
		
		// Advertorial call to action
		case promotedPlaceAdvertorialCTA(type: String?, link: URL)
		
		// Opening a place detail
		case placeDetail(place: Place)
		
		// Tapping on a place from the promoted places
		case promotedPlaceSelected(place: Place)
		
		// When the user drag down the promoted places
		case promotedPlacesDismissed
		
		case splashScreen
	}
}

// MARK: - Mixpanel

private extension Analytics.Event {
	var mixpanelEvent: String? {
		switch self {
			case .login:
				return "Sign In"

			case .register:
				return "Sign Up"

			case .reviewPosted:
				return "Review Posted"

			case .favoritePlaceAdded:
				return "Favorite Added"

			case .affiliateCTA:
				return "Affiliate CTA"

			case .advertorialCTA:
				return "Advertorial CTA"
			
			case .promotedPlaceAdvertorialCTA:
				return "Bottom sheet CTA"
			
			case .placeDetail:
				return "Place detail"
			
			case .promotedPlaceSelected:
				return "Bottom sheet place selected"
			
			case .promotedPlacesDismissed:
				return "Drag down bottom sheet"
			
			case .splashScreen:
				return "SplashScreen"
		}
	}

	var mixpanelParams: [String: MixpanelType]? {
		switch self {
			case let .login(kind):
				return ["Sign In Kind": kind.rawValue]

			case let .register(kind):
				return ["Sign Up Kind": kind.rawValue]

			case let .reviewPosted(review, shared):
				return removeNilValues(["Place Name": review.place.name,
				                        "Place City": review.place.city,
				                        "Photos Count": review.images.count,
				                        "Review Text Length": review.text.count,
				                        "Shared": shared])

			case let .favoritePlaceAdded(place):
				return removeNilValues(["Place Name": place.name,
				                        "Place City": place.city])

			case let .affiliateCTA(type, link):
				var params: [String: String] = ["Affiliate URL": link.absoluteString]
				if let type = type {
					params["Affiliate Type"] = type
				}
				return params
			
			case let .promotedPlaceAdvertorialCTA(type, link):
				var params: [String: String] = ["Affiliate URL": link.absoluteString]
				if let type = type {
					params["Affiliate Type"] = type
				}
				return params

			case let .advertorialCTA(slug, link):
				var params: [String: String] = ["URL": link.absoluteString]
				if let slug = slug {
					params["Slug"] = slug
				}
				return params
			
			case let .placeDetail(place):
				return removeNilValues(["Place Name": place.name,
										"Place City": place.city])
			
			case let .promotedPlaceSelected(place):
				return removeNilValues(["Place Name": place.name,
										"Place City": place.city])

			default:
				return nil
		}
	}

	var mixpanelAction: (() -> Void)? {
		switch self {
			case let .login(kind):
				return { Analytics.User.set(.facebookConnected, value: kind == .facebook) }
			case .reviewPosted:
				return { Analytics.User.increment(.ownReviewCount, value: 1) }
			default:
				return nil
		}
	}
}

private func removeNilValues(_ dict: [String: MixpanelType?]) -> [String: MixpanelType] {
	dict.reduce([:]) { result, keyValuePair -> [String: MixpanelType] in
		var combined = result
		if let value = keyValuePair.value {
			combined[keyValuePair.key] = value
		}
		return combined
	}
}
