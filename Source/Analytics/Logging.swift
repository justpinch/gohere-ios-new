import Foundation
import OSLog

extension Logger {
	fileprivate static var subsystem = Bundle.main.bundleIdentifier ?? "GoHere"

	/// General logger
	static let general = Logger(subsystem: subsystem, category: "general")

	/// Logs the ApiClient events
	static let apiClient = Logger(subsystem: subsystem, category: "api.client")
}
