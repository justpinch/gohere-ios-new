import Foundation
import os.log
import Sentry

extension Error {
	/// Report the error to the globally configured error reporting service
	/// Additionally log the error using provided logger or default.
	func report(logger: Logger? = nil,
	            context: [String: String] = [:],
	            file: StaticString = #file, line: UInt = #line)
	{
		SentrySDK.capture(error: self) { scope in
			scope.setContext(value: context, key: "Error")
		}
		self.log(with: logger, context: context, file: file, line: line)
	}

	/// Log error with configured logging system
	func log(with logger: Logger? = nil,
	         context _: [String: String] = [:],
	         file _: StaticString = #file, line _: UInt = #line)
	{
		logger?.error("\(self.localizedDescription)")
	}
}
