import SwiftUI

struct SavedPlacesView: View {
	@State var signInPrompt: Bool = false
	@State var isPresentingOnboarding: Bool = false

	@Environment(\.apiClient.services) var services
	@Environment(\.userInteractor.savedPlaces) var interactor
	@EnvironmentObject var userState: UserState

	var body: some View {
		NavigationView {
			if !isPresentingOnboarding, userState.isAuthenticated, let interactor = interactor {
				SavedPlacesContainerView(model: SavedPlacesModel(userState: userState, interactor: interactor, placeService: services.places))
					.navigationTitle("saved_places")
					.navigationBarTitleDisplayMode(.inline)
			} else {
				SignInPlaceholderView(action: .savePlace, isPresentingOnboarding: $isPresentingOnboarding)
					.navigationTitle("saved_places")
					.navigationBarTitleDisplayMode(.inline)
			}
		}
		.navigationViewStyle(StackNavigationViewStyle())
		.accentColor(.gh(.onNavbar))
	}
}

private struct SavedPlacesContainerView: View {
	@StateObject var model: SavedPlacesModel

	var body: some View {
		ListLoadingStateView(model: model, buildRow: { place in
			NavigationLink(
				destination: PlaceDetailsView(place: place),
				label: {
					VStack {
						PlaceInlineView(place: place)
						Divider()
					}
				}
			)
			.padding([.leading, .trailing])
		}, buildNoContent: {
			VStack(spacing: 6) {
				Image(systemName: "bookmark")
					.font(.system(size: 36))
					.foregroundColor(.gh(.labelTertiary))
					.padding(.bottom, 6)

				Text("no_saved_places")
					.font(.gh(.title2))
					.foregroundColor(.gh(.labelPrimary))

				Text("no_saved_places_message")
					.font(.gh(.callout))
					.foregroundColor(.gh(.labelSecondary))
			}
			.multilineTextAlignment(.center)
			.padding(48)
		}, contentInsets: .init(top: 16, leading: 0, bottom: 16, trailing: 0))
			.background(Color.gh(.backgroundPrimary).ignoresSafeArea())
			.onAppear(perform: model.bindObservables)
			.animation(.none)
	}
}

struct SavedPlacesView_Previews: PreviewProvider {
	static var previews: some View {
		NavigationView {
			SavedPlacesContainerView(model: model)
				.navigationBarTitleDisplayMode(.inline)
		}
		.injectPreviewsEnvironment()
	}

	static var model: SavedPlacesModel {
		let model = SavedPlacesModel(userState: UserState(authToken: .init(.fixture1)),
		                             interactor: PreviewUserSavedPlacesInteractor(),
		                             placeService: PreviewPlaceService())

//		model.content = .loading
//		model.content = .model([])
//		model.content = .model([.fixture1, .fixture2])
//		model.content = .error(.fixture1)

		return model
	}
}
