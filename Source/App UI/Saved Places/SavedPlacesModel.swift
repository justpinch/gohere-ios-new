import Combine
import SwiftUI

final class SavedPlacesModel: ObservableObject, ContentPagingModel, ContentRefreshableModel {
	private let userState: UserState
	private let interactor: UserSavedPlacesInteractor
	private let placeService: PlaceService
	private let notificationCenter: NotificationCenterProtocol

	@Published var content: ModelState<[Place]> = .none

	private var canPaginate: Bool = true
	private let pageSize: UInt = 30

	var isPaginating: Bool {
		content.isLoading && content.modelOrPrevious != nil
	}

	private var fetchRequest: AnyCancellable?
	private var cancellables = Set<AnyCancellable>()

	init(userState: UserState, interactor: UserSavedPlacesInteractor, placeService: PlaceService, notificationCenter: NotificationCenterProtocol = NotificationCenter.default) {
		self.userState = userState
		self.interactor = interactor
		self.placeService = placeService
		self.notificationCenter = notificationCenter
	}

	func bindObservables() {
		guard cancellables.isEmpty else {
			return
		}

		// Observe place saved
		notificationCenter
			.publisher(for: Place.Notification.placeSaved, object: nil)
			.sink(receiveValue: { [unowned self] notification in
				guard let place = notification.userInfo?[Place.Notification.Key.place] as? Place else {
					return
				}

				if var places = self.content.model, !places.contains(place) {
					places.append(place)
					self.content = .model(places)
				}
			})
			.store(in: &cancellables)

		// Observe place unsaved
		notificationCenter
			.publisher(for: Place.Notification.placeUnsaved, object: nil)
			.sink(receiveValue: { [unowned self] notification in
				guard let place = notification.userInfo?[Place.Notification.Key.place] as? Place else {
					return
				}

				if var places = self.content.model, let index = places.firstIndex(of: place) {
					places.remove(at: index)
					self.content = .model(places)
				}
			})
			.store(in: &cancellables)
	}

	func refresh() {
		fetchNext(offset: 0)
	}

	func loadIfNeeded() {
		guard content == .none || content.error != nil else {
			return
		}

		fetchNext(offset: 0)
	}

	func itemAppeared(at index: Int) {
		guard canPaginate, !content.isLoading,
		      index > (content.model?.count ?? .max) - 3,
		      let offset = content.model?.count
		else {
			return
		}

		fetchNext(offset: offset)
	}

	private func fetchNext(offset: Int) {
		let replaceContent = offset == 0

		fetchRequest = placeService
			.favorites(offset: UInt(offset), limit: pageSize)
			.map { [unowned self] in
				let visiblePlaces = replaceContent ? [] : (self.content.modelOrPrevious ?? [])
				let pageResults = $0.results.filter { !visiblePlaces.contains($0) }

				// update statuses locally
				pageResults.map(\.id).forEach {
					self.interactor.setStatus(for: $0, isSaved: true)
				}

				let content = visiblePlaces + pageResults
				self.canPaginate = $0.count > content.count
				return content
			}
			.mapModelState(current: content.modelOrPrevious)
			.assign(to: \.content, onUnowned: self)
	}
}
