import SwiftUI

struct RootView: View {
	@Environment(\.appSettings) var appSettings
	@Environment(\.apiClient.services) var services
	@Environment(\.userInteractor) var userInteractor
	@EnvironmentObject var userState: UserState

	@State private var isAppMenuShown = false
	@State private var profileMenuDestination: ProfileMenuDestination?
	@State private var isPresentingOnboarding: Bool = false
	@State private var isPresentingFAQ: Bool = false

	var body: some View {
		TabView {
			ExploreView(model: ExploreModel(biasRegion: appSettings.defaultRegion,
											promotedPlacesSearchRadius: appSettings.promotedPlacesSearchRadius,
			                                placeService: services.places,
			                                placeExternalService: services.placesExternal)
			)
			.tabItem {
				Label("tab_explore".localizedOverrideable(), image: "tab-icon-explore")
			}

			ActivityFeedView()
				.tabItem {
					Label("tab_activity".localizedOverrideable(), systemImage: "list.bullet")
				}

			SavedPlacesView()
				.tabItem {
					if #available(iOS 15.0, *) {
						Label("tab_saved".localizedOverrideable(), systemImage: "bookmark")
							.environment(\.symbolVariants, .none) // fix icon being filled
					} else {
						Label("tab_saved".localizedOverrideable(), systemImage: "bookmark")
					}
				}

			NavigationView {
				Group {
					if let user = userState.user.modelOrPrevious, !isPresentingOnboarding {
						UserProfileView(user)
							.navigationBarTitleDisplayMode(.inline)
					} else {
						SignInPlaceholderView(action: .viewOwnProfile, isPresentingOnboarding: $isPresentingOnboarding)
							.navigationBarTitleDisplayMode(.inline)
					}
				}
				.navigationBarItems(trailing: appMenuBarItem())
				.navigate(using: $profileMenuDestination, destination: profileMenuDestinationView)
				.fullScreenCover(isPresented: $isPresentingFAQ, content: {
					SafariView(url: AppSettings.faqURL)
				})
			}
			.navigationViewStyle(StackNavigationViewStyle())
			.accentColor(.gh(.onNavbar))
			.tabItem {
				if #available(iOS 15.0, *) {
					Label("tab_profile".localizedOverrideable(), systemImage: "person.crop.circle")
						.environment(\.symbolVariants, .none) // fix icon being filled
				} else {
					Label("tab_profile".localizedOverrideable(), systemImage: "person.crop.circle")
				}
			}
		}
		.accentColor(.gh(.onTabbarSelected))
		.appMenuBottomSheet(isPresented: $isAppMenuShown, items: appMenuItems)
	}

	@ViewBuilder
	private func appMenuBarItem() -> some View {
		Button(action: {
			isAppMenuShown.toggle()
		}, label: {
			Image(systemName: "line.horizontal.3")
		})
		.buttonStyle(NavbarButtonStyle())
		.padding(8)
	}

	private var appMenuItems: [AppMenuItem] {
		var items: [AppMenuItem] = [
			.init(title: "frequently_asked_questions", image: "questionmark", action: { isPresentingFAQ = true }),
			.init(title: "about_the_app", image: "info", action: { profileMenuDestination = .about }),
		]

		if userState.isAuthenticated {
			items.append(.init(title: "sign_out", image: "arrow.right.square", action: userInteractor.signOut))
		}

		return items
	}

	@ViewBuilder
	private func profileMenuDestinationView(_ dest: ProfileMenuDestination) -> some View {
		switch dest {
			case .about:
				AboutView()
		}
	}
}

private enum ProfileMenuDestination {
	case about
}

struct RootView_Previews: PreviewProvider {
	static var previews: some View {
		RootView()
			.onAppear(perform: {
				Theme.configureAppearance()
			})
			.injectPreviewsEnvironment()
	}
}
