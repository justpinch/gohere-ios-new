import Combine
import SwiftUI

struct BootstrapView: View {
	var publisher: AnyPublisher<Never, Error>
	@State var error: Error?

	@State private var cancellable: AnyCancellable?

	var body: some View {
		Group {
			if let error = error {
				VStack(spacing: 10) {
					Text(error.localizedDescription)
						.multilineTextAlignment(.center)
						.font(.gh(.body))
						.foregroundColor(.gh(.labelPrimary))

					Button(action: load, label: {
						Text("Retry")
							.font(.gh(.body))
							.foregroundColor(.gh(.link))
					})
				}
				.padding()
			} else {
				Image("splash-logo")
					.resizable()
					.aspectRatio(contentMode: .fit)
					.padding([.leading, .trailing], 45)
					.ignoresSafeArea()
			}
		}
		.onAppear(perform: load)
	}

	private func load() {
		publisher
			.handleEvents(receiveSubscription: { _ in
				self.error = nil
			})
			.catch { error -> AnyPublisher<Never, Never> in
				self.error = error
				return Empty(completeImmediately: true).eraseToAnyPublisher()
			}
			.subscribeIgnoringEvents()
	}
}

struct BootstrapView_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			BootstrapView(publisher: Empty(completeImmediately: true).setFailureType(to: Error.self).eraseToAnyPublisher())

			BootstrapView(publisher: Fail(error: NSError(domain: "domain", code: 2, userInfo: nil)).eraseToAnyPublisher())
		}

		Group {
			BootstrapView(publisher: Empty(completeImmediately: true).setFailureType(to: Error.self).eraseToAnyPublisher())

			BootstrapView(publisher: Fail(error: NSError(domain: "domain", code: 2, userInfo: nil)).eraseToAnyPublisher())
		}
		.background(Color.gh(.backgroundPrimary))
		.environment(\.colorScheme, .dark)
	}
}
