import SwiftUI

struct PlaceDetailsReviewsView: View {
	let reviews: [ReviewItem]
	let onToggleTranslationClick: (ReviewItem) -> ()

	var body: some View {
		LazyVStack {
			ForEach(reviews) { reviewItem in
				ReviewLargeView(reviewItem: reviewItem,
								onToggleTranslationClick: onToggleTranslationClick,
				                actions: {
									ReviewActionsEllipsisView(review: reviewItem.review)
				                		.accentColor(.gh(.labelSecondary))
				                		.offset(x: 10, y: -10)
				                })
			}
		}
		.padding([.top, .bottom])
	}
}

struct PlaceDetailsReviewsView_Previews: PreviewProvider {
	static var previews: some View {
		PlaceDetailsReviewsView(
			reviews: [.fixture1, .fixture2, .fixture3],
			onToggleTranslationClick: {_ in }
		)
			.injectPreviewsEnvironment()
	}
}
