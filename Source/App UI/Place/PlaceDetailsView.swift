import SwiftUI

struct PlaceDetailsView: View {
	fileprivate enum Source {
		case id(Place.ID)
		case place(Place)
		case lookup(PlaceLookup)
		case autocomplete(PlaceAutocomplete)
	}

	private let source: Source
	@Environment(\.apiClient.services) var services
	@Environment(\.userInteractor) var userInteractor
	@Environment(\.translatorRepository) var translatorRepo

	init(id: Place.ID) {
		source = .id(id)
	}

	init(place: Place) {
		source = .place(place)
	}

	init(lookup: PlaceLookup) {
		source = .lookup(lookup)
	}

	init(autocomplete: PlaceAutocomplete) {
		source = .autocomplete(autocomplete)
	}

	@State var test = 0
	var body: some View {
		PlaceDetailsContainerView(model: model())
			.navigationBarTitleDisplayMode(.inline)
	}

	func model() -> PlaceDetailsModel {
		switch source {
			case let .id(id):
				return PlaceDetailsModel(id: id,
				                         placeService: services.places,
				                         placeExternalService: services.placesExternal,
				                         reviewService: services.reviews,
										 userInteractor: userInteractor,
										 translatorRepo: translatorRepo)
			case let .place(place):
				return PlaceDetailsModel(place: place,
				                         placeService: services.places,
										 placeExternalService: services.placesExternal,
										 translatorRepo: translatorRepo,
				                         reviewService: services.reviews,
				                         userInteractor: userInteractor)
			case let .lookup(lookup):
				return PlaceDetailsModel(lookupID: lookup.lookupId,
				                         providerID: lookup.providerId,
				                         placeService: services.places,
				                         placeExternalService: services.placesExternal,
				                         reviewService: services.reviews,
										 userInteractor: userInteractor,
										 translatorRepo: translatorRepo)
			case let .autocomplete(autocomplete):
				return PlaceDetailsModel(lookupID: autocomplete.lookupId,
				                         providerID: autocomplete.providerId,
				                         placeService: services.places,
				                         placeExternalService: services.placesExternal,
				                         reviewService: services.reviews,
										 userInteractor: userInteractor,
										 translatorRepo: translatorRepo)
		}
	}
}

struct PlaceDetailsContainerView: View {
	@StateObject var model: PlaceDetailsModel

	var body: some View {
		Group {
			switch model.place {
				case .none:
					Color.gh(.backgroundPrimary)
				case let .loading(place):
					if let place = place {
						PlaceDetailsContentView(place: place, reviews: model.reviewItems, onToggleTranslationClick: model.onToggleTranslationClick)
					} else {
						ZStack {
							ProgressIndicator(size: .large)
						}
					}
				case let .model(place):
				PlaceDetailsContentView(place: place, reviews: model.reviewItems, onToggleTranslationClick: model.onToggleTranslationClick)
				case let .error(error):
					ZStack {
						Text(error.localizedDescription)
							.multilineTextAlignment(.center)
							.lineLimit(10)
							.font(.gh(.footnote))
							.foregroundColor(.gh(.utilityError))
							.padding()
					}
			}
		}
		.onAppear(perform: model.bindObservables)
		.background(EmptyView().alert(isPresented: $model.showingError, content: {
			Alert(
				title: Text("error"),
				message: Text(model.errorMessage?.localizedOverrideable() ?? "generic_error"),
				dismissButton: .cancel())
		}))
	}
}

struct PlaceDetailsContentView: View {
	let place: Place
	let reviews: ModelState<[ReviewItem]>
	let onToggleTranslationClick: (ReviewItem) -> ()

	@State private var fullScreenGalleryImage: ImageGallery.Image? = nil
	@State private var selectedRating: IdentifiableWrapper<Int, Int>?
	@State private var isShowingSignIn: Bool = false

	private let width: CGFloat = UIScreen.main.bounds.width

	@Environment(\.apiClient.services) var services
	@EnvironmentObject var userState: UserState

	var body: some View {
		ScrollView {
			if let photo = place.photo {
				ImageGallery(images: [.init(url: photo.url(for: .screenWidth), fallback: photo.originalSizeURL())], imageSelected: showFullscreenImage)
					.frame(width: width, height: width * 9 / 16)
					.sheet(item: $fullScreenGalleryImage, content: imageGallery)
			} else {
				Spacer().frame(height: 20)
			}

			VStack {
				PlaceDetailsHeaderView(place: place)
					.padding(.bottom, 8)

				switch reviews {
					case .none:
						EmptyView()
					case let .loading(model):
						if let model = model {
							reviewsView(with: model, onToggleTranslationClick: onToggleTranslationClick)
						} else {
							ProgressIndicator(size: .large)
						}
					case let .model(model):
						reviewsView(with: model, onToggleTranslationClick: onToggleTranslationClick)
					case let .error(error):
						Text(error.localizedDescription)
							.lineLimit(4)
							.font(.gh(.footnote))
							.foregroundColor(.gh(.utilityError))
				}
			}
			.padding([.leading, .trailing], 20)
		}
		.signInPrompt(isPresented: $isShowingSignIn)
		.sheet(item: $selectedRating, content: reviewAddView)
	}

	@ViewBuilder
	private func reviewsView(with reviews: [ReviewItem], onToggleTranslationClick: @escaping (ReviewItem) -> ()) -> some View {
		VStack {
			VStack(alignment: .leading) {
				Text((reviews.isEmpty ? "leave_first_rating" : (Config.isRatingAllowed() ? "rate_and_review" : "reviews")).localizedOverrideable())
					.font(.gh(.title3))
					.foregroundColor(.gh(.labelPrimary))

				HStack(alignment: .center, spacing: 16) {
					if Config.isRatingAllowed() {
						AvatarView(path: userState.user.profile?.avatar, initials: userState.user.initials)
							.frame(width: 40, height: 40)
						
						RatingInputSelectionView(onRatingSelected: selectRating, foregroundColor: .gh(.rating))
						.frame(height: 32)
						.fixedSize()
						
						Spacer()
					} else {
						Button("place_review", action: {
							selectRating(rating: AppSettings.maxRatingValue)
						}).buttonStyle(PrimaryButtonStyle(size: .small))
					}
				}

				PlaceDetailsReviewsView(
					reviews: reviews,
					onToggleTranslationClick: onToggleTranslationClick
				)
			}
		}
	}

	@ViewBuilder
	private func reviewAddView(rating: IdentifiableWrapper<Int, Int>) -> some View {
		NavigationView {
			AnyPlaceReviewAddView(model: AnyPlaceReviewAddModel(source: .place(place),
			                                                    initialRating: rating.wrapped,
			                                                    placeService: services.places,
			                                                    placeExternalService: services.placesExternal))
		}
		.provideEnvironmentObjects()
		.accentColor(.gh(.onNavbar))
	}

	private func showFullscreenImage(_ image: ImageGallery.Image) {
		fullScreenGalleryImage = image
	}

	@ViewBuilder
	private func imageGallery(with image: ImageGallery.Image) -> some View {
		ImageGallery(images: [image], isFullScreen: true, closeAction: {
			fullScreenGalleryImage = nil
		})
	}
	
	private func selectRating(rating: Int) {
		if userState.isAuthenticated {
			selectedRating = .init(rating)
		} else {
			isShowingSignIn = true
		}
	}
}

struct PlaceDetailsView_Previews: PreviewProvider {
	static var previews: some View {
		NavigationView {
			PlaceDetailsContentView(place: .fixture1, reviews: .model([.fixture1, .fixture2]), onToggleTranslationClick: {_ in })
				.navigationBarTitleDisplayMode(.inline)
		}
		.injectPreviewsEnvironment()
	}
}
