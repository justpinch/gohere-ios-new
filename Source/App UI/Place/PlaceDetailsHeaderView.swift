import SwiftUI

struct PlaceDetailsHeaderView: View {
	let place: Place

	@Environment(\.colorScheme) private var colorScheme

	var body: some View {
		VStack(alignment: .leading) {
			HStack(alignment: .top) {
				VStack(alignment: .leading, spacing: 4) {
					Text(place.name)
						.font(.gh(.title2))
						.foregroundColor(.gh(.labelPrimary))

					if let rating = place.averageRating {
						RatingView(rating: rating, activeColor: .gh(.labelPrimary))
							.frame(height: 14)
							.fixedSize()
							.padding(.bottom, 8)
					}

					HStack {
						PlaceCategoryImage(icon: place.category.icon)
							.frame(width: 16, height: 16)

						Text(place.category.localizedName)
							.lineLimit(1)
							.font(.gh(.caption1))
							.foregroundColor(.gh(.labelPrimary))

						if let distance = place.location.prettyDistanceFromUser() {
							Text("• \(distance)")
								.font(.gh(.caption1))
								.foregroundColor(.gh(.labelTertiary))
						}
					}
				}

				Spacer(minLength: 8)

				SavePlaceButton(place: place)
					.offset(x: 8) // negative padding, helps visually
			}

			Text(place.address)
				.lineLimit(2)
				.font(.gh(.caption1))
				.foregroundColor(.gh(.labelSecondary))
				.fixedSize(horizontal: false, vertical: true)

			if let hours = place.info?.openingHours {
				OpeningHoursView(data: hours)
			}

			PlaceDetailsActionsView(place: place)
		}
	}
}

struct PlaceDetailsHeaderView_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			PlaceDetailsHeaderView(place: .fixture1)
				.padding()
				.previewLayout(.sizeThatFits)
			PlaceDetailsHeaderView(place: .fixture1)
				.preferredColorScheme(.dark)
				.padding()
				.previewLayout(.sizeThatFits)
		}
		.injectPreviewsEnvironment()
	}
}
