import Combine
import Foundation

final class PlaceDetailsModel: ObservableObject {
	enum ID {
		case place(Place.ID)
		case lookup(String, providerID: Int)
	}

	private let id: ID

	@Published private(set) var place: ModelState<Place> = .none
	@Published private(set) var reviewItems: ModelState<[ReviewItem]> = .none
	@Published private(set) var errorMessage: String? = nil
	@Published var showingError: Bool = false
	
	@Published private var reviews: ModelState<[Review]> = .none
	@Published private var translatedReviews: Dictionary<String, ReviewItem> = [:]

	private let placeService: PlaceService
	private let placeExternalService: PlaceExternalService
	private let reviewService: ReviewService
	private let userInteractor: UserInteractor?
	private let notificationCenter: NotificationCenterProtocol
	private let translatorRepository: TranslatorRepository

	private var cancellables: Set<AnyCancellable> = []
	private var translatorCancellables: Set<AnyCancellable> = []
	
	init(id: Place.ID,
	     placeService: PlaceService,
	     placeExternalService: PlaceExternalService,
	     reviewService: ReviewService,
	     userInteractor: UserInteractor,
		 translatorRepo: TranslatorRepository,
	     notificationCenter: NotificationCenterProtocol = NotificationCenter.default)
	{
		self.id = .place(id)
		self.placeService = placeService
		self.placeExternalService = placeExternalService
		self.reviewService = reviewService
		self.userInteractor = userInteractor
		self.notificationCenter = notificationCenter
		self.translatorRepository = translatorRepo
	}

	convenience init(place: Place,
					 placeService: PlaceService,
					 placeExternalService: PlaceExternalService,
					 translatorRepo: TranslatorRepository,
					 reviewService: ReviewService,
					 userInteractor: UserInteractor)
	{
		self.init(id: place.id, placeService: placeService, placeExternalService: placeExternalService, reviewService: reviewService, userInteractor: userInteractor, translatorRepo: translatorRepo)
		self.place = .model(place)
	}

	init(lookupID: String,
	     providerID: Int,
	     placeService: PlaceService,
	     placeExternalService: PlaceExternalService,
	     reviewService: ReviewService,
	     userInteractor: UserInteractor,
		 translatorRepo: TranslatorRepository,
	     notificationCenter: NotificationCenterProtocol = NotificationCenter.default)
	{
		self.id = .lookup(lookupID, providerID: providerID)
		self.placeService = placeService
		self.placeExternalService = placeExternalService
		self.reviewService = reviewService
		self.userInteractor = userInteractor
		self.notificationCenter = notificationCenter
		self.translatorRepository = translatorRepo
	}

	func bindObservables() {
		guard cancellables.isEmpty else {
			return
		}

		let publisher: AnyPublisher<Place, API.Error>

		switch id {
			case let .place(id):
				publisher = placeService.get(id: id)
			case let .lookup(lookupID, providerID):
				publisher = placeExternalService.import(lookupId: lookupID, providerId: providerID)
		}

		publisher
			.map { [unowned self] in
				var mutablePlace = $0
				if mutablePlace.averageRating == nil {
					mutablePlace.averageRating = self.place.modelOrPrevious?.averageRating
				}
				return mutablePlace
			}
			.mapModelState(current: place.modelOrPrevious)
			.sink(receiveValue: { [unowned self] placeState in
				if let placeValue = placeState.model {
					Analytics.log(.placeDetail(place: placeValue))
				}
				self.place = placeState
			})
			.store(in: &cancellables)

		$place
			.compactMap { [unowned self] place -> AnyPublisher<ModelState<[Review]>, Never>? in
				if let id = place.modelOrPrevious?.id {
					return self.reviewService
						.list(params: .init(placeID: id, userScope: true))
						.map(\.results)
						.mapModelState()
				}

				return nil
			}
			.switchToLatest()
			.assign(to: \.reviews, onUnowned: self)
			.store(in: &cancellables)
		
		$reviews.combineLatest($translatedReviews).map { reviewState, translatedReviews  in
			reviewState.map { reviewList in
				reviewList.map { review in
					let reviewItem = translatedReviews[review.id]
					return ReviewItem(review: review, translation: reviewItem?.translation, isTranslationVisible: reviewItem?.isTranslationVisible ?? false)
				}
			}
		}
		.assign(to: \.reviewItems, onUnowned: self)
		.store(in: &cancellables)

		// Observe any reviews posted for this place
		notificationCenter
			.publisher(for: Review.Notification.posted, object: nil)
			.compactMap { $0.userInfo?[Review.Notification.Key.review] as? Review }
			.filter { [unowned self] in
				$0.place.id == self.id.placeID || $0.place.idGoogle == self.id.googleID
			}
			.sink(receiveValue: { [unowned self] review in
				if var reviews = self.reviews.model {
					if let index = reviews.map(\.id).firstIndex(of: review.id) {
						reviews[index] = review
					} else {
						reviews.insert(review, at: 0)
					}
					self.reviews = .model(reviews)
				}
			})
			.store(in: &cancellables)

		// Observe review delete
		notificationCenter
			.publisher(for: Review.Notification.deleted, object: nil)
			.compactMap { $0.userInfo?[Review.Notification.Key.review] as? Review }
			.sink(receiveValue: { [unowned self] review in
				if var reviews = self.reviews.model, let index = reviews.firstIndex(of: review) {
					reviews.remove(at: index)
					self.reviews = .model(reviews)
				}
			})
			.store(in: &cancellables)
	}
	
	deinit {
		translatorCancellables.forEach({ $0.cancel() })
	}
	
	func onToggleTranslationClick(reviewItem: ReviewItem) {
		if reviewItem.translation != nil {
			self.translatedReviews[reviewItem.id] = ReviewItem(
				review: reviewItem.review,
				translation: reviewItem.translation,
				isTranslationVisible: !reviewItem.isTranslationVisible
			)
		} else {
			guard let currentLanguage = Locale.current.languageCode else {
				return
			}
			translatorRepository.translateReview(reviewId: reviewItem.review.id, to: currentLanguage)
				.receive(on: DispatchQueue.main)
				.sink { [weak self] completion in
					if case .failure(_) = completion {
						self?.errorMessage = "generic_error"
						self?.showingError = true
					}
				} receiveValue: { [weak self] translation in
					self?.translatedReviews[reviewItem.id] = ReviewItem(
						review: reviewItem.review,
						translation: translation,
						isTranslationVisible: !reviewItem.isTranslationVisible
					)
				}
				.store(in: &translatorCancellables)
		}
	}
}

extension PlaceDetailsModel.ID {
	var googleID: String? {
		if case let .lookup(id, _) = self {
			return id
		}
		return nil
	}

	var placeID: Place.ID? {
		if case let .place(id) = self {
			return id
		}
		return nil
	}
}
