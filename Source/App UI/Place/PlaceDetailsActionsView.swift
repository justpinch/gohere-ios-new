import MapKit
import SwiftUI

struct PlaceDetailsActionsView: View {
	let place: Place

	@State var isShownDirectionsPrompt: Bool = false
	@State var isShownWebsite: Bool = false
	@State var isShownCallToAction: Bool = false

	var body: some View {
		VStack(alignment: .leading, spacing: 8) {
			#warning("TODO: Opening hours")

			HStack {
				Button(action: showDirections, label: {
					Label("directions", systemImage: "ellipsis")
				})
				.quinaryButtonStyle(size: .small, width: .fitContent)
				.actionSheet(isPresented: $isShownDirectionsPrompt, content: {
					ActionSheet(
						title: Text(""),
						buttons: [
							.default(Text("Apple Maps"), action: showAppleMaps),
							.default(Text("Google Maps"), action: showGoogleMaps),
							.cancel(Text("cancel")),
						]
					)
				})

				if callURL != nil {
					Button(action: call, label: {
						Label("call", systemImage: "ellipsis")
					})
					.quinaryButtonStyle(size: .small, width: .fitContent)
				}

				if let website = place.website {
					Button(action: openWebsite, label: {
						Label("website", systemImage: "ellipsis")
					})
					.quinaryButtonStyle(size: .small, width: .fitContent)
					.sheet(isPresented: $isShownWebsite, content: {
						SafariView(url: website)
					})
				}
			}

			if let affiliate = place.affiliate {
				Button(action: openCallToAction, label: {
					Text(affiliate.callToAction)
				})
				.affiliateButtonStyle(size: .small, width: .infinity)
				.fullScreenCover(isPresented: $isShownCallToAction, content: {
					SafariView(url: affiliate.actionURL)
				})
			}
		}
	}

	var callURL: URL? {
		let phoneNumber = place.phone.removeAllNonNumericValues()
		guard !phoneNumber.isEmpty, let url = URL(string: "telprompt:\(phoneNumber)"), UIApplication.shared.canOpenURL(url) else {
			return nil
		}
		return url
	}

	private func openCallToAction() {
		isShownCallToAction = true

		if let affiliate = place.affiliate {
			Analytics.log(.affiliateCTA(type: affiliate.kind, link: affiliate.actionURL))
		}
	}

	private func call() {
		if let url = callURL {
			UIApplication.shared.open(url, options: [:], completionHandler: nil)
		}
	}

	private func openWebsite() {
		isShownWebsite = true
	}

	private func showDirections() {
		guard UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!) else {
			isShownDirectionsPrompt = false
			showAppleMaps()
			return
		}

		isShownDirectionsPrompt = true
	}

	private func showAppleMaps() {
		let placemark = MKPlacemark(coordinate: place.location.coordinate)
		let mapItem = MKMapItem(placemark: placemark)
		mapItem.name = place.name
		mapItem.url = place.website
		mapItem.openInMaps(launchOptions: nil)
	}

	private func showGoogleMaps() {
		let coordinate = place.location.coordinate
		let googlePlaceID = place.idGoogle

		var urlString = "comgooglemapsurl://www.google.com/maps/search/?api=1&query=\(coordinate.latitude),\(coordinate.longitude)"
		if let placeId = googlePlaceID {
			urlString = "\(urlString)&query_place_id=\(placeId)"
		}

		UIApplication.shared.open(URL(string: urlString)!, options: [:], completionHandler: nil)
	}
}

struct PlaceDetailsActionsView_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			PlaceDetailsActionsView(place: .fixture1)
				.padding()
				.previewLayout(.sizeThatFits)
			PlaceDetailsActionsView(place: .fixture1)
				.preferredColorScheme(.dark)
				.padding()
				.previewLayout(.sizeThatFits)
		}
	}
}
