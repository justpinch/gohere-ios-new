import SwiftUI

struct AboutView: View {
	var body: some View {
		ScrollView {
			VStack(alignment: .leading) {
				ZStack {
					Color.gh(.appIcon)

					if let image = UIImage(named: "about-header") {
						Image(uiImage: image)
							.resizable()
							.aspectRatio(contentMode: .fit)
							.overlay(
								Image("about-logo")
									.resizable()
									.aspectRatio(contentMode: .fit)
									.padding()
							)
					} else if let image = UIImage(named: "about-logo") {
						Image(uiImage: image)
							.resizable()
							.aspectRatio(contentMode: .fit)
							.padding()
							.padding([.leading, .trailing], 45)
					}
				}

				VStack(alignment: .leading, spacing: 10) {
					Text("about_title".localizedFromTarget())
						.font(.gh(.title3))
						.foregroundColor(.gh(.labelPrimary))
						.fixedSize(horizontal: false, vertical: true)

					Text("about_text".localizedFromTarget())
						.font(.gh(.footnote))
						.foregroundColor(.gh(.labelPrimary))
						.fixedSize(horizontal: false, vertical: true)

					AboutCreditsView()
						.padding(.top, 30)
				}
				.padding()
			}
		}
		.navigationBarTitleDisplayMode(.inline)
	}
}

struct AboutCreditsView: View {
	@State private var isShowingSafari = false

	var body: some View {
		VStack {
			Text("about_credits")
				.font(.gh(.footnote))
				.foregroundColor(.gh(.labelPrimary))

			Button(action: {
				isShowingSafari = true
			}, label: {
				VStack {
					Image("gohere-logo-inline")
					Text("gohere.app")
						.underline()
						.font(.gh(.footnote))
						.foregroundColor(.gh(.labelPrimary))
				}
			})
		}
		.frame(maxWidth: .infinity)
		.padding([.top, .bottom], 30)
		.background(
			RoundedRectangle(cornerRadius: 10)
				.foregroundColor(.gh(.backgroundTertiary))
				.elevationLevel2()
		)
		.fullScreenCover(isPresented: $isShowingSafari, content: {
			SafariView(url: AppSettings.defaultWhiteLabelWebURL)
		})
	}
}

struct AboutView_Previews: PreviewProvider {
	static var previews: some View {
		NavigationView {
			AboutView()
		}
	}
}
