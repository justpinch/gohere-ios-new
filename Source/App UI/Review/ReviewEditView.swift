import SwiftUI

struct ReviewEditView: View {
	@StateObject var model: ReviewEditModel
	var isShowingDismissButton: Bool = false

	@EnvironmentObject var userState: UserState

	@Environment(\.isModalPresented) var isModalPresented
	@Environment(\.presentationMode) var presentationMode

	@ViewBuilder
	var submitNavButton: some View {
		Button(action: model.submit, label: {
			Text("place_review_short")
		})
		.buttonStyle(NavbarButtonStyle())
		.disabled(!model.isValid)
	}

	@ViewBuilder
	var closeNavButton: some View {
		Button(action: dismiss, label: {
			Image(systemName: "xmark")
				.font(.system(size: 20, weight: .semibold))
				.foregroundColor(.gh(.labelPrimary))
				.padding(8)
		})
		.buttonStyle(NavbarButtonStyle())
		.disabled(!model.isValid)
	}

	var body: some View {
		ZStack {
			ScrollView {
				VStack(alignment: .leading) {
					Text("How do you like \(model.place.name)?")
						.font(.gh(.title2))
						.foregroundColor(.gh(.labelPrimary))
						.padding(.bottom)
					if Config.isRatingAllowed() {
						Text("rating")
							.font(.gh(.subhead))
							.foregroundColor(.gh(.labelPrimary))
						
						HStack(alignment: .center, spacing: 16) {
							AvatarView(path: userState.user.profile?.avatar, initials: userState.user.initials)
								.frame(width: 40, height: 40)
							
							RatingInputView(rating: $model.review.rating)
								.frame(height: 28)
								.padding(8)
								.padding([.leading, .trailing], 10)
								.background(Capsule().foregroundColor(.gh(.backgroundTertiary)))
								.fixedSize()
							
							Spacer()
						}
					}

					if model.review.rating != nil {
						Text("review")
							.font(.gh(.subhead))
							.foregroundColor(.gh(.labelPrimary))

						LimitedCharactersTextEditView(text: $model.review.text)
							.frame(height: 100)

						Text("photos")
							.font(.gh(.subhead))
							.foregroundColor(.gh(.labelPrimary))
						ReviewPhotosEditView(photos: $model.review.photos)

						if let error = model.submitState.error {
							Text(error.localizedDescription)
								.lineLimit(3)
								.multilineTextAlignment(.center)
								.font(.gh(.footnote))
								.foregroundColor(.gh(.utilityError))
								.padding(.top, 8)
								.fixedSize(horizontal: false, vertical: true)
								.padding(.bottom, -20) // smaller padding between err & button
						}

						Button(action: model.submit, label: {
							Text("place_review")
						})
						.primaryButtonStyle()
						.disabled(!model.isValid)
						.padding(.top, 30)
					}
				}
				.padding(20)
			}

			if model.submitState.isLoading {
				ZStack {
					Color.black.opacity(0.5)
						.edgesIgnoringSafeArea(.all)
					ProgressIndicator(size: .large)
				}
			}
		}
		.navigationTitle("rate_and_review")
		.navigationBarTitleDisplayMode(.inline)
		.navigationBarItems(leading: isShowingDismissButton ? closeNavButton : nil, trailing: submitNavButton)
		.onChange(of: model.submitState, perform: onSubmitReview)
	}

	private func onSubmitReview(_ state: ModelState<Review>) {
		guard case .model = state else {
			return
		}

		dismiss()
	}

	private func dismiss() {
		presentationMode.wrappedValue.dismiss()
		isModalPresented.wrappedValue = false
	}
}

struct ReviewEditView_Previews: PreviewProvider {
	struct Container: View {
		init() {
			Theme.configureAppearance()
		}

		var body: some View {
			NavigationView {
				ReviewEditView(model: ReviewEditModel(place: .fixture1, reviewService: PreviewReviewService()))
			}
		}
	}

	static var previews: some View {
		Container()
			.injectPreviewsEnvironment()
	}
}
