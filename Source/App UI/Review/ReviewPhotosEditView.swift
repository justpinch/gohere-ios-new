import SwiftUI

struct ReviewPhotosEditView: View {
	@Binding var photos: [DraftReview.Photo]
	@State private var image: UIImage?

	@State private var removingPhoto: DraftReview.Photo?

	var canAddPhotos: Bool {
		photos.count < 5
	}

	var body: some View {
		ScrollView(.horizontal, showsIndicators: false) {
			LazyHStack {
				Spacer(minLength: 10)

				if canAddPhotos {
					ImagePickerButton(image: $image) {
						Image(systemName: "camera.fill")
							.font(.title2)
							.padding(16)
							.foregroundColor(.gh(.labelPrimary))
							.contentShape(Rectangle())
					}
					.zIndex(1) // fix for tappable area
				}

				if photos.isEmpty {
					Text("add_your_photos")
						.foregroundColor(.gh(.labelSecondary))
						.font(.gh(.body))
						.padding(.trailing, 16)
				}

				ForEach(photos) { photo in
					Button(action: {
						removingPhoto = photo
					}, label: {
						ReviewPhotoView(photo: photo)
							.frame(width: 60, height: 60)
					})
					.actionSheet(item: $removingPhoto, content: removePhotoPrompt)
				}
			}
		}
		.frame(height: 60)
		.padding([.top, .bottom], 10)
		.background(Color.gh(.backgroundTertiary))
		.cornerRadius(8)
		.onChange(of: image, perform: onImageChange)
	}

	private func onImageChange(_ image: UIImage?) {
		if let image = image {
			photos.append(.new(image))
		}
	}

	private func removePhotoPrompt(_ photo: DraftReview.Photo) -> ActionSheet {
		ActionSheet(
			title: Text(""),
			buttons: [
				.destructive(Text("remove_photo"), action: {
					withAnimation {
						remove(photo)
					}
				}),
				.cancel(Text("cancel")),
			]
		)
	}

	private func remove(_ photo: DraftReview.Photo) {
		if let idx = photos.firstIndex(of: photo) {
			photos.remove(at: idx)
		}
	}
}

private struct ReviewPhotoView: View {
	let photo: DraftReview.Photo

	var body: some View {
		GeometryReader(content: { geometry in
			switch photo {
				case let .existing(image):
					RemoteImage(path: image.path, size: geometry.size.width)
						.placeholder { Color.gh(.backgroundTertiary) }
						.aspectRatio(contentMode: .fill)
						.frame(maxWidth: geometry.size.width)
				case let .new(image):
					Image(uiImage: image)
						.resizable()
						.aspectRatio(contentMode: .fill)
						.clipped()
			}
		})
		// TODO: check if compositingGroup is still necessary
		// w/o it, the clipped image part absorbs all touches
		// https://stackoverflow.com/a/62111034
		.compositingGroup()
		.cornerRadius(8)
	}
}

struct ReviewPhotosEditView_Previews: PreviewProvider {
	struct Container: View {
		@State var photos: [DraftReview.Photo] = [
			.existing(.init(id: "1", path: .fixture(.place1))),
			.existing(.init(id: "2", path: .fixture(.place2))),
			.new(UIImage(named: "place1")!),
			.new(UIImage(named: "place2")!),
		]

		var body: some View {
			ReviewPhotosEditView(photos: $photos)
		}
	}

	static var previews: some View {
		Container()
			.previewLayout(.sizeThatFits)
	}
}
