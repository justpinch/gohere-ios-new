import Combine
import Foundation
import UIKit

struct DraftReview {
	var id: Review.ID?
	var rating: Int?
	var text: String
	var photos: [Photo]

	init(from review: Review) {
		id = review.id
		rating = review.rating
		text = review.text
		photos = review.images.map(Photo.existing)
	}

	init(rating: Int? = nil, text: String = "", photos: [Photo] = []) {
		self.id = nil
		self.rating = rating
		self.text = text
		self.photos = photos
	}

	enum Photo: Identifiable, Equatable {
		case existing(Review.Image)
		case new(UIImage)

		var id: String {
			switch self {
				case let .existing(image):
					return "existing-\(image.id)"
				case let .new(image):
					return "new-\(image.hashValue)"
			}
		}
	}

	func validate() -> (id: Review.ID?, rating: Int, text: String, photos: [Photo])? {
		guard let rating = rating else {
			return nil
		}

		return (id: id, rating: rating, text: text, photos: photos)
	}
}

extension Review {
	enum Notification {
		static let posted = NSNotification.Name(rawValue: "GoHere.Review.created")
		static let deleted = NSNotification.Name(rawValue: "GoHere.Review.deleted")

		enum Key {
			static let review = "review"
		}
	}
}

final class ReviewEditModel: ObservableObject {
	let place: Place
	@Published var review: DraftReview
	@Published var submitState: ModelState<Review> = .none

	private let reviewService: ReviewService
	private let notificationCenter: NotificationCenterProtocol

	// Local cache of uploaded images for submission in progress
	// or previously failed submission (essentially, a cache to prevent uploading
	// the same image multiple times in case something goes wrong during review posting).
	private var uploadedImages: [UIImage: Review.Image] = [:]

	private var submitRequest: AnyCancellable?

	init(place: Place, draft: DraftReview, reviewService: ReviewService, notificationCenter: NotificationCenterProtocol = NotificationCenter.default) {
		self.place = place
		self.review = draft
		self.reviewService = reviewService
		self.notificationCenter = notificationCenter
	}

	convenience init(place: Place, initialRating: Int? = nil, reviewService: ReviewService, notificationCenter: NotificationCenterProtocol = NotificationCenter.default) {
		let draft = DraftReview(rating: initialRating)
		self.init(place: place, draft: draft, reviewService: reviewService, notificationCenter: notificationCenter)
	}

	var isValid: Bool {
		return (Config.isRatingAllowed() && review.rating != nil)
						|| (!Config.isRatingAllowed() && !review.text.isEmpty)
	}

	func submit() {
		guard let draft = review.validate() else {
			return
		}

		// All photos, also map already uploaded ones
		let photos = draft.photos.map { photo -> DraftReview.Photo in
			if case let .new(image) = photo, let existing = self.uploadedImages[image] {
				return .existing(existing)
			}
			return photo
		}

		// New images we need to upload
		let imagesToUpload = photos.compactMap { photo -> UIImage? in
			if case let .new(image) = photo {
				return image
			}
			return nil
		}

		// Image IDs we already uploaded in the past
		let uploadedImageIDs = photos.compactMap { photo -> String? in
			if case let .existing(image) = photo {
				return image.id
			}
			return nil
		}

		let publisher = Just(imagesToUpload)
			.flatMap { [unowned self] images -> ApiResponsePublisher<[(UIImage, Review.Image)]> in
				guard !images.isEmpty else {
					return Just([]).setFailureType(to: API.Error.self).eraseToAnyPublisher()
				}
				// 1️⃣ Upload images
				return self.reviewService.upload(images: images).map {
					Array(zip(imagesToUpload, $0))
				}
				.eraseToAnyPublisher()
			}
			.map { [unowned self] pairs -> [String] in
				// 2️⃣ Cache uploaded images locally
				pairs.forEach { pair in
					self.uploadedImages[pair.0] = pair.1
				}

				// 3️⃣ Map only image IDs to be sent with the review
				let imageIDs = Set(pairs.map(\.1.id)).union(Set(uploadedImageIDs))
				return Array(imageIDs)
			}
			.flatMap { [unowned self] imageIDs -> ApiResponsePublisher<Review> in
				// 4️⃣ Finally, submit the review
				if let existingID = draft.id {
					return self.reviewService.update(id: existingID, imageIds: imageIDs, rating: draft.rating, text: draft.text).eraseToAnyPublisher()
				} else {
					return self.reviewService.create(place: self.place.id, imageIds: imageIDs, rating: draft.rating, text: draft.text).eraseToAnyPublisher()
				}
			}

		submitRequest = publisher
			.subscribe(on: DispatchQueue.global(qos: .userInitiated))
			.mapModelState()
			.receive(on: DispatchQueue.main)
			.handleEvents(receiveOutput: { [unowned self] state in
				if let model = state.model {
					// notify observers new review is created
					let userInfo = [Review.Notification.Key.review: model]
					self.notificationCenter.post(name: Review.Notification.posted, object: nil, userInfo: userInfo)

					if draft.id == nil {
						Analytics.log(.reviewPosted(review: model, shared: false))
					}
				}
			})
			.handleCompletionSuccess { [unowned self] in self.uploadedImages.removeAll() }
			.assign(to: \.submitState, onUnowned: self)
	}
}
