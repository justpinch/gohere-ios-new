import Combine
import SwiftUI

struct StartReviewView: View {
	@Environment(\.apiClient.services) var services

	var body: some View {
		StartReviewContentView(model: ReviewPlaceSearchModel(search: services.placesExternal.autocomplete))
	}
}

struct StartReviewContentView: View {
	@StateObject var model: ReviewPlaceSearchModel
	@State var selectedPlace: PlaceAutocomplete?

	@EnvironmentObject var userState: UserState
	@Environment(\.presentationMode) var presentationMode

	var contentPadding: CGFloat = 20

	var body: some View {
		ScrollView {
			VStack(alignment: .leading, spacing: 16) {
				VStack(alignment: .leading) {
					Text("Hi \(userState.user.model?.firstName ?? ""),")
					Text("what_do_you_want_to_review")
				}
				.font(.gh(.title2))
				.foregroundColor(.gh(.labelPrimary))

				SearchTextField(placeholder: "name_of_the_place", text: $model.query)

				switch model.results {
					case .none:
						EmptyView()
					case .loading:
						LazyVStack(spacing: 0, content: {
							PlaceLookupResultView
								.loadingPlaceholder()
							Divider()
						})
					case let .model(places):
						LazyVStack(spacing: 0, content: {
							if places.isEmpty {
								Text("no_results_found")
									.font(.gh(.footnote))
									.foregroundColor(.gh(.labelSecondary))
									.fixedSize(horizontal: false, vertical: true)
									.padding(.top)
							} else {
								ForEach(places, id: \.lookupId) { place in
									PlaceLookupResultView(title: place.title, subtitle: place.vicinity, icon: place.category?.icon)
										.frame(maxWidth: .infinity, alignment: .leading)
										.contentShape(Rectangle())
										.onTapGesture {
											self.selectedPlace = place
										}
									Divider()
								}
							}
						})
					case let .error(error):
						Text(error.localizedDescription)
							.font(.gh(.footnote))
							.foregroundColor(.gh(.utilityError))
							.fixedSize(horizontal: false, vertical: true)
				}

				Spacer()
			}
			.padding([.top, .leading, .trailing], contentPadding)
		}
		.onAppear(perform: model.startObservingQuery)
		.navigationBarTitleDisplayMode(.inline)
		.navigationTitle("review")
		.navigationBarItems(leading: Button(action: dismiss, label: {
			Image(systemName: "xmark")
		}))
		.navigate(using: $selectedPlace, destination: placeDetailsView)
	}

	@ViewBuilder
	private func placeDetailsView(for place: PlaceAutocomplete) -> some View {
		PlaceDetailsView(autocomplete: place)
	}

	private func dismiss() {
		presentationMode.wrappedValue.dismiss()
	}
}

struct StartReviewView_Previews: PreviewProvider {
	static var previews: some View {
		NavigationView {
			StartReviewContentView(model: ReviewPlaceSearchModel(
				search: { _, _ in
					Just([.fixture1, .fixture2, .fixture3]).setFailureType(to: API.Error.self).eraseToAnyPublisher()
				},
				locationManager: .init(manager: PreviewLocationManager()),
				results: .none
			))
		}
		.injectPreviewsEnvironment()
	}
}
