import SwiftUI

struct AnyPlaceReviewAddView: View {
	@StateObject var model: AnyPlaceReviewAddModel

	@Environment(\.apiClient.services) var services

	var body: some View {
		switch model.place {
			case .none:
				Color.gh(.backgroundPrimary)
					.onAppear(perform: model.load)
			case .loading:
				ProgressView()
			case let .model(place):
				ReviewEditView(model: ReviewEditModel(place: place, initialRating: model.initialRating, reviewService: services.reviews), isShowingDismissButton: true)
			case let .error(error):
				VStack {
					Text(error.localizedDescription)
						.lineLimit(3)
						.multilineTextAlignment(.center)
						.font(.gh(.footnote))
						.foregroundColor(.gh(.utilityError))
						.padding(.top, 8)
						.fixedSize(horizontal: false, vertical: true)

					Button("try_again", action: model.load)
						.quaternaryButtonStyle(size: .large, width: .fitContent)
						.padding()
				}
		}
	}
}

struct AnyPlaceReviewAddView_Previews: PreviewProvider {
	static var previews: some View {
		AnyPlaceReviewAddView(model: .init(source: .lookup(.fixture1), placeService: PreviewPlaceService(), placeExternalService: PreviewPlaceExternalService()))
			.injectPreviewsEnvironment()
	}
}
