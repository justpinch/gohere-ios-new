import Combine

final class AnyPlaceReviewAddModel: ObservableObject {
	let source: AnyPlace
	let initialRating: Int?
	@Published var place: ModelState<Place> = .none

	private let placeService: PlaceService
	private let placeExternalService: PlaceExternalService
	private var cancellable: AnyCancellable?

	init(source: AnyPlace, initialRating: Int? = nil, placeService: PlaceService, placeExternalService: PlaceExternalService) {
		self.source = source
		self.initialRating = initialRating
		self.placeService = placeService
		self.placeExternalService = placeExternalService

		if case let .place(place) = source {
			self.place = .model(place)
		}
	}

	func load() {
		let publisher: ApiResponsePublisher<Place>?

		switch source {
			case let .placeID(id):
				publisher = placeService.get(id: id)
			case let .lookup(lookup):
				publisher = placeExternalService.import(lookupId: lookup.lookupId, providerId: lookup.providerId)
			case .place:
				publisher = nil
			case let .autocomplete(autocomplete):
				publisher = placeExternalService.import(lookupId: autocomplete.lookupId, providerId: autocomplete.providerId)
		}

		guard let publisher = publisher else {
			return
		}

		cancellable = publisher
			.mapModelState()
			.assign(to: \.place, onUnowned: self)
	}
}
