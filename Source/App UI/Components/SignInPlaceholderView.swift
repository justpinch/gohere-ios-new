import AuthenticationServices
import SwiftUI

struct SignInPlaceholderView: View {
	struct Action {
		let title: LocalizedStringKey
		let icon: Image

		static let savePlace = Action(title: "sign_in_block_action_save_place", icon: Image(systemName: "bookmark"))
		static let viewOwnProfile = Action(title: "sign_in_block_action_own_profile", icon: Image(systemName: "person.circle"))
	}

	private struct OnboardingStart: Identifiable {
		let stage: OnboardingView.OnboardingStage
		var authRoute: AuthFlow.Route = .start

		var id: String {
			"\(stage.rawValue)\(authRoute)"
		}
	}

	let action: Action
	@Binding var isPresentingOnboarding: Bool

	@State var loadingState = LoadingState.none
	@State private var presentedOnboarding: OnboardingStart? {
		didSet {
			isPresentingOnboarding = presentedOnboarding != nil
		}
	}

	@Environment(\.apiClient.services) var services
	@Environment(\.userInteractor) var userInteractor
	@EnvironmentObject var userState: UserState

	var body: some View {
		VStack(spacing: 30) {
			Spacer()

			action.icon
				.font(.largeTitle)
				.foregroundColor(.gh(.labelTertiary))

			Text(action.title)
				.multilineTextAlignment(.center)
				.font(.gh(.title1))
				.foregroundColor(.gh(.labelPrimary))
				.fixedSize(horizontal: false, vertical: true)

			VStack(spacing: 20) {
				Button(action: {
					presentedOnboarding = OnboardingStart(stage: .auth, authRoute: .signUp)
				}, label: {
					Text("sign_up_for_free")
				})
				.primaryButtonStyle()

				Button(action: continueWithApple, label: {
					Label("continue_with_apple", systemImage: "applelogo")
				})
				.buttonStyle(QuinaryButtonStyle())

				Button(action: continueWithFacebook, label: {
					Text("continue_with_facebook")
				})
				.buttonStyle(QuinaryButtonStyle())
			}

			PlatformDisclaimerLabelView()
				.fixedSize(horizontal: false, vertical: true)
				.padding([.leading, .trailing])
				.ignoresSafeArea(.keyboard, edges: .bottom)

			Button(action: {
				presentedOnboarding = OnboardingStart(stage: .auth, authRoute: .signIn)
			}, label: {
				Group {
					Text("already_have_an_account")
						.foregroundColor(.gh(.labelSecondary))

					Text("sign_in")
						.bold()
						.foregroundColor(.gh(.tintPrimary))
				}
				.font(.gh(.footnote))
			})

			Spacer()
		}
		.padding([.leading, .trailing], 48)
		.overlay(loadingOverlay().ignoresSafeArea())
		.alert(item: alertError(), content: { error -> Alert in
			Alert(title: Text(error.wrapped.localizedDescription))
		})
		.sheet(item: $presentedOnboarding, content: continueOnboardingView)
	}

	@ViewBuilder
	private func continueOnboardingView(_ start: OnboardingStart) -> some View {
		OnboardingView(onboardingFinished: {
		               	// TODO: HACK: queue wrap is to quickly fix GOH-739,
		               	// SavedPlacesView reads bool isPresented onboarding but it also needs
		               	// saved places interactor which gets set *after* this so the login screen lingers on...
		               	DispatchQueue.main.async {
		               		presentedOnboarding = nil
		               	}
		               },
		               stage: start.stage,
		               authFlowStart: start.authRoute)
			.provideEnvironmentObjects()
	}

	@ViewBuilder
	private func loadingOverlay() -> some View {
		if loadingState.isLoading {
			ZStack {
				Color.black.opacity(0.85)
				ProgressView()
					.progressViewStyle(CircularProgressViewStyle(tint: .white))
			}
		} else {
			EmptyView()
		}
	}

	private func alertError() -> Binding<IdentifiableWrapper<Error, String>?> {
		.init(
			get: {
				if let error = loadingState.error {
					return .init(error, identity: error.reflectedString)
				}
				return nil
			},
			set: { if $0 == nil { loadingState = .none } }
		)
	}

	private func continueWithApple() {
		userInteractor
			.authenticateWithApple()
			.replaceError(with: (), if: { ($0.reason as? ASAuthorizationError)?.code == .canceled })
			.assignLoadingState(binding: $loadingState)
	}

	private func continueWithFacebook() {
		userInteractor
			.authenticateWithFacebook()
			.replaceError(with: (), if: { ($0.reason as? FacebookError) == .cancelled })
			.assignLoadingState(binding: $loadingState)
	}
}

struct SignInBlockView_Previews: PreviewProvider {
	static var previews: some View {
		SignInPlaceholderView(action: .savePlace, isPresentingOnboarding: .constant(false))
			.injectPreviewsEnvironment()
	}
}
