//
//  ErrorTryAgainView.swift
//  GoHere
//
//  Created by jorisfavier on 20/06/2022.
//

import SwiftUI

struct ErrorTryAgainView: View {
	
	var tryAgainCallback: (() -> ())? = nil
	
    var body: some View {
		VStack {
			Image("no-network")
				.resizable()
				.aspectRatio(contentMode: .fit)
				.frame(width: 56, height: 56)
				.foregroundColor(.gh(.labelTertiary))
			Text("generic_error")
				.font(.gh(.title2))
				.foregroundColor(.gh(.labelPrimary))
			if let cb = tryAgainCallback {
				Button("try_again", action: cb)
					.quaternaryButtonStyle()
			}
		}.padding()
    }
}

struct ErrorTryAgainView_Previews: PreviewProvider {
    static var previews: some View {
		ErrorTryAgainView(tryAgainCallback: {})
    }
}
