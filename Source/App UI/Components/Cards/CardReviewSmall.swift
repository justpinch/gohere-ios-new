import SwiftUI

struct CardReviewSmall: View {
	let photo: ImagePath?
	let name: String
	let category: String
	let categoryIcon: ImagePath
	let city: String
	let rating: Int

	private let isPlaceholder: Bool
	@State private var rowHeight: CGFloat = 0

	init(review: Review) {
		self.photo = review.place.photo
		self.name = review.place.name
		self.category = review.place.category.localizedName
		self.categoryIcon = review.place.category.icon
		self.city = review.place.city
		self.rating = review.rating
		self.isPlaceholder = false
	}

	fileprivate init(photo: ImagePath? = nil, name: String, category: String, categoryIcon: ImagePath, city: String, isPlaceholder: Bool, rating: Int) {
		self.photo = photo
		self.name = name
		self.category = category
		self.categoryIcon = categoryIcon
		self.city = city
		self.rating = rating
		self.isPlaceholder = isPlaceholder
	}

	static func loadingPlaceholder() -> some View {
		CardReviewSmall(photo: nil, name: "***********", category: "***********", categoryIcon: .init(path: ""), city: "********", isPlaceholder: true, rating: 0)
			.redacted(reason: .placeholder)
	}

	var body: some View {
		CardPlaceContainerSmall(photo: photo,
		                        name: name,
		                        category: category,
		                        categoryIcon: categoryIcon,
		                        city: city) {
			RatingView(rating: rating)
				.frame(height: 10)
				.fixedSize()
		}
	}
}

struct CardReviewSmall_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			Group {
				CardReviewSmall(review: .fixture1)
				CardReviewSmall(review: .fixture2)
				CardReviewSmall.loadingPlaceholder()
				CardReviewSmall(review: .fixture1)
					.preferredColorScheme(.dark)
			}
			.padding()
			.previewLayout(.sizeThatFits)

			CardReviewSmall(review: .fixture2)
				.previewLayout(.fixed(width: 240, height: 90))
		}
	}
}
