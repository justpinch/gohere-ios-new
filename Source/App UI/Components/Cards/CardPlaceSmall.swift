import SwiftUI

struct CardPlaceSmall: View {
	let photo: ImagePath?
	let name: String
	let category: String
	let categoryIcon: ImagePath
	let city: String

	private let isPlaceholder: Bool
	@State private var rowHeight: CGFloat = 0

	init(place: Place) {
		self.photo = place.photo
		self.name = place.name
		self.category = place.category.localizedName
		self.categoryIcon = place.category.icon
		self.city = place.city
		self.isPlaceholder = false
	}

	fileprivate init(photo: ImagePath? = nil, name: String, category: String, categoryIcon: ImagePath, city: String, isPlaceholder: Bool) {
		self.photo = photo
		self.name = name
		self.category = category
		self.categoryIcon = categoryIcon
		self.city = city
		self.isPlaceholder = isPlaceholder
	}

	static func loadingPlaceholder() -> some View {
		CardPlaceSmall(name: "********************", category: "********", categoryIcon: .init(path: ""), city: "*********", isPlaceholder: true)
			.redacted(reason: .placeholder)
	}

	var body: some View {
		CardPlaceContainerSmall(photo: photo,
		                        name: name,
		                        category: category,
		                        categoryIcon: categoryIcon,
		                        city: city)
	}
}

struct CardPlaceSmall_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			Group {
				CardPlaceSmall(place: .fixture1)
				CardPlaceSmall.loadingPlaceholder()
			}
			.previewLayout(.fixed(width: 380, height: 160))

			CardPlaceSmall(place: .fixture2)
				.previewLayout(.fixed(width: 240, height: 70))
		}
	}
}
