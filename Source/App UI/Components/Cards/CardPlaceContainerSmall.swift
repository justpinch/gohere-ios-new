import SwiftUI

struct CardPlaceContainerSmall<Content: View>: View {
	let photo: ImagePath?
	let name: String
	let category: String
	let categoryIcon: ImagePath
	let city: String
	let content: () -> Content

	let isPlaceholder: Bool

	@State private var rowHeight: CGFloat = 76
	private let minRowHeight: CGFloat = 76

	@Environment(\.colorScheme) private var colorScheme

	init(photo: ImagePath?, name: String, category: String, categoryIcon: ImagePath, city: String, isPlaceholder: Bool = false, @ViewBuilder content: @escaping () -> Content) {
		self.photo = photo
		self.name = name
		self.category = category
		self.categoryIcon = categoryIcon
		self.city = city
		self.content = content
		self.isPlaceholder = isPlaceholder
	}

	var body: some View {
		HStack(spacing: 0) {
			if let image = photo {
				RemoteImage(path: image, size: .screenWidth)
					.placeholder { Color.gh(.backgroundTertiary) }
					.aspectRatio(contentMode: .fill)
					.frame(width: rowHeight, height: rowHeight)
					.clipped()
			} else {
				ZStack {
					Color.gh(.backgroundTertiary)
					Image("no-image")
						.foregroundColor(.gh(.labelQuaternary))
				}
				.frame(width: rowHeight, height: rowHeight)
			}

			VStack(alignment: .leading, spacing: 6) {
				Text(name)
					.lineLimit(1)
					.font(.gh(.subhead))
					.foregroundColor(.gh(.labelPrimary))

				HStack {
					PlaceCategoryImage(icon: categoryIcon)
						.frame(width: 16, height: 16)

					Text(category)
						.lineLimit(1)
						.font(.gh(.caption1))
						.foregroundColor(.gh(.labelPrimary))

					Text(city)
						.lineLimit(1)
						.font(.gh(.caption1))
						.foregroundColor(.gh(.labelTertiary))
				}

				// Extra content
				content()
			}
			.padding([.leading, .trailing])
			.padding([.top, .bottom], 10)
			.background(GeometryReader { proxy in
				Color.clear.preference(
					key: MaxValuePreferenceKey.self,
					value: proxy.size.height
				)
			})
			.onPreferenceChange(MaxValuePreferenceKey.self) { rowHeight = max($0, minRowHeight) }

			Spacer(minLength: 0)
		}
		.background(Color.gh(.backgroundPrimary))
		.cornerRadius(8)
		.elevationLevel2()
		.redacted(reason: isPlaceholder ? .placeholder : [])
	}
}

extension CardPlaceContainerSmall where Content == EmptyView {
	init(photo: ImagePath?, name: String, category: String, categoryIcon: ImagePath, city: String, isPlaceholder: Bool = false) {
		self.init(photo: photo, name: name, category: category, categoryIcon: categoryIcon, city: city, isPlaceholder: isPlaceholder) { EmptyView() }
	}
}

struct CardPlaceContainerSmall_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			Group {
				CardPlaceContainerSmall(photo: Place.fixture1.photo,
				                        name: Place.fixture1.name,
				                        category: Place.fixture1.category.localizedName,
				                        categoryIcon: Place.fixture1.category.icon,
				                        city: Place.fixture1.city) {
					Text("EXTRA CONTENT")
				}

				CardPlaceContainerSmall(photo: nil,
				                        name: "Van Gogh Museum",
				                        category: "Museum",
				                        categoryIcon: .fixture(.category1),
				                        city: "Amsterdam",
				                        isPlaceholder: true) {
					Text("EXTRA CONTENT")
				}
			}
			.previewLayout(.fixed(width: 380, height: 160))

			CardPlaceContainerSmall(photo: nil,
			                        name: "Barcelona Museum of contemporary arts",
			                        category: "Museum",
			                        categoryIcon: .fixture(.category2),
			                        city: "Barcelona City")
				.previewLayout(.fixed(width: 240, height: 70))
		}
		.padding()
	}
}
