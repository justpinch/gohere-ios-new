import SwiftUI

struct RatingInputView: View {
	@Binding var rating: Int?
	@Environment(\.isEnabled) var isEnabled
	var foregroundColor = Color.gh(.rating)

	var body: some View {
		HStack {
			ForEach(1 ..< 6) { position in
				Button(action: { rating = position * 2 }, label: {
					Image(star(at: position).rawValue)
						.resizable()
						.aspectRatio(contentMode: .fit)
				})

				if position < 5 {
					Spacer(minLength: 20)
				}
			}
		}
		.foregroundColor(foregroundColor.opacity(isEnabled ? 1 : 0.25))
	}

	private func star(at position: Int) -> RatingView.Star {
		guard let r = rating else {
			return .outline
		}

		let rating = r / 2
		return rating < position ? .outline : .full
	}
}

struct RatingInputSelectionView: View {
	let onRatingSelected: (Int) -> Void
	var foregroundColor: Color = .gh(.labelQuaternary)
	@State private var rating: Int?

	var body: some View {
		RatingInputView(rating: $rating, foregroundColor: foregroundColor)
			.onChange(of: rating, perform: { value in
				if let value = value {
					onRatingSelected(value)
					DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
						// revert rating after briefly showing it on display
						rating = nil
					}
				}
			})
	}
}

struct RatingInputView_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			RatingInputView(rating: .constant(4))
				.padding()

			RatingInputView(rating: .constant(3))
				.previewDarkTheme()
		}
		.previewLayout(.sizeThatFits)

		Group {
			RatingInputView(rating: .constant(4))
				.padding()

			RatingInputView(rating: .constant(3))
				.previewDarkTheme()
		}
		.previewLayout(.sizeThatFits)
		.disabled(true)
	}
}
