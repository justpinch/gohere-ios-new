import SwiftUI

private let digitCount: Int = 6

struct PinCodeView: View {
	@Binding var code: String
	let onCommit: () -> Void

	@State var isActive: Bool = true

	var activeDigitIndex: Int? {
		isActive ? code.count : nil
	}

	var body: some View {
		ZStack {
			HiddenTextView(isActive: $isActive, code: $code)
				.frame(width: 0, height: 0)

			HStack {
				ForEach(0 ..< digitCount) { index in
					PinDigitView(digit: digit(at: index), isActive: activeDigitIndex == index)
				}
			}
			.contentShape(RoundedRectangle(cornerRadius: 8).inset(by: -8))
			.contextMenu {
				if UIPasteboard.general.hasStrings {
					Button(
						action: {
							if let string = UIPasteboard.general.string, isValid(code: string) {
								code = string
							}
						},
						label: {
							Label("paste_from_clipboard", systemImage: "doc.on.clipboard")
						}
					)
				}
			}
			.onTapGesture(perform: beginEditing)
		}
		.onChange(of: code, perform: { value in
			if value.count == digitCount {
				onCommit()
			}
		})
	}

	private func beginEditing() {
		if code.count == digitCount {
			code = ""
		}

		isActive = true
	}

	private func digit(at index: Int) -> String {
		guard index < code.count else { return "" }
		return String(code[code.index(code.startIndex, offsetBy: index)])
	}
}

private struct PinDigitView: View {
	let digit: String
	let isActive: Bool

	@Environment(\.isEnabled) var isEnabled

	var body: some View {
		Text(digit)
			.font(.gh(.title2))
			.frame(width: Theme.Font.title2.info().size / 1.8, height: Theme.Font.title2.info().size * 1.2)
			.padding()
			.foregroundColor(.gh(isEnabled ? .labelPrimary : .labelTertiary))
			.background(Color.gh(.backgroundTertiary))
			.clipShape(RoundedRectangle(cornerRadius: 8))
			.overlay(borderOverlay())
	}

	@ViewBuilder
	private func borderOverlay() -> some View {
		if isEnabled {
			RoundedRectangle(cornerRadius: 8)
				.strokeBorder()
				.foregroundColor(isActive ? .red : .gh(.labelQuaternary))
		} else {
			EmptyView()
		}
	}
}

struct HiddenTextView: UIViewRepresentable {
	@Binding var isActive: Bool
	@Binding var code: String

	func makeUIView(context: Context) -> UITextField {
		let textField = UITextField()
		textField.delegate = context.coordinator
		textField.textContentType = .oneTimeCode
		textField.keyboardType = .numberPad
		textField.isHidden = true
		return textField
	}

	func updateUIView(_ textField: UITextField, context _: Context) {
		textField.text = code

		if isActive {
			textField.becomeFirstResponder()
		} else {
			textField.resignFirstResponder()
		}
	}

	func makeCoordinator() -> Coordinator {
		Coordinator(for: self)
	}

	class Coordinator: NSObject, UITextFieldDelegate {
		private let context: HiddenTextView

		init(for context: HiddenTextView) {
			self.context = context
			super.init()
		}

		func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
			// if new string is a 6-char code, good chance it's being auto filled or pasted so replace the whole thing
			if string.count == digitCount, isValid(code: string) {
				context.code = string
				context.isActive = false
				return false
			}

			let currentText = textField.text ?? ""
			guard let stringRange = Range(range, in: currentText) else { return false }
			let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

			guard isValid(code: updatedText) else {
				return false
			}

			context.code = String(updatedText.prefix(digitCount))

			if updatedText.count == digitCount {
				// last char
				context.isActive = false
				return false
			}

			return true
		}

		func textFieldDidBeginEditing(_: UITextField) {}

		func textFieldDidEndEditing(_: UITextField) {}
	}
}

private func isValid(code: String) -> Bool {
	let disallowedCharacters = CharacterSet(charactersIn: "0123456789").inverted
	return code.rangeOfCharacter(from: disallowedCharacters) == nil
}

struct PinCodeView_Previews: PreviewProvider {
	struct PinCodeTest: View {
		@State var code: String = "123"
		var body: some View {
			VStack {
				Text("Code: \(code)")
				PinCodeView(code: $code, onCommit: {})
			}
		}
	}

	static var previews: some View {
		PinCodeTest()
	}
}
