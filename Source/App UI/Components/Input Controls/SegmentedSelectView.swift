import SwiftUI

struct SegmentedSelectView<Item>: View where Item: Equatable {
	@State private var frames: [CGRect]
	private var selectedIndex: Int { items.firstIndex(of: selectedItem)! }

	let items: [Item]
	@Binding var selectedItem: Item
	var itemTitle: (Item) -> String
	var itemBadge: ((Item) -> Int)?

	init(items: [Item], selected: Binding<Item>, itemTitle: @escaping (Item) -> String, itemBadge: ((Item) -> Int)? = nil) {
		self.items = items
		self._selectedItem = selected
		self.itemTitle = itemTitle
		self.itemBadge = itemBadge
		self._frames = State(initialValue: [CGRect](repeating: .zero, count: items.count))
	}

	var body: some View {
		HStack(spacing: 10) {
			ForEach(items.indices, id: \.self) { index in
				Button(action: { withAnimation { selectedItem = items[index] } }) {
					ZStack(alignment: .topTrailing) {
						Text(itemTitle(items[index]))
							.minimumScaleFactor(0.5)
							.scaledToFill()
							.padding()

						if let badge = itemBadge?(items[index]) {
							NotificationBadge(value: badge)
								.offset(x: -5, y: 5)
						}
					}
				}
				.frame(maxWidth: .infinity)
				.foregroundColor(.gh(selectedIndex == index ? .labelPrimary : .labelTertiary))
				.font(.gh(.subhead))
				.background(
					GeometryReader { geo in
						Color.clear.preference(
							key: SegmentFramePreferenceKey.self,
							value: SegmentFramePreferenceKey.Segments(frames: [.init(index: index, frame: geo.frame(in: .global))])
						)
					}
				)
			}
		}
		.background(
			Rectangle()
				.fill(Color.gh(.tintPrimary))
				.frame(width: self.frames[self.selectedIndex].width,
				       height: 3, alignment: .bottomLeading)
				.offset(x: self.frames[self.selectedIndex].minX - self.frames[0].minX),
			alignment: .bottomLeading
		)
		.onPreferenceChange(SegmentFramePreferenceKey.self, perform: { value in
			value.frames.forEach { segment in
				setFrame(index: segment.index, frame: segment.frame)
			}
		})
	}

	private func setFrame(index: Int, frame: CGRect) {
		self.frames[index] = frame
	}
}

extension SegmentedSelectView where Item: LocalizedStringConvertible {
	init(items: [Item], selected: Binding<Item>, itemBadge: ((Item) -> Int)? = nil) {
		self.init(items: items, selected: selected, itemTitle: { $0.localizableString }, itemBadge: itemBadge)
	}
}

private struct SegmentFramePreferenceKey: PreferenceKey {
	struct Segments: Equatable {
		struct SegmentFrame: Equatable {
			let index: Int
			let frame: CGRect
		}

		var frames: [SegmentFrame] = []
	}

	static let defaultValue = Segments()

	static func reduce(value: inout Segments, nextValue: () -> Segments) {
		value.frames.append(contentsOf: nextValue().frames)
	}
}

struct SegmentedSelectView_Previews: PreviewProvider {
	enum Test: String, Equatable, CaseIterable, LocalizedStringConvertible {
		case phone
		case email
		case facebook

		var localizableString: String { self.rawValue.capitalized }
	}

	static var selected = Test.phone

	static var previews: some View {
		VStack(spacing: 50) {
			SegmentedSelectView(items: Test.allCases, selected: .init(get: { selected }, set: { selected = $0 }))
			SegmentedSelectView(items: Test.allCases, selected: .init(get: { selected }, set: { selected = $0 }), itemBadge: {
				switch $0 {
					case .phone:
						return 123
					case .email:
						return 0
					case .facebook:
						return 3
				}
			})
			Spacer()
		}
	}
}
