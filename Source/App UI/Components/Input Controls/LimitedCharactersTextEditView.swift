import SwiftUI

struct LimitedCharactersTextEditView: View {
	@Binding var text: String
	@State var maxCharacters: Int = 300


	private var limitedText: Binding<String> {
		.init(get: { text },
		      set: {
		      	if $0.count <= maxCharacters {
		      		text = $0
		      	}
		      })
	}

	var body: some View {
		VStack(spacing: 0) {
			TextEditor(text: limitedText)
				.font(.gh(.body))

			HStack {
				Spacer()
				Text("\(text.count)/\(maxCharacters)")
					.font(.gh(.caption1))
					.foregroundColor(text.count < maxCharacters ? .gh(.labelPrimary) : .gh(.utilityError))
			}
		}
		.padding(10)
		.background(Color.gh(.backgroundTertiary))
		.cornerRadius(8)
	}
}

struct ReviewTextEditView_Previews: PreviewProvider {
	struct Container: View {
		@State var text: String = "Review text"

		init() {
			Theme.configureAppearance()
		}

		var body: some View {
			LimitedCharactersTextEditView(text: $text)
				.frame(width: 375, height: 150)
				.padding()
		}
	}

	static var previews: some View {
		Container()
			.previewLayout(.sizeThatFits)
	}
}
