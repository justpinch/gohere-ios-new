import SwiftUI

struct SearchTextField: View {
	let placeholder: LocalizedStringKey
	@Binding var text: String

	var onEditingChanged: (Bool) -> Void = { _ in }
	var onCommit: () -> Void = {}

	var body: some View {
		HStack {
			Image(systemName: "magnifyingglass")
			TextField(placeholder, text: $text, onEditingChanged: onEditingChanged, onCommit: onCommit)
				.font(text.isEmpty ? .gh(.footnote) : .gh(.subhead))

			if !text.isEmpty {
				Button(action: {
					self.text = ""
				}) {
					Image(systemName: "multiply.circle.fill")
						.foregroundColor(.secondary)
				}
			}
		}
		.padding()
		.foregroundColor(text.isEmpty ? .gh(.labelSecondary) : .gh(.labelPrimary))
		.background(Color.gh(.backgroundTertiary))
		.cornerRadius(8)
	}
}

struct ClearButton: ViewModifier {
	@Binding var text: String

	public func body(content: Content) -> some View {
		HStack {
			content
			Button(action: {
				self.text = ""
			}) {
				Image(systemName: "multiply.circle.fill")
					.foregroundColor(.secondary)
			}
		}
	}
}

struct SearchTextField_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			SearchTextField(placeholder: "Placeholder", text: .constant(""))
				.previewSupportedLocales()

			SearchTextField(placeholder: "Placeholder", text: .constant(""))
				.previewDarkTheme()

			SearchTextField(placeholder: "Placeholder", text: .constant("Query"))

			SearchTextField(placeholder: "Placeholder", text: .constant("Query"))
				.previewDarkTheme()
		}
		.padding(40)
		.frame(width: 400)
		.previewLayout(.sizeThatFits)
	}
}
