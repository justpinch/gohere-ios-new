import SwiftUI

struct GoHereTextFieldStyle: TextFieldStyle {
	struct StyleView: View {
		@Environment(\.isEnabled) var isEnabled
		let configuration: TextField<_Label>

		var body: some View {
			configuration
				.font(.gh(isEnabled ? .body : .callout))
				.foregroundColor(.gh(.labelPrimary))
				.padding()
				.background(Color.gh(isEnabled ? .backgroundTertiary : .backgroundSecondary))
				.cornerRadius(8)
		}
	}

	func _body(configuration: TextField<_Label>) -> some View {
		StyleView(configuration: configuration)
	}
}

struct GoHerePasswordTextFieldStyle: TextFieldStyle {
	@Binding var isShowingPassword: Bool

	struct StyleView: View {
		@Environment(\.isEnabled) var isEnabled
		let configuration: TextField<_Label>
		@Binding var isShowingPassword: Bool

		var body: some View {
			HStack {
				configuration

				Button(action: {
					isShowingPassword.toggle()
				}, label: {
					Image(systemName: isShowingPassword ? "eye.slash" : "eye")
						.font(.footnote)
				})
			}
			.opacity(isEnabled ? 1 : 0.5)
			.font(.gh(isEnabled ? .body : .callout))
			.foregroundColor(.gh(.labelPrimary))
			.padding()
			.background(Color.gh(isEnabled ? .backgroundTertiary : .backgroundSecondary))
			.cornerRadius(8)
		}
	}

	func _body(configuration: TextField<_Label>) -> some View {
		StyleView(configuration: configuration, isShowingPassword: $isShowingPassword)
	}
}

struct TextFieldStyles_Previews: PreviewProvider {
	static var previews: some View {
		VStack {
			TextField("Placeholder", text: .constant(""))
				.textFieldStyle(GoHereTextFieldStyle())

			TextField("Placeholder", text: .constant("Filled text"))
				.textFieldStyle(GoHereTextFieldStyle())

			TextField("Placeholder", text: .constant("Filled text"))
				.textFieldStyle(GoHereTextFieldStyle())
				.disabled(true)

			TextField("Placeholder", text: .constant("Filled text"))
				.textFieldStyle(GoHereTextFieldStyle())
				.disabled(true)

			TextField("Placeholder", text: .constant(""))
				.textFieldStyle(GoHerePasswordTextFieldStyle(isShowingPassword: .constant(true)))

			TextField("Placeholder", text: .constant("Filled text"))
				.textFieldStyle(GoHerePasswordTextFieldStyle(isShowingPassword: .constant(false)))

			TextField("Placeholder", text: .constant("Filled text"))
				.textFieldStyle(GoHerePasswordTextFieldStyle(isShowingPassword: .constant(true)))
				.disabled(true)

			TextField("Placeholder", text: .constant("Filled text"))
				.textFieldStyle(GoHerePasswordTextFieldStyle(isShowingPassword: .constant(false)))
				.disabled(true)
		}
		.previewLayout(.sizeThatFits)
	}
}
