import SwiftUI

struct PasswordTextField: View {
	@Binding var text: String
	var onCommit: () -> Void = {}

	@State private var isShowingPassword = false

	var body: some View {
		if isShowingPassword {
			TextField(LocalizedStringKey("password"), text: $text, onCommit: onCommit)
				.textFieldStyle(GoHerePasswordTextFieldStyle(isShowingPassword: $isShowingPassword))
				.textContentType(.password)
		} else {
			SecureField("password", text: $text, onCommit: onCommit)
				.textFieldStyle(GoHerePasswordTextFieldStyle(isShowingPassword: $isShowingPassword))
				.textContentType(.password)
		}
	}
}

struct PasswordTextField_Previews: PreviewProvider {
	struct TestView: View {
		@State var text: String = ""

		var body: some View {
			Group {
				PasswordTextField(text: $text)
					.previewSupportedLocales()

				PasswordTextField(text: $text)
					.previewDarkTheme()

				PasswordTextField(text: $text)

				PasswordTextField(text: $text)
					.previewDarkTheme()
			}
		}
	}

	static var previews: some View {
		TestView()
			.padding(40)
			.frame(width: 400)
			.previewLayout(.sizeThatFits)
	}
}
