import SwiftUI

struct PullToRefresh: View {
	let isLoading: Bool
	let coordinateSpace: String
	let action: () -> Void

	@State private var isRefreshing: Bool = false
	@State private var lastRefresh = Date()

	private let triggerOffset: CGFloat = 80
	private let progressHeight: CGFloat = 28
	private let progressPadding: CGFloat = 10
	private let circleVisibilityOffset: CGFloat = 2

	private let refreshDoSTimeout: TimeInterval = 2

	var body: some View {
		GeometryReader(content: { geometry in
			if isRefreshing || offset(from: geometry) > circleVisibilityOffset {
				CircleProgress(trim: circleTrim(for: geometry), isRefreshing: isRefreshing)
					.frame(width: progressHeight, height: progressHeight)
					.padding(progressPadding)
					.frame(maxWidth: .infinity, alignment: .center)
			}
		})
		.frame(height: progressHeight + 2 * progressPadding)
		.offset(x: 0, y: isRefreshing ? 0 : -progressHeight - 2 * progressPadding)
		.padding(.bottom, isRefreshing ? 0 : -progressHeight - 2 * progressPadding + 1) // this 1 is needed otherwise view gets completely hidden
		.animation(.default)
		.onChange(of: isLoading, perform: { value in
			if !value { // we only want to show refresh if started from this component, not outside
				isRefreshing = false
			}
		})
	}

	func startRefresh() {
		DispatchQueue.main.async {
			lastRefresh = Date()
			isRefreshing = true
			action()
			UINotificationFeedbackGenerator().notificationOccurred(.success)
		}
	}

	func circleTrim(for proxy: GeometryProxy) -> CGFloat {
		let value = min(1, offset(from: proxy) / triggerOffset)

		if !isRefreshing, value == 1, abs(lastRefresh.timeIntervalSinceNow) > refreshDoSTimeout {
			startRefresh()
		}

		return value
	}

	func offset(from proxy: GeometryProxy) -> CGFloat {
		proxy.frame(in: .named(coordinateSpace)).minY
	}
}

private struct CircleProgress: View {
	let trim: CGFloat
	let isRefreshing: Bool
	@State var isRotating = false

	var body: some View {
		Circle()
			.trim(from: 0, to: isRotating ? 0.8 : trim)
			.stroke(
				LinearGradient(gradient: Gradient(colors: [.gh(.backgroundSecondary), .gh(.tintPrimary)]), startPoint: .topTrailing, endPoint: .bottomLeading),
				style: StrokeStyle(lineWidth: 4, lineCap: .round)
			)
			.rotationEffect(isRotating ? .init(degrees: 360) : .zero)
			.animation(isRotating ? .linear.repeatForever(autoreverses: false) : nil)
			.onChange(of: isRefreshing, perform: { value in
				isRotating = value
			})
	}
}

struct PullToRefresh_Previews: PreviewProvider {
	struct TestView: View {
		@State var isLoading: Bool = false

		var body: some View {
			ScrollView {
				PullToRefresh(isLoading: isLoading, coordinateSpace: "ptr") {
					isLoading = true
					// simulate work
					DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
						isLoading = false
					}
				}

				VStack {
					Circle()
						.frame(width: 100, height: 100, alignment: .center)
					Rectangle()
						.frame(width: 100, height: 100, alignment: .center)
				}
			}
			.coordinateSpace(name: "ptr")
		}
	}

	static var previews: some View {
		NavigationView {
			TestView()
				.navigationBarTitleDisplayMode(.inline)
		}
	}
}
