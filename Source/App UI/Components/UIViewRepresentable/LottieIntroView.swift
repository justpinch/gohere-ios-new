import Lottie
import SwiftUI

struct LottieIntroView: UIViewRepresentable {
	struct AnimationSegment: Equatable {
		let fromMarker: String
		let toMarker: String
		let text: String
	}

	let name: String
	let animationSegments: [AnimationSegment]
	let segmentText: Binding<String>

	func makeUIView(context _: UIViewRepresentableContext<LottieIntroView>) -> UIView {
		let animationView = AnimationView()
		animationView.animation = Animation.named(name)
		animationView.contentMode = .scaleAspectFit

		DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
			playAnimation(view: animationView)
		}
		return animationView
	}

	func updateUIView(_ uiView: UIView, context _: UIViewRepresentableContext<LottieIntroView>) {
		uiView.setContentHuggingPriority(.defaultHigh, for: .vertical)
		uiView.setContentHuggingPriority(.defaultHigh, for: .horizontal)
	}

	private func playAnimation(view: AnimationView, segmentIndex: Int = 0) {
		let segment = animationSegments[segmentIndex]
		let nextIndex: Int = segmentIndex.advanced(by: 1) % animationSegments.count

		view.play(fromMarker: segment.fromMarker, toMarker: segment.toMarker, loopMode: .playOnce) { _ in
			playAnimation(view: view, segmentIndex: nextIndex)
		}

		segmentText.wrappedValue = segment.text
	}
}

struct LottieIntroView_Previews: PreviewProvider {
	struct TestView: View {
		@State var text: String = ""

		var body: some View {
			VStack {
				Text(text)
				LottieIntroView(name: "onboarding-animation", animationSegments: [
					LottieIntroView.AnimationSegment(fromMarker: "StartCopy-1", toMarker: "EndCopy-1", text: "onboarding_animation_copy1".localizedFromTarget()),
					LottieIntroView.AnimationSegment(fromMarker: "StartCopy-2", toMarker: "EndCopy-2", text: "onboarding_animation_copy2".localizedFromTarget()),
					LottieIntroView.AnimationSegment(fromMarker: "StartCopy-3", toMarker: "EndCopy-3", text: "onboarding_animation_copy3".localizedFromTarget()),
				], segmentText: $text)
					.border(Color.black)
			}
		}
	}

	static var previews: some View {
		TestView()
	}
}
