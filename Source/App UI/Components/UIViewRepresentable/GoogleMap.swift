import Combine
import GoogleMaps
import MapKit
import SwiftUI
import UIKit

struct GMap: UIViewRepresentable {
	let markers: [GMSMarker]
	let onGooglePlaceSelected: (String) -> Void

	@Binding var selectedMarker: SelectedMapMarker?
	@Binding var region: MKCoordinateRegion
	@Environment(\.colorScheme) var colorScheme: ColorScheme

	@State private var lightStyle: GMSMapStyle = {
		let styleURL = Bundle.main.url(forResource: "mapstyle-light-override", withExtension: "json") ?? Bundle.main.url(forResource: "mapstyle-light", withExtension: "json")!
		return try! GMSMapStyle(contentsOfFileURL: styleURL)
	}()

	@State private var darkStyle: GMSMapStyle = {
		let styleURL = Bundle.main.url(forResource: "mapstyle-dark-override", withExtension: "json") ?? Bundle.main.url(forResource: "mapstyle-dark", withExtension: "json")!
		return try! GMSMapStyle(contentsOfFileURL: styleURL)
	}()

	private var mapStyle: GMSMapStyle {
		switch colorScheme {
			case .light:
				return lightStyle
			case .dark:
				return darkStyle
			@unknown default:
				return lightStyle
		}
	}

	// MARK: - UIViewRepresentable

	func makeCoordinator() -> GMap.Coordinator {
		Coordinator(for: self)
	}

	func makeUIView(context: UIViewRepresentableContext<GMap>) -> GMSMapView {
		GMSServices.initializeSDK()

		let mapView = GMSMapView()
		mapView.delegate = context.coordinator

		mapView.mapStyle = mapStyle
		mapView.isIndoorEnabled = false
		mapView.accessibilityElementsHidden = false
		mapView.isMyLocationEnabled = true

		// WORKAROUND: deselect previously selected marker if it was an external place marker
		context.coordinator.selectedMarkerChange = mapView.observe(\.selectedMarker, options: .old) { _, change in
			if change.oldValue is MapLookupMarker {
				change.oldValue??.map = nil
			}
		}

		return mapView
	}

	func updateUIView(_ mapView: GMSMapView, context _: UIViewRepresentableContext<GMap>) {
		let currentRegion = mapView.visibleRegion()

		if region != currentRegion {
			let cameraUpdate = GMSCameraUpdate.fit(region.toGoogleCoordinateBounds())
			mapView.animate(with: cameraUpdate)
		}

		if mapStyle != mapView.mapStyle {
			mapView.mapStyle = mapStyle
		}

		// set mapview to each new marker
		for (index, marker) in markers.filter({ $0.map != mapView }).enumerated() {
			marker.map = mapView
			//In case of 2 markers being close to each other the last one will have higher priority
			marker.zIndex = Int32(index)
		}

		// Update appearance for each marker
		markers.forEach { ($0 as? GoHereMarker)?.isDarkMode = colorScheme == .dark }

		// Update selected marker
		let marker = selectedMarker?.marker()
		if marker != mapView.selectedMarker {
			marker?.map = mapView
			mapView.selectedMarker = marker
		}
	}

	// MARK: - Interaction and delegate implementation

	class Coordinator: NSObject, GMSMapViewDelegate {
		private let context: GMap
		private var shouldUpdateRegionChange = false

		fileprivate var selectedMarkerChange: NSKeyValueObservation?

		init(for context: GMap) {
			self.context = context
			super.init()
		}

		func mapView(_: GMSMapView, willMove gesture: Bool) {
			shouldUpdateRegionChange = gesture
		}

		func mapView(_: GMSMapView, idleAt _: GMSCameraPosition) {
			shouldUpdateRegionChange = false
		}

		func mapView(_ mapView: GMSMapView, didChange _: GMSCameraPosition) {
			guard shouldUpdateRegionChange else { return }

			let region = mapView.visibleRegion()
			context.region = region
		}

		func mapView(_: GMSMapView, didTap marker: GMSMarker) -> Bool {
			guard let selectedMarker = SelectedMapMarker(marker: marker) else {
				return false
			}

			context.selectedMarker = selectedMarker
			if !context.region.contains(selectedMarker.position) || context.region.spanInMeters().latitudeMeters > 5000 {
				context.region = MKCoordinateRegion(center: selectedMarker.position, span: ExploreView.Const.ZoomSpan.level1)
			} else {
				//Putting the marker at the center prevents the card to appear on top of it
				context.region = MKCoordinateRegion(center: selectedMarker.position, span: context.region.span)
			}

			return true
		}

		func mapView(_: GMSMapView, didTapAt _: CLLocationCoordinate2D) {
			context.selectedMarker = nil
		}

		func mapView(_: GMSMapView, didTapPOIWithPlaceID placeID: String, name _: String, location _: CLLocationCoordinate2D) {
			context.onGooglePlaceSelected(placeID)
		}
	}
}

private extension GMSMapView {
	func visibleRegion() -> MKCoordinateRegion {
		let visibleRegion = projection.visibleRegion()
		let bounds = GMSCoordinateBounds(region: visibleRegion)

		let latitudeDelta = bounds.northEast.latitude - bounds.southWest.latitude

		let centre: CLLocationCoordinate2D
		let longitudeDelta: CLLocationDegrees

		if bounds.northEast.longitude >= bounds.southWest.longitude {
			// Standard case
			centre = CLLocationCoordinate2DMake(
				(bounds.southWest.latitude + bounds.northEast.latitude) / 2,
				(bounds.southWest.longitude + bounds.northEast.longitude) / 2
			)
			longitudeDelta = bounds.northEast.longitude - bounds.southWest.longitude
		} else {
			// Region spans the international dateline
			centre = CLLocationCoordinate2DMake(
				(bounds.southWest.latitude + bounds.northEast.latitude) / 2,
				(bounds.southWest.longitude + bounds.northEast.longitude + 360) / 2
			)
			longitudeDelta = bounds.northEast.longitude + 360 - bounds.southWest.longitude
		}

		let span = MKCoordinateSpan(latitudeDelta: latitudeDelta, longitudeDelta: longitudeDelta)
		return MKCoordinateRegion(center: centre, span: span)
	}
}

extension MKCoordinateRegion {
	func toGoogleCoordinateBounds() -> GMSCoordinateBounds {
		GMSCoordinateBounds(
			coordinate: .init(latitude: center.latitude - span.latitudeDelta / 2,
			                  longitude: center.longitude + span.longitudeDelta / 2),
			coordinate: .init(latitude: center.latitude + span.latitudeDelta / 2,
			                  longitude: center.longitude - span.longitudeDelta / 2)
		)
	}
}
