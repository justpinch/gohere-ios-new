import SwiftUI

struct ImagePicker: UIViewControllerRepresentable {
	@Environment(\.presentationMode) var presentationMode
	@Binding var image: UIImage?

	let allowsEditing: Bool
	let sourceType: UIImagePickerController.SourceType

	static var availableSourceTypes: [UIImagePickerController.SourceType] {
		[.camera, .photoLibrary].filter(UIImagePickerController.isSourceTypeAvailable)
	}

	func makeUIViewController(context: UIViewControllerRepresentableContext<ImagePicker>) -> UIImagePickerController {
		let picker = UIImagePickerController()
		picker.allowsEditing = allowsEditing
		picker.delegate = context.coordinator
		picker.sourceType = sourceType
		return picker
	}

	func updateUIViewController(_: UIImagePickerController, context _: UIViewControllerRepresentableContext<ImagePicker>) {}

	func makeCoordinator() -> Coordinator {
		Coordinator(self)
	}

	class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
		let parent: ImagePicker

		init(_ parent: ImagePicker) {
			self.parent = parent
		}

		func imagePickerController(_: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
			parent.image = (info[.editedImage] ?? info[.originalImage]) as? UIImage
			parent.presentationMode.wrappedValue.dismiss()
		}
	}
}

extension UIImagePickerController.SourceType: Identifiable {
	public var id: Int {
		self.rawValue
	}

	var localizedTitle: String {
		switch self {
			case .photoLibrary:
				return NSLocalizedString("use_existing_picture", comment: "")
			case .camera:
				return NSLocalizedString("take_picture", comment: "")
			case .savedPhotosAlbum:
				return NSLocalizedString("use_existing_picture", comment: "")
			@unknown default:
				return "\(self)"
		}
	}
}
