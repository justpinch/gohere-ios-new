import MessageUI
import SwiftUI
import UIKit

struct MailComposer: UIViewControllerRepresentable {
	@Environment(\.presentationMode) var presentation

	var configureComposer: ((MFMailComposeViewController) -> Void)?

	static var canSendEmail: Bool {
		MFMailComposeViewController.canSendMail()
	}

	class Coordinator: NSObject, MFMailComposeViewControllerDelegate {
		@Binding var presentation: PresentationMode

		init(presentation: Binding<PresentationMode>) {
			_presentation = presentation
		}

		func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith _: MFMailComposeResult, error: Error?) {
			defer { $presentation.wrappedValue.dismiss() }

			if let error = error {
				let alert = UIAlertController(title: "error", message: error.localizedDescription, preferredStyle: .alert)
				controller.present(alert, animated: true, completion: nil)
				return
			}
		}
	}

	func makeCoordinator() -> Coordinator {
		Coordinator(presentation: presentation)
	}

	func makeUIViewController(context: UIViewControllerRepresentableContext<MailComposer>) -> MFMailComposeViewController {
		let composeViewController = MFMailComposeViewController()
		configureComposer?(composeViewController)
		composeViewController.mailComposeDelegate = context.coordinator
		return composeViewController
	}

	func updateUIViewController(_: MFMailComposeViewController, context _: UIViewControllerRepresentableContext<MailComposer>) {}
}

struct MailComposer_Previews: PreviewProvider {
	static var previews: some View {
		MailComposer()
	}
}
