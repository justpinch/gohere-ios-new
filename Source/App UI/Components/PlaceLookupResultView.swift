import SwiftUI

struct PlaceLookupResultView: View {
	let title: String
	let subtitle: String?
	let icon: ImagePath?

	@State var isPlaceholder = false

	@Environment(\.colorScheme) private var colorScheme

	var body: some View {
		HStack(spacing: 6) {
			if let icon = icon {
				PlaceCategoryImage(icon: icon)
					.frame(width: 24, height: 24)
					.padding()
			} else {
				Image(systemName: "mappin.circle.fill")
					.foregroundColor(.gh(.labelPrimary))
					.frame(width: 24, height: 24)
					.padding()
			}

			redacted {
				VStack(alignment: .leading) {
					Text(title)
						.font(.gh(.subhead))
						.foregroundColor(.gh(.labelPrimary))

					if let subtitle = subtitle {
						Text(subtitle)
							.font(.gh(.caption1))
							.foregroundColor(.gh(.labelSecondary))
					}
				}
			}

			Spacer()
		}
		.padding([.top, .bottom], 6)
	}

	@ViewBuilder func redacted<Content: View>(@ViewBuilder content: () -> Content) -> some View {
		if isPlaceholder {
			content().redacted(reason: .placeholder)
		} else {
			content()
		}
	}

	static func loadingPlaceholder() -> PlaceLookupResultView {
		PlaceLookupResultView(title: "********", subtitle: "****************", icon: nil, isPlaceholder: true)
	}
}

struct PlaceLookupResultView_Previews: PreviewProvider {
	static var previews: some View {
		VStack {
			PlaceLookupResultView(title: "Cafe Bar Oslo", subtitle: "New York Dude", icon: .fixture(.category1))
			Divider()
			PlaceLookupResultView(title: "Loading...", subtitle: "Loading...", icon: nil, isPlaceholder: true)
			Divider()
			PlaceLookupResultView(title: "Otra Mas", subtitle: "Barcelona, Spain", icon: nil)

			Divider()

			Group {
				PlaceLookupResultView(title: "Cafe Bar Oslo", subtitle: "New York Dude", icon: .fixture(.category1))
				Divider()
				PlaceLookupResultView(title: "Loading...", subtitle: "Loading...", icon: nil, isPlaceholder: true)
				Divider()
				PlaceLookupResultView(title: "Otra Mas", subtitle: "Barcelona, Spain", icon: nil)
			}
			.background(Color(.systemBackground))
			.colorScheme(.dark)
		}
		.previewLayout(.sizeThatFits)
	}
}
