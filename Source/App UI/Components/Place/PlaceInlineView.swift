import SwiftUI

struct PlaceInlineView: View {
	let place: Place

	@Environment(\.colorScheme) private var colorScheme

	var body: some View {
		HStack {
			if let image = place.photo {
				RemoteImage(path: image, size: .screenWidth)
					.placeholder { Color.gh(.backgroundTertiary) }
					.frame(width: 80, height: 80)
					.clipped()
			} else {
				ZStack {
					Color.gh(.backgroundSecondary)
					Image("no-image")
						.foregroundColor(.gh(.labelQuaternary))
				}
				.frame(width: 80, height: 80)
			}

			HStack(alignment: .top) {
				VStack(alignment: .leading, spacing: 4) {
					Text(place.name)
						.lineLimit(2)
						.minimumScaleFactor(0.85)
						.font(.gh(.title3))
						.foregroundColor(.gh(.labelPrimary))
						.multilineTextAlignment(.leading)
						.fixedSize(horizontal: false, vertical: true)

					HStack {
						if let category = place.category {
							PlaceCategoryImage(icon: category.icon)
								.frame(width: 24, height: 24)
						}

						Text("\(place.city), \(place.country)")
							.font(.gh(.caption1))
							.foregroundColor(.gh(.labelSecondary))
					}
				}
				Spacer()

				SavePlaceButton(place: place)
					.offset(y: -8)
			}
			.padding(.leading, 8)
		}
	}
}

struct PlaceInlineView_Previews: PreviewProvider {
	static var placeNoPhoto: Place {
		Place(id: UUID().uuidString, idGoogle: UUID().uuidString, name: "A place without a photo", address: "Some address", city: "Niš", country: "Serbia", location: .init(latitude: 54, longitude: 4), category: .fixture1, website: nil, phone: "", affiliate: nil, photo: nil, info: nil, averageRating: 3, labels: nil, isFavorite: nil, lastReview: nil)
	}

	static var previews: some View {
		VStack {
			PlaceInlineView(place: .fixture1)
			Divider()
			PlaceInlineView(place: .fixture2)
			Divider()
			PlaceInlineView(place: placeNoPhoto)
		}
		.padding()
		.previewLayout(.sizeThatFits)
		.injectPreviewsEnvironment()
	}
}
