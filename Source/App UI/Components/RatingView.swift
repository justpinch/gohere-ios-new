import SwiftUI

struct RatingView: View {
	let rating: Int?
	var activeColor = Color.gh(.rating)
	var inactiveColor = Color(.systemGray5)

	enum Star: String {
		case outline = "rating-star-outline"
		case half = "rating-star-half"
		case full = "rating-star-full"
	}

	var body: some View {
		if Config.isRatingAllowed() {
			HStack(spacing: 0) {
				ForEach(1 ..< 6) { position in
					if let rating = rating {
						starView(at: position, rating: rating)
					} else {
						image(for: .outline, color: inactiveColor)
					}
					if position < 5 { Spacer(minLength: 4) }
				}
			}
			.unredacted()
		} else {
			EmptyView()
		}
	}

	@ViewBuilder
	private func starView(at position: Int, rating: Int) -> some View {
		let position = Double(position)
		let rating = Double(rating) / 2

		switch rating {
			case _ where rating < position:
				if rating >= position - 0.5 {
					ZStack {
						image(for: .full, color: inactiveColor)
						image(for: .half, color: activeColor)
					}
				} else {
					image(for: .full, color: inactiveColor)
				}
			default:
				image(for: .full, color: activeColor)
		}
	}

	@ViewBuilder
	private func image(for star: Star, color: Color) -> some View {
		Image(star.rawValue)
			.resizable()
			.aspectRatio(contentMode: .fit)
			.foregroundColor(color)
	}
}

struct RatingView_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			RatingView(rating: 5)
				.padding()
			RatingView(rating: nil)
				.padding()
			RatingView(rating: 8)
				.padding()
			RatingView(rating: 3)
				.padding()
			RatingView(rating: 7)
				.preferredColorScheme(.dark)
				.padding()
		}
		.previewLayout(.sizeThatFits)
	}
}
