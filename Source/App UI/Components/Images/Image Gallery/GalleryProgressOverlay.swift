import SwiftUI

struct GalleryProgressOverlay: View {
	let page: Int
	let total: Int

	var body: some View {
		Text("\(page) / \(total)")
			.font(.gh(.caption1))
			.foregroundColor(.white)
			.padding([.leading, .trailing], 16)
			.padding([.top, .bottom], 10)
			.background(
				Capsule(style: .circular)
					.foregroundColor(Color.black.opacity(0.35))
			)
	}
}

struct GalleryProgressOverlay_Previews: PreviewProvider {
	static var previews: some View {
		ZStack {
			Image("place0")
				.resizable()
				.frame(width: 260, height: 140)
			GalleryProgressOverlay(page: 3, total: 12).offset(x: 85, y: 35)
		}
		.previewLayout(.sizeThatFits)

		ZStack {
			Image("place4")
				.resizable()
				.frame(width: 260, height: 140)
			GalleryProgressOverlay(page: 3, total: 12).offset(x: 85, y: 35)
		}
		.previewLayout(.sizeThatFits)
	}
}
