import SwiftUI

struct ImageGallery: View {
	struct Image: Identifiable {
		var id: String {
			url.absoluteString
		}
		
		let url: URL
		var fallback: URL?
	}
	
	let images: [Image]
	var isFullScreen: Bool = false
	
	@State var selectedIndex: Int = 0
	@State var isShowingOverlay: Bool = true
	@StateObject var counter = CountDown()
	
	var imageSelected: (Image) -> Void = { _ in }
	var closeAction: () -> Void = {}
	
	var body: some View {
		GeometryReader(content: { geometry in
			if images.isEmpty {
				EmptyView()
			} else {
				ZStack(alignment: isFullScreen ? .top : .bottom) {
					if images.count > 1 {
						TabView(selection: $selectedIndex) {
							ForEach(0 ..< images.count, id: \.self) { idx in
								row(image: images[idx], geometry: geometry)
							}
						}
						.tabViewStyle(.page(indexDisplayMode: .never))
					} else {
						row(image: images.first!, geometry: geometry)
					}
					
					if isShowingOverlay {
						HStack {
							if isFullScreen {
								CloseButton(action: closeAction)
							}
							Spacer()
							if images.count > 1 {
								GalleryProgressOverlay(page: selectedIndex + 1, total: images.count)
							}
						}
						.padding()
					}
				}
			}
		})
		.background(Color.black)
		.onAppear(perform: showOverlay)
		.onTapGesture(perform: showOverlay)
		.edgesIgnoringSafeArea(isFullScreen ? .all : [])
	}
	
	private func showOverlay() {
		withAnimation {
			isShowingOverlay = true
		}
		
		guard isFullScreen else {
			return
		}
		
		counter.start(from: 3) {
			withAnimation {
				isShowingOverlay = false
			}
		}
	}
	
	private func row(image: Image, geometry: GeometryProxy) -> some View {
		ZStack {
			RemoteImage(url: image.url)
				.fallback(url: image.fallback)
				.aspectRatio(contentMode: isFullScreen ? .fit : .fill)
				.frame(width: geometry.size.width, height: geometry.size.height)
				.clipped()
		}
		.onTapGesture(perform: {
			imageSelected(image)
		})
	}
	
}

struct ImageGallery_Previews: PreviewProvider {
	static let images: [ImageGallery.Image] = (0 ..< 5).map { .init(url: Bundle.main.url(forResource: "place\($0)", withExtension: "jpg")!) }
	
	static var previews: some View {
		ImageGallery(images: images)
			.frame(height: 200)
			.previewLayout(.sizeThatFits)
		
		ImageGallery(images: images, isFullScreen: true)
		
		ImageGallery(images: [])
			.frame(height: 200)
	}
}
