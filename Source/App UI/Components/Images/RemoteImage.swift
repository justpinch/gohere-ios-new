import Combine
import Nuke
import SwiftUI

struct RemoteImage<Placeholder: View>: View {
	struct Options {
		var request: ImageRequest
		var fallbackRequest: ImageRequest?
		var placeholder: () -> Placeholder
	}

	private var options: Options
	@State private var isFallback = false
	@StateObject private var image = FetchImage()

	var body: some View {
		ZStack {
			if let image = image.view {
				image
					.resizable()
					.aspectRatio(contentMode: .fill)
			} else {
				options.placeholder()
			}
		}
		.onReceive(onErrorDelayed(), perform: { _ in
			if !isFallback, let fallback = options.fallbackRequest {
				isFallback = true
				image.load(fallback)
			}
		})
		.onAppear {
			withoutAnimation {
				image.load(options.request)
			}
		}
		.onChange(of: options.request.url) { image.load($0) }
//		.onDisappear(perform: image.reset) // TODO: causes unowned reference crash??
		.transition(.opacity)
		.animation(.easeIn(duration: 0.1), value: image.view)
	}

	private func onErrorDelayed() -> AnyPublisher<Error, Never> {
		image.$result.compactMap { result -> Error? in
			switch result {
				case let .failure(error):
					return error
				default:
					break
			}
			return nil
		}
		.delay(for: .seconds(1), scheduler: RunLoop.main)
		.eraseToAnyPublisher()
	}

	private func withoutAnimation(_ closure: () -> Void) {
		var transaction = Transaction(animation: nil)
		transaction.disablesAnimations = true
		withTransaction(transaction, closure)
	}
}

extension RemoteImage {
	init(url: URL, processors: [ImageProcessing] = []) where Placeholder == EmptyView {
		self.init(options: .init(request: ImageRequest(url: url, processors: processors), placeholder: EmptyView.init))
	}

	init(path: ImagePath, size: ImagePath.Size, processors: [ImageProcessing] = []) where Placeholder == EmptyView {
		let request = ImageRequest(url: path.url(for: size), processors: processors)
		let fallback = ImageRequest(url: path.originalSizeURL(), processors: processors)
		self.init(options: .init(request: request, fallbackRequest: fallback, placeholder: EmptyView.init))
	}

	init(path: ImagePath, size: Int, processors: [ImageProcessing] = []) where Placeholder == EmptyView {
		self.init(path: path, size: ImagePath.Size(value: size), processors: processors)
	}

	init(path: ImagePath, size: CGFloat, processors: [ImageProcessing] = []) where Placeholder == EmptyView {
		self.init(path: path, size: ImagePath.Size(value: size), processors: processors)
	}

	func placeholder<P: View>(_ content: @escaping () -> P) -> RemoteImage<P> {
		RemoteImage<P>(options: .init(request: options.request, fallbackRequest: options.fallbackRequest, placeholder: content))
	}

	func fallback(url: URL?) -> Self {
		guard let url = url else {
			return self
		}

		var image = self
		let original = image.options.request
		let request = ImageRequest(url: url, processors: original.processors, priority: original.priority, options: original.options)
		image.options.fallbackRequest = request
		return image
	}
}

struct RemoteImage_Previews: PreviewProvider {
	static var previews: some View {
		RemoteImage(path: .fixture(.place1), size: .screenWidth)
			.placeholder { Rectangle().foregroundColor(.red) }
			.frame(width: 300, height: 200, alignment: .center)
			.clipped()
	}
}
