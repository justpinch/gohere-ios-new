import Nuke
import SwiftUI

struct PlaceCategoryImage: View {
	let icon: ImagePath
	var tintColor = UIColor.gh(.labelPrimary)

	@Environment(\.colorScheme) var colorScheme

	var body: some View {
		RemoteImage(url: icon.originalSizeURL(), processors: [ImageProcessors.TintOverlay(color: resolvedColor)])
			.id(resolvedColor)
			.aspectRatio(contentMode: .fit)
			.clipped()
	}

	private var resolvedColor: UIColor {
		tintColor.forceResolve(with: colorScheme)
	}
}

struct PlaceCategoryImage_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			HStack {
				Group {
					PlaceCategoryImage(icon: .fixture(.category1))
					PlaceCategoryImage(icon: .fixture(.category2))
					PlaceCategoryImage(icon: .fixture(.category3))
				}
				.frame(width: 48, height: 48)
				.padding()
			}

			HStack {
				Group {
					PlaceCategoryImage(icon: .fixture(.category1))
					PlaceCategoryImage(icon: .fixture(.category2))
					PlaceCategoryImage(icon: .fixture(.category3))
				}
				.frame(width: 48, height: 48)
				.padding()
			}
			.preferredColorScheme(.dark)
		}
		.previewLayout(.sizeThatFits)
	}
}
