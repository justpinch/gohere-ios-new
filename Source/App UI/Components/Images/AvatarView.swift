import SwiftUI

struct AvatarView: View {
	enum Source {
		case image(UIImage?)
		case path(ImagePath?)
		case url(URL?)
	}

	private let source: Source
	private let initials: String?

	@Environment(\.colorScheme) var colorScheme

	init(image: UIImage?, initials: String? = nil) {
		self.source = .image(image)
		self.initials = initials
	}

	init(path: ImagePath? = nil, initials: String? = nil) {
		self.source = .path(path)
		self.initials = initials
	}

	init(source: Source, initials: String? = nil) {
		self.source = source
		self.initials = initials
	}

	var body: some View {
		image()
			.background(Color.gh(.backgroundSecondary))
			.clipShape(Circle())
			.overlay(
				Circle()
					.strokeBorder()
					.foregroundColor(Color(.label).opacity(colorScheme == .dark ? 0.15 : 0.05))
			)
	}

	@ViewBuilder
	private func image() -> some View {
		switch source {
			case let .image(image):
				if let image = image {
					Image(uiImage: image)
						.resizable()
						.aspectRatio(contentMode: .fit)
				} else {
					GeometryReader(content: { geometry in
						placeholder(initials: initials, size: geometry.size)
					})
				}
			case let .path(path):
				if let path = path {
					GeometryReader(content: { geometry in
						RemoteImage(path: path, size: geometry.size.width)
							.placeholder {
								placeholder(size: geometry.size)
							}
							.aspectRatio(contentMode: .fit)
					})
				} else {
					GeometryReader(content: { geometry in
						placeholder(initials: initials, size: geometry.size)
					})
				}
			case let .url(url):
				if let url = url {
					GeometryReader(content: { geometry in
						RemoteImage(url: url)
							.placeholder {
								placeholder(size: geometry.size)
							}
							.aspectRatio(contentMode: .fit)
					})
				} else {
					GeometryReader(content: { geometry in
						placeholder(initials: initials, size: geometry.size)
					})
				}
		}
	}

	@ViewBuilder
	func placeholder(initials: String? = nil, size: CGSize) -> some View {
		if let text = initials, !text.isEmpty {
			Text(text)
				.font(.system(size: size.height * 0.5))
				.foregroundColor(.gh(.labelQuaternary))
				.lineLimit(0)
				.minimumScaleFactor(0.01)
				.padding([.leading, .trailing], size.width * 0.15)
				.frame(width: size.width, height: size.height)
		} else {
			Image(systemName: "person.fill")
				.resizable()
				.foregroundColor(Color.gh(.labelQuaternary))
				.padding(size.width / 4)
		}
	}
}

struct AvatarView_Previews: PreviewProvider {
	static var previews: some View {
		VStack {
			HStack {
				AvatarView(image: UIImage(named: "avatar1")!)
					.frame(width: 36, height: 36)
					.padding()

				AvatarView(image: UIImage(named: "avatar2")!)
					.frame(width: 36, height: 36)
					.previewDarkTheme()

				AvatarView(path: ImagePath(path: "/"))
					.frame(width: 36, height: 36)
					.padding()

				AvatarView(path: ImagePath(path: "/"))
					.frame(width: 36, height: 36)
					.previewDarkTheme()

				AvatarView(path: ImagePath(path: "/"))
					.frame(width: 36, height: 36)
					.padding()

				AvatarView(path: ImagePath(path: "/"))
					.frame(width: 36, height: 36)
					.previewDarkTheme()

				AvatarView(initials: "JdV")
					.frame(width: 36, height: 36)
					.padding()

				AvatarView(initials: "WwW")
					.frame(width: 36, height: 36)
					.previewDarkTheme()
			}
			HStack {
				AvatarView(image: UIImage(named: "avatar1")!)
					.frame(width: 96, height: 96)
					.padding()

				AvatarView(image: UIImage(named: "avatar2")!)
					.frame(width: 96, height: 96)
					.previewDarkTheme()

				AvatarView(path: ImagePath(path: "/"), initials: "MM")
					.frame(width: 96, height: 96)
					.padding()

				AvatarView(path: ImagePath(path: "/"), initials: "QZ")
					.frame(width: 96, height: 96)
					.previewDarkTheme()

				AvatarView(initials: "W")
					.frame(width: 96, height: 96)
					.padding()

				AvatarView(initials: "WwW")
					.frame(width: 96, height: 96)
					.previewDarkTheme()
			}
			HStack {
				AvatarView(image: UIImage(named: "avatar1")!)
					.frame(width: 120, height: 120)
					.padding()

				AvatarView(path: ImagePath(path: "/"), initials: "BW")
					.frame(width: 120, height: 120)
					.previewDarkTheme()

				AvatarView(initials: "WwW")
					.frame(width: 120, height: 120)
					.padding()
			}
		}
		.previewLayout(.sizeThatFits)
	}
}
