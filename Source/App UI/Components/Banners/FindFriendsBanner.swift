import SwiftUI

struct FindFriendsBanner: View {
	var addressBookAction: (() -> Void)?
	var facebookAction: (() -> Void)?

	var body: some View {
		HStack {
			VStack(alignment: .leading) {
				Text("Find more friends on GoHere!")
					.font(.gh(.title2))
					.foregroundColor(.gh(.onTintTertiary))

				Text("Because friends know what’s best!")
					.font(.gh(.callout))
					.foregroundColor(.gh(.onTintTertiary))

				if let action = addressBookAction {
					Button(action: action, label: {
						Text("search_your_address_book")
					})
					.primaryButtonStyle(size: .medium, width: .fitContent)
				}

				if let action = facebookAction {
					Button(action: action, label: {
						Text("connect_facebook")
					})
					.facebookButtonStyle(size: addressBookAction != nil ? .small : .medium, width: .fitContent)
				}
			}

			Spacer()
		}
		.padding()
		.background(Color.gh(.tintTertiary))
	}
}

struct FindFriendsBanner_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			FindFriendsBanner(addressBookAction: {}, facebookAction: {})
			FindFriendsBanner(addressBookAction: {})
			FindFriendsBanner(facebookAction: {})

			FindFriendsBanner(addressBookAction: {}, facebookAction: {})
				.preferredColorScheme(.dark)
		}
		.previewLayout(.sizeThatFits)
	}
}
