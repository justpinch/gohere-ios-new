//
//  HtmlText.swift
//  GoHere
//
//  Created by jorisfavier on 02/11/2022.
//

import SwiftUI

struct HtmlText: View {
	let text: String
	private var htmlTags: [String]
	@State private var url: URL?
	private var isShowingSafari: Binding<Bool> {
		Binding {
			url != nil
		} set: { value in
			if !value {
				self.url = nil
			}
		}
	}
	
	init(text: String) {
		self.text = text
		self.htmlTags = text.split(with: "<[^>]+>[^<]*</\\w+>")
	}
	
    var body: some View {
		VStack(alignment: .leading) {
			ForEach(htmlTags, id: \.self) { tag in
				htmlToSwiftUI(htmlString: tag)
			}
		}
    }
	
	@ViewBuilder
	func htmlToSwiftUI(htmlString: String) -> some View {
		if htmlString.contains("<a") {
			let urlPattern = "(http|www).[^\"]*"
			if let url = htmlString.firstOrNull(pattern: urlPattern) {
				Button(action: {
					self.url = URL(string: url)!
				}, label: {
						Text(htmlString.stripHtmlTags())
							.bold()
							.foregroundColor(.gh(.tintPrimary))
				})
				.fullScreenCover(isPresented: isShowingSafari, content: {
					SafariView(url: self.url!)
				})
			}
		} else {
			Text(htmlString.stripHtmlTags().gh_trimmed())
		}
	}
}

struct HtmlText_Previews: PreviewProvider {
    static var previews: some View {
        HtmlText(text: "")
    }
}
