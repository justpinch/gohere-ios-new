import SwiftUI

struct ProgressIndicator: View {
	let size: Size
	@State private var isAnimating = false

	enum Size {
		case large
		case small

		fileprivate var height: CGFloat {
			switch self {
				case .large:
					return 11
				case .small:
					return 6
			}
		}
	}

	var body: some View {
		HStack(spacing: 0) {
			DotView()
			Spacer(minLength: size.height / 3)
			DotView(delay: 0.1)
			Spacer(minLength: size.height / 3)
			DotView(delay: 0.2)
		}
		.foregroundColor(.gh(.labelQuaternary))
		.frame(height: size.height)
		.fixedSize()
	}
}

private struct DotView: View {
	@State var delay: Double = 0
	@State var opacity: Double = 0.2

	var body: some View {
		Circle()
			.aspectRatio(contentMode: .fit)
			.opacity(opacity)
			.animation(Animation.easeInOut(duration: 0.3).repeatForever().delay(delay))
			.onAppear {
				withAnimation { self.opacity = 1 }
			}
	}
}

struct ProgressIndicator_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			ProgressIndicator(size: .large)
				.padding()

			ProgressIndicator(size: .small)
				.padding()
		}
		.previewLayout(.sizeThatFits)
	}
}
