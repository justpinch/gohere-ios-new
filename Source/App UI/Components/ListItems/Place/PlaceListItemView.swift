import SwiftUI
import Nuke

struct PlaceListItemView: View {
	let place: Place

	@Environment(\.colorScheme) private var colorScheme

	var body: some View {
		VStack(alignment: .leading, spacing: 0) {
			if let image = place.photo {
				GeometryReader { geometry in
					RemoteImage(
						path: image,
						size: .screenWidth,
						processors: [
							ImageProcessors.Resize(
								size: CGSize(width: geometry.size.width, height: ExploreView.Const.Card.imageHeight),
								crop: true
							)
						]
					)
					.placeholder { Color.gh(.backgroundTertiary) }
					.id(image.path)
				}
				.frame(height: ExploreView.Const.Card.imageHeight)
			}

			HStack(alignment: .top) {
				VStack(alignment: .leading, spacing: 4) {
					Text(place.name)
						.lineLimit(2)
						.minimumScaleFactor(0.85)
						.font(.gh(.title3))
						.foregroundColor(.gh(.labelPrimary))

					if let category = place.category {
						HStack {
							PlaceCategoryImage(icon: place.category.icon)
								.frame(width: 24, height: 24)

							Text(category.localizedName)
								.font(.gh(.caption1))
								.foregroundColor(.gh(.labelPrimary))

							if let distance = place.location.prettyDistanceFromUser() {
								Text("• \(distance)")
									.font(.gh(.caption1))
									.foregroundColor(.gh(.labelTertiary))
							}
						}
					}
				}
				Spacer()
				SavePlaceButton(place: place)
			}
			.padding()

			if let review = place.lastReview {
				ReviewSmallView(avatar: review.user.profile.avatar,
				                initials: review.user.initials,
				                name: review.user.fullName,
				                rating: review.rating,
				                text: review.text,
				                images: [])
					.padding([.leading, .trailing, .bottom])
			}
		}
		.background(Color(.systemBackground))
		.clipped()
		.elevationLevel1()
	}
}

struct PlaceListItemView_Previews: PreviewProvider {
	static var previews: some View {
		PlaceListItemView(place: .fixture1)
			.injectPreviewsEnvironment()
	}
}
