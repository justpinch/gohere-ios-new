import SwiftUI

struct ReviewListItemView: View {
	let review: Review

	var body: some View {
		HStack {
			if let image = review.place.photo {
				RemoteImage(path: image, size: .screenWidth)
					.placeholder { Color.gh(.backgroundTertiary) }
					.aspectRatio(contentMode: .fill)
					.frame(width: 80, height: 80)
					.clipped()
			} else {
				ZStack {
					Color.gh(.backgroundSecondary)
					Image("no-image")
						.foregroundColor(.gh(.labelQuaternary))
				}
				.frame(width: 80, height: 80)
			}

			HStack(alignment: .top) {
				VStack(alignment: .leading, spacing: 4) {
					Text(review.place.name)
						.lineLimit(2)
						.minimumScaleFactor(0.85)
						.font(.gh(.title3))
						.foregroundColor(.gh(.labelPrimary))
						.fixedSize(horizontal: false, vertical: true)

					HStack {
						if let category = review.place.category {
							PlaceCategoryImage(icon: category.icon)
								.frame(width: 24, height: 24)
						}

						Text("\(review.place.city), \(review.place.country)")
							.font(.gh(.caption1))
							.foregroundColor(.gh(.labelSecondary))
					}

					RatingView(rating: review.rating, activeColor: .gh(.labelPrimary), inactiveColor: Color(.systemGray4))
						.frame(height: 10)
						.fixedSize(horizontal: true, vertical: true)
				}
				Spacer()

				SavePlaceButton(place: review.place)
					.opacity(1)
					.offset(y: -8)
			}
			.padding(.leading, 8)
		}
		.background(Color.gh(.backgroundPrimary))
	}
}

struct ReviewListItemView_Previews: PreviewProvider {
	static var previews: some View {
		ReviewListItemView(review: .fixture1)
			.injectPreviewsEnvironment()
	}
}
