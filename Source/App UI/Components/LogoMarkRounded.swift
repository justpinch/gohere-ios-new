import SwiftUI

struct LogoMarkRounded: View {
	var body: some View {
		Image("logo-mark-rounded")
			.resizable()
			.aspectRatio(contentMode: .fit)
			.frame(width: 40, height: 40)
			.padding()
			.background(Color.gh(.appIcon))
			.clipShape(Circle())
			.overlay(
				Circle()
					.strokeBorder()
					.foregroundColor(.gh(.labelQuaternary))
			)
	}
}

struct LogoMarkRounded_Previews: PreviewProvider {
	static var previews: some View {
		VStack {
			ZStack {
				Color.gh(.backgroundPrimary)
				LogoMarkRounded()
			}

			ZStack {
				Color.gh(.backgroundPrimary)
				LogoMarkRounded()
			}
			.colorScheme(.dark)
		}
	}
}
