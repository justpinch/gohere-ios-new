import SwiftUI

struct FixedFooterBar: View {
	let buttonTitle: LocalizedStringKey
	let action: () -> Void

	var body: some View {
		VStack {
			Button(buttonTitle, action: action)
				.primaryButtonStyle()
		}
		.padding()
		.background(
			Color.gh(.backgroundPrimary)
				.shadow(radius: 12, y: 1)
				.edgesIgnoringSafeArea(.bottom)
		)
	}
}

struct FixedFooterBar_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			VStack {
				Spacer()
				FixedFooterBar(buttonTitle: "Continue", action: {})
			}

			VStack {
				Spacer()
				FixedFooterBar(buttonTitle: "Continue", action: {})
			}
			.background(Color.gray)
			.environment(\.colorScheme, .dark)
		}
		.frame(height: 200)
		.previewLayout(.sizeThatFits)
	}
}
