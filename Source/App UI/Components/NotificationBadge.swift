import SwiftUI

struct NotificationBadge: View {
	let value: Int

	var body: some View {
		if value == 0 {
			EmptyView()
		} else {
			Text("\(value)")
				.font(.gh(.notification))
				.foregroundColor(.gh(.onNotification))
				.padding(6)
				.background(
					Circle()
						.foregroundColor(Color.gh(.notification))
				)
		}
	}
}

struct NotificationBadge_Previews: PreviewProvider {
	static var previews: some View {
		HStack {
			Group {
				NotificationBadge(value: 1)
				NotificationBadge(value: 5)
				NotificationBadge(value: 334)
			}
			.padding()
		}
		.previewLayout(.sizeThatFits)
	}
}
