//
//  SearchTextButton.swift
//  GoHere
//
//  Created by jorisfavier on 25/01/2022.
//

import SwiftUI
import MapKit

struct SearchTextButton: View {
	let placeholder: LocalizedStringKey
	let onTap: () -> Void
	
	@State private var text = ""
	
	init(placeholder: LocalizedStringKey, onTap: @escaping () -> Void) {
		self.placeholder = placeholder
		self.onTap = onTap
	}
	
    var body: some View {
		HStack {
			Image(systemName: "magnifyingglass")
			TextField(placeholder, text: $text)
				.font(.gh(.footnote))
		}
		.padding()
		.foregroundColor(.gh(.labelSecondary))
		.background(Color.gh(.backgroundTertiary))
		.cornerRadius(8)
		.onTapGesture {
			onTap()
		}
    }
}

struct SearchTextButton_Previews: PreviewProvider {
    static var previews: some View {
		SearchTextButton(placeholder: "Find friends on \(Config.appName)"){}
    }
}
