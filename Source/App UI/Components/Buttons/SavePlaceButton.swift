import Combine
import SwiftUI

struct SavePlaceButton: View {
	let place: Place
	var activeColor: Color = .gh(.labelPrimary)
	var inactiveColor: Color = .gh(.labelQuaternary)

	@State var signInPrompt: Bool = false

	@Environment(\.userInteractor) var userInteractor
	@EnvironmentObject var userState: UserState

	@State
	private var refreshCancellable: AnyCancellable?

	/// WORKAROUND:
	/// The `isActive` var should just be a read-only computed property and return the value
	/// for`userState.savedPlaces[place.id]` BUT there's some kind of an issue I can't figure out.
	/// In a list (e.g. Activity), the userState change triggers an update *before* the value for the place is updated
	/// and it only shows the correct state after the view re-renders (e.g. another save button is tapped).
	/// See here: https://justpinch.atlassian.net/browse/GOH-835
	///
	/// In absence of a better idea to fix this, I've made `isActive` a local state property and keep it in sync with
	/// the value from `userState.savedPlaces` for this place ID. This seems to do the trick..
	@State private var isActive: Bool = false

	var body: some View {
		Button(action: action, label: {
			Image(systemName: isActive ? "bookmark.fill" : "bookmark")
				.foregroundColor(isActive ? activeColor : inactiveColor)
				.imageScale(.large)
				.padding(8)
		})
		.signInPrompt(isPresented: $signInPrompt)
		.onAppear(perform: configureInitialState)
		.onChange(of: userState.userID, perform: { _ in
			userInteractor.savedPlaces?.refreshStatus(for: place)
		})
		.onReceive(userState.$savedPlaceStatuses, perform: { mapping in
			self.isActive = mapping[place.id] ?? false
		})
	}

	private func action() {
		guard let interactor = userInteractor.savedPlaces else { // user not authenticated
			signInPrompt = true
			return
		}

		isActive ? interactor.remove(place: place) : interactor.add(place: place)
		UINotificationFeedbackGenerator().notificationOccurred(.success)
	}

	private func configureInitialState() {
		let hasCachedStatus = userState.savedPlaceStatuses[place.id] != nil
		let shouldRefresh = place.isFavorite == nil && !hasCachedStatus

		if shouldRefresh {
			userInteractor.savedPlaces?.refreshStatus(for: place)
		}

		if let isActive = place.isFavorite, !hasCachedStatus {
			userInteractor.savedPlaces?.setStatus(for: place.id, isSaved: isActive)
		}

		isActive = userState.savedPlaceStatuses[place.id] ?? false
	}
}

struct SavePlaceButton_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			HStack {
				SavePlaceButton(place: .fixture1)
				SavePlaceButton(place: .fixture2)
			}

			HStack {
				SavePlaceButton(place: .fixture1)
				SavePlaceButton(place: .fixture2)
			}
			.preferredColorScheme(.dark)

			HStack {
				SavePlaceButton(place: .fixture1)
					.environment(\.sizeCategory, .small)
				SavePlaceButton(place: .fixture1)
					.environment(\.sizeCategory, .large)
				SavePlaceButton(place: .fixture1)
					.environment(\.sizeCategory, .accessibilityExtraLarge)
				SavePlaceButton(place: .fixture1)
					.environment(\.sizeCategory, .accessibilityExtraExtraLarge)
				SavePlaceButton(place: .fixture1)
					.environment(\.sizeCategory, .accessibilityExtraExtraExtraLarge)
			}
		}
		.padding()
		.previewLayout(.sizeThatFits)
		.injectPreviewsEnvironment()
	}
}
