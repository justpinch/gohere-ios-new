import SwiftUI

struct CloseButton: View {
	let action: () -> Void

	var body: some View {
		Button(action: action, label: {
			Image(systemName: "xmark.circle.fill")
				.font(.system(size: 36))
				.foregroundColor(.white)
		})
		.elevationLevel2()
	}
}

struct CloseButton_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			CloseButton(action: {})
		}
		.previewLayout(.sizeThatFits)
	}
}
