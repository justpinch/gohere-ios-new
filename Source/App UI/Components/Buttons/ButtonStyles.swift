import SwiftUI

extension View {
	func primaryButtonStyle(size: ButtonSize = .large, width: ButtonWidth? = nil) -> some View {
		buttonStyle(PrimaryButtonStyle(size: size, width: width ?? size.preferredWidth()))
	}

	func quaternaryButtonStyle(size: ButtonSize = .large, width: ButtonWidth? = nil) -> some View {
		buttonStyle(QuaternaryButtonStyle(size: size, width: width ?? size.preferredWidth()))
	}

	func quinaryButtonStyle(size: ButtonSize = .large, width: ButtonWidth? = nil) -> some View {
		buttonStyle(QuinaryButtonStyle(size: size, width: width ?? size.preferredWidth()))
	}

	func destructiveButtonStyle(size: ButtonSize = .large, width: ButtonWidth? = nil) -> some View {
		buttonStyle(DestructiveButtonStyle(size: size, width: width ?? size.preferredWidth()))
	}

	func facebookButtonStyle(size: ButtonSize = .large, width: ButtonWidth? = nil) -> some View {
		buttonStyle(FacebookButtonStyle(size: size, width: width ?? size.preferredWidth()))
	}

	func affiliateButtonStyle(size: ButtonSize = .large, width: ButtonWidth? = nil) -> some View {
		buttonStyle(AffiliateButtonStyle(size: size, width: width ?? size.preferredWidth()))
	}

	func floatingActionButtonStyle(width: FloatingActionButtonStyle.Width = .compact, backgroundColor: Color = .gh(.backgroundPrimary)) -> some View {
		buttonStyle(FloatingActionButtonStyle(width: width, backgroundColor: backgroundColor))
	}
}

enum ButtonSize {
	case large
	case medium
	case small

	fileprivate func preferredWidth() -> ButtonWidth {
		switch self {
			case .large, .medium:
				return .infinity
			case .small:
				return .fitContent
		}
	}

	fileprivate func font() -> Font {
		switch self {
			case .large:
				return .gh(.headline)
			case .medium:
				return .gh(.callout)
			case .small:
				return .gh(.caption1)
		}
	}

	fileprivate func padding() -> CGFloat {
		switch self {
			case .large:
				return 16
			case .medium:
				return 12
			case .small:
				return 10
		}
	}
}

enum ButtonWidth {
	case fitContent
	case infinity

	fileprivate func maxWidth() -> CGFloat? {
		switch self {
			case .infinity:
				return .infinity
			case .fitContent:
				return nil
		}
	}
}

private struct ButtonStyleView: View {
	@Environment(\.isEnabled) var isEnabled
	let configuration: ButtonStyle.Configuration
	var size: ButtonSize
	var width: ButtonWidth
	let foregroundColor: Color
	let backgroundColor: Color
	let isOutline: Bool

	private func backgroundOpacity(for configuration: ButtonStyle.Configuration) -> Double {
		if !isEnabled {
			return 0.2
		}
		return configuration.isPressed ? 0.75 : 1
	}

	var body: some View {
		if isOutline {
			content
				.overlay(
					Capsule(style: .circular)
						.strokeBorder()
						.foregroundColor(backgroundColor)
				)
				.opacity(backgroundOpacity(for: configuration))
				.contentShape(Capsule(style: .circular))
		} else {
			content
				.background(backgroundColor.opacity(backgroundOpacity(for: configuration)))
				.clipShape(Capsule(style: .circular))
		}
	}

	var content: some View {
		configuration
			.label
			.lineLimit(1)
			.layoutPriority(1)
			.frame(maxWidth: width.maxWidth())
			.font(size.font())
			.foregroundColor(foregroundColor)
			.padding([.leading, .trailing], size.padding() * 1.4)
			.padding([.top, .bottom], size.padding())
	}
}

struct PrimaryButtonStyle: ButtonStyle {
	var size: ButtonSize = .large
	var width: ButtonWidth = .infinity

	func makeBody(configuration: Configuration) -> some View {
		ButtonStyleView(configuration: configuration,
		                size: size,
		                width: width,
		                foregroundColor: .gh(.onTintPrimary),
		                backgroundColor: .gh(.tintPrimary),
		                isOutline: false)
	}
}

struct QuaternaryButtonStyle: ButtonStyle {
	var size: ButtonSize = .large
	var width: ButtonWidth = .infinity

	func makeBody(configuration: Configuration) -> some View {
		ButtonStyleView(configuration: configuration,
		                size: size,
		                width: width,
		                foregroundColor: .gh(.labelPrimary),
		                backgroundColor: Color(.systemGray5),
		                isOutline: true)
	}
}

struct QuinaryButtonStyle: ButtonStyle {
	var size: ButtonSize = .large
	var width: ButtonWidth = .infinity

	func makeBody(configuration: Configuration) -> some View {
		ButtonStyleView(configuration: configuration,
		                size: size,
		                width: width,
		                foregroundColor: .gh(.onTintQuaternary),
		                backgroundColor: .gh(.tintQuaternary),
		                isOutline: false)
	}
}

struct DestructiveButtonStyle: ButtonStyle {
	var size: ButtonSize = .large
	var width: ButtonWidth = .infinity

	func makeBody(configuration: Configuration) -> some View {
		ButtonStyleView(configuration: configuration,
		                size: size,
		                width: width,
		                foregroundColor: .gh(.onUtilityError),
		                backgroundColor: .gh(.utilityError),
		                isOutline: false)
	}
}

struct FacebookButtonStyle: ButtonStyle {
	var size: ButtonSize = .large
	var width: ButtonWidth = .infinity

	func makeBody(configuration: Configuration) -> some View {
		ButtonStyleView(configuration: configuration,
		                size: size,
		                width: width,
		                foregroundColor: .gh(.onTintPrimary),
		                backgroundColor: Color(UIColor(rgb: 0x3B5998)),
		                isOutline: false)
	}
}

struct AffiliateButtonStyle: ButtonStyle {
	var size: ButtonSize = .large
	var width: ButtonWidth = .infinity

	func makeBody(configuration: Configuration) -> some View {
		ButtonStyleView(configuration: configuration,
		                size: size,
		                width: width,
		                foregroundColor: .gh(.onAffiliate),
		                backgroundColor: .gh(.affiliate),
		                isOutline: false)
	}
}

struct NavbarButtonStyle: ButtonStyle {
	private struct ButtonView: View {
		@Environment(\.isEnabled) var isEnabled
		let configuration: Configuration

		var body: some View {
			configuration
				.label
				.font(.gh(.subhead))
				.foregroundColor(Color.gh(.onNavbar))
				.opacity(configuration.isPressed || !isEnabled ? 0.2 : 1)
		}
	}

	func makeBody(configuration: Configuration) -> some View {
		ButtonView(configuration: configuration)
	}
}

struct HighlightButtonStyle: ButtonStyle {
	enum Kind {
		case opacity(Double)
	}

	let kind: Kind

	init(_ kind: Kind = .opacity(0.85)) {
		self.kind = kind
	}

	@ViewBuilder
	func makeBody(configuration: Configuration) -> some View {
		switch kind {
			case let .opacity(value):
				configuration
					.label
					.opacity(configuration.isPressed ? value : 1.0)
		}
	}
}

struct FloatingActionButtonStyle: ButtonStyle {
	enum Width {
		case compact
		case expanded
		case small
	}

	let width: Width
	let backgroundColor: Color

	func makeBody(configuration: Configuration) -> some View {
		configuration.label
			.padding()
			.background(
				Capsule(style: .circular)
					.foregroundColor(backgroundColor)
					.elevationLevel2()
					.modifier(WidthModifier(width: width))
			)
			.contentShape(Capsule(style: .circular))
			.opacity(configuration.isPressed ? 0.5 : 1.0)
	}

	struct WidthModifier: ViewModifier {
		let width: Width

		@ViewBuilder
		func body(content: Content) -> some View {
			if width == .compact {
				content.aspectRatio(1, contentMode: .fill)
			} else if width == .small {
				content.frame(width: 30, height: 30)
			} else {
				content
			}
		}
	}
}

struct ButtonStyle_Previews: PreviewProvider {
	static var previews: some View {
		previewButtons
			.previewLayout(.sizeThatFits)
	}

	@ViewBuilder
	static var previewButtons: some View {
		HStack {
			VStack {
				Button("Primary Button", action: {})
					.buttonStyle(PrimaryButtonStyle(size: .small))
					.previewButtonPreset()
			}
			VStack {
				Button("Quaternary Button", action: {})
					.buttonStyle(QuaternaryButtonStyle(size: .small))
					.previewButtonPreset()
			}
			VStack {
				Button("Quinary Button", action: {})
					.buttonStyle(QuinaryButtonStyle(size: .small))
					.previewButtonPreset()
			}
		}
		.frame(minWidth: 1000)
	}

	struct FABTestView: View {
		@State var isLonger = false

		var body: some View {
			Button(action: {
				isLonger.toggle()
			}, label: {
				HStack(spacing: isLonger ? 8 : 0) {
					Image(systemName: "location.fill")

					if isLonger {
						Text("Longer title label")
					}
				}
				.foregroundColor(.accentColor)
			})
			.floatingActionButtonStyle(width: isLonger ? .expanded : .compact)
		}
	}

	@ViewBuilder
	static var previewFAB: some View {
		FABTestView()
			.padding()
	}
}
