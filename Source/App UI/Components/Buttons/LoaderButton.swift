import SwiftUI

struct LoaderButton<Label: View>: View {
	let isLoading: Bool
	let action: () -> Void
	let label: () -> Label

	init(isLoading: Bool, action: @escaping () -> Void, @ViewBuilder label: @escaping () -> Label) {
		self.isLoading = isLoading
		self.action = action
		self.label = label
	}

	var body: some View {
		Button(action: action, label: {
			if isLoading {
				ProgressView()
			} else {
				label()
			}
		})
	}
}

extension LoaderButton where Label == Text {
	init(_ titleKey: LocalizedStringKey, isLoading: Bool, action: @escaping () -> Void) {
		self.init(isLoading: isLoading, action: action) {
			Text(titleKey)
		}
	}
}

struct LoaderButton_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			LoaderButton("Button", isLoading: false, action: {})
			LoaderButton("Button", isLoading: true, action: {})
		}
		.previewLayout(.sizeThatFits)
	}
}
