import SwiftUI

struct InviteFriendButton: View {
	let contact: Contact

	@State private var isShowingPrompt = false
	@State private var selectedEmail: IdentifiableWrapper<String, String>?

	@State private var alertError: IdentifiableWrapper<String, String>?

	@EnvironmentObject var userState: UserState

	var body: some View {
		Button("invite", action: action)
			.quaternaryButtonStyle(size: .small, width: .fitContent)
			.actionSheet(isPresented: $isShowingPrompt, content: actionSheetContent)
			.sheet(item: $selectedEmail, content: emailComposer(for:))
			.alert(item: $alertError, content: errorAlert)
	}

	private func errorAlert(_ string: IdentifiableWrapper<String, String>) -> Alert {
		Alert(title: Text("error"), message: Text(string.wrapped), dismissButton: .cancel())
	}

	private func actionSheetContent() -> ActionSheet {
		ActionSheet(title: Text("Invite \(contact.fullName)"), message: nil, buttons: actionSheetButtons())
	}

	private func actionSheetButtons() -> [ActionSheet.Button] {
		let phoneButtons = contact.phoneNumbers.prefix(2).map { phoneNumber in
			ActionSheet.Button.default(Text(phoneNumber), action: {
				promptInvite(contact: contact, toPhoneNumber: phoneNumber)
			})
		}

		let emailButtons = contact.emails.prefix(2).map { email in
			ActionSheet.Button.default(Text(email), action: {
				promptInvite(contact: contact, toEmail: email)
			})
		}

		return phoneButtons + emailButtons + [.cancel()]
	}

	private func action() {
		if contact.emails.isEmpty, contact.phoneNumbers.count == 1, let phoneNumber = contact.phoneNumbers.first {
			// 1 phone number, go directly
			promptInvite(contact: contact, toPhoneNumber: phoneNumber)
		} else if contact.phoneNumbers.isEmpty, contact.emails.count == 1, let email = contact.emails.first {
			// 1 email, go directly
			promptInvite(contact: contact, toEmail: email)
		} else {
			isShowingPrompt = true
		}
	}

	private func promptInvite(contact _: Contact, toPhoneNumber phone: String) {
		let link = API.Environment.current.createInviteLink(for: userState.userID)

		var messageText = "join_me_on_to".localizedFromTarget() + "\n\n" + link.absoluteString
		messageText = URLEncoding.default.escape(messageText)
		if let smsURL = URL(string: "sms:\(phone)&body=\(messageText)") {
			UIApplication.shared.open(smsURL, options: [:], completionHandler: nil)
		}
	}

	private func promptInvite(contact _: Contact, toEmail email: String) {
		guard MailComposer.canSendEmail else {
			alertError = .init(NSLocalizedString("email_client_is_not_configured", comment: ""))
			return
		}

		selectedEmail = .init(email)
	}

	@ViewBuilder
	private func emailComposer(for email: IdentifiableWrapper<String, String>) -> some View {
		MailComposer { composeViewController in
			let link = API.Environment.current.createInviteLink(for: userState.userID)
			composeViewController.setSubject("join_me_on".localizedFromTarget())
			let messageText = "join_me_on_to".localizedFromTarget() + "\n\n" + link.absoluteString
			composeViewController.setMessageBody(messageText, isHTML: false)
			composeViewController.setToRecipients([email.wrapped])
		}
	}
}

struct InviteFriendButton_Previews: PreviewProvider {
	static var previews: some View {
		VStack {
			HStack {
				InviteFriendButton(contact: .fixture1)
					.previewSupportedLocales()
			}
			.padding()

			HStack {
				InviteFriendButton(contact: .fixture1)
					.previewSupportedLocales()
			}
			.padding()
			.background(Color(.systemBackground))
			.colorScheme(.dark)
		}
		.previewLayout(.sizeThatFits)
		.injectPreviewsEnvironment()
	}
}
