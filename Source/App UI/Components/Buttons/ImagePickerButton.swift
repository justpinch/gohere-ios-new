import SwiftUI

struct ImagePickerButton<Label: View>: View {
	@Binding var image: UIImage?
	let label: () -> Label

	@State private var imagePickerSource: UIImagePickerController.SourceType? = nil
	@State private var showingSourceSelector = false

	var body: some View {
		Button(action: {
			if ImagePicker.availableSourceTypes.count > 1 {
				showingSourceSelector = true
			} else if let source = ImagePicker.availableSourceTypes.first {
				imagePickerSource = source
			}
		}, label: label)
			.buttonStyle(HighlightButtonStyle())
			.actionSheet(isPresented: $showingSourceSelector, content: {
				ActionSheet(title: Text(""),
				            message: nil,
				            buttons: ImagePicker.availableSourceTypes.map { source in
				            	.default(Text(source.localizedTitle), action: { imagePickerSource = source })
				            } + [.cancel()])
			})
			.sheet(item: $imagePickerSource, content: { source in
				ImagePicker(image: self.$image, allowsEditing: true, sourceType: source)
			})
	}
}

struct ImagePickerButton_Previews: PreviewProvider {
	struct Container: View {
		@State var image: UIImage?

		var body: some View {
			ImagePickerButton(image: $image) {
				Text("Button")
			}
		}
	}

	static var previews: some View {
		Container()
	}
}
