import SwiftUI

struct FollowButton: View {
	let userID: User.ID
	let isPrivateProfile: Bool
	let size: ButtonSize

	@State var signInPrompt: Bool = false

	@Environment(\.userInteractor) var userInteractor
	@Environment(\.userInteractor.connections) var interactor
	@EnvironmentObject var userState: UserState

	var body: some View {
		if let state = followState() {
			FollowButtonView(state: state, size: size, action: action)
				.signInPrompt(isPresented: $signInPrompt)
		} else {
			EmptyView()
		}
	}

	private func action() {
		guard let interactor = interactor else { // user not authenticated
			signInPrompt = true
			return
		}

		guard let state = followState() else { return }

		switch state {
			case .follow:
				interactor.follow(id: userID, isPrivate: isPrivateProfile)
			case .unfollow, .requested:
				interactor.unfollow(id: userID)
			case .loading:
				break
		}

		UINotificationFeedbackGenerator().notificationOccurred(.success)
	}

	private func followState() -> FollowButtonView.State? {
		// don't show button for self
		guard userState.userID != userID else {
			return nil
		}

		guard let status = userState.connectionStatuses[userID] else {
			// if no status is found cached, request an update
			interactor?.updateStatus(for: userID)
			return .follow // default
		}

		switch status {
			case .none:
				return nil
			case .loading:
				return .loading
			case let .model(status):
				switch status {
					case .active:
						return .unfollow
					case .pending:
						return .requested
					case .ignored, .blocked:
						return nil // shouldn't happen
					case .none:
						return .follow
				}
			case .error:
				return nil
		}
	}
}

struct FollowButtonView: View {
	enum State: String, CaseIterable {
		case follow
		case unfollow = "following"
		case requested
		case loading

		var localizedString: String {
			NSLocalizedString(rawValue, comment: "")
		}
	}

	let state: State
	let size: ButtonSize
	let action: () -> Void

	var body: some View {
		switch state {
			case .follow:
				button
					.primaryButtonStyle(size: size)
			case .loading:
				ProgressIndicator(size: .small)
			default:
				button
					.quaternaryButtonStyle(size: size)
		}
	}

	var button: Button<Text> {
		Button(state.localizedString, action: action)
	}
}

struct FollowButton_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			ForEach(FollowButtonView.State.allCases, id: \.rawValue, content: { state in
				FollowButtonView(state: state, size: .small, action: {})
			})
		}
		.previewLayout(.sizeThatFits)
	}
}
