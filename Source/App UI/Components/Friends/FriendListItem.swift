import SwiftUI

struct FriendFollowListItem: View {
	let user: User

	var body: some View {
		NavigationLink(
			destination: UserProfileView(user),
			label: {
				FriendListItem(avatar: .path(user.profile.avatar), name: user.fullName, initials: user.initials) {
					FollowButton(userID: user.id, isPrivateProfile: user.profile.isPrivate, size: .small)
				}
			}
		)
	}
}

struct FriendConnectionListItem<Accessory: View>: View {
	let connection: UserConnection
	let accessoryView: () -> Accessory

	var body: some View {
		NavigationLink(
			destination: UserProfileView(connection.user),
			label: {
				FriendListItem(avatar: .path(connection.user.profile.avatar),
				               name: connection.user.fullName,
				               initials: connection.user.initials,
				               accessoryView: accessoryView)
			}
		)
	}
}

struct FriendInviteListItem: View {
	let contact: Contact

	var body: some View {
		FriendListItem(avatar: .image(contact.avatar), name: contact.fullName, initials: contact.initials) {
			InviteFriendButton(contact: contact)
		}
	}
}

private struct FriendListItem<Accessory: View>: View {
	let avatar: AvatarView.Source
	let name: String
	let initials: String?
	let accessoryView: () -> Accessory

	var body: some View {
		HStack {
			AvatarView(source: avatar, initials: initials)
				.frame(width: 36, height: 36)

			Text(name)
				.font(.gh(.subhead))
				.foregroundColor(.gh(.labelPrimary))

			Spacer()

			accessoryView()
		}
		.padding()
	}
}

struct FriendListItem_Previews: PreviewProvider {
	static var previews: some View {
		NavigationView {
			VStack(alignment: .leading, spacing: 0) {
				Text("Users ⤵️").font(.gh(.body)).padding()
				ForEach([User.fixture1, .fixture2]) { user in
					FriendFollowListItem(user: user)
					Divider()
				}

				ForEach([UserConnection.fixture1, .fixture2], id: \.friendListItemPreviewID) { connection in
					FriendConnectionListItem(connection: connection) {
						HStack {
							Image(systemName: "ellipsis")
								.accentColor(.gh(.labelTertiary))
								.padding(.trailing, 6)

							Button("Action", action: {})
								.primaryButtonStyle(size: .small, width: .fitContent)
						}
					}
					Divider()
				}

				Text("Contacts ⤵️").font(.gh(.body)).padding()
				ForEach([Contact.fixture1, .fixture2]) { contact in
					FriendInviteListItem(contact: contact)
					Divider()
				}

				Spacer()
			}
		}
		.injectPreviewsEnvironment()
		.accentColor(.gh(.onNavbar))
	}
}

private extension UserConnection {
	var friendListItemPreviewID: String {
		"\(user.id)\(kind.rawValue)\(status.rawValue)"
	}
}
