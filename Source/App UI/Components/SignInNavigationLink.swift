import SwiftUI

struct SignInNavigationLink<Label, Destination>: View where Label: View, Destination: View {
	let destination: Destination
	let label: () -> Label

	@State private var signInPrompt: Bool = false
	@EnvironmentObject var userState: UserState

	/// Creates an instance that presents `destination`.
	init(destination: Destination, @ViewBuilder label: @escaping () -> Label) {
		self.destination = destination
		self.label = label
	}

	var body: some View {
		if userState.isAuthenticated {
			NavigationLink(
				destination: destination,
				label: label
			)
		} else {
			Button(action: {
				signInPrompt = true
			}, label: label)
				.signInPrompt(isPresented: $signInPrompt)
		}
	}
}

struct SignInNavigationLink_Previews: PreviewProvider {
	static var previews: some View {
		NavigationView {
			VStack {
				SignInNavigationLink(destination: Text("DEST")) {
					Text("Button authenticated")
				}

				Divider()
				Divider()

				SignInNavigationLink(destination: Text("DEST")) {
					Text("Button anonymous")
				}
				.environmentObject(UserState(authToken: .init(nil)))
			}
		}
		.injectPreviewsEnvironment()
		.accentColor(.gh(.onNavbar))
	}
}
