import SwiftUI

struct AppMenuItem: Identifiable {
	var id: String {
		"\(title)\(image)\(String((title + image).hashValue))"
	}

	let title: String
	let image: String
	let action: () -> Void
}

struct AppMenuView: View {
	@Binding var isPresented: Bool
	let items: [AppMenuItem]

	@Environment(\.userInteractor) var userInteractor
	@EnvironmentObject var userState: UserState

	@State private var isStartingSecretGesture = false
	@State private var isShowingDevelopmentScreen = false

	var body: some View {
		VStack(alignment: .leading, spacing: 0) {
			ForEach(items) { item in
				Button(action: {
					item.action()
					isPresented = false
				}, label: {
					AppMenuItemView(title: item.title, image: item.image)
						.contentShape(Rectangle())
				})
				.buttonStyle(AppMenuButtonStyle())
			}

			Text(UIApplication.shared.fullApplicationVersion)
				.font(.gh(.footnote))
				.foregroundColor(.gh(.labelTertiary))
				.multilineTextAlignment(.center)
				.frame(maxWidth: .infinity)
				.fixedSize(horizontal: false, vertical: true)
				.padding(.top)
				.onLongPressGesture(minimumDuration: 5, perform: { isStartingSecretGesture = true })
				.simultaneousGesture(TapGesture(count: 3).onEnded {
					defer { isStartingSecretGesture = false }
					if isStartingSecretGesture {
						isShowingDevelopmentScreen = true
					}
				})
				.sheet(isPresented: $isShowingDevelopmentScreen, content: DeveloperSettings.init)
		}
		.padding(.bottom, 50)
		.background(Color.gh(.backgroundPrimary))
	}
}

private struct AppMenuButtonStyle: ButtonStyle {
	func makeBody(configuration: Configuration) -> some View {
		configuration.label.opacity(configuration.isPressed ? 0.5 : 1)
	}
}

struct AppMenuItemView: View {
	let title: String
	let image: String

	var body: some View {
		HStack {
			Image(systemName: image)
				.font(.system(.headline))
				.frame(width: 30, height: 30)

			Text(NSLocalizedString(title, comment: ""))
				.font(.gh(.body))
				.foregroundColor(.gh(.labelPrimary))

			Spacer()
		}
		.padding()

		Divider()
	}
}

struct AppMenuViewModifier: ViewModifier {
	@Binding var isPresented: Bool
	let items: [AppMenuItem]

	@ViewBuilder
	func body(content: Content) -> some View {
		content
			.overlay(dimOverlay())
			.overlay(menuOverlay())
			.ignoresSafeArea(edges: .bottom)
	}

	@ViewBuilder
	func dimOverlay() -> some View {
		if isPresented {
			Color.black.opacity(0.7)
				.contentShape(Rectangle())
				.onTapGesture {
					self.isPresented = false
				}
				.ignoresSafeArea()
		} else {
			EmptyView()
		}
	}

	@ViewBuilder
	func menuOverlay() -> some View {
		if isPresented {
			VStack(spacing: 0) {
				Spacer()

				RoundedRectangle(cornerRadius: 25, style: .circular)
					.fill(Color.gh(.backgroundPrimary))
					.frame(height: 30)
					.offset(y: 15)
					.elevationLevel3()

				AppMenuView(isPresented: $isPresented, items: items)
			}
			.transition(.asymmetric(insertion: .move(edge: .bottom), removal: .move(edge: .bottom)))
			.animation(.easeInOut)
		} else {
			EmptyView()
		}
	}
}

extension View {
	func appMenuBottomSheet(isPresented: Binding<Bool>, items: [AppMenuItem]) -> some View {
		modifier(AppMenuViewModifier(isPresented: isPresented, items: items))
	}
}

struct AppMenuOptionsView_Previews: PreviewProvider {
	struct TestView: View {
		@State var isPresented: Bool = false
		var items: [AppMenuItem] {
			[
				.init(title: "settings", image: "gear", action: {}),
				.init(title: "frequently_asked_questions", image: "map", action: {}),
				.init(title: "about_the_app", image: "cross", action: {}),
				.init(title: "sign_out", image: "arrow.right.square", action: {}),
			]
		}

		var body: some View {
			TabView {
				VStack(spacing: 0) {
					Rectangle()
						.foregroundColor(.purple)
						.onTapGesture {
							isPresented.toggle()
						}
				}
			}
			.appMenuBottomSheet(isPresented: $isPresented, items: items)
		}
	}

	static var previews: some View {
		NavigationView {
			TestView()
				.navigationBarTitleDisplayMode(.inline)
		}
		.injectPreviewsEnvironment()
	}
}
