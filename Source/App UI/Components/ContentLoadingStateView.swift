import SwiftUI

protocol ContentLoadingModel {
	associatedtype Content

	var content: ModelState<Content> { get }
	func loadIfNeeded()
}

protocol ContentPagingModel: ContentLoadingModel {
	var isPaginating: Bool { get }
	func itemAppeared(at index: Int)
}

protocol ContentRefreshableModel {
	func refresh()
}

struct ContentLoadingStateView<Model, NoContentView, ContentView>: View where Model: ContentLoadingModel & ObservableObject, NoContentView: View, ContentView: View {
	enum State {
		case initial
		case loading
		case content(Model.Content)
		case empty
		case failure(Error)
	}

	@ObservedObject var model: Model

	var buildContent: (Model.Content) -> ContentView
	var buildNoContent: () -> NoContentView

	var body: some View {
		switch internalState {
			case .initial:
				Color.gh(.backgroundPrimary)
					.onAppear(perform: model.loadIfNeeded)
			case .loading:
				// With our own spinner wrapped inside NavigationView
				// there's a weird animation from top corner to the middle (iOS 14).
				// Seems like a SwiftUI bug. So temporarily use system spinner
				// but check later if our own works good again and use that.
				// ProgressIndicator(size: .large)
				ProgressView()
					.padding()
					.frame(maxWidth: .infinity, maxHeight: .infinity)
			case let .content(content):
				contentView(with: content)
			case .empty:
				buildNoContent()
					.frame(maxWidth: .infinity, maxHeight: .infinity)
			case let .failure(error):
				VStack {
					Text(error.localizedDescription)
						.lineLimit(3)
						.multilineTextAlignment(.center)
						.font(.gh(.footnote))
						.foregroundColor(.gh(.utilityError))
						.padding(.top, 8)
						.fixedSize(horizontal: false, vertical: true)

					Button("try_again", action: {
						model.loadIfNeeded()
					})
					.quaternaryButtonStyle(size: .large, width: .fitContent)
					.padding()
				}
				.frame(maxWidth: .infinity, maxHeight: .infinity)
		}
	}

	fileprivate var internalState: State {
		switch model.content {
			case .none:
				return .initial
			case let .loading(previous: content):
				if let content = content {
					return .content(content)
				}
				return .loading
			case let .model(content):
				if (content as? [Any])?.isEmpty ?? false {
					return .empty
				}
				return .content(content)
			case let .error(error):
				return .failure(error)
		}
	}

	@ViewBuilder
	func contentView(with content: Model.Content) -> some View {
		buildContent(content)
	}
}

struct ListLoadingStateView<Model, Row, RowView, NoContentView>: View
	where Model: ContentPagingModel & ObservableObject,
	Model.Content == [Row],
	Row: Identifiable & Hashable,
	RowView: View,
	NoContentView: View
{
	@ObservedObject var model: Model

	@ViewBuilder var buildRow: (Row) -> RowView
	@ViewBuilder var buildNoContent: () -> NoContentView

	var contentInsets = EdgeInsets()

	var body: some View {
		ContentLoadingStateView(model: model, buildContent: { content in
			ScrollView {
				if let refreshAction = refreshAction() {
					PullToRefresh(isLoading: model.content.isLoading, coordinateSpace: "contentLoadingScrollView", action: refreshAction)
				}

				LazyVStack {
					ForEach(Array(content.enumerated()), id: \.element) { pair in
						buildRow(pair.element)
							.onAppear(perform: {
								model.itemAppeared(at: pair.offset)
							})
					}
					if model.isPaginating {
						ProgressIndicator(size: .small)
							.padding()
							.frame(maxWidth: .infinity)
					}
				}
				.padding(contentInsets)
			}
			.coordinateSpace(name: "contentLoadingScrollView")
		}, buildNoContent: buildNoContent)
	}

	private func refreshAction() -> (() -> Void)? {
		guard let refreshable = model as? ContentRefreshableModel else {
			return nil
		}
		return refreshable.refresh
	}
}

struct ContentLoadingStateView_Previews: PreviewProvider {
	class Model: ContentPagingModel, ObservableObject {
		struct Item: Identifiable, Hashable {
			let id: String
		}

		@Published var content: ModelState<[Item]> = .model([])
		var isPaginating: Bool = false
		func loadIfNeeded() {}
		func itemAppeared(at _: Int) {}
	}

	static var previews: some View {
		ListLoadingStateView(model: model) {
			Text("\($0.id)")
		} buildNoContent: {
			Text("Empty")
		}
	}

	static var model: Model {
		let model = Model()

//		model.content = .loading
//		model.content = .loading(previous: [.init(id: "prev1")])
//		model.content = .model([])
//		model.content = .model([.init(id: "s1"), .init(id: "s2")])
		model.content = .error(.fixture1)

		return model
	}
}
