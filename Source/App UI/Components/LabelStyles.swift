import SwiftUI

struct TrailingIconLabelStyle: LabelStyle {
	@ViewBuilder
	func makeBody(configuration: Configuration) -> some View {
		HStack {
			configuration.title
			configuration.icon
		}
	}
}
