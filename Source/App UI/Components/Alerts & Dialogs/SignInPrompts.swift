import SwiftUI

extension View {
	func signInPrompt(isPresented: Binding<Bool>) -> some View {
		self.modifier(SignInPromptModifier(isPresented: isPresented))
	}

	func sheetIfSignedIn<Destination: View>(isPresented: Binding<Bool>, @ViewBuilder content: @escaping () -> Destination) -> some View {
		self.modifier(SignInWrapperModifier(kind: .sheet, isPresented: isPresented, destination: content))
	}

	func fullScreenCoverIfSignedIn<Destination: View>(isPresented: Binding<Bool>, @ViewBuilder content: @escaping () -> Destination) -> some View {
		self.modifier(SignInWrapperModifier(kind: .fullScreenCover, isPresented: isPresented, destination: content))
	}
}

struct SignInPromptModifier: ViewModifier {
	@Binding var isPresented: Bool

	@ViewBuilder
	func body(content: Content) -> some View {
		ZStack {
			content
				.sheet(isPresented: $isPresented, content: {
					OnboardingView(onboardingFinished: { isPresented = false }, stage: .auth)
						.provideEnvironmentObjects()
				})
		}
	}
}

private struct SignInWrapperModifier<Destination: View>: ViewModifier {
	enum Kind {
		case sheet
		case fullScreenCover
	}

	let kind: Kind
	@Binding var isPresented: Bool
	var destination: () -> Destination

	@EnvironmentObject private var userState: UserState

	private var isAuthPresented: Binding<Bool> {
		.init(
			get: { isPresented && !userState.isAuthenticated },
			set: {
				if !$0 {
					isPresented = false
				}
			}
		)
	}

	private var isContentPresented: Binding<Bool> {
		.init(
			get: { isPresented && userState.isAuthenticated },
			set: { isPresented = $0 }
		)
	}

	@ViewBuilder
	func body(content: Content) -> some View {
		ZStack {
			switch kind {
				case .sheet:
					content
						.sheet(isPresented: isContentPresented, content: destination().provideEnvironmentObjects)
						.signInPrompt(isPresented: isAuthPresented)
				case .fullScreenCover:
					content
						.fullScreenCover(isPresented: isContentPresented, content: destination().provideEnvironmentObjects)
						.signInPrompt(isPresented: isAuthPresented)
			}
		}
	}
}

struct SignInPrompt_Previews: PreviewProvider {
	struct Container: View {
		@State var isPresented: Bool = false

		var body: some View {
			VStack {
				Button(action: {
					isPresented = true
				}, label: {
					Text("Present")
				})
				.fullScreenCoverIfSignedIn(isPresented: $isPresented, content: {
					Text("PRESENTED")
				})
			}
		}
	}

	static var previews: some View {
		NavigationView {
			VStack {
				Container()
			}
		}
		.injectPreviewsEnvironment()
	}
}
