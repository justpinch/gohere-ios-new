import SwiftUI

struct OpeningHoursView: View {
	let data: OpeningHours
	@State private var isExpanded = false

	var body: some View {
		VStack(alignment: .leading, spacing: 0) {
			Button(action: {
				withAnimation {
					isExpanded.toggle()
				}
			}, label: {
				HStack {
					Image(systemName: "clock.fill")
					Text("\(data.formattedToday())")
					Image(systemName: isExpanded ? "chevron.up" : "chevron.down")
				}
			})
			.quinaryButtonStyle(size: .small, width: .fitContent)
			.padding(.bottom, CGFloat(isExpanded ? 10 : 0))

			if isExpanded {
				if data.isAlwaysOpen {
					Text("always_open")
						.font(.gh(.footnote))
						.padding(.bottom, 6)
						.padding(.leading)
						.transition(.opacity)
				} else {
					ForEach(data.formattedDays(), id: \.day) { day in
						HStack(alignment: .top) {
							Text(day.day)
								.frame(minWidth: 120, alignment: .leading)
							Text(day.times)
						}
						.font(.gh(.footnote).weight(day.isToday ? .bold : .regular))
						.padding(.bottom, 6)
						.padding(.leading)
					}
					.transition(.opacity)
				}
			}
		}
	}
}

struct OpeningHoursView_Previews: PreviewProvider {
	static var previews: some View {
		OpeningHoursView(data: OpeningHours(periods: [
			.init(open: .init(day: 1, time: "1700"), close: .init(day: 1, time: "2000")),
			.init(open: .init(day: 2, time: "1000"), close: nil),
			.init(open: .init(day: 3, time: "1000"), close: .init(day: 1, time: "1230")),
			.init(open: .init(day: 3, time: "1500"), close: .init(day: 1, time: "1830")),
			.init(open: .init(day: 4, time: "1700"), close: .init(day: 1, time: "2000")),
		]))
	}
}
