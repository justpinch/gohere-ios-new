import SwiftUI

struct ReviewReportView: View {
	let review: Review

	@State var reason: ReviewReportReason = .other
	@State var loadingState: LoadingState = .none

	@Environment(\.apiClient.services.moderation) var moderation
	@Environment(\.presentationMode) var presentationMode

	var body: some View {
		VStack(alignment: .leading, spacing: 16) {
			Text("review")
				.foregroundColor(.gh(.labelPrimary))
				.font(.gh(.title2))

			ReviewLargeView(review: review,
			                actions: EmptyView.init)

			switch loadingState {
				case .none:
					reasonPicker()
				case .loading:
					ProgressView()
						.frame(maxWidth: .infinity, maxHeight: .infinity)
				case .finished:
					VStack(spacing: 10) {
						Image(systemName: "checkmark.circle")
							.foregroundColor(.gh(.utilityPositive))
							.font(.system(size: 30))

						Text("this_review_has_been_reported")
							.font(.gh(.body))
							.foregroundColor(.gh(.labelPrimary))
							.multilineTextAlignment(.center)
							.fixedSize(horizontal: false, vertical: true)

						Text("review_report_message")
							.font(.gh(.footnote))
							.foregroundColor(.gh(.labelSecondary))
							.multilineTextAlignment(.center)
							.fixedSize(horizontal: false, vertical: true)
					}
					.frame(maxWidth: .infinity, maxHeight: .infinity)
				case let .error(error):
					reasonPicker()

					Text(error.localizedDescription)
						.font(.gh(.footnote))
						.foregroundColor(.gh(.utilityError))
						.lineLimit(5)
						.multilineTextAlignment(.center)
			}

			Spacer()

			if loadingState != .finished {
				Button(action: submit, label: {
					Text("submit")
				})
				.primaryButtonStyle()
			}
		}
		.disabled(loadingState.isLoading)
		.padding(30)
		.navigationTitle("report_review")
		.navigationBarItems(leading: closeButton(), trailing: submitButton())
		.navigationBarTitleDisplayMode(.inline)
	}

	@ViewBuilder
	private func reasonPicker() -> some View {
		Text("reason")
			.foregroundColor(.gh(.labelPrimary))
			.font(.gh(.title2))

		Picker(selection: $reason, label: Text("reason"), content: {
			ForEach(ReviewReportReason.allCases) { reason in
				Text(NSLocalizedString(reason.rawValue, comment: "")).tag(reason)
			}
		})
		.pickerStyle(SegmentedPickerStyle())
	}

	@ViewBuilder
	func closeButton() -> some View {
		Button(action: dismiss, label: {
			Image(systemName: "xmark")
				.font(.system(size: 14, weight: .semibold))
				.foregroundColor(.gh(.labelPrimary))
				.padding(8)
				.background(Circle().foregroundColor(.gh(.tintQuaternary)))
		})
	}

	@ViewBuilder
	func submitButton() -> some View {
		Button("submit", action: submit)
			.font(.gh(.subhead))
			.accentColor(.gh(.tintPrimary))
			.disabled(loadingState.isLoading || loadingState == .finished)
	}

	private func dismiss() {
		presentationMode.wrappedValue.dismiss()
	}

	private func submit() {
		moderation
			.reportReview(id: review.id, reason: reason)
			.assignLoadingState(binding: $loadingState)
	}
}

struct ReviewReportView_Previews: PreviewProvider {
	static var previews: some View {
		NavigationView {
			ReviewReportView(review: .fixture1, loadingState: .none)
		}
		.injectPreviewsEnvironment()
	}
}
