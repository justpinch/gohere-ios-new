import SwiftUI

struct ReviewLargeImagesView: View {
	let images: [ImagePath]

	private typealias IdentifiableIndex = IdentifiableWrapper<Int, Int>
	@State private var fullScreenIndex: IdentifiableIndex? = nil

	private let imageSize: CGFloat = 64
	private let maxImages = 3

	var body: some View {
		HStack(spacing: 2) {
			ForEach(0 ..< min(images.count, maxImages)) { idx in
				ZStack {
					RemoteImage(path: images[idx], size: imageSize)
						.placeholder { Color.gh(.backgroundSecondary) }
						.aspectRatio(contentMode: .fill)

					if images.count > 3, idx == maxImages - 1 {
						Color.black.opacity(0.35)
						Text("+\(images.count - 2)")
							.font(.gh(.subhead))
							.foregroundColor(.white)
					}
				}
				.frame(width: imageSize, height: imageSize)
				.clipped()
				.cornerRadius(3.0)
				.onTapGesture(perform: {
					fullScreenIndex = IdentifiableIndex(idx)
				})
			}
		}
		.sheet(item: $fullScreenIndex, content: imageGallery)
	}

	private func imageGallery(starting index: IdentifiableIndex) -> ImageGallery {
		let images = self.images.map {
			ImageGallery.Image(url: $0.url(for: .screenWidth), fallback: $0.originalSizeURL())
		}

		return ImageGallery(images: images, isFullScreen: true, selectedIndex: index.wrapped, closeAction: {
			fullScreenIndex = nil
		})
	}
}

struct ReviewLargeImagesView_Previews: PreviewProvider {
	static var previews: some View {
		ReviewLargeImagesView(images: [
			.fixture(.place1), .fixture(.place2), .fixture(.place3), .fixture(.place4),
		])
		.padding()
		.previewLayout(.sizeThatFits)
	}
}
