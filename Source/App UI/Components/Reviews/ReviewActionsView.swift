import SwiftUI

struct ReviewActionsEllipsisView: View {
	let review: Review

	@State private var isReporting: Bool = false
	@State private var isEditing: Bool = false
	@State private var isDeleting: Bool = false

	@EnvironmentObject var userState: UserState

	var body: some View {
		if userState.isAuthenticated {
			Menu {
				ReviewActionsButtonsView(review: review, isReporting: $isReporting, isEditing: $isEditing, isDeleting: $isDeleting)
			} label: {
				Image(systemName: "ellipsis")
					.padding([.leading, .trailing])
					.padding([.top, .bottom], 18)
			}
			.reviewActions(for: review, isReporting: $isReporting, isEditing: $isEditing, isDeleting: $isDeleting)
		}
	}
}

struct ReviewActionsButtonsView: View {
	let review: Review

	@Binding var isReporting: Bool
	@Binding var isEditing: Bool
	@Binding var isDeleting: Bool

	@EnvironmentObject var userState: UserState

	init(review: Review,
	     isReporting: Binding<Bool> = .constant(false),
	     isEditing: Binding<Bool> = .constant(false),
	     isDeleting: Binding<Bool> = .constant(false))
	{
		self.review = review
		self._isReporting = isReporting
		self._isEditing = isEditing
		self._isDeleting = isDeleting
	}

	var body: some View {
		if review.user.id == userState.userID {
			Button(action: { isDeleting = true }, label: {
				Label("remove_review", systemImage: "trash")
			})

			Button(action: { isEditing = true }, label: {
				Label("edit_review", systemImage: "square.and.pencil")
			})
		} else {
			Button(action: { isReporting = true }, label: {
				Label("report_review", systemImage: "flag")
			})
		}
	}
}

extension View {
	func reviewActions(for review: Review,
	                   isReporting: Binding<Bool> = .constant(false),
	                   isEditing: Binding<Bool> = .constant(false),
	                   isDeleting: Binding<Bool> = .constant(false)) -> some View
	{
		modifier(ReviewActionsModifier(review: review, isReporting: isReporting, isEditing: isEditing, isDeleting: isDeleting))
	}
}

struct ReviewActionsModifier: ViewModifier {
	let review: Review

	@Binding var isReporting: Bool
	@Binding var isEditing: Bool
	@Binding var isDeleting: Bool

	@State private var alertMessage: IdentifiableWrapper<String, String>? = nil

	@Environment(\.apiClient.services) var services
	@EnvironmentObject var userState: UserState

	func body(content: Content) -> some View {
		content
			.sheet(isPresented: $isEditing, content: editReviewView)
			.sheet(isPresented: $isReporting, content: reportReviewView)
			.alert(isPresented: $isDeleting, content: deleteReviewAlert)
			.background(EmptyView().alert(item: $alertMessage, content: alertWithMessage))
	}

	@ViewBuilder
	private func reportReviewView() -> some View {
		NavigationView {
			ReviewReportView(review: review)
		}
		.provideEnvironmentObjects()
		.accentColor(.gh(.onNavbar))
	}

	@ViewBuilder
	private func editReviewView() -> some View {
		NavigationView {
			ReviewEditView(model: .init(place: review.place, draft: DraftReview(from: review), reviewService: services.reviews), isShowingDismissButton: true)
		}
		.provideEnvironmentObjects()
		.accentColor(.gh(.onNavbar))
	}

	private func deleteReviewAlert() -> Alert {
		Alert(title: Text("are_you_sure_delete_review"),
		      primaryButton: .destructive(Text("remove_review"), action: deleteReview),
		      secondaryButton: .cancel())
	}

	private func deleteReview() {
		services.reviews
			.delete(id: review.id)
			.handleCompletionSuccess {
				let userInfo = [Review.Notification.Key.review: review]
				NotificationCenter.default.post(name: Review.Notification.deleted, object: nil, userInfo: userInfo)
			}
			.handleCompletionError {
				alertMessage = .init($0.localizedDescription)
			}
			.subscribeIgnoringEvents()
	}

	private func alertWithMessage(message: IdentifiableWrapper<String, String>) -> Alert {
		Alert(title: Text("error"), message: Text(message.wrapped), dismissButton: .default(Text("ok")))
	}
}

struct ReviewActionsView_Previews: PreviewProvider {
	static var previews: some View {
		NavigationView {
			VStack {
				ReviewActionsEllipsisView(review: .fixture1)
					.padding()

				ReviewActionsEllipsisView(review: .fixture1)
					.previewDarkTheme()
			}
		}
		.injectPreviewsEnvironment()
		.previewLayout(.sizeThatFits)
		.accentColor(.gh(.onNavbar))
	}
}
