import SwiftUI

struct ReviewLargeView<Actions: View>: View {
	let reviewItem: ReviewItem
	let onToggleTranslationClick: ((ReviewItem) -> ())?
	@ViewBuilder let actions: () -> Actions
	
	init(reviewItem: ReviewItem,
		 onToggleTranslationClick:((ReviewItem) -> ())?,
		 actions: @escaping () -> Actions) {
		self.reviewItem = reviewItem
		self.onToggleTranslationClick = onToggleTranslationClick
		self.actions = actions
	}
	
	init(review: Review, actions: @escaping () -> Actions) {
		self.init(
			reviewItem: ReviewItem(review: review, translation: nil, isTranslationVisible: false),
			onToggleTranslationClick: nil,
			actions: actions
		)
	}

	var body: some View {
		HStack(alignment: .top) {
			AvatarView(path: reviewItem.review.user.profile.avatar, initials: reviewItem.review.user.initials)
				.frame(width: 40, height: 40)
			VStack(alignment: .leading, spacing: 5) {
				HStack(alignment: .top) {
					VStack(alignment: .leading, spacing: 5) {
						Text(reviewItem.review.user.fullName)
							.font(.gh(.subhead))
							.foregroundColor(.gh(.labelPrimary))
						RatingView(rating: reviewItem.review.rating, activeColor: .gh(.labelPrimary))
							.frame(height: 10)
							.fixedSize()
					}

					Spacer()
					actions()
				}

				if let text = reviewItem.review.text, !text.isEmpty {
					HtmlText(text: reviewItem.translation != nil && reviewItem.isTranslationVisible ? reviewItem.translation!.text : text)
						.font(.gh(.callout))
						.foregroundColor(.gh(.labelPrimary))
						.padding(.top, 8)
					if let originalLanguage = reviewItem.translation?.originalLanguage,
					   let currentLocal = Locale.current.languageCode,
						reviewItem.isTranslationVisible {
						let languageName = Locale(identifier: currentLocal).localizedString(forLanguageCode: originalLanguage)!
						Text("translated_from_\(languageName)")
							.font(.gh(.footnote))
							.foregroundColor(.gh(.labelSecondary))
							.italic()
					}
					if let toggleTranslation = onToggleTranslationClick,
						Locale.current.languageCode != reviewItem.review.language {
						Button(action: {
							toggleTranslation(reviewItem)
						}) {
							Text(reviewItem.isTranslationVisible ? "show_original" : "translate")
								.font(.gh(.subhead))
								.foregroundColor(.gh(.labelPrimary))
						}
					}
				}

				if !reviewItem.review.images.isEmpty {
					ReviewLargeImagesView(images: reviewItem.review.images.map(\.path))
						.padding(.top, 8)
				}
			}

			Spacer(minLength: 0)
		}
		.padding(16)
		.background(Color.gh(.backgroundTertiary))
		.cornerRadius(8)
	}
}

struct ReviewLargeView_Previews: PreviewProvider {
	static var previews: some View {
		VStack(spacing: 20) {
			ReviewLargeView(reviewItem: .fixture1,
							onToggleTranslationClick: {_ in },
			                actions: { EmptyView() })

			ReviewLargeView(reviewItem: .fixture2,
							onToggleTranslationClick: {_ in },
			                actions: { Text("ººº") })

			ReviewLargeView(reviewItem: .fixture3,
							onToggleTranslationClick: {_ in },
			                actions: { EmptyView() })
		}
		.padding()
		.previewLayout(.sizeThatFits)

		VStack(spacing: 20) {
			ReviewLargeView(reviewItem: .fixture1,
							onToggleTranslationClick: {_ in },
			                actions: { Text("ººº") })

			ReviewLargeView(reviewItem: .fixture2,
							onToggleTranslationClick: {_ in },
			                actions: { EmptyView() })
		}
		.preferredColorScheme(.dark)
		.padding()
		.previewLayout(.sizeThatFits)
	}
}
