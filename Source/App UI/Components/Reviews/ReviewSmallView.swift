import SwiftUI

struct ReviewSmallView: View {
	let avatar: ImagePath?
	let initials: String
	var name: String?
	let rating: Int
	let text: String?
	let images: [ImagePath]

	var body: some View {
		HStack(alignment: .center) {
			AvatarView(path: avatar, initials: initials)
				.frame(width: 32, height: 32)
				.id(avatar)
			Spacer().frame(width: 10)
			VStack(alignment: .leading, spacing: 0) {
				HStack {
					if let name = name {
						Text(name)
							.font(.gh(.subhead))
							.foregroundColor(.gh(.labelPrimary))
					}
					RatingView(rating: rating, activeColor: .gh(.labelPrimary))
						.frame(height: 10)
						.fixedSize()
				}

				if let text = text?.stripHtmlTags(), !text.isEmpty {
					Text(text)
						.lineLimit(1)
						.minimumScaleFactor(0.85)
						.font(.gh(.caption1))
						.foregroundColor(.gh(.labelPrimary))
						.padding(.top, name == nil ? 4 : 0)
				}
			}

			Spacer(minLength: 0)
		}
		.padding(12)
		.background(Color.gh(.backgroundTertiary))
		.cornerRadius(8)
	}
}

struct ReviewSmallView_Previews: PreviewProvider {
	static var previews: some View {
		VStack(spacing: 20) {
			ReviewSmallView(avatar: .fixture(.avatar2),
			                initials: "IV",
			                name: "Ivan Vasic",
			                rating: 7,
			                text: "The kaassoufflé came with satésaus. Lekker hoor.",
			                images: [])

			ReviewSmallView(avatar: nil,
			                initials: "RR",
			                name: "Ronatello",
			                rating: 9,
			                text: nil,
			                images: [])

			ReviewSmallView(avatar: .fixture(.avatar1),
			                initials: "EP",
//			                name: "Emilie Peeters",
			                rating: 7,
			                text: "The kaassoufflé came with satésaus. Lekker hoor.",
			                images: [.fixture(.place1), .fixture(.place2), .fixture(.place3), .fixture(.place4)])
		}
		.padding()
		.previewLayout(.sizeThatFits)

		VStack(spacing: 20) {
			ReviewSmallView(avatar: .fixture(.avatar2),
			                initials: "IV",
			                name: "Ivan Vasic",
			                rating: 7,
			                text: "The kaassoufflé came with satésaus. Lekker hoor.",
			                images: [])

			ReviewSmallView(avatar: nil,
			                initials: "RR",
			                name: "Ronatello",
			                rating: 9,
			                text: nil,
			                images: [])
		}
		.preferredColorScheme(.dark)
		.padding()
		.previewLayout(.sizeThatFits)
	}
}
