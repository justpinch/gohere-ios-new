import iPhoneNumberField
import PhoneNumberKit
import SwiftUI

struct CredentialsInputView: View {
	enum Credential: Equatable, LocalizedStringConvertible, CaseIterable {
		case phone
		case email

		var localizableString: String {
			switch self {
				case .phone:
					return NSLocalizedString("phone", comment: "")
				case .email:
					return NSLocalizedString("email", comment: "")
			}
		}
	}

	@State var selection = Credential.phone
	let loadingState: LoadingState

	typealias Result = (credential: Credential, value: String)
	let continueAction: (Result) -> Void

	@State private var phoneNumber = ""
	@State private var email = ""

	private var isValid: Bool {
		switch selection {
			case .phone:
				return !phoneNumber.gh_trimmed().isEmpty
			case .email:
				return email.gh_trimmed().isValidEmail()
		}
	}

	var body: some View {
		VStack(spacing: 2) {
			SegmentedSelectView(items: Credential.allCases, selected: $selection)

			Group {
				switch selection {
					case .phone:
						iPhoneNumberField(text: $phoneNumber)
							.flagHidden(false)
							.flagSelectable(true)
							.prefixHidden(false)
							.autofillPrefix(true)
							.formatted(false)
					case .email:
						TextField(LocalizedStringKey("your_email"), text: $email, onCommit: submit)
							.keyboardType(.emailAddress)
							.autocapitalization(.none)
							.disableAutocorrection(true)
							.textContentType(.username)
				}
			}
			.font(.gh(.body))
			.frame(height: Theme.Font.body.info().size * 1.2)
			.padding()
			.background(Color.gh(.backgroundTertiary))
			.cornerRadius(8)

			if let error = loadingState.error {
				Text(error.localizedDescription)
					.lineLimit(3)
					.multilineTextAlignment(.center)
					.font(.gh(.footnote))
					.foregroundColor(.gh(.utilityError))
					.padding(.top, 8)
					.fixedSize(horizontal: false, vertical: true)
			}

			LoaderButton("continue", isLoading: loadingState.isLoading, action: submit)
				.primaryButtonStyle()
				.padding(.top, 30)
				.disabled(!isValid)
		}
		.frame(maxWidth: .infinity)
		.disabled(loadingState.isLoading)
	}

	private func submit() {
		guard isValid else { return }
		switch selection {
			case .phone:
				continueAction((selection, phoneNumber.gh_trimmed()))
			case .email:
				continueAction((selection, email.gh_trimmed()))
		}
	}
}

struct CredentialsInput_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			CredentialsInputView(loadingState: .none, continueAction: { _ in })
			CredentialsInputView(loadingState: .loading, continueAction: { _ in })
			CredentialsInputView(loadingState: .error(.fixture1), continueAction: { _ in })
			CredentialsInputView(loadingState: .finished, continueAction: { _ in })

			CredentialsInputView(selection: .email, loadingState: .none, continueAction: { _ in })
				.previewDarkTheme()
		}
		.previewLayout(.sizeThatFits)
	}
}
