import SwiftUI

struct AuthFlowLandingView: View {
	@Binding var next: AuthFlow.Route?
	@State var loadingState = LoadingState.none

	@EnvironmentObject var interactor: AuthFlowInteractor

	var body: some View {
		VStack(spacing: 30) {
			Spacer()

			LogoMarkRounded()

			Text("onboarding_landing_title".localizedOverrideable())
				.multilineTextAlignment(.center)
				.font(.gh(.title1))
				.foregroundColor(.gh(.labelPrimary))
				.fixedSize(horizontal: false, vertical: true)

			VStack(spacing: 20) {
				Button(action: {
					next = .signUp
				}, label: {
					Text("sign_up_for_free")
				})
				.primaryButtonStyle()

				Button(action: {
					interactor
						.continueWithApple()
						.assignLoadingState(binding: $loadingState)
				}, label: {
					Label("continue_with_apple", systemImage: "applelogo")
				})
				.buttonStyle(QuinaryButtonStyle())

				Button(action: {
					interactor
						.continueWithFacebook()
						.assignLoadingState(binding: $loadingState)
				}, label: {
					Text("continue_with_facebook")
				})
				.buttonStyle(QuinaryButtonStyle())

				Button("have_a_look_first", action: {
					interactor.finishFlow()
				})
				.buttonStyle(QuaternaryButtonStyle())
			}

			PlatformDisclaimerLabelView()
				.fixedSize(horizontal: false, vertical: true)
				.padding([.leading, .trailing])
				.ignoresSafeArea(.keyboard, edges: .bottom)

			Button(action: {
				next = .signIn
			}, label: {
				Group {
					Text("already_have_an_account")
						.foregroundColor(.gh(.labelSecondary))

					Text("sign_in")
						.bold()
						.foregroundColor(.gh(.tintPrimary))
				}
				.font(.gh(.footnote))
			})

			Spacer()
		}
		.padding([.leading, .trailing], 48)
		.overlay(loadingOverlay().ignoresSafeArea())
		.alert(item: alertError(), content: { error -> Alert in
			Alert(title: Text(error.wrapped.localizedDescription))
		})
	}

	@ViewBuilder
	private func loadingOverlay() -> some View {
		if loadingState.isLoading {
			ZStack {
				Color.black.opacity(0.85)
				ProgressView()
					.progressViewStyle(CircularProgressViewStyle(tint: .white))
			}
		} else {
			EmptyView()
		}
	}

	private func alertError() -> Binding<AlertError?> {
		.init(
			get: { loadingState.error?.wrap() },
			set: { if $0 == nil { loadingState = .none } }
		)
	}
}

struct LandingView_Previews: PreviewProvider {
	static var previews: some View {
		ForEach(ColorScheme.allCases, id: \.self) {
			AuthFlowLandingView(next: .constant(nil), loadingState: .none)
				.preferredColorScheme($0)

			AuthFlowLandingView(next: .constant(nil), loadingState: .loading)
				.preferredColorScheme($0)
		}
		.injectPreviewsEnvironment()
	}
}

private struct AlertError: Error, Identifiable {
	var id: String { wrapped.reflectedString }
	let wrapped: Error
}

private extension Error {
	func wrap() -> AlertError {
		AlertError(wrapped: self)
	}
}
