import Combine
import SwiftUI

struct VerifyPhoneTokenView: View {
	let phoneNumber: String
	@Binding var next: AuthFlow.Route?

	@EnvironmentObject var interactor: AuthFlowInteractor

	var body: some View {
		TVPTokenVerifyView(phoneNumber: phoneNumber, onTokenVerified: onTokenVerified)
	}

	private func onTokenVerified(_ token: String) -> ApiResponsePublisher<Void> {
		let credentials = AuthFlow.CredentialsPhone(phone: phoneNumber, tvpToken: token)

		return interactor
			.authenticate(with: credentials)
			.replaceError(with: (), if: { error in
				if error.statusCode == 403 {
					// in case we get 403, user does not exist – continue sign up
					next = .createUser(credentials: .phone(credentials))
					return true
				}
				return false
			})
			.eraseToAnyPublisher()
	}
}

struct TVPTokenVerifyView: View {
	let phoneNumber: String
	var onTokenVerified: (String) -> ApiResponsePublisher<Void>

	@State var code: String = ""
	@State var verifyCodeState = LoadingState.none
	@State var resendCodeState = LoadingState.none

	@StateObject private var counter = CountDown()

	@Environment(\.apiClient.services.verifications) var verificationService

	private let codeLength = 6
	@State private var waitPeriod = 30 // seconds
	private let waitPeriodIncrease = 1.5 // every next retry wait 1.5x longer

	private var interactionDisabled: Bool {
		verifyCodeState.isLoading || resendCodeState.isLoading
	}

	var body: some View {
		VStack(alignment: .leading, spacing: 24) {
			VStack(alignment: .leading, spacing: 12) {
				Text("verify_your_account")
					.foregroundColor(.gh(.labelPrimary))
					.font(.gh(.title1))

				Text("Enter code that was just sent to you on \(phoneNumber)")
					.foregroundColor(.gh(.labelSecondary))
					.font(.gh(.callout))
			}

			VStack(alignment: .leading, spacing: 8) {
				PinCodeView(code: $code, onCommit: verifyCode)

				switch verifyCodeState {
					case .none:
						EmptyView()
					case .loading:
						HStack {
							ProgressIndicator(size: .small)
							Text("code_is_being_checked")
								.font(.gh(.footnote))
								.foregroundColor(.gh(.labelSecondary))
						}
					case let .error(error):
						Text(formatterCodeError(error))
							.foregroundColor(.gh(.utilityError))
							.font(.gh(.footnote))
							.fixedSize(horizontal: false, vertical: true)
					case .finished:
						HStack(spacing: 4) {
							Image(systemName: "checkmark")
								.foregroundColor(Color(.systemGreen))
							Text("success")
								.font(.gh(.footnote))
								.foregroundColor(.gh(.labelSecondary))
						}
				}
			}

			VStack(alignment: .leading, spacing: 8) {
				Button(action: resendCode, label: {
					Text("resend_verification_code")
					if counter.isCounting {
						Text("(\(counter.count))")
					}
				})
				.accentColor(.gh(.labelSecondary))
				.font(Font.gh(.footnote).bold())
				.disabled(counter.isCounting)

				if resendCodeState.isLoading {
					HStack {
						ProgressIndicator(size: .small)
						Text("sending_phone_code")
							.font(.gh(.footnote))
							.foregroundColor(.gh(.labelSecondary))
					}
				}

				if let error = resendCodeState.error {
					Text(error.localizedDescription)
						.foregroundColor(.gh(.utilityError))
						.font(.gh(.footnote))
						.fixedSize(horizontal: false, vertical: true)
				}
			}
		}
		.padding([.leading, .trailing], 48)
		.onChange(of: code, perform: { _ in
			clearErrors()
		})
		.disabled(interactionDisabled)
	}

	private func verifyCode() {
		verificationService
			.verifyCode(code, for: phoneNumber)
			.flatMap { token in
				onTokenVerified(token)
			}
			.assignLoadingState(binding: $verifyCodeState)
	}

	private func resendCode() {
		verificationService
			.createCode(phoneNumber: phoneNumber)
			.handleEvents(receiveCompletion: { _ in
				counter.start(from: waitPeriod)
				waitPeriod = Int(Double(waitPeriod) * waitPeriodIncrease)
			})
			.assignLoadingState(binding: $resendCodeState)
	}

	private func clearErrors() {
		if resendCodeState.error != nil {
			resendCodeState = .none
		}

		if verifyCodeState.error != nil {
			verifyCodeState = .none
		}
	}

	private func formatterCodeError(_ error: API.Error) -> String {
		if error.statusCode == 400 {
			return NSLocalizedString("invalid_code_entered", comment: "")
		}

		return error.localizedDescription
	}
}

struct TVPTokenVerifyView_Previews: PreviewProvider {
	static var error: API.Error {
		API.Error(reason: NSError(domain: "", code: 1, userInfo: nil))
	}

	static func publisher(_: String) -> ApiResponsePublisher<Void> {
		Just(()).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	static var previews: some View {
		Group {
			TVPTokenVerifyView(phoneNumber: "+31234567890", onTokenVerified: publisher)
			TVPTokenVerifyView(phoneNumber: "+31234567890", onTokenVerified: publisher, verifyCodeState: .loading)
			TVPTokenVerifyView(phoneNumber: "+31234567890", onTokenVerified: publisher, verifyCodeState: .error(error))
			TVPTokenVerifyView(phoneNumber: "+31234567890", onTokenVerified: publisher, verifyCodeState: .finished)
			TVPTokenVerifyView(phoneNumber: "+31234567890", onTokenVerified: publisher, resendCodeState: .loading)
			TVPTokenVerifyView(phoneNumber: "+31234567890", onTokenVerified: publisher, resendCodeState: .error(error))
		}
		.previewLayout(.sizeThatFits)
		.injectPreviewsEnvironment()
	}
}
