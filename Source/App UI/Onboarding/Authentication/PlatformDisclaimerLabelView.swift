import SwiftUI

struct PlatformDisclaimerLabelView: View {
	enum Sheet: String, Identifiable {
		case terms
		case privacy

		var id: String { rawValue }
	}

	@State private var currentSheet: Sheet?

	var body: some View {
		Menu(content: {
			Button("privacy_policy", action: { currentSheet = .privacy })
			Button("user_agreement", action: { currentSheet = .terms })
		}, label: {
			Text("By continuing, you agree to your \(Text("User Agreement").bold()) and \(Text("Privacy Policy").bold())")
				.multilineTextAlignment(.center)
				.font(.gh(.footnote))
				.foregroundColor(.gh(.labelSecondary))
				.padding([.leading, .trailing])
		})
		.sheet(item: $currentSheet) { sheet in
			switch sheet {
				case .terms:
					SafariView(url: AppSettings.termsOfUseURL)
				case .privacy:
					SafariView(url: AppSettings.privacyPolicyURL)
			}
		}
	}
}

struct PlatformDisclaimerLabelView_Previews: PreviewProvider {
	static var previews: some View {
		PlatformDisclaimerLabelView()
			.previewLayout(.sizeThatFits)
	}
}
