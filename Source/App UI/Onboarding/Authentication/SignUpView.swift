import Combine
import SwiftUI

struct SignUpView: View {
	@Binding var next: AuthFlow.Route?
	@State var loadState: LoadingState = .none

	@EnvironmentObject var interactor: AuthFlowInteractor

	var body: some View {
		VStack(spacing: 25) {
			VStack(spacing: 20) {
				LogoMarkRounded()

				Text("Sign up for free with \(Image("gohere-logo-inline"))")
					.multilineTextAlignment(.leading)
					.font(.gh(.title1))
					.foregroundColor(.gh(.labelPrimary))
					.fixedSize(horizontal: false, vertical: true)
			}
			.padding([.leading, .trailing], 35)

			CredentialsInputView(loadingState: loadState, continueAction: handle(result:))

			PlatformDisclaimerLabelView()
				.fixedSize(horizontal: false, vertical: true)
				.padding([.leading, .trailing], 20)
				.ignoresSafeArea(.keyboard, edges: .bottom)
		}
		.padding([.leading, .trailing], 45)
	}

	private func handle(result: CredentialsInputView.Result) {
		switch result.credential {
			case .phone:
				interactor
					.sendPhoneVerificationCode(phoneNumber: result.value)
					.handleEvents(receiveCompletion: { completion in
						if case .finished = completion {
							next = .verifyPhoneCode(phone: result.value)
						}
					})
					.assignLoadingState(binding: $loadState)
			case .email:
				interactor
					.isExistingUser(with: result.value)
					.handleEvents(receiveOutput: { isExistingUser in
						next = isExistingUser ? .verifyPassword(email: result.value, fromSignUp: true) : .createPassword(email: result.value)
					})
					.assignLoadingState(binding: $loadState)
		}
	}
}

struct SignUpView_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			SignUpView(next: .constant(nil))
			SignUpView(next: .constant(nil))
				.previewDarkTheme()
		}
		.injectPreviewsEnvironment()
	}
}
