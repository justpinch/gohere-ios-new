import Combine
import SwiftUI

struct SignInView: View {
	@Binding var next: AuthFlow.Route?
	@State var loadState = LoadingState.none
	@EnvironmentObject var interactor: AuthFlowInteractor

	var body: some View {
		VStack(spacing: 30) {
			VStack(spacing: 20) {
				LogoMarkRounded()

				Text("sign_in")
					.multilineTextAlignment(.leading)
					.font(.gh(.title1))
					.foregroundColor(.gh(.labelPrimary))
			}
			.padding([.leading, .trailing], 35)

			CredentialsInputView(loadingState: loadState, continueAction: handle(result:))
		}
		.padding([.leading, .trailing], 45)
	}

	private func handle(result: CredentialsInputView.Result) {
		switch result.credential {
			case .phone:
				interactor
					.sendPhoneVerificationCode(phoneNumber: result.value)
					.handleEvents(receiveCompletion: { completion in
						if case .finished = completion {
							next = .verifyPhoneCode(phone: result.value)
						}
					})
					.assignLoadingState(binding: $loadState)
			case .email:
				next = .verifyPassword(email: result.value, fromSignUp: false)
		}
	}
}

struct SignInView_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			SignInView(next: .constant(nil))
			SignInView(next: .constant(nil))
				.previewDarkTheme()
		}
		.injectPreviewsEnvironment()
	}
}
