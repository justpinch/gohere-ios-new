import SwiftUI

struct AuthFlowRouterView: View {
	@State var route: AuthFlow.Route
	@State var destination: AuthFlow.Route? = nil

	var body: some View {
		ZStack {
			switch route {
				case .start:
					AuthFlowLandingView(next: $destination)
				case .signUp:
					SignUpView(next: $destination)
				case .signIn:
					SignInView(next: $destination)
				case let .verifyPhoneCode(phone):
					VerifyPhoneTokenView(phoneNumber: phone, next: $destination)
				case let .createPassword(email):
					CreatePasswordView(email: email, next: $destination)
				case let .verifyPassword(email, fromSignUp):
					VerifyPasswordView(email: email, showWhitelabelDisclaimer: fromSignUp)
				case let .createUser(credentials: credentials):
					CreateProfileView(credentials: credentials)
			}
		}
		.navigate(using: $destination, destination: { AuthFlowRouterView(route: $0) })
	}
}

struct RouterView_Previews: PreviewProvider {
	static var previews: some View {
		AuthFlowRouterView(route: .verifyPhoneCode(phone: "+31234567890"))
			.injectPreviewsEnvironment()
	}
}
