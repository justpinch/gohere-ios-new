import SwiftUI

struct ForgotPasswordView: View {
	@State var email: String = ""
	@State var loadingState = LoadingState.none
	let onFinished: () -> Void

	@Environment(\.apiClient.services.users) var userService

	var isValid: Bool {
		email.gh_trimmed().isValidEmail()
	}

	var body: some View {
		VStack(alignment: .leading, spacing: 30) {
			Text("reset_your_password")
				.multilineTextAlignment(.leading)
				.font(.gh(.title1))
				.foregroundColor(.gh(.labelPrimary))

			Text("reset_password_description")
				.multilineTextAlignment(.leading)
				.font(.gh(.footnote))
				.foregroundColor(.gh(.labelPrimary))

			VStack(alignment: .leading) {
				Text("enter_your_email")
					.foregroundColor(.gh(.labelPrimary))
					.font(.gh(.subhead))

				TextField(LocalizedStringKey("email"), text: $email, onCommit: submit)
					.textFieldStyle(GoHereTextFieldStyle())
					.textContentType(.emailAddress)
					.keyboardType(.emailAddress)
					.autocapitalization(.none)

				if let error = loadingState.error {
					Text(error.localizedDescription)
						.lineLimit(3)
						.font(.gh(.footnote))
						.foregroundColor(.gh(.utilityError))
						.fixedSize(horizontal: false, vertical: true)
				}
			}

			LoaderButton("send_reset_password", isLoading: loadingState.isLoading, action: submit)
				.primaryButtonStyle()
				.disabled(!isValid)
		}
		.padding([.leading, .trailing], 45)
	}

	private func submit() {
		guard isValid else { return }
		userService
			.resetPassword(for: email.gh_trimmed())
			.handleCompletionSuccess(onFinished)
			.assignLoadingState(binding: $loadingState)
	}
}

struct ForgotPasswordView_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			ForgotPasswordView(onFinished: {})
			ForgotPasswordView(loadingState: .loading, onFinished: {})
			ForgotPasswordView(loadingState: .error(.fixture1), onFinished: {})
				.previewDarkTheme()
			ForgotPasswordView(onFinished: {})
				.previewDarkTheme()
				.previewSupportedLocales()
		}
		.injectPreviewsEnvironment()
	}
}
