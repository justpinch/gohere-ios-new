import SwiftUI

struct VerifyPasswordView: View {
	@State var showWhitelabelDisclaimer: Bool
	@State var credentials: AuthFlow.CredentialsEmail
	@State var isValid: Bool = false
	@State var loadingState = LoadingState.none

	@State var isPresentingForgotPassword: Bool = false
	@State var isPresentingWhitelabelDisclaimer = false

	@EnvironmentObject var interactor: AuthFlowInteractor

	init(email: String, showWhitelabelDisclaimer: Bool) {
		self._credentials = State(initialValue: .init(email: email, password: ""))
		self._showWhitelabelDisclaimer = State(initialValue: showWhitelabelDisclaimer)
	}

	var body: some View {
		VStack(alignment: .leading, spacing: 30) {
			Spacer()

			if showWhitelabelDisclaimer {
				VStack(spacing: 20) {
					LogoMarkRounded()

					Text("Please sign in with \(Image("gohere-logo-inline"))")
						.multilineTextAlignment(.leading)
						.font(.gh(.title1))
						.foregroundColor(.gh(.labelPrimary))
						.fixedSize(horizontal: false, vertical: true)
				}
				.padding([.leading, .trailing], 35)

				VStack(alignment: .leading, spacing: 0) {
					Text("password_whitelabel_disclaimer")
						.font(Font.gh(.footnote))
						.accentColor(.gh(.labelPrimary))

					Spacer()
						.frame(height: 8)
						.frame(maxWidth: .infinity)

					Button(action: {
						isPresentingWhitelabelDisclaimer = true
					}, label: {
						Text("read_more")
							.font(Font.gh(.footnote).bold())
							.accentColor(.gh(.labelPrimary))

					})
					.sheet(isPresented: $isPresentingWhitelabelDisclaimer, content: {
						NavigationView {
							ExistingAccountDisclaimerView()
						}.accentColor(.gh(.onNavbar))
					})
				}
				.padding()
				.overlay(RoundedRectangle(cornerRadius: 8).strokeBorder(Color.gh(.backgroundSecondary)))
				.fixedSize(horizontal: false, vertical: true)

			} else {
				Text("verify_your_account")
					.multilineTextAlignment(.leading)
					.font(.gh(.title1))
					.foregroundColor(.gh(.labelPrimary))
			}

			VStack(alignment: .leading) {
				Text(showWhitelabelDisclaimer ? "enter_gohere_password" : "enter_your_password")
					.foregroundColor(.gh(.labelPrimary))
					.font(.gh(.subhead))

				PasswordTextField(text: $credentials.password, onCommit: submit)

				if let error = loadingState.error {
					Text(formatted(error: error))
						.lineLimit(3)
						.font(.gh(.footnote))
						.foregroundColor(.gh(.utilityError))
						.fixedSize(horizontal: false, vertical: true)
				}
			}

			LoaderButton("continue", isLoading: loadingState.isLoading, action: submit)
				.primaryButtonStyle()
				.disabled(!isValid)

			HStack {
				NavigationLink(
					destination: ForgotPasswordView(email: credentials.email, onFinished: { isPresentingForgotPassword = false }),
					isActive: $isPresentingForgotPassword,
					label: {
						Text("forgot_your_password")
							.font(Font.gh(.footnote).bold())
							.accentColor(.gh(.labelPrimary))
					}
				)
				Spacer()
			}

			Spacer()
		}
		.padding([.leading, .trailing], 45)
		.onChange(of: credentials, perform: validate)
	}

	private func formatted(error: API.Error) -> String {
		if error.statusCode == 401 {
			return NSLocalizedString("sign_in_failed_check_password", comment: "")
		}

		return error.localizedDescription
	}

	private func validate(credentials: AuthFlow.CredentialsEmail) {
		isValid = !credentials.password.isEmpty
	}

	private func submit() {
		validate(credentials: credentials)
		guard isValid else { return }

		interactor
			.authenticate(with: credentials)
			.assignLoadingState(binding: $loadingState)
	}
}

struct VerifyPasswordView_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			VerifyPasswordView(email: "ivan@gohere.app", showWhitelabelDisclaimer: true)
			VerifyPasswordView(email: "ivan@gohere.app", showWhitelabelDisclaimer: false)
			VerifyPasswordView(email: "ivan@gohere.app", showWhitelabelDisclaimer: false)
				.previewDarkTheme()
			VerifyPasswordView(email: "ivan@gohere.app", showWhitelabelDisclaimer: true)
				.previewDarkTheme()
		}
		.injectPreviewsEnvironment()
	}
}
