import SwiftUI

struct CreatePasswordView: View {
	@Binding var next: AuthFlow.Route?

	@State var credentials: AuthFlow.CredentialsEmail
	@State var isValid: Bool = false
	@State var loadState: LoadingState = .none

	init(email: String, next: Binding<AuthFlow.Route?>) {
		self._credentials = State(initialValue: .init(email: email, password: ""))
		self._next = next
	}

	var body: some View {
		VStack(alignment: .leading, spacing: 24) {
			VStack(alignment: .leading, spacing: 12) {
				Text("protect_your_account")
					.foregroundColor(.gh(.labelPrimary))
					.font(.gh(.title1))

				Text("enter_password_to_protect_account")
					.foregroundColor(.gh(.labelSecondary))
					.font(.gh(.callout))
			}

			VStack(alignment: .leading) {
				Text("enter_your_password")
					.foregroundColor(.gh(.labelPrimary))
					.font(.gh(.subhead))

				PasswordTextField(text: $credentials.password, onCommit: onSubmit)
			}

			LoaderButton("continue", isLoading: loadState.isLoading, action: onSubmit)
				.primaryButtonStyle()
				.disabled(!isValid)
		}
		.fixedSize(horizontal: false, vertical: true)
		.padding([.leading, .trailing], 48)
		.onChange(of: credentials, perform: validate)
	}

	private func validate(credentials: AuthFlow.CredentialsEmail) {
		isValid = credentials.password.count >= 10
	}

	private func onSubmit() {
		validate(credentials: credentials)
		guard isValid else { return }

		next = .createUser(credentials: .email(credentials))
	}
}

struct CreatePasswordView_Previews: PreviewProvider {
	static var previews: some View {
		CreatePasswordView(email: "ivan@gohere.app", next: .constant(nil))
			.injectPreviewsEnvironment()
	}
}
