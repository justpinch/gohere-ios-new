import SwiftUI

struct ExistingAccountDisclaimerView: View {
	@Environment(\.presentationMode) var presentationMode
	@State private var isShowingPrivacy: Bool = false

	var body: some View {
		ScrollView {
			VStack(alignment: .leading, spacing: 16) {
				Text("\(Config.appName) uses services by GoHere")
					.font(.gh(.title2))
					.foregroundColor(.gh(.labelPrimary))
					.fixedSize(horizontal: false, vertical: true)

				Text("whitelabel_disclaimer_1")
					.font(.gh(.subhead))
					.foregroundColor(.gh(.labelPrimary))
					.fixedSize(horizontal: false, vertical: true)

				Text("whitelabel_disclaimer_2")
					.font(.gh(.footnote))
					.foregroundColor(.gh(.labelSecondary))
					.fixedSize(horizontal: false, vertical: true)

				Text("whitelabel_disclaimer_3")
					.font(.gh(.footnote))
					.foregroundColor(.gh(.labelSecondary))
					.fixedSize(horizontal: false, vertical: true)

				Button("privacy_policy") {
					isShowingPrivacy = true
				}
				.font(.gh(.footnote))
			}
			.padding([.leading, .trailing], 40)
		}
		.navigationBarItems(leading: Button(action: dismiss, label: {
			Image(systemName: "xmark")
				.font(.system(size: 20, weight: .semibold))
				.foregroundColor(.gh(.labelPrimary))
				.padding(8)
		}))
		.fullScreenCover(isPresented: $isShowingPrivacy, content: {
			SafariView(url: AppSettings.privacyPolicyURL)
		})
	}

	private func dismiss() {
		presentationMode.wrappedValue.dismiss()
	}
}

struct ExistingAccountDisclaimerView_Previews: PreviewProvider {
	static var previews: some View {
		NavigationView {
			ExistingAccountDisclaimerView()
		}
	}
}
