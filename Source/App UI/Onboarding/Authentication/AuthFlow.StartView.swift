import SwiftUI

struct AuthFlow {
	enum Route {
		case start // Landing view
		case signUp // SignUp view
		case signIn // SignIn view
		case verifyPhoneCode(phone: String) // Code enter view
		case verifyPassword(email: String, fromSignUp: Bool) // Password enter view
		case createPassword(email: String) // Create password view
		case createUser(credentials: AuthFlow.Credentials) // New account onboarding
	}

	struct CredentialsPhone: Equatable {
		let phone: String
		let tvpToken: String
	}

	struct CredentialsEmail: Equatable {
		let email: String
		var password: String
	}

	enum Credentials {
		case email(CredentialsEmail)
		case phone(CredentialsPhone)
	}
}

struct AuthFlowStartView: View {
	@Environment(\.apiClient.services) var services
	@Environment(\.userInteractor) var userInteractor

	let route: AuthFlow.Route
	let onFinish: () -> Void

	var body: some View {
		NavigationView {
			AuthFlowRouterView(route: route)
				.navigationBarHidden(true)
				.navigationBarTitleDisplayMode(.inline)
		}
		.navigationViewStyle(StackNavigationViewStyle())
		.accentColor(.gh(.onNavbar))
		.environmentObject(createAuthFlowInteractor())
	}

	private func createAuthFlowInteractor() -> AuthFlowInteractor {
		AuthFlowInteractor(finishFlow: onFinish,
		                   userInteractor: userInteractor,
		                   authService: services.auth,
		                   userService: services.users,
		                   verificationsService: services.verifications)
	}
}

struct StartView_Previews: PreviewProvider {
	static var previews: some View {
		AuthFlowStartView(route: .start, onFinish: {})
	}
}
