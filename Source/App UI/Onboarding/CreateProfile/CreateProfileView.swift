import SwiftUI

struct CreateProfileView: View {
	let credentials: AuthFlow.Credentials

	@State var firstName: String = ""
	@State var lastName: String = ""
	@State var isPrivate: Bool = false
	@State var avatar: UIImage? = nil

	@State var loadingState = LoadingState.none
	@EnvironmentObject var interactor: AuthFlowInteractor

	var isValid: Bool { !firstName.isEmpty && !lastName.isEmpty }

	var firstNameError: String? {
		getErrorString(for: "first_name")
	}

	var lastNameError: String? {
		getErrorString(for: "last_name")
	}

	var body: some View {
		ScrollView {
				VStack(alignment: .leading, spacing: 30) {
					Text("make_it_personal")
						.lineLimit(3)
						.minimumScaleFactor(0.7)
						.font(.gh(.title1))
						.foregroundColor(.gh(.labelPrimary))
					
					VStack(alignment: .leading, spacing: 8) {
						Text("add_your_photo")
							.font(.gh(.subhead))
							.foregroundColor(.gh(.labelPrimary))
						
						AvatarPickerView(image: $avatar)
					}
					
					VStack(alignment: .leading, spacing: 8) {
						Text("enter_your_name")
							.foregroundColor(.gh(.labelPrimary))
							.font(.gh(.subhead))
						
						TextField(LocalizedStringKey("first_name"), text: $firstName, onCommit: {})
							.textFieldStyle(GoHereTextFieldStyle())
							.textContentType(.givenName)
						
						if let error = firstNameError {
							Text(error)
								.font(.gh(.footnote))
								.foregroundColor(.gh(.utilityError))
								.fixedSize(horizontal: false, vertical: true)
						}
						
						TextField(LocalizedStringKey("last_name"), text: $lastName, onCommit: {})
							.textFieldStyle(GoHereTextFieldStyle())
							.textContentType(.familyName)
						
						if let error = lastNameError {
							Text(error)
								.font(.gh(.footnote))
								.foregroundColor(.gh(.utilityError))
								.fixedSize(horizontal: false, vertical: true)
						}
					}
					
					VStack(alignment: .leading, spacing: 8) {
						Text("privacy")
							.foregroundColor(.gh(.labelPrimary))
							.font(.gh(.subhead))
						
						Menu(content: {
							Button("profile_privacy_select_private", action: { isPrivate = true })
							Button("profile_privacy_select_public", action: { isPrivate = false })
						}, label: {
							Label(isPrivate ? "profile_privacy_select_private" : "profile_privacy_select_public", systemImage: "chevron.down")
								.labelStyle(TrailingIconLabelStyle())
								.font(.gh(.caption1))
								.accentColor(.gh(.labelSecondary))
						})
						.animation(nil)
					}
					
					if loadingState.isLoading {
						ProgressIndicator(size: .small)
							.frame(maxWidth: .infinity)
					} else if let error = loadingState.error {
						Text(error.localizedDescription)
							.font(.gh(.footnote))
							.foregroundColor(.gh(.utilityError))
							.fixedSize(horizontal: false, vertical: true)
						
						if let errorDescription = getOtherErrors() {
							Text(errorDescription)
								.font(.gh(.footnote))
								.foregroundColor(.gh(.utilityError))
								.fixedSize(horizontal: false, vertical: true)
						}
					}
					
					Button("continue", action: submit)
						.primaryButtonStyle()
						.padding(.bottom)
						.disabled(!isValid)
				}
				.disabled(loadingState.isLoading)
				.padding([.leading, .trailing], 45)
		}
	}

	private func submit() {
		interactor.registerUser(credentials: credentials,
		                        avatar: avatar,
		                        firstName: firstName,
		                        lastName: lastName,
		                        isPrivate: isPrivate)
			.assignLoadingState(binding: $loadingState)

		switch credentials {
			case .email:
				Analytics.log(.register(kind: .credentials))
			case .phone:
				Analytics.log(.register(kind: .phone))
		}
	}

	private func getErrorString(for key: String) -> String? {
		guard let errors = (loadingState.error?.reason as? API.FormValidationError)?.fieldErrors[key] else {
			return nil
		}

		return errors.joined(separator: "\n • ")
	}

	private func getOtherErrors() -> String? {
		guard var errors = (loadingState.error?.reason as? API.FormValidationError)?.fieldErrors else {
			return nil
		}

		errors = errors.filter { !Set(["first_name", "last_name"]).contains($0.key) }
		guard !errors.isEmpty else {
			return nil
		}

		var string = ""

		errors.forEach { error in
			string += error.key + "\n • " + error.value.joined(separator: "\n • ")
		}

		return string
	}
}

struct CreateProfileView_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			CreateProfileView(credentials: .email(.init(email: "ivan@gohere.app", password: "password")))
			CreateProfileView(credentials: .email(.init(email: "ivan@gohere.app", password: "password")), loadingState: .loading)
			CreateProfileView(credentials: .email(.init(email: "ivan@gohere.app", password: "password")), loadingState: .error(.init(reason: NSError(domain: "", code: 1, userInfo: nil))))

			CreateProfileView(credentials: .email(.init(email: "ivan@gohere.app", password: "password")))
				.previewDarkTheme()
		}
	}
}
