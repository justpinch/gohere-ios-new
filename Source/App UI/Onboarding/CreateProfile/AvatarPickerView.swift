import SwiftUI

struct AvatarPickerView: View {
	@State private var imagePickerSource: UIImagePickerController.SourceType? = nil
	@State private var showingSourceSelector = false

	@Binding var image: UIImage?

	var body: some View {
		ImagePickerButton(image: $image) {
			Image(systemName: "camera.fill")
				.foregroundColor(.gh(.labelTertiary))
				.padding(28)
				.overlay(overlayImage())
				.background(
					Circle()
						.foregroundColor(.gh(.backgroundTertiary))
				)
		}
	}

	@ViewBuilder
	private func overlayImage() -> some View {
		if let image = image {
			Image(uiImage: image)
				.resizable()
				.aspectRatio(contentMode: .fill)
				.clipShape(Circle())
				.overlay(
					Circle()
						.strokeBorder()
						.foregroundColor(Color(.systemGray4))
				)
		} else {
			EmptyView()
		}
	}
}

struct AvatarPickerView_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			AvatarPickerView(image: .constant(nil))

			AvatarPickerView(image: .constant(UIImage(named: "avatar1")))

			AvatarPickerView(image: .constant(nil))
				.background(Color.black)
				.environment(\.colorScheme, .dark)

			AvatarPickerView(image: .constant(UIImage(named: "avatar2")))
				.background(Color.black)
				.environment(\.colorScheme, .dark)
		}
		.padding()
		.previewLayout(.sizeThatFits)
	}
}
