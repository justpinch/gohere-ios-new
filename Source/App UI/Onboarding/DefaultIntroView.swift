import SwiftUI

struct DefaultIntroView: View {
	@State var text: String = ""
	let introCompleted: () -> Void

	var segments: [LottieIntroView.AnimationSegment] {
		[
			.init(fromMarker: "StartCopy-1", toMarker: "EndCopy-1", text: "onboarding_animation_copy1".localizedFromTarget()),
			.init(fromMarker: "StartCopy-2", toMarker: "EndCopy-2", text: "onboarding_animation_copy2".localizedFromTarget()),
			.init(fromMarker: "StartCopy-3", toMarker: "EndCopy-3", text: "onboarding_animation_copy3".localizedFromTarget()),
		]
	}

	var body: some View {
		VStack {
			Spacer()

			LottieIntroView(name: "onboarding-animation", animationSegments: segments, segmentText: $text)

			Text(text)
				.font(.gh(.title2))
				.foregroundColor(.gh(.labelPrimary))
				.multilineTextAlignment(.center)
				.padding([.leading, .trailing], 48)
				.padding(.top, 35)

			Spacer()

			Button("start", action: introCompleted)
				.primaryButtonStyle()
				.padding([.leading, .trailing, .bottom])
		}
	}
}

struct DefaultIntroView_Previews: PreviewProvider {
	static var previews: some View {
		DefaultIntroView(introCompleted: {})
			.preferredColorScheme(.dark)
	}
}
