import SwiftUI

struct OnboardingView: View {
	let onboardingFinished: () -> Void

	enum OnboardingStage: Int, CaseIterable {
		case tour
		case auth
		case friends
		case review
	}

	@State var stage: OnboardingStage = .tour
	var authFlowStart: AuthFlow.Route = .start

	@Environment(\.apiClient.services) var services
	@Environment(\.userInteractor) var userInteractor
	@EnvironmentObject var userState: UserState

	var body: some View {
		Group {
			switch stage {
				case .tour:
					OnboardingIntroView(introCompleted: goToNextStage)
				case .auth:
					AuthFlowStartView(route: authFlowStart, onFinish: goToNextStage)
						.transition(.asymmetric(insertion: .move(edge: .trailing), removal: .move(edge: .leading)))
				case .friends:
					NavigationView {
						createFriendsView()
							.navigationBarTitleDisplayMode(.inline)
					}
					.transition(.asymmetric(insertion: .move(edge: .trailing), removal: .move(edge: .leading)))
					.accentColor(.gh(.onNavbar))
				case .review:
					NavigationView {
						OnboardingReviewView(model: .init(placeExternalService: services.placesExternal, reviewService: services.reviews, onFinish: goToNextStage))
							.navigationBarTitleDisplayMode(.inline)
					}
					.transition(.asymmetric(insertion: .move(edge: .trailing), removal: .move(edge: .leading)))
					.accentColor(.gh(.onNavbar))
			}
		}
		.animation(.default, value: stage)
	}

	private static func stage(after stage: OnboardingStage, isUserAuthenticated: Bool, hasReviews: Bool) -> OnboardingStage? {
		guard stage != OnboardingStage.allCases.last else {
			return nil
		}

		// we can't show user stuff unless they are authenticated
		if stage == .auth, !isUserAuthenticated {
			return nil
		}

		let nextStage = OnboardingStage.allCases[OnboardingStage.allCases.index(after: stage.rawValue)]

		// if prompting for 1st review and user already has reviews, skip to next stage
		if nextStage == .review, hasReviews {
			return Self.stage(after: nextStage, isUserAuthenticated: isUserAuthenticated, hasReviews: hasReviews)
		}

		return nextStage
	}

	private func goToNextStage() {
		guard let next = Self.stage(after: stage, isUserAuthenticated: userState.isAuthenticated, hasReviews: userState.user.reviewCount ?? 0 > 0) else {
			onboardingFinished()
			return
		}

		stage = next
	}

	private func createFriendsView() -> OnboardingFollowFriendsView {
		let userAuthChanged = userState.$userID.removeDuplicates().map { _ in }.eraseToAnyPublisher()
		let suggestionsModel = FriendSuggestionsModel(userChanged: userAuthChanged, userService: services.users)

		return OnboardingFollowFriendsView(
			finish: goToNextStage,
			userSearch: .init(
				searchAction: services.users.search,
				updateStatusAction: { userIDs in
					userInteractor.connections?.updateStatuses(for: userIDs)
				}
			),
			friendSuggestions: suggestionsModel
		)
	}
}

struct OnboardingView_Previews: PreviewProvider {
	static var previews: some View {
		OnboardingView(onboardingFinished: {})
			.injectPreviewsEnvironment()
	}
}
