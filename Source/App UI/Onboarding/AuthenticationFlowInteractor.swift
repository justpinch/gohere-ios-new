import AuthenticationServices
import Combine
import SwiftUI

final class AuthFlowInteractor: ObservableObject {
	let finishFlow: () -> Void

	private let userInteractor: UserInteractor

	private let authService: AuthService
	private let userService: UserService
	private let verificationsService: VerificationsService

	private var cancellables = Set<AnyCancellable>()

	init(finishFlow: @escaping () -> Void,
	     userInteractor: UserInteractor,
	     authService: AuthService,
	     userService: UserService,
	     verificationsService: VerificationsService)
	{
		self.finishFlow = finishFlow
		self.userInteractor = userInteractor
		self.authService = authService
		self.userService = userService
		self.verificationsService = verificationsService
	}

	func registerUser(credentials: AuthFlow.Credentials,
	                  avatar: UIImage?,
	                  firstName: String,
	                  lastName: String,
	                  isPrivate: Bool) -> ApiResponsePublisher<Void>
	{
		let registrationPublisher: ApiResponsePublisher<AuthToken>

		switch credentials {
			case let .email(credentials):
				registrationPublisher = authService.register(firstName: firstName, lastName: lastName, email: credentials.email, password: credentials.password)
			case let .phone(credentials):
				registrationPublisher = authService.register(firstName: firstName, lastName: lastName, phoneNumber: credentials.phone, tvpToken: credentials.tvpToken)
		}

		var authToken: AuthToken?
		var publisher = userInteractor.authenticate(with: registrationPublisher.handleEvents(receiveOutput: {
			// weird workaround to save token in local context...
			authToken = $0
		}).eraseToAnyPublisher())

		let userService = self.userService

		publisher = publisher
			.map { _ -> ApiResponsePublisher<Void> in
				guard let authToken = authToken else {
					return Just(()).setFailureType(to: API.Error.self).eraseToAnyPublisher()
				}

				return userService
					.update(isPrivate: isPrivate, avatar: avatar, for: authToken.userId)
					.map { _ in () }
					.eraseToAnyPublisher()
			}
			.switchToLatest()
			.eraseToAnyPublisher()

		return publisher
			.onCompleteFinishFlow(finishFlow)
			.eraseToAnyPublisher()
	}

	func sendPhoneVerificationCode(phoneNumber: String) -> ApiResponsePublisher<Void> {
		verificationsService.createCode(phoneNumber: phoneNumber)
	}

	func verifyPhoneCode(phoneNumber: String, code: String) -> ApiResponsePublisher<AuthFlow.CredentialsPhone> {
		verificationsService
			.verifyCode(code, for: phoneNumber)
			.map { .init(phone: phoneNumber, tvpToken: $0) }
			.eraseToAnyPublisher()
	}

	func continueWithApple() -> ApiResponsePublisher<Void> {
		userInteractor.authenticateWithApple()
			.handleEvents(receiveCompletion: { [weak self] completion in
				if case .finished = completion {
					self?.finishFlow()
				}
			})
			.replaceError(with: (), if: { ($0.reason as? ASAuthorizationError)?.code == .canceled })
			.eraseToAnyPublisher()
	}

	func continueWithFacebook() -> ApiResponsePublisher<Void> {
		userInteractor.authenticateWithFacebook()
			.handleEvents(receiveCompletion: { [weak self] completion in
				if case .finished = completion {
					self?.finishFlow()
				}
			})
			.replaceError(with: (), if: { ($0.reason as? FacebookError) == .cancelled })
			.eraseToAnyPublisher()
	}

	func isExistingUser(with email: String) -> ApiResponsePublisher<Bool> {
		userService.isEmailTaken(email)
	}

	func authenticate(with credentials: AuthFlow.CredentialsPhone) -> ApiResponsePublisher<Void> {
		userInteractor
			.authenticate(phoneNumber: credentials.phone, tvpToken: credentials.tvpToken)
			.onCompleteFinishFlow(finishFlow)
			.eraseToAnyPublisher()
	}

	func authenticate(with credentials: AuthFlow.CredentialsEmail) -> ApiResponsePublisher<Void> {
		userInteractor
			.authenticate(email: credentials.email, password: credentials.password)
			.onCompleteFinishFlow(finishFlow)
			.eraseToAnyPublisher()
	}
}

private extension Publisher where Output == Void {
	func onCompleteFinishFlow(_ finisher: @escaping () -> Void) -> Publishers.HandleEvents<Self> {
		handleCompletionSuccess(finisher)
	}
}
