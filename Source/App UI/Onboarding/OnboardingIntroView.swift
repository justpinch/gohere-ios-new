import SwiftUI

struct OnboardingIntroView: View {
	let introCompleted: () -> Void

	var body: some View {
		Config.onboardingIntroView(introCompleted: introCompleted)
	}
}

struct OnboardingIntroView_Previews: PreviewProvider {
	static var previews: some View {
		OnboardingIntroView(introCompleted: {})
	}
}
