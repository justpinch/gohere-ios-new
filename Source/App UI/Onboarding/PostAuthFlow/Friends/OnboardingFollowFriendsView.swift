import Combine
import SwiftUI

struct OnboardingFollowFriendsView: View {
	private let contentPadding: CGFloat = 45

	private var isAddressBookConnected: Bool {
		friendSuggestions.isAddressBookConnected
	}

	private var isFacebookConnected: Bool {
		friendSuggestions.isFacebookConnected
	}

	let finish: () -> Void
	@State var addressBookError: IdentifiableWrapper<Error, String>? = nil
	@State var facebookError: IdentifiableWrapper<Error, String>? = nil

	@StateObject var userSearch: UserSearchModel
	@StateObject var friendSuggestions: FriendSuggestionsModel

	var body: some View {
		VStack(spacing: 0) {
			ScrollView {
				VStack(alignment: .leading, spacing: 30) {
					VStack(alignment: .leading, spacing: 10) {
						Text("follow_your_best_friends")
							.font(.gh(.title1))
							.foregroundColor(.gh(.labelPrimary))

						Text("because_friends_know_whats_best")
							.font(.gh(.callout))
							.foregroundColor(.gh(.labelSecondary))
					}
					.fixedSize(horizontal: false, vertical: true)
					.padding([.leading, .top, .trailing], contentPadding)

					VStack(alignment: .leading) {
						Text("Find friends on \(Config.appName)")
							.foregroundColor(.gh(.labelPrimary))
							.font(.gh(.subhead))

						SearchTextField(placeholder: "name_of_your_friend", text: $userSearch.query)
					}
					.padding([.leading, .trailing], contentPadding)

					VStack(alignment: .leading) {
						if !userSearch.results.isEmpty {
							Text("Friends using \(Config.appName)")
								.foregroundColor(.gh(.labelPrimary))
								.font(.gh(.subhead))
								.padding([.leading, .trailing], contentPadding)

							LazyVStack(spacing: 0) {
								Divider()
								ForEach(userSearch.results) { user in
									FriendFollowListItem(user: user)
									Divider()
								}
							}
						}

						if friendSuggestions.suggestions.isLoading {
							ProgressView().frame(maxWidth: .infinity)
						}

						switch userSearch.searchState {
							case .none:
								EmptyView()
							case .loading:
								ProgressView()
									.frame(maxWidth: .infinity)
							case .finished:
								if userSearch.results.isEmpty, !userSearch.query.isEmpty {
									Text("no_results_found")
										.foregroundColor(.gh(.labelPrimary))
										.font(.gh(.footnote))
										.frame(maxWidth: .infinity)
										.padding([.leading, .trailing], contentPadding)
								}
							case let .error(error):
								Text(error.localizedDescription)
									.lineLimit(3)
									.multilineTextAlignment(.center)
									.font(.gh(.footnote))
									.foregroundColor(.gh(.utilityError))
									.frame(maxWidth: .infinity)
									.fixedSize(horizontal: false, vertical: true)
									.padding([.leading, .trailing], contentPadding)
						}
					}
					.animation(.easeInOut(duration: 0.1))

					if !isAddressBookConnected || !isFacebookConnected {
						VStack(alignment: .leading) {
							Text("Find more friends")
								.foregroundColor(.gh(.labelPrimary))
								.font(.gh(.subhead))

							if !isAddressBookConnected {
								contactsButton()
							}

							if !isFacebookConnected {
								facebookButton()
							}
						}
						.padding(.leading, contentPadding)
						.padding(.bottom, contentPadding)
					}
				}
			}

			FixedFooterBar(buttonTitle: "continue", action: finish)
		}
		.onAppear(perform: friendSuggestions.bindObservables)
		.onChange(of: friendSuggestions.suggestions, perform: { suggestions in
			userSearch.setSearchSuggestions(suggestions.model ?? [])
		})
		.navigationBarItems(trailing: Button("skip", action: finish).buttonStyle(NavbarButtonStyle()))
		.ignoresSafeArea(.keyboard)
	}

	@ViewBuilder
	private func contactsButton() -> some View {
		Button(action: {
			friendSuggestions.connectAddressBook { error in
				guard let error = error else { return }
				addressBookError = IdentifiableWrapper(error, identity: String(describing: error))
			}
		}, label: {
			Label("search_your_address_book", systemImage: "person.crop.circle")
		})
		.primaryButtonStyle(size: .small)
		.alert(item: $addressBookError) { error -> Alert in
			if error.wrapped as? ContactStoreError == ContactStoreError.unauthorized {
				return Alert(title: Text("cannot_access_contacts"),
				             message: Text("grant_contacts_access"),
				             primaryButton: .default(Text("settings"), action: {
				             	if let url = URL(string: UIApplication.openSettingsURLString) {
				             		UIApplication.shared.open(url, options: [:], completionHandler: nil)
				             	}
				             }),
				             secondaryButton: .cancel())
			} else {
				return Alert(title: Text(error.wrapped.localizedDescription), dismissButton: .cancel())
			}
		}
	}

	@ViewBuilder
	private func facebookButton() -> some View {
		Button(action: {
			friendSuggestions.connectFacebook { error in
				guard let error = error else { return }
				facebookError = IdentifiableWrapper(error, identity: String(describing: error))
			}
		}, label: {
			HStack {
				Image("facebook-icon")
					.resizable()
					.frame(width: 16, height: 16)
					.padding(.bottom, 1)

				Text("find_friends_on_facebook")
			}
		})
		.facebookButtonStyle(size: .small)
		.alert(item: $facebookError) { error -> Alert in
			Alert(title: Text(error.wrapped.localizedDescription), dismissButton: .cancel())
		}
	}
}

struct OnboardingFollowFriendsView_Previews: PreviewProvider {
	static var previews: some View {
		NavigationView {
			OnboardingFollowFriendsView(
				finish: {}, userSearch: .init(
					searchAction: { _ in
						Just(.init(count: 1, results: [.fixture1]))
							.setFailureType(to: API.Error.self)
							.eraseToAnyPublisher()
					},
					updateStatusAction: { _ in }
				),
				friendSuggestions: .stub
			)
		}.accentColor(.gh(.onNavbar))
	}
}
