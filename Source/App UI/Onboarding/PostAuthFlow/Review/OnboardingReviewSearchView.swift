import Combine
import SwiftUI

struct OnboardingReviewSearchView: View {
	var contentPadding: CGFloat = 48

	@State private var isTyping: Bool = false
	@Binding var selectedPlace: PlaceAutocomplete?
	@StateObject var model: ReviewPlaceSearchModel

	var body: some View {
		VStack(spacing: 20) {
			VStack(alignment: .leading) {
				Text("enter_name_restaurant_museum_bar")
					.foregroundColor(.gh(.labelPrimary))
					.font(.gh(.subhead))
					.fixedSize(horizontal: false, vertical: true)

				SearchTextField(placeholder: "name_of_the_place", text: $model.query, onEditingChanged: { isTyping = $0 })
			}
			.padding([.leading, .trailing], contentPadding)

			switch model.results {
				case .none:
					EmptyView()
				case .loading:
					ProgressView()
				case let .model(places):
					LazyVStack(spacing: 0, content: {
						ForEach(places, id: \.lookupId) { place in
							PlaceLookupResultView(title: place.title, subtitle: place.vicinity, icon: place.category?.icon)
								.frame(maxWidth: .infinity, alignment: .leading)
								.contentShape(Rectangle())
								.onTapGesture { selectedPlace = place }
							Divider()
						}
					})
				case let .error(error):
					Text(error.localizedDescription)
						.font(.gh(.footnote))
						.foregroundColor(.gh(.utilityError))
						.fixedSize(horizontal: false, vertical: true)
			}

			if isTyping {
				Spacer()
			}
		}
		.onAppear(perform: model.startObservingQuery)
	}
}

struct OnboardingReviewSearchView_Previews: PreviewProvider {
	static var previews: some View {
		OnboardingReviewSearchView(
			selectedPlace: .constant(nil),
			model:
			ReviewPlaceSearchModel(
				search: { _, _ in
					Just([.fixture1, .fixture2, .fixture3]).setFailureType(to: API.Error.self).eraseToAnyPublisher()
				},
				locationManager: .init(manager: PreviewLocationManager()),
				results: .none
			)
		)
	}
}
