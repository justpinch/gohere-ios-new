import SwiftUI

struct OnboardingReviewPlaceView: View {
	let model: ModelState<Place>
	let onPlaceRemove: () -> Void

	@State private var rowHeight: CGFloat = 0

	init(model: ModelState<Place>, onPlaceRemove: @escaping () -> Void = {}) {
		self.model = model
		self.onPlaceRemove = onPlaceRemove
	}

	var body: some View {
		switch model {
			case .none:
				EmptyView()
			case .loading:
				CardPlaceSmall.loadingPlaceholder()
					.transition(.slide)
			case let .model(place):
				HStack(spacing: 12) {
					CardPlaceSmall(place: place)
						.transition(.scale)

					Button(action: onPlaceRemove, label: {
						Image(systemName: "multiply.circle.fill")
							.imageScale(.large)
							.foregroundColor(.gh(.labelQuaternary))
					})
					.padding(4)
				}
			case let .error(error):
				Text(error.localizedDescription)
					.font(.gh(.footnote))
					.foregroundColor(.gh(.utilityError))
					.fixedSize(horizontal: false, vertical: true)
		}
	}
}

struct OnboardingReviewPlaceView_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			OnboardingReviewPlaceView(model: .loading)
			OnboardingReviewPlaceView(model: .model(.fixture1))
			OnboardingReviewPlaceView(model: .error(.fixture1))
			OnboardingReviewPlaceView(model: .model(.fixture2))
		}
		.previewLayout(.fixed(width: 380, height: 160))
	}
}
