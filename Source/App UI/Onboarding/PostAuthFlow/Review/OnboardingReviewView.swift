import SwiftUI

struct OnboardingReviewView: View {
	private let contentPadding: CGFloat = 48

	@StateObject var model: OnboardingReviewModel
	@Environment(\.apiClient.services.placesExternal) var placesExternalService

	var body: some View {
		ScrollView {
			VStack(alignment: .leading, spacing: 20) {
				VStack(alignment: .leading, spacing: 20) {
					Text("share_your_first_tip")
						.font(.gh(.title1))
						.foregroundColor(.gh(.labelPrimary))

					Text("recommend_nice_place_to_visit")
						.font(.gh(.callout))
						.foregroundColor(.gh(.labelSecondary))

					OnboardingReviewPlaceView(model: model.importPlace, onPlaceRemove: {
						withAnimation {
							model.selectedPlace = nil
						}
					})
					.padding(.trailing, -contentPadding / 1.8) // weird formatting by design :|)

					if model.importPlace != .none {
						OnboardingReviewRateView(rating: $model.rating)

						Button(action: model.submitReview, label: {
							if model.review.isLoading {
								ProgressView()
							} else {
								Text("continue")
							}
						})
						.primaryButtonStyle()
						.disabled(!model.canSubmitReview)

						if let error = model.review.error {
							Text(error.localizedDescription)
								.font(.gh(.footnote))
								.foregroundColor(.gh(.utilityError))
								.fixedSize(horizontal: false, vertical: true)
						}
					}
				}
				.padding([.leading, .trailing], contentPadding)

				if model.selectedPlace == nil {
					OnboardingReviewSearchView(contentPadding: contentPadding,
					                           selectedPlace: $model.selectedPlace,
					                           model: ReviewPlaceSearchModel(search: placesExternalService.autocomplete))
				}
			}
			.padding(.top, 45)

			Spacer()
		}
		.navigationBarItems(trailing: Button("skip", action: model.finish).buttonStyle(NavbarButtonStyle()))
		.disabled(model.isLoading)
		.onAppear(perform: model.bindObservables)
	}
}

struct OnboardingReviewView_Previews: PreviewProvider {
	static var previews: some View {
		NavigationView {
			OnboardingReviewView(model: .init(placeExternalService: PreviewPlaceExternalService(), reviewService: PreviewReviewService(), onFinish: {}))
		}
		.injectPreviewsEnvironment()
	}
}
