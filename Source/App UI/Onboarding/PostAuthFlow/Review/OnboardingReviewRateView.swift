import SwiftUI

struct OnboardingReviewRateView: View {
	@Binding var rating: Int?
	@EnvironmentObject var userState: UserState

	var body: some View {
		VStack(alignment: .leading) {
			Text("how_would_you_rate_it")
				.font(.gh(.subhead))
				.foregroundColor(.gh(.labelPrimary))

			HStack(alignment: .center, spacing: 16) {
				AvatarView(path: userState.user.profile?.avatar, initials: userState.user.initials)
					.frame(width: 40, height: 40)

				RatingInputView(rating: $rating)
					.frame(height: 32)
			}
		}
	}
}

struct OnboardingReviewRateView_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			OnboardingReviewRateView(rating: .constant(4))
		}
		.previewLayout(.fixed(width: 380, height: 160))
		.injectPreviewsEnvironment()
	}
}
