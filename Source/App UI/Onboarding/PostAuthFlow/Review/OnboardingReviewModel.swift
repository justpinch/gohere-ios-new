import Combine
import SwiftUI

final class OnboardingReviewModel: ObservableObject {
	@Published var selectedPlace: PlaceAutocomplete? = nil
	@Published var importPlace: ModelState<Place> = .none
	@Published var review: ModelState<Review> = .none
	@Published var rating: Int? = nil

	private let placeExternalService: PlaceExternalService
	private let reviewService: ReviewService
	private var cancellables = Set<AnyCancellable>()

	let finish: () -> Void

	var isLoading: Bool {
		importPlace.isLoading || review.isLoading
	}

	var canSubmitReview: Bool {
		importPlace.model?.id != nil && rating != nil
	}

	init(placeExternalService: PlaceExternalService, reviewService: ReviewService, onFinish: @escaping () -> Void) {
		self.placeExternalService = placeExternalService
		self.reviewService = reviewService
		self.finish = onFinish
	}

	func bindObservables() {
		cancellables.removeAll()

		$selectedPlace
			.compactMap { [unowned self] place -> PlaceAutocomplete? in
				guard let place = place else {
					self.importPlace = .none
					self.review = .none
					return nil
				}

				return place
			}
			.map { [unowned self] in
				self.placeExternalService
					.import(lookupId: $0.lookupId, providerId: $0.providerId)
					.handleCompletionError { _ in self.selectedPlace = nil } // clear selected place on errors
					.mapModelState()
			}
			.switchToLatest()
			.assign(to: \.importPlace, onUnowned: self)
			.store(in: &cancellables)
	}

	func submitReview() {
		guard let placeID = importPlace.id, let rating = rating else {
			assertionFailure()
			return
		}

		reviewService
			.create(place: placeID, imageIds: [], rating: rating, text: "")
			.handleCompletionSuccess { [weak self] in self?.finish() }
			.mapModelState()
			.assign(to: \.review, onUnowned: self)
			.store(in: &cancellables)
	}
}
