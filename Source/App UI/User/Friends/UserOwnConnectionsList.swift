import SwiftUI

struct UserOwnConnectionsList: View {
	@StateObject var model: UserOwnConnectionsModel

	@EnvironmentObject var userState: UserState
	@Environment(\.userInteractor) var userInteractor

	@State private var rejectingRequest: IdentifiableWrapper<UserConnection, String>?

	let margins = EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20)

	var requests: [UserConnection] {
		userState.connectionRequests.filter { $0.kind == model.kind }
	}

	var body: some View {
		ScrollView {
			LazyVStack(alignment: .leading, spacing: 0) {
				if model.kind == .follower, !requests.isEmpty {
					Text(model.requestsTitle)
						.font(.gh(.caption1))
						.foregroundColor(.gh(.labelSecondary))
						.padding([.top, .bottom])
						.padding(margins)

					ForEach(requests, id: \.user.id) { connection in
						FriendConnectionListItem(connection: connection) {
							connectionRequestAccessoryView(for: connection)
						}
						Divider()
					}
				}

				Text(model.connectionsTitle)
					.font(.gh(.caption1))
					.foregroundColor(.gh(.labelSecondary))
					.padding([.top, .bottom])
					.padding(margins)
					.onAppear(perform: {
						// when caption is shown, load from index 0 (if needed)
						model.loadConnectionsIfNeeded()
					})

				switch model.connections {
					case .none:
						EmptyView()
					case let .loading(connections):
						if let connections = connections {
							connectionsList(connections)
						} else {
							ProgressIndicator(size: .large)
								.padding()
								.frame(maxWidth: .infinity)
						}
					case let .model(connections):
						connectionsList(connections)
					case let .error(error):
						Text(error.localizedDescription)
							.lineLimit(3)
							.multilineTextAlignment(.center)
							.font(.gh(.footnote))
							.foregroundColor(.gh(.utilityError))
							.fixedSize(horizontal: false, vertical: true)
							.frame(maxWidth: .infinity)
							.padding()
				}
			}
		}
	}

	@ViewBuilder
	private func connectionsList(_ connections: [UserConnection]) -> some View {
		Group {
			ForEach(Array(connections.enumerated()), id: \.element.user.id) { item in
				FriendFollowListItem(user: item.element.user)
					.onAppear(perform: {
						model.appearedConnection(at: item.offset)
					})
				Divider()
			}

			if model.isPaginating {
				ProgressIndicator(size: .large)
					.padding()
					.frame(maxWidth: .infinity)
			}
		}
	}

	@ViewBuilder
	private func connectionRequestAccessoryView(for connection: UserConnection) -> some View {
		HStack {
			Button(action: {
				rejectingRequest = IdentifiableWrapper(connection, identity: \.user.id)
			}, label: {
				Image(systemName: "ellipsis")
			})
			.accentColor(.gh(.labelTertiary))
			.padding(10)
			.padding([.top, .bottom], 6)
			.actionSheet(item: $rejectingRequest, content: rejectRequestActionSheet)

			Button(action: {
				userInteractor.connections?.acceptFollowRequest(for: connection)
			}, label: {
				Text("accept")
			})
			.primaryButtonStyle(size: .small, width: .fitContent)
		}
	}

	private func rejectRequestActionSheet(_ connection: IdentifiableWrapper<UserConnection, String>) -> ActionSheet {
		ActionSheet(title: Text(connection.wrapped.user.fullName), message: nil, buttons: [
			.default(Text("ignore"), action: {
				userInteractor.connections?.ignoreFollowRequest(for: connection.wrapped)
			}),

			.destructive(Text("block"), action: {
				userInteractor.connections?.blockFollowRequest(for: connection.wrapped)
			}),

			.cancel(),
		])
	}
}

struct UserOwnConnectionsList_Previews: PreviewProvider {
	static var previews: some View {
		NavigationView {
			UserOwnConnectionsList(model: .stubFollowers)
		}
		.injectPreviewsEnvironment()
	}
}
