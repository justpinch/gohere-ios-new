import SwiftUI

struct UserOtherConnectionsListView: View {
	@StateObject var model: UserOtherConnectionsListModel

	var body: some View {
		VStack(spacing: 0) {
			/// ⚠️ WORKAROUND:
			/// For some reason the view does not re-render on model change, I could not figure out why
			/// I'm signing it off as a SwiftUI bug, this little invisible text forces re-render.
			/// Do check in future releases if this can be removed (along with the parent VStack)
			Text("\(model.content.model?.count ?? 0)")
				.frame(height: 0)
				.hidden()

			ListLoadingStateView(model: model) { item in
				FriendFollowListItem(user: item.connection.user)
				Divider()
			} buildNoContent: {
				VStack(spacing: 6) {
					Image(systemName: "person.circle")
						.font(.system(size: 36))
						.foregroundColor(.gh(.labelTertiary))
						.padding(.bottom, 6)

					Text(model.emptyContentMessage)
						.font(.gh(.title2))
						.foregroundColor(.gh(.labelPrimary))
				}
				.multilineTextAlignment(.center)
				.padding(48)
			}
		}
	}
}

struct UserOtherConnectionsListView_Previews: PreviewProvider {
	static var previews: some View {
		NavigationView {
			UserOtherConnectionsListView(model: .init(kind: .follower, userID: User.fixture2.id, connectionsPublisher: PreviewUserService().connections, updateConnectionStatuses: { _ in }))
		}
		.injectPreviewsEnvironment()
	}
}
