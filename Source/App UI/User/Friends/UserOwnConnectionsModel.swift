import Combine
import SwiftUI

final class UserOwnConnectionsModel: ObservableObject {
	typealias UpdateConnectionStatusesAction = ([User.ID]) -> Void
	typealias GetConnectionsPublisher = (User.ID, UserConnection.Kind?, UserConnection.Status?, _ offset: UInt, _ limit: UInt) -> ApiResponsePublisher<PaginatedResponse<UserConnection>>

	let kind: UserConnection.Kind
	let userID: User.ID
	private let connectionsPublisher: GetConnectionsPublisher
	private let updateConnectionStatuses: UpdateConnectionStatusesAction

	@Published var connections: ModelState<[UserConnection]> = .none

	private var canPaginate: Bool = true

	private var connectionsRequest: AnyCancellable?

	var isPaginating: Bool {
		connections.isLoading && connections.modelOrPrevious != nil
	}

	var requestsTitle: LocalizedStringKey {
		switch kind {
			case .follower:
				return "follow_requests"
			case .following:
				return "your_follow_requests"
		}
	}

	var connectionsTitle: String {
		switch kind {
			case .follower:
				return NSLocalizedString("friends_that_follow_you", comment: "")
			case .following:
				return NSLocalizedString("friends_that_you_follow", comment: "")
		}
	}

	init(kind: UserConnection.Kind, userID: User.ID, connectionsPublisher: @escaping GetConnectionsPublisher, updateConnectionStatuses: @escaping UpdateConnectionStatusesAction) {
		self.kind = kind
		self.userID = userID
		self.connectionsPublisher = connectionsPublisher
		self.updateConnectionStatuses = updateConnectionStatuses
	}

	func loadConnectionsIfNeeded() {
		guard connections == .none else {
			return
		}

		fetchConnections()
	}

	func appearedConnection(at index: Int) {
		guard canPaginate, !connections.isLoading,
		      index > (connections.model?.count ?? .max) - 3,
		      let offset = connections.model?.count
		else {
			return
		}

		fetchConnections(offset: offset)
	}

	private func fetchConnections(offset: Int = 0) {
		/// For followers only fetch active ones, for own following list, fetch active but also pending (so we send nil)
		let status: UserConnection.Status? = kind == .follower ? .active : nil

		connectionsRequest = connectionsPublisher(userID, kind, status, UInt(offset), 30)
			.map { [unowned self] in
				let visibleConnections = self.connections.modelOrPrevious ?? []

				let pageResults = $0.results.filter { !visibleConnections.contains($0) }

				// eagerly batch update statuses
				updateConnectionStatuses(pageResults.map(\.user.id))

				let connections = visibleConnections + pageResults
				self.canPaginate = $0.count > connections.count
				return connections
			}
			.mapModelState(current: connections.modelOrPrevious)
			.assign(to: \.connections, onUnowned: self)
	}
}
