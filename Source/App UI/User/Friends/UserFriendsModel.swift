import SwiftUI

final class UserFriendsModel: ObservableObject {
	enum Tab: Equatable, CaseIterable {
		case followers
		case following
		case more
	}

	@Published var selectedTab: Tab

	private let userState: UserState
	private let isSearchFriendsOnly: Bool
	
	var isShowingTabs: Bool {
		if isSearchFriendsOnly { return false }
		guard let user = userState.user.modelOrPrevious else { return false }
		guard let followers = user.followersCount, let following = user.followingCount else {
			return false
		}

		return followers > 0 || following > 0
	}

	init(userState: UserState, initialTab: Tab, isSearchFriendsOnly: Bool) {
		self.userState = userState
		self.selectedTab = initialTab
		self.isSearchFriendsOnly = isSearchFriendsOnly
	}

	func title(for tab: Tab) -> String {
		switch tab {
			case .following:
				var extra = ""
				if let value = userState.user.followingCount ?? nil {
					extra = " (\(value))"
				}
				return NSLocalizedString("following", comment: "") + extra
			case .followers:
				var extra = ""
				if let value = userState.user.followersCount ?? nil {
					extra = " (\(value))"
				}
				return NSLocalizedString("followers", comment: "") + extra
			case .more:
				return NSLocalizedString("more_friends", comment: "")
		}
	}

	func badge(for tab: Tab) -> Int {
		switch tab {
			case .followers:
				return userState.connectionRequests.count
			case .following, .more:
				return 0
		}
	}
}
