import SwiftUI

struct UserFriendsView: View {
	var initialTab: UserFriendsModel.Tab = .more
	var searchFriendsOnly: Bool = false

	@EnvironmentObject var userState: UserState

	var body: some View {
		UserFriendsTabView(model: createModel())
			.navigationBarTitleDisplayMode(.inline)
			.navigationTitle("friends")
	}

	private func createModel() -> UserFriendsModel {
		UserFriendsModel(userState: userState, initialTab: initialTab, isSearchFriendsOnly: searchFriendsOnly)
	}
}

struct UserFriendsTabView: View {
	@StateObject var model: UserFriendsModel

	@EnvironmentObject var userState: UserState
	@Environment(\.apiClient.services) var services
	@Environment(\.userInteractor) var userInteractor

	var body: some View {
		VStack(spacing: 0) {
			if model.isShowingTabs {
				SegmentedSelectView(items: UserFriendsModel.Tab.allCases,
				                    selected: $model.selectedTab,
				                    itemTitle: model.title(for:),
				                    itemBadge: model.badge(for:))
					.frame(maxWidth: .infinity)

				TabView(selection: $model.selectedTab) {
					UserOwnConnectionsList(model: createFollowersModel())
						.tag(UserFriendsModel.Tab.followers)

					UserOwnConnectionsList(model: createFollowingModel())
						.tag(UserFriendsModel.Tab.following)

					MoreFriendsView(suggestionsModel: createSuggestionsModel(), searchModel: createSearchModel())
						.tag(UserFriendsModel.Tab.more)
				}
				.tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
			} else {
				MoreFriendsView(suggestionsModel: createSuggestionsModel(), searchModel: createSearchModel())
			}

			Spacer()
		}
	}

	private func createSuggestionsModel() -> FriendSuggestionsModel {
		let userAuthChanged = userState.$userID.removeDuplicates().map { _ in }.eraseToAnyPublisher()
		return FriendSuggestionsModel(userChanged: userAuthChanged, userService: services.users)
	}

	private func createSearchModel() -> UserSearchModel {
		let updateStatusAction: UserSearchModel.UpdateStatusAction = { userIDs in
			userInteractor.connections?.updateStatuses(for: userIDs)
		}

		return UserSearchModel(searchAction: services.users.search, updateStatusAction: updateStatusAction)
	}

	private func createFollowersModel() -> UserOwnConnectionsModel {
		UserOwnConnectionsModel(kind: .follower,
		                        userID: userState.userID!,
		                        connectionsPublisher: services.users.connections,
		                        updateConnectionStatuses: userInteractor.connections?.updateStatuses ?? { _ in })
	}

	private func createFollowingModel() -> UserOwnConnectionsModel {
		UserOwnConnectionsModel(kind: .following,
		                        userID: userState.userID!,
		                        connectionsPublisher: services.users.connections,
		                        updateConnectionStatuses: userInteractor.connections?.updateStatuses ?? { _ in })
	}
}

struct UserFriendsView_Previews: PreviewProvider {
	static var previews: some View {
		NavigationView {
			UserFriendsView()
		}
		.injectPreviewsEnvironment()
	}
}
