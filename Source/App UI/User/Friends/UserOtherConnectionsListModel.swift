import Combine
import SwiftUI

final class UserOtherConnectionsListModel: ObservableObject, ContentPagingModel, ContentRefreshableModel {
	typealias UpdateConnectionStatusesAction = ([User.ID]) -> Void
	typealias GetConnectionsPublisher = (User.ID, UserConnection.Kind?, UserConnection.Status?, _ offset: UInt, _ limit: UInt) -> ApiResponsePublisher<PaginatedResponse<UserConnection>>

	struct Item: Identifiable, Equatable, Hashable {
		var id: String { "\(connection.user.id)\(connection.kind)" }
		let connection: UserConnection
	}

	let kind: UserConnection.Kind
	let userID: User.ID

	private let connectionsPublisher: GetConnectionsPublisher
	private let updateConnectionStatuses: UpdateConnectionStatusesAction

	@Published var content: ModelState<[Item]> = .none

	private var canPaginate: Bool = true

	private var connectionsRequest: AnyCancellable?

	var isPaginating: Bool {
		content.isLoading && content.modelOrPrevious != nil
	}

	var emptyContentMessage: String {
		switch kind {
			case .follower:
				return NSLocalizedString("no_followers", comment: "")
			case .following:
				return NSLocalizedString("no_following", comment: "")
		}
	}

	init(kind: UserConnection.Kind, userID: User.ID, connectionsPublisher: @escaping GetConnectionsPublisher, updateConnectionStatuses: @escaping UpdateConnectionStatusesAction) {
		self.kind = kind
		self.userID = userID
		self.connectionsPublisher = connectionsPublisher
		self.updateConnectionStatuses = updateConnectionStatuses
	}

	func refresh() {
		fetchConnections(offset: 0)
	}

	func loadIfNeeded() {
		guard content == .none || content.error != nil else {
			return
		}

		fetchConnections()
	}

	func itemAppeared(at index: Int) {
		guard canPaginate, !content.isLoading,
		      index > (content.model?.count ?? .max) - 3,
		      let offset = content.model?.count
		else {
			return
		}

		fetchConnections(offset: offset)
	}

	private func fetchConnections(offset: Int = 0) {
		/// For followers only fetch active ones, for own following list, fetch active but also pending (so we send nil)
		let status: UserConnection.Status? = kind == .follower ? .active : nil

		let replaceContent = offset == 0

		connectionsRequest = connectionsPublisher(userID, kind, status, UInt(offset), 30)
			.map { [unowned self] in
				let visibleConnections = replaceContent ? [] : (self.content.modelOrPrevious ?? [])

				let pageResults = $0.results
					.map(Item.init)
					.filter { !visibleConnections.map(\.id).contains($0.id) }

				// eagerly batch update statuses
				updateConnectionStatuses(pageResults.map(\.connection.user.id))

				let connections = visibleConnections + pageResults
				self.canPaginate = $0.count > connections.count
				return connections
			}
			.mapModelState(current: content.modelOrPrevious)
			.assign(to: \.content, onUnowned: self)
	}
}
