import Combine
import SwiftUI

struct MoreFriendsView: View {
	@StateObject var suggestionsModel: FriendSuggestionsModel
	@StateObject var searchModel: UserSearchModel

	@State var addressBookError: Error?
	@State var facebookError: Error?

	@State var isSearching = false

	let margins = EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20)

	var body: some View {
		VStack {
			SearchTextField(placeholder: "Find friends on \(Config.appName)",
			                text: $searchModel.query,
			                onEditingChanged: {
			                	isSearching = $0
			                })
			                .padding(margins)
			                .padding(.top)

			ScrollView {
				if isSearching || !searchModel.query.isEmpty {
					LazyVStack(alignment: .leading, spacing: 0) {
						ForEach(searchModel.results) { user in
							FriendFollowListItem(user: user)
							Divider()
						}
					}
				} else {
					LazyVStack(alignment: .leading, spacing: 0) {
						Text("suggestions")
							.font(.gh(.caption1))
							.foregroundColor(.gh(.labelSecondary))
							.padding([.top, .bottom])
							.padding(margins)

						if let suggestions = suggestionsModel.suggestions.modelOrPrevious {
							ForEach(suggestions) { suggestion in
								FriendFollowListItem(user: suggestion)
								Divider()
							}
						}

						Text("invite_friends")
							.font(.gh(.caption1))
							.foregroundColor(.gh(.labelSecondary))
							.padding([.top, .bottom])
							.padding(margins)

						if let contacts = suggestionsModel.contacts {
							ForEach(contacts) { contact in
								FriendInviteListItem(contact: contact)
								Divider()
							}
						}

						FindFriendsBanner(addressBookAction: addressBookAction(), facebookAction: facebookAction())
					}
				}
			}
		}
		.onAppear(perform: suggestionsModel.bindObservables)
		.onChange(of: suggestionsModel.suggestions, perform: { suggestions in
			searchModel.setSearchSuggestions(suggestions.model ?? [])
		})
	}

	private func addressBookAction() -> (() -> Void)? {
		guard !suggestionsModel.isAddressBookConnected else {
			return nil
		}

		return {
			suggestionsModel.connectAddressBook { error in
				self.addressBookError = error
			}
		}
	}

	private func facebookAction() -> (() -> Void)? {
		guard !suggestionsModel.isFacebookConnected else {
			return nil
		}

		return {
			suggestionsModel.connectFacebook { error in
				self.facebookError = error
			}
		}
	}
}

struct MoreFriendsView_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			MoreFriendsView(suggestionsModel: .stub, searchModel: .init(
				searchAction: { _ in
					Just(.init(count: 2, results: [.fixture1, .fixture2]))
						.setFailureType(to: API.Error.self)
						.eraseToAnyPublisher()
				},
				updateStatusAction: { _ in }
			))

			MoreFriendsView(suggestionsModel: .stub, searchModel: .init(
				searchAction: { _ in
					Just(.init(count: 3, results: [.fixture1, .fixture2, .fixture3]))
						.setFailureType(to: API.Error.self)
						.eraseToAnyPublisher()
				},
				updateStatusAction: { _ in }
			), isSearching: true)
		}
		.injectPreviewsEnvironment()
	}
}
