import SwiftUI

struct UserOtherConnectionsView: View {
	enum Tab: Equatable, CaseIterable {
		case followers
		case following
	}

	let userID: User.ID
	@State var selectedTab: Tab

	@Environment(\.apiClient.services) var services
	@Environment(\.userInteractor) var userInteractor

	var body: some View {
		VStack(spacing: 0) {
			SegmentedSelectView(items: UserOtherConnectionsView.Tab.allCases,
			                    selected: $selectedTab,
			                    itemTitle: sectionTitle(for:))
				.background(Color.gh(.backgroundPrimary))
				.compositingGroup()
				.elevationLevel2()
				.frame(maxWidth: .infinity)

			TabView(selection: $selectedTab) {
				UserOtherConnectionsListView(model: createFollowersModel())
					.tag(Tab.followers)

				UserOtherConnectionsListView(model: createFollowingModel())
					.tag(Tab.following)
			}
			.tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))

			Spacer(minLength: 0)
		}
		.navigationTitle("friends")
	}

	private func sectionTitle(for tab: Tab) -> String {
		switch tab {
			case .followers:
				return NSLocalizedString("followers", comment: "")
			case .following:
				return NSLocalizedString("following", comment: "")
		}
	}

	private func createFollowersModel() -> UserOtherConnectionsListModel {
		UserOtherConnectionsListModel(kind: .follower,
		                              userID: userID,
		                              connectionsPublisher: services.users.connections,
		                              updateConnectionStatuses: userInteractor.connections?.updateStatuses ?? { _ in })
	}

	private func createFollowingModel() -> UserOtherConnectionsListModel {
		UserOtherConnectionsListModel(kind: .following,
		                              userID: userID,
		                              connectionsPublisher: services.users.connections,
		                              updateConnectionStatuses: userInteractor.connections?.updateStatuses ?? { _ in })
	}
}

struct UserOtherConnectionsView_Previews: PreviewProvider {
	static var previews: some View {
		NavigationView {
			UserOtherConnectionsView(userID: User.fixture2.id, selectedTab: .followers)
				.navigationBarTitleDisplayMode(.inline)
		}
		.injectPreviewsEnvironment()
	}
}
