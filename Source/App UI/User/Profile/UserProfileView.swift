import Combine
import SwiftUI

struct UserProfileView: View {
	@EnvironmentObject var userState: UserState
	@Environment(\.userInteractor) var userInteractor
	@Environment(\.apiClient.services) var services

	let source: UserProfileModel.Source

	init(id: User.ID) {
		source = .id(id)
	}

	init(_ user: User) {
		source = .user(user)
	}

	var body: some View {
		UserProfileContentView(model: createModel())
			.navigationBarTitleDisplayMode(.inline)
	}

	private func createModel() -> UserProfileModel {
		UserProfileModel(source,
		                 userState: userState,
		                 userInteractor: userInteractor,
		                 userService: services.users,
		                 reviewService: services.reviews)
	}
}

struct UserProfileContentView: View {
	@StateObject var model: UserProfileModel
	@EnvironmentObject var userState: UserState
	@Environment(\.apiClient.services) var services
	@State private var isShowingFriendsView: Bool = false

	var body: some View {
		ScrollView {
			PullToRefresh(isLoading: model.user.isLoading || model.reviews.isLoading, coordinateSpace: "profileScrollView") {
				model.refresh(force: true)
			}

			LazyVStack(alignment: .leading, spacing: 10) {
				headerView()

				profileActionsView
				
				if model.isCurrentUser {
					NavigationLink(destination: UserFriendsView(searchFriendsOnly: true), isActive: $isShowingFriendsView) {}
					Spacer().frame(height: 4)
					SearchTextButton(placeholder: "Find friends on \(Config.appName)") {
						isShowingFriendsView = true
					}
				}

				Spacer().frame(height: 4)
				UserProfileReviewsSection(reviews: model.reviews,
				                          totalCount: model.user.reviewCount,
				                          isPaginating: model.isPaginating,
				                          isCurrentUser: model.isCurrentUser,
				                          wantsNextPage: model.paginateReviews)
			}
			.padding(24)
		}
		.coordinateSpace(name: "profileScrollView")
		.navigationBarItems(trailing: profileActionsBarItemsView)
		.onAppear(perform: model.bindObservables)
	}

	@ViewBuilder
	var profileActionsView: some View {
		if model.isCurrentUser && model.user.model?.profile.biography.isEmpty == true {
			NavigationLink("add_bio", destination: UserEditProfileView(model: editProfileModel()))
				.quaternaryButtonStyle(size: .small)
		} else if model.user.modelOrPrevious?.followersCount != nil {
			FollowButton(userID: model.id,
			             isPrivateProfile: model.user.modelOrPrevious?.profile.isPrivate ?? false,
			             size: .small)
		} else {
			EmptyView()
		}
	}

	@ViewBuilder
	var profileActionsBarItemsView: some View {
		if model.isCurrentUser || model.user.modelOrPrevious?.isMothership ?? true {
			EmptyView()
		} else {
			Menu {
				Button(action: model.blockUserConnection, label: {
					Label("block_this_user", systemImage: "person.crop.circle.badge.xmark")
				})
			} label: {
				Image(systemName: "ellipsis")
			}
			.buttonStyle(NavbarButtonStyle())
		}
	}

	private func editProfileModel() -> UserEditProfileModel {
		UserEditProfileModel(userState: userState, update: services.users.update, phoneVerification: services.verifications.createCode)
	}

	@ViewBuilder
	private func headerView() -> some View {
		UserProfileHeaderView(
			user: model.user,
			followRequests: model.isCurrentUser ? userState.connectionRequests.count : 0,
			profileDestination: { UserEditProfileView(model: editProfileModel()) },
			followersDestination: {
				if model.isCurrentUser {
					UserFriendsView(initialTab: .followers)
				} else {
					UserOtherConnectionsView(userID: model.id, selectedTab: .followers)
				}
			},
			followingDestination: {
				if model.isCurrentUser {
					UserFriendsView(initialTab: .following)
				} else {
					UserOtherConnectionsView(userID: model.id, selectedTab: .following)
				}
			}
		)
	}

	private func refreshableBinding() -> Binding<Bool> {
		.init(get: { model.user.isLoading || model.reviews.isLoading }, set: { value in
			if value {
				model.refresh(force: true)
			} // we ignore `false`
		})
	}
}

struct UserProfileView_Previews: PreviewProvider {
	static var previews: some View {
		NavigationView {
			UserProfileView(UserExtended.fixture1)
		}
		.injectPreviewsEnvironment()
	}
}
