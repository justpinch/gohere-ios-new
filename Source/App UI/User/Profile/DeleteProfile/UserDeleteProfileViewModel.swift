//
//  UserDeleteProfileViewModel.swift
//  GoHere
//
//  Created by jorisfavier on 20/06/2022.
//

import Foundation
import Combine

final class UserDeleteProfileViewModel: ObservableObject {
	
	private let userInteractor: UserInteractor
	
	@Published
	private(set) var deletionState: LoadingState = .none
	@Published
	var showDeleteConfirmation: Bool = false
	
	private var deletingProfileCancellable: AnyCancellable?
	
	init(userInteractor: UserInteractor) {
		self.userInteractor = userInteractor
	}
	
	deinit {
		deletingProfileCancellable?.cancel()
	}
	
	func deleteAccount() {
		guard !deletionState.isLoading && deletionState != .finished else {
			return
		}
		deletingProfileCancellable = userInteractor.delete()
			.mapLoadingState()
			.sink(receiveValue: { [weak self] state in
				self?.deletionState = state
			})
	}
	
	func onDismiss() {
		if deletionState == .finished {
			userInteractor.signOut()
		}
	}
}
