//
//  UserDeleteProfileView.swift
//  GoHere
//
//  Created by jorisfavier on 13/06/2022.
//

import SwiftUI

struct UserDeleteProfileView: View {
	
	@Environment(\.presentationMode) var presentationMode
	
	@StateObject var viewModel: UserDeleteProfileViewModel
	
    var body: some View {
		content
		.padding(.horizontal, 25)
		.padding(.vertical, 20)
		.alert(isPresented: $viewModel.showDeleteConfirmation, content: deleteAccountConfirmationAlert)
		.navigationBarItems(leading: closeNavButton)
		.navigationBarTitleDisplayMode(.inline)
    }
	
	@ViewBuilder
	private var content: some View {
		switch viewModel.deletionState {
		case .none:
			deleteProfileContent
		case .loading:
			ProgressIndicator(size: .large)
		case .finished:
			successDeletionContent
		case .error(_):
			ErrorTryAgainView(tryAgainCallback: viewModel.deleteAccount)
		}
	}
	
	
	@ViewBuilder
	var closeNavButton: some View {
		Button(action: dismiss, label: {
			Image(systemName: "xmark")
				.font(.system(size: 20, weight: .semibold))
				.foregroundColor(.gh(.labelPrimary))
				.padding(8)
		})
		.buttonStyle(NavbarButtonStyle())
	}
	
	@ViewBuilder
	var deleteProfileContent: some View {
		VStack {
			ScrollView {
				VStack(alignment: .leading, spacing: 0) {
					Text("delete_account")
						.foregroundColor(.gh(.labelPrimary))
						.font(.gh(.title2))
					Text("delete_account_warning")
						.padding(.top, 20)
						.padding(.bottom, 10)
						.foregroundColor(.gh(.labelPrimary))
						.font(.gh(.headline))
					HStack(alignment:.top){
						Image(systemName: "exclamationmark.triangle")
							.imageScale(.large)
							.foregroundColor(.gh(.utilityError))
						Spacer().frame(width: 16)
						Text("delete_account_info_1")
							.foregroundColor(.gh(.labelPrimary))
							.font(.gh(.body))
					}
					.padding(.top, 30)
					.padding(.bottom, 10)
					HStack(alignment:.top){
						Image(systemName: "person")
							.imageScale(.large)
							.foregroundColor(.gh(.utilityError))
						Spacer().frame(width: 16)
						Text("delete_account_info_2")
							.foregroundColor(.gh(.labelPrimary))
							.font(.gh(.body))
					}.padding(.vertical, 18)
					HStack(alignment:.top){
						Image(systemName: "trash.fill")
							.imageScale(.large)
							.foregroundColor(.gh(.utilityError))
						Spacer().frame(width: 16)
						Text("delete_account_info_3")
							.foregroundColor(.gh(.labelPrimary))
							.font(.gh(.body))
					}.padding(.vertical, 18)
				}
			}
			Spacer()
			Button(
				"delete",
				action: { viewModel.showDeleteConfirmation = true }
			)
			.destructiveButtonStyle()
			Spacer().frame(height: 14)
			Button(
				"cancel",
				action: dismiss
			)
			.quaternaryButtonStyle()
			
		}
	}
	
	@ViewBuilder
	private var successDeletionContent: some View {
		VStack(alignment: .center) {
			Image(systemName: "checkmark")
				.resizable()
				.frame(width: 56, height: 56)
				.foregroundColor(.gh(.utilityPositive))
			Text("account_deleted")
				.font(.gh(.title2))
				.foregroundColor(.gh(.labelPrimary))
			Button("ok", action: dismiss)
				.quaternaryButtonStyle()
		}
	}
	
	private func deleteAccountConfirmationAlert() -> Alert {
		Alert(
			title: Text("are_you_sure"),
			message: Text("last_warning_delete_account"),
			primaryButton: .destructive(
				Text("delete"),
				action: { self.viewModel.deleteAccount() }
			),
			secondaryButton: .cancel()
		)
	}
	
	private func dismiss() {
		presentationMode.wrappedValue.dismiss()
		viewModel.onDismiss()
	}

}


struct UserDeleteProfileView_Previews: PreviewProvider {
    static var previews: some View {
		UserDeleteProfileView(viewModel: UserDeleteProfileViewModel(userInteractor: PreviewUserInteractor()))
    }
}
