import SwiftUI

struct UserProfileHeaderView<FollowersDestination, FollowingDestination, ProfiileDestination>: View
where FollowersDestination: View, FollowingDestination: View, ProfiileDestination: View {
	struct Data {
		let id: User.ID
		let avatar: ImagePath?
		let name: String
		let initials: String
		let location: String?
		let followers: Int?
		let following: Int?
		let followRequests: Int
		let reviews: Int
		let isPrivate: Bool
		let biography: String
	}

	let data: Data
	let followersDestination: () -> FollowersDestination
	let followingDestination: () -> FollowingDestination
	let profileDestination: () -> ProfiileDestination
	let isPlaceholder: Bool
	

	@EnvironmentObject var userState: UserState

	init(user: ModelState<UserExtended>,
	     followRequests: Int? = nil,
		 @ViewBuilder profileDestination: @escaping () -> ProfiileDestination,
	     @ViewBuilder followersDestination: @escaping () -> FollowersDestination,
	     @ViewBuilder followingDestination: @escaping () -> FollowingDestination)
	{
		if let user = user.modelOrPrevious {
			data = Data(id: user.id,
			            avatar: user.profile.avatar,
			            name: user.fullName,
			            initials: user.initials,
			            location: user.profile.hometown,
			            followers: user.followersCount,
			            following: user.followingCount,
			            followRequests: followRequests ?? 0,
			            reviews: user.reviewCount,
			            isPrivate: user.profile.isPrivate,
						biography: user.profile.biography)

			isPlaceholder = false
		} else {
			data = .placeholderData
			isPlaceholder = true
		}
		
		self.profileDestination = profileDestination
		self.followersDestination = followersDestination
		self.followingDestination = followingDestination
	}

	var body: some View {
		VStack(alignment: .leading) {
			HStack(spacing: 16) {
				ZStack(alignment: .topTrailing) {
					NavigationLink(
						destination: UserProfileAvatarView(avatar: data.avatar, name: data.name),
						label: {
							AvatarView(path: data.avatar, initials: data.initials)
						}
					).frame(width: 76, height: 76)
					if isCurrentLoggedInUser {
						NavigationLink(
							destination: profileDestination(),
							label: {
								Image("edit")
									.aspectRatio(contentMode: .fit)
									.imageScale(.small)
							}
						).floatingActionButtonStyle(width: .small).offset(x: 15, y: -15)
					}
				}

				HeaderStats(value: data.reviews, caption: "reviews")

				if let followers = data.followers {
					showFriendsStat(destination: followersDestination) {
						HeaderStats(value: followers, caption: "followers", badge: data.followRequests)
					}
				}

				if let following = data.following {
					showFriendsStat(destination: followingDestination) {
						HeaderStats(value: following, caption: "following")
					}
				}
			}
			.padding(.bottom, 8)

			Text(data.name)
				.font(.gh(.subhead))
				.foregroundColor(.gh(.labelPrimary))

			Text(data.location ?? "")
				.font(.gh(.caption1))
				.foregroundColor(.gh(.labelSecondary))
			
			if !data.biography.isEmpty {
				Text(data.biography)
					.font(.gh(.caption1))
					.foregroundColor(.gh(.labelPrimary))
					.padding(.vertical, 5)
			}
		}
		.redacted(reason: isPlaceholder ? .placeholder : [])
		.disabled(isPlaceholder)
	}

	@ViewBuilder
	func showFriendsStat<Destination: View, Label: View>(destination: @escaping () -> Destination, label: @escaping () -> Label) -> some View {
		if canShowFriends {
			NavigationLink(destination: destination(), label: label)
		} else {
			label()
		}
	}

	var canShowFriends: Bool {
		guard data.isPrivate else {
			return true
		}

		guard userState.userID != data.id else {
			return true // same user
		}

		// we can show friends for private profiles only if user follows them
		return userState.connectionStatuses[data.id]?.modelOrPrevious == .active
	}
	
	var isCurrentLoggedInUser: Bool {
		return userState.userID == data.id
	}
}

private struct HeaderStats: View {
	let value: Int
	let caption: LocalizedStringKey
	var badge: Int = 0

	var body: some View {
		VStack {
			ZStack {
				Text("\(value)")
					.font(.gh(.title3))
					.foregroundColor(.gh(.labelPrimary))
					.fixedSize()

				NotificationBadge(value: badge)
					.offset(x: 10, y: -10)
			}
			Text(caption)
				.font(.gh(.caption1))
				.foregroundColor(.gh(.labelSecondary))
				.fixedSize()
		}
	}
}

extension UserProfileHeaderView.Data {
	static var placeholderData: UserProfileHeaderView.Data {
		.init(id: "",
		      avatar: nil,
		      name: "******* ********",
		      initials: "",
		      location: "********, *********",
		      followers: 10,
		      following: 10,
		      followRequests: 0,
		      reviews: 10,
			  isPrivate: false,
			  biography: "*********")
	}
}

struct UserProfileHeaderView_Previews: PreviewProvider {
	static var previews: some View {
		UserProfileHeaderView(
			user: .loading,
			followRequests: 3,
			profileDestination: { EmptyView() },
			followersDestination: { EmptyView() },
			followingDestination: { EmptyView() }
		)
		.padding()
		.previewLayout(.sizeThatFits)
		.preferredColorScheme(.dark)

		
		UserProfileHeaderView(
			user: .model(.fixtureExtended1),
			followRequests: 3,
			profileDestination: { EmptyView() },
			followersDestination: { EmptyView() },
			followingDestination: { EmptyView() }
		)
		.padding()
		.previewSupportedLocales()

		UserProfileHeaderView(
			user: .model(.fixtureExtended2),
			followRequests: 3,
			profileDestination: { EmptyView() },
			followersDestination: { EmptyView() },
			followingDestination: { EmptyView() }
		)
		.padding()
		.previewContentSize(.extraExtraExtraLarge)
		.previewLayout(.sizeThatFits)
	}
}
