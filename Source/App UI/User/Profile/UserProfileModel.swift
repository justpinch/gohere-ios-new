import Combine
import Foundation

final class UserProfileModel: ObservableObject, ContentRefreshableModel {
	enum Source {
		case id(User.ID)
		case user(User)

		fileprivate var id: User.ID {
			switch self {
				case let .id(id):
					return id
				case let .user(user):
					return user.id
			}
		}
	}

	let id: User.ID
	@Published var user: ModelState<UserExtended> = .none
	@Published var reviews: ModelState<[Review]> = .none

	var isCurrentUser: Bool {
		userState.userID == id
	}

	private var canPaginate: Bool = true
	private let pageSize: UInt = 30

	var isPaginating: Bool {
		reviews.isLoading && reviews.modelOrPrevious != nil
	}

	private let userState: UserState
	private let userInteractor: UserInteractor
	private let userService: UserService
	private let reviewService: ReviewService
	private let notificationCenter: NotificationCenterProtocol

	private var userLoadCanceller: AnyCancellable?
	private var reviewsLoadCanceller: AnyCancellable?
	private var cancellables = Set<AnyCancellable>()

	init(_ source: Source,
	     userState: UserState,
	     userInteractor: UserInteractor,
	     userService: UserService,
	     reviewService: ReviewService,
	     notificationCenter: NotificationCenterProtocol = NotificationCenter.default)
	{
		self.id = source.id
		self.userState = userState
		self.userInteractor = userInteractor
		self.userService = userService
		self.reviewService = reviewService
		self.notificationCenter = notificationCenter

		if case let .user(user) = source {
			if let userExtended = user as? UserExtended {
				self.user = .model(userExtended)
			} else {
				// Use partial information from base user and load the rest
				self.user = .model(.init(base: user, followersCount: 0, followingCount: 0, reviewCount: 0, favoriteCount: 0))
			}
		}
	}

	func bindObservables() {
		guard cancellables.isEmpty else {
			return
		}

		// Refresh own profile when review is posted
		notificationCenter
			.publisher(for: Review.Notification.posted, object: nil)
			.filter { [unowned self] _ in self.isCurrentUser }
			.sink(receiveValue: { [unowned self] _ in
				self.refresh(force: true)
			})
			.store(in: &cancellables)

		// Observe review delete
		notificationCenter
			.publisher(for: Review.Notification.deleted, object: nil)
			.compactMap { $0.userInfo?[Review.Notification.Key.review] as? Review }
			.sink(receiveValue: { [unowned self] review in
				if var reviews = self.reviews.model, let index = reviews.firstIndex(where: { $0.id == review.id }) {
					reviews.remove(at: index)
					self.reviews = .model(reviews)
				}
			})
			.store(in: &cancellables)

		// Observe global user
		userState.$user
			.filter { [unowned self] _ in
				self.userState.userID == self.id
			}
			.assign(to: \.user, onUnowned: self)
			.store(in: &cancellables)

		// initially load everything
		refresh(force: true)
	}

	func refreshUser() {
		if isCurrentUser {
			userInteractor.refreshProfile()
		} else {
			userLoadCanceller = userService
				.profile(id: id)
				.mapModelState(current: user.modelOrPrevious)
				.assign(to: \.user, onUnowned: self)
		}
	}

	func refreshReviews() {
		fetchNextReviews(from: 0)
	}

	func refresh() {
		refresh(force: false)
	}

	func refresh(force: Bool) {
		if user.modelOrPrevious == nil || force {
			refreshUser()
		}

		if reviews.modelOrPrevious == nil || force {
			refreshReviews()
		}
	}

	func paginateReviews() {
		guard canPaginate, !reviews.isLoading, let offset = reviews.model?.count else {
			return
		}

		fetchNextReviews(from: offset)
	}

	private func fetchNextReviews(from offset: Int) {
		let replaceContent = offset == 0

		let params = ReviewServiceListParameters(userID: id, offset: UInt(offset), limit: pageSize)
		reviewsLoadCanceller = reviewService
			.list(params: params)
			.map { [unowned self] in
				let visibleReviews = replaceContent ? [] : (self.reviews.modelOrPrevious ?? [])
				let pageResults = $0.results.filter { !visibleReviews.contains($0) }

				// update statuses locally
				self.userInteractor.savedPlaces?.refreshStatus(for: pageResults.map(\.place))

				let content = offset == 0 ? pageResults : visibleReviews + pageResults
				self.canPaginate = $0.count > content.count
				return content
			}
			.mapModelState(current: reviews.modelOrPrevious)
			.assign(to: \.reviews, onUnowned: self)
	}

	func blockUserConnection() {
		guard let currentUserID = userState.userID else {
			assertionFailure()
			return
		}

		userInteractor.connections?.unfollow(id: id)
		userService.setConnectionStatus(status: .blocked, from: id, to: currentUserID).subscribeIgnoringEvents()
	}
}
