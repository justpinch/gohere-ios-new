import Combine
import Nuke
import PhoneNumberKit
import SwiftUI

final class UserEditProfileModel: ObservableObject {
	let userState: UserState
	@Published var originalAvatar: UIImage?

	@Published var user: DraftUser
	@Published var errors: [ErrorField: [String]] = [:]
	@Published var loadState: LoadingState = .none
	@Published var unverifiedPhoneNumber: IdentifiableWrapper<String, String>?
	@Published var showDeleteProfile: Bool = false

	typealias UpdateAction = (UpdateUser) -> ApiResponsePublisher<User>
	typealias SendPhoneVerificationToken = (String) -> ApiResponsePublisher<Void>

	private let updateAction: UpdateAction
	private let sendPhoneVerificationToken: SendPhoneVerificationToken

	private var updateCanceller: AnyCancellable?
	private var sendPhoneTokenCanceller: AnyCancellable?

	private var initialAvatarTask: ImageTask?

	enum ErrorField: String {
		case nonField
		case avatar = "profile.avatar"
		case firstName = "first_name"
		case lastName = "last_name"
		case hometown = "profile.hometown"
		case biography = "profile.biography"
		case email
		case phoneNumber = "phone_number"
		case isPrivate = "profile.is_private"
	}

	var avatarWrapper: Binding<UIImage?> {
		.init(get: {
			self.user.avatar ?? self.originalAvatar
		}, set: {
			self.user.avatar = $0
		})
	}

	init(userState: UserState, update: @escaping UpdateAction, phoneVerification: @escaping SendPhoneVerificationToken) {
		self.userState = userState
		self.user = DraftUser(user: userState.user.modelOrPrevious)
		self.updateAction = update
		self.sendPhoneVerificationToken = phoneVerification
	}

	func loadInitialAvatar() {
		guard let path = userState.user.modelOrPrevious?.profile.avatar, user.avatar == nil, originalAvatar == nil else {
			return
		}

		initialAvatarTask?.cancel()
		initialAvatarTask = ImagePipeline.shared.loadImage(with: path.url(for: 76)) { [weak self] result in
			if let image = try? result.get() {
				self?.originalAvatar = image.image
			}
		}
	}

	func validateLocally() -> [ErrorField: [String]] {
		var errors: [ErrorField: [String]] = [:]

		if user.firstName.isEmpty {
			errors[.firstName] = [NSLocalizedString("field_required", comment: "")]
		}

		if user.lastName.isEmpty {
			errors[.lastName] = [NSLocalizedString("field_required", comment: "")]
		}

		// one of email or phone is required (preferably both)
		if user.email.isEmpty, user.phoneNumber.isEmpty {
			errors[.email] = [NSLocalizedString("field_required", comment: "")]
			errors[.phoneNumber] = [NSLocalizedString("field_required", comment: "")]
		}

		if !user.phoneNumber.isEmpty {
			if PhoneNumberFormatter.toE164(user.phoneNumber) == nil {
				errors[.phoneNumber] = [NSLocalizedString("enter_valid_phone_number", comment: "")]
			}
		}

		if !user.email.isEmpty, !user.email.isValidEmail() {
			errors[.email] = [NSLocalizedString("enter_valid_email", comment: "")]
		}

		return errors
	}

	func submit(presentationMode: Binding<PresentationMode>?) {
		let localErrors = validateLocally()

		guard localErrors.isEmpty, let original = userState.user.modelOrPrevious else {
			self.errors = localErrors
			return
		}

		errors.removeAll()

		guard let updateUser = user.difference(to: original) else {
			// if no differences, just dismiss
			presentationMode?.wrappedValue.dismiss()
			return
		}

		if let phoneNumber = updateUser.phoneNumber, !phoneNumber.isEmpty, updateUser.tvpToken == nil {
			sendPhoneTokenCanceller = sendPhoneVerificationToken(phoneNumber)
				.mapLoadingState()
				.sink(receiveValue: { [unowned self] state in
					self.loadState = state

					if case let .error(error) = state {
						self.handle(error: error)
					}

					if state == .finished {
						self.unverifiedPhoneNumber = .init(phoneNumber)
					}
				})

			return
		}

		updateCanceller = updateAction(updateUser)
			.handleEvents(receiveOutput: { [weak self] user in
				guard let existing = self?.userState.user.modelOrPrevious else {
					return
				}

				// update existing user with new info
				let newUser = UserExtended(base: user,
				                           followersCount: existing.followersCount,
				                           followingCount: existing.followingCount,
				                           reviewCount: existing.reviewCount,
				                           favoriteCount: existing.favoriteCount)
				self?.userState.user = .model(newUser)
			})
			.mapLoadingState()
			.sink(receiveValue: { [unowned self] state in
				self.loadState = state

				if case let .error(error) = state {
					self.handle(error: error)
				}

				if state == .finished {
					presentationMode?.wrappedValue.dismiss()
				}
			})
	}
	
	func navigateToDeleteProfile() {
		self.showDeleteProfile = true
	}

	private func handle(error: API.Error) {
		guard let validationError = (error.reason as? API.FormValidationError) else {
			self.errors = [.nonField: [error.localizedDescription]]
			return
		}

		setValidationFailed(with: validationError)
	}

	private func getValidationErrors(from error: API.FormValidationError) -> [ErrorField: [String]] {
		var errors: [ErrorField: [String]] = [:]
		var nonFieldErrors: [String] = []

		// Field errors
		error.fieldErrors.forEach { pair in
			if let field = ErrorField(rawValue: pair.key) {
				errors[field] = pair.value
			} else {
				pair.value.forEach {
					nonFieldErrors.append("\(pair.key): \($0)")
				}
			}
		}

		// Non-field errors
		nonFieldErrors.append(contentsOf: error.nonFieldErrors)
		if !nonFieldErrors.isEmpty {
			errors[.nonField] = nonFieldErrors
		}

		return errors
	}

	private func setValidationFailed(with error: API.FormValidationError) {
		let errors = getValidationErrors(from: error)
		self.errors = errors
	}
}

struct DraftUser: Equatable {
	var email: String
	var phoneNumber: String
	var firstName: String
	var lastName: String
	var tvpToken: String?
	var avatar: UIImage?
	var hometown: String
	var isPrivate: Bool
	var biography: String

	init(user: User?) {
		avatar = nil
		firstName = user?.firstName ?? ""
		lastName = user?.lastName ?? ""
		email = user?.email ?? ""
		phoneNumber = PhoneNumberFormatter.toE164(user?.phoneNumber ?? "") ?? ""
		hometown = user?.profile.hometown ?? ""
		isPrivate = user?.profile.isPrivate ?? false
		biography = user?.profile.biography ?? ""
	}

	func difference(to user: User) -> UpdateUser? {
		var diff = UpdateUser(id: user.id)
		var hasDifference = false

		if avatar != nil {
			diff.avatar = avatar
			hasDifference = true
		}

		if user.firstName != firstName {
			diff.firstName = firstName
			hasDifference = true
		}

		if user.lastName != lastName {
			diff.lastName = lastName
			hasDifference = true
		}

		if user.profile.hometown != hometown {
			diff.hometown = hometown
			hasDifference = true
		}

		if user.email != email {
			diff.email = email
			hasDifference = true
		}

		if (user.phoneNumber ?? "") != phoneNumber {
			diff.phoneNumber = phoneNumber
			diff.tvpToken = tvpToken
			hasDifference = true
		}

		if user.profile.isPrivate != isPrivate {
			diff.isPrivate = isPrivate
			hasDifference = true
		}
		
		if user.profile.biography != biography {
			diff.biography = biography
			hasDifference = true
		}

		return hasDifference ? diff : nil
	}
}
