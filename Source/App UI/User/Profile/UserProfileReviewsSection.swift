import SwiftUI

struct UserProfileReviewsSection: View {
	let reviews: ModelState<[Review]>
	let totalCount: Int?
	let isPaginating: Bool
	let isCurrentUser: Bool
	let wantsNextPage: () -> Void

	@State private var isEditingReview: Bool = false
	@State private var isDeletingReview: Bool = false

	var body: some View {
		Section(header: reviewsHeader) {
			switch reviews {
				case .none:
					EmptyView()
				case let .loading(reviews):
					if let reviews = reviews, !reviews.isEmpty {
						listItems(for: reviews)
					} else {
						ProgressIndicator(size: .large)
							.padding(.top, 30)
					}
				case let .model(reviews):
					if reviews.isEmpty {
						emptyView
					} else {
						listItems(for: reviews)
					}
				case let .error(error):
					Text(error.localizedDescription)
						.font(.gh(.footnote))
						.foregroundColor(.gh(.utilityError))
			}
		}
		.frame(maxWidth: .infinity, maxHeight: .infinity)
	}

	@ViewBuilder
	var reviewsHeader: some View {
		HStack {
			Text(NSLocalizedString("reviews", comment: "") + (totalCount == nil ? "" : " (\(totalCount!))"))
				.font(.gh(.subhead))
				.foregroundColor(.gh(.labelPrimary))
			Spacer()
		}
		.fixedSize(horizontal: false, vertical: true)
	}

	@ViewBuilder
	var emptyView: some View {
		VStack(spacing: 10) {
			Text("no_reviews_title")
				.font(.gh(.title2))
				.foregroundColor(.gh(.labelPrimary))

			if isCurrentUser {
				Text("no_reviews_message")
					.font(.gh(.callout))
					.foregroundColor(.gh(.labelSecondary))

				NavigationLink(
					destination: StartReviewView(),
					label: {
						Text("leave_your_first_review")
					}
				)
				.primaryButtonStyle(size: .medium)
				.fixedSize()
			}
		}
	}

	@ViewBuilder
	func listItems(for reviews: [Review]) -> some View {
		ForEach(Array(reviews.enumerated()), id: \.element.id) { item in
			UserProfileReviewListItem(review: item.element)
				.onAppear(perform: {
					if item.offset > reviews.count - 2 {
						wantsNextPage()
					}
				})

			Divider()
		}

		if isPaginating {
			ProgressIndicator(size: .small)
				.padding()
				.frame(maxWidth: .infinity)
		}
	}
}

struct UserProfileReviewsSection_Previews: PreviewProvider {
	static var previews: some View {
		VStack {
			UserProfileReviewsSection(reviews: .loading, totalCount: 5, isPaginating: false, isCurrentUser: true, wantsNextPage: {})
			UserProfileReviewsSection(reviews: .loading(previous: [.fixture1, .fixture2]), totalCount: 5, isPaginating: true, isCurrentUser: true, wantsNextPage: {})
			UserProfileReviewsSection(reviews: .model([]), totalCount: nil, isPaginating: false, isCurrentUser: false, wantsNextPage: {})
			UserProfileReviewsSection(reviews: .model([]), totalCount: nil, isPaginating: false, isCurrentUser: true, wantsNextPage: {})
			UserProfileReviewsSection(reviews: .error(.fixture1), totalCount: 5, isPaginating: false, isCurrentUser: true, wantsNextPage: {})
			Spacer()
		}
		.previewLayout(.sizeThatFits)
		.injectPreviewsEnvironment()
	}
}
