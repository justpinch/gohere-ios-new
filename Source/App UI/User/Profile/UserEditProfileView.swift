import Combine
import iPhoneNumberField
import SwiftUI

struct UserEditProfileView: View {
	@Environment(\.presentationMode) var presentationMode
	@Environment(\.userInteractor) var userInteractor
	
	@StateObject var model: UserEditProfileModel

	var body: some View {
		ScrollView {
			VStack(alignment: .leading, spacing: 25) {
				VStack(alignment: .leading, spacing: 8) {
					Text("your_photo")
						.font(.gh(.subhead))
						.foregroundColor(.gh(.labelPrimary))

					AvatarPickerView(image: model.avatarWrapper)
						.onAppear(perform: model.loadInitialAvatar)
					errorsView(for: .avatar)
				}

				VStack(alignment: .leading, spacing: 8) {
					Text("your_name")
						.foregroundColor(.gh(.labelPrimary))
						.font(.gh(.subhead))

					TextField(LocalizedStringKey("first_name"), text: $model.user.firstName)
						.textContentType(.givenName)
						.textFieldStyle(GoHereTextFieldStyle())

					errorsView(for: .firstName)

					TextField(LocalizedStringKey("last_name"), text: $model.user.lastName)
						.textContentType(.familyName)
						.textFieldStyle(GoHereTextFieldStyle())

					errorsView(for: .lastName)
				}
				
				VStack(alignment: .leading, spacing: 8) {
					Text("biography")
						.foregroundColor(.gh(.labelPrimary))
						.font(.gh(.subhead))

					LimitedCharactersTextEditView(text: $model.user.biography, maxCharacters: 160)
						.frame(height: 100)

					errorsView(for: .hometown)
				}
				
				Divider()

				VStack(alignment: .leading, spacing: 8) {
					Text("hometown")
						.foregroundColor(.gh(.labelPrimary))
						.font(.gh(.subhead))

					TextField("Amsterdam, The Netherlands", text: $model.user.hometown)
						.textContentType(.addressCity)
						.textFieldStyle(GoHereTextFieldStyle())

					errorsView(for: .hometown)
				}

				VStack(alignment: .leading, spacing: 8) {
					Text("email")
						.foregroundColor(.gh(.labelPrimary))
						.font(.gh(.subhead))

					TextField("your@email.com", text: $model.user.email)
						.textContentType(.emailAddress)
						.keyboardType(.emailAddress)
						.textFieldStyle(GoHereTextFieldStyle())

					errorsView(for: .email)
				}

				VStack(alignment: .leading, spacing: 8) {
					Text("phone")
						.foregroundColor(.gh(.labelPrimary))
						.font(.gh(.subhead))

					iPhoneNumberField(text: $model.user.phoneNumber)
						.flagHidden(false)
						.flagSelectable(true)
						.prefixHidden(false)
						.autofillPrefix(true)
						.formatted(false)
						.font(.gh(.body))
						.frame(height: Theme.Font.body.info().size * 1.2)
						.padding()
						.background(Color.gh(.backgroundTertiary))
						.cornerRadius(8)

					errorsView(for: .phoneNumber)
				}

				VStack(alignment: .leading, spacing: 8) {
					Text("privacy")
						.foregroundColor(.gh(.labelPrimary))
						.font(.gh(.subhead))

					Menu(content: {
						Button("profile_privacy_select_private", action: { model.user.isPrivate = true })
						Button("profile_privacy_select_public", action: { model.user.isPrivate = false })
					}, label: {
						Label(model.user.isPrivate ? "profile_privacy_select_private" : "profile_privacy_select_public", systemImage: "chevron.down")
							.labelStyle(TrailingIconLabelStyle())
							.font(.gh(.caption1))
							.accentColor(.gh(.labelSecondary))
					})
					.animation(nil)
				}

				submitButton
					.primaryButtonStyle()
					.padding(.bottom)
				
				Group {
					Divider()
					HStack {
						Text("delete_account")
							.foregroundColor(.gh(.labelPrimary))
							.font(.gh(.subhead))
						Spacer()
						Image(systemName: "trash.fill")
							.foregroundColor(.gh(.labelPrimary))
					}.onTapGesture {
						model.navigateToDeleteProfile()
					}
				}
			}
			.padding(30)
		}
		.disabled(model.loadState.isLoading)
		.clearErrorsOnChange(of: model.user, in: model)
		.onChange(of: model.user.phoneNumber, perform: clearVerificationToken)
		.sheet(item: $model.unverifiedPhoneNumber, content: phoneVerificationView)
		.sheet(isPresented: $model.showDeleteProfile, content: deleteProfileView)
		.navigationBarTitle(model.loadState.isLoading ? "saving..." : "edit_profile")
		.navigationBarItems(trailing: submitButton.buttonStyle(NavbarButtonStyle()))
		.navigationBarTitleDisplayMode(.inline)
	}

	private var submitButton: some View {
		Button("save", action: {
			model.submit(presentationMode: presentationMode)
		})
	}

	@ViewBuilder
	private func phoneVerificationView(for phoneNumber: IdentifiableWrapper<String, String>) -> some View {
		TVPTokenVerifyView(phoneNumber: phoneNumber.wrapped) { token in
			model.user.tvpToken = token
			model.unverifiedPhoneNumber = nil
			model.submit(presentationMode: presentationMode)

			return Just(()).setFailureType(to: API.Error.self).eraseToAnyPublisher()
		}
	}

	@ViewBuilder
	private func errorsView(for field: UserEditProfileModel.ErrorField) -> some View {
		if let errors = model.errors[field], !errors.isEmpty {
			Text(combineErrors(errors))
				.font(.gh(.footnote))
				.foregroundColor(.gh(.utilityError))
				.fixedSize(horizontal: false, vertical: true)
				.padding(.bottom, 6)
		} else {
			EmptyView()
		}
	}
	
	@ViewBuilder
	private func deleteProfileView() -> some View {
		NavigationView {
			UserDeleteProfileView(viewModel: UserDeleteProfileViewModel(userInteractor: userInteractor))
		}.accentColor(.gh(.onNavbar))
	}

	private func combineErrors(_ strings: [String]) -> String {
		if strings.count > 1 {
			return "• " + strings.joined(separator: "\n• ")
		} else {
			return strings.first ?? ""
		}
	}

	private func clearVerificationToken(for _: String) {
		model.user.tvpToken = nil
	}
}

private extension View {
	@ViewBuilder
	func clearErrorsOnChange(of user: DraftUser, in model: UserEditProfileModel) -> some View {
		self.onChange(of: user.avatar, perform: { _ in
			model.errors.removeValue(forKey: .avatar)
		})
		.onChange(of: user.firstName, perform: { _ in
			model.errors.removeValue(forKey: .firstName)
		})
		.onChange(of: user.lastName, perform: { _ in
			model.errors.removeValue(forKey: .lastName)
		})
		.onChange(of: user.hometown, perform: { _ in
			model.errors.removeValue(forKey: .hometown)
		})
		.onChange(of: user.email, perform: { _ in
			model.errors.removeValue(forKey: .email)
		})
		.onChange(of: user.phoneNumber, perform: { _ in
			model.errors.removeValue(forKey: .phoneNumber)
		})
		.onChange(of: user.biography, perform: { _ in
			model.errors.removeValue(forKey: .biography)
		})
	}
}

struct UserEditProfileView_Previews: PreviewProvider {
	static var model: UserEditProfileModel {
		let state = UserState(authToken: .init(.fixture1))
		state.user = .model(.fixtureExtended1)

		let model = UserEditProfileModel(userState: state, update: { _ in
			Just(.fixture1).setFailureType(to: API.Error.self).eraseToAnyPublisher()
		}, phoneVerification: { _ in
			Just(()).setFailureType(to: API.Error.self).eraseToAnyPublisher()
		})

		model.errors = [.avatar: ["dioqwd"],
		                .firstName: ["Testerror", "ne radi"],
		                .lastName: ["Error lastname"],
		                .hometown: ["Error hometown"],
						.biography: ["Error with the bio"],
		                .email: ["Error email"]]
		return model
	}

	static var previews: some View {
		NavigationView {
			UserEditProfileView(model: model)
		}
		.injectPreviewsEnvironment()
	}
}
