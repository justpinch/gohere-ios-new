import SwiftUI

struct UserProfileAvatarView: View {
	let avatar: ImagePath?
	let name: String

	var body: some View {
		Group {
			if let avatar = avatar {
				RemoteImage(path: avatar, size: .screenWidth)
					.placeholder {
						Color.gh(.labelQuaternary)
					}
					.aspectRatio(contentMode: .fit)
			} else {
				ZStack {
					Image("no-image")
				}
				.frame(maxWidth: .infinity, maxHeight: .infinity)
				.background(Color.gh(.labelQuaternary))
			}
		}
		.navigationBarTitleDisplayMode(.inline)
		.navigationTitle(name)
	}
}

struct UserProfileAvatarView_Previews: PreviewProvider {
	static var previews: some View {
		NavigationView {
			UserProfileAvatarView(avatar: .fixture(.avatar2), name: "Ivan")
		}
	}
}
