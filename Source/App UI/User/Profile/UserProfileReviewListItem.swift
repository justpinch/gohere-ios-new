import SwiftUI

struct UserProfileReviewListItem: View {
	let review: Review
	@State private var isEditingReview: Bool = false
	@State private var isDeletingReview: Bool = false

	var body: some View {
		NavigationLink(
			destination: PlaceDetailsView(place: review.place),
			label: {
				ReviewListItemView(review: review)
					.padding([.top, .bottom], 4)
			}
		)
		.buttonStyle(HighlightButtonStyle())
		.contextMenu(menuItems: {
			ReviewActionsButtonsView(review: review, isEditing: $isEditingReview, isDeleting: $isDeletingReview)
		})
		.reviewActions(for: review, isEditing: $isEditingReview, isDeleting: $isDeletingReview)
	}
}

struct UserProfileReviewListItem_Previews: PreviewProvider {
	static var previews: some View {
		VStack {
			UserProfileReviewListItem(review: .fixture1)
			UserProfileReviewListItem(review: .fixture2)
			UserProfileReviewListItem(review: .fixture3)
			Spacer()
		}
		.previewLayout(.sizeThatFits)
		.injectPreviewsEnvironment()
	}
}
