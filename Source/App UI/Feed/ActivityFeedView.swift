import Combine
import SwiftUI

struct ActivityFeedView: View {
	@Environment(\.apiClient.services) var services
	@Environment(\.userInteractor) var userInteractor
	@EnvironmentObject var userState: UserState

	var body: some View {
		NavigationView {
			ActivityFeedContentView(model: ActivityFeedModel(feedService: services.feed,
			                                                 userChangedPublisher: userChangedPublisher,
			                                                 updateSavedStatusesAction: userInteractor.savedPlaces?.refreshStatus(for:)))
				.navigationBarTitleDisplayMode(.inline)
				.navigationTitle("activity".localizedOverrideable())
		}
		.navigationViewStyle(StackNavigationViewStyle())
		.accentColor(.gh(.onNavbar))
	}

	private var userChangedPublisher: AnyPublisher<Void, Never> {
		userState.$userID
			.removeDuplicates()
			.map { _ in }
			.dropFirst()
			.eraseToAnyPublisher()
	}
}

struct ActivityFeedContentView: View {
	@StateObject var model: ActivityFeedModel

	var body: some View {
		ListLoadingStateView(model: model, buildRow: { item in
			itemView(from: item)
		}, buildNoContent: {
			VStack(spacing: 6) {
				Image(systemName: "bolt.horizontal")
					.font(.system(size: 36))
					.foregroundColor(.gh(.labelTertiary))
					.padding(.bottom, 6)
			}
			.multilineTextAlignment(.center)
			.padding(48)
		})
		.background(Color.gh(.backgroundSecondary).ignoresSafeArea())
		.onAppear(perform: model.bindObservables)
	}

	@ViewBuilder
	private func itemView(from item: FeedItem) -> some View {
		switch item.kind {
			case let .review(review):
				FeedReviewView(review: review)
			case let .savedPlace(savedPlace):
				FeedSavedPlaceView(user: savedPlace.user, place: savedPlace.place)
			case let .advertorial(advertorial):
				FeedAdvertorialView(advertorial: advertorial)
			case let .instagram(post):
				FeedInstagramView(post: post)
			case let .tweet(post):
				FeedTweetView(post: post)
			default:
				EmptyView()
		}
	}
}

struct ActivityFeedView_Previews: PreviewProvider {
	static var previews: some View {
		NavigationView {
			ActivityFeedContentView(model: model)
				.navigationBarTitleDisplayMode(.inline)
		}
		.injectPreviewsEnvironment()
	}

	static var model: ActivityFeedModel {
		let model = ActivityFeedModel(feedService: PreviewFeedService(),
		                              userChangedPublisher: PassthroughSubject<Void, Never>().eraseToAnyPublisher(),
		                              updateSavedStatusesAction: nil)

//		model.content = .loading
//		model.content = .model([])
//		model.content = .model([.fixture1, .fixture2])
//		model.content = .error(.fixture1)

		return model
	}
}
