import SwiftUI

struct FeedTweetView: View {
	let post: FeedItem.Tweet

	var body: some View {
		NavigationLink(
			destination: destination(),
			label: {
				FeedTweetContentView(post: post)
			}
		)
	}

	@ViewBuilder
	private func destination() -> some View {
		VStack {
			FeedTweetContentView(post: post, isFullDetail: true)
			Spacer()
		}
		.navigationTitle(post.user.name)
		.background(Color.gh(.backgroundSecondary))
		.elevationLevel2()
	}
}

struct FeedTweetContentView: View {
	let post: FeedItem.Tweet
	var isFullDetail = false

	private let contentInsets = EdgeInsets(top: 0, leading: 16, bottom: 0, trailing: 16)

	var body: some View {
		VStack(alignment: .leading, spacing: 0) {
			FeedSocialActionHeaderView(avatar: post.user.avatar, name: post.user.name, screenName: post.user.screenName, icon: {
				Image("twitter-icon")
			})
			.padding(contentInsets)
			.padding([.top, .bottom])

			Divider()

			FeedSocialPostTextView(text: post.text)
				.padding(contentInsets)
				.padding([.top, .bottom])

			if let urls = photoURLs, !urls.isEmpty {
				FeedSocialPhotosInline(urls: urls, ratio: .landscape)
					.lineLimit(isFullDetail ? nil : 4)
					.padding(contentInsets)
					.padding(.bottom)
			}
		}
		.background(Color.gh(.backgroundPrimary))
		.compositingGroup()
	}

	var photoURLs: [URL]? {
		post.media?
			.filter { $0.kind == .photo }
			.map(\.url)
	}
}

struct FeedTweetView_Previews: PreviewProvider {
	static var previews: some View {
		NavigationView {
			ScrollView {
				VStack {
					FeedTweetView(post: .fixture1)
						.elevationLevel2()

					FeedTweetView(post: .fixture1)
						.elevationLevel2()
				}
			}
			.padding([.top, .bottom])
			.background(Color(.systemGroupedBackground))
			.navigationBarTitleDisplayMode(.inline)
		}
		.previewLayout(.sizeThatFits)
		.injectPreviewsEnvironment()
	}
}
