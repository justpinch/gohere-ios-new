import SwiftUI

struct FeedInstagramView: View {
	let post: FeedItem.InstagramPost

	var body: some View {
		NavigationLink(
			destination: destination(),
			label: {
				FeedInstagramContentView(post: post)
			}
		)
	}

	@ViewBuilder
	private func destination() -> some View {
		VStack {
			FeedInstagramContentView(post: post, isFullDetail: true)
			Spacer()
		}
		.navigationTitle(post.user.fullName)
		.background(Color.gh(.backgroundSecondary))
		.elevationLevel2()
	}
}

struct FeedInstagramContentView: View {
	let post: FeedItem.InstagramPost
	var isFullDetail = false

	private let margin: CGFloat = 16
	private let contentInsets = EdgeInsets(top: 0, leading: 16, bottom: 0, trailing: 16)

	var body: some View {
		VStack(alignment: .leading, spacing: 0) {
			FeedSocialActionHeaderView(avatar: post.user.avatar, name: post.user.fullName, screenName: post.user.username, icon: {
				Image("instagram-icon")
			})
			.padding(contentInsets)
			.padding([.top, .bottom], margin)

			if let urls = photoURLs, !urls.isEmpty {
				FeedSocialPhotosInline(urls: urls, ratio: .square)
					.padding(contentInsets)
					.padding(.bottom, margin)
			}

			FeedSocialPostTextView(text: post.text)
				.lineLimit(isFullDetail ? nil : 3)
				.padding(contentInsets)
				.padding(.bottom)
		}
		.background(Color.gh(.backgroundPrimary))
		.compositingGroup()
	}

	var photoURLs: [URL]? {
		post.media?
			.filter { $0.kind == .photo }
			.map(\.url)
	}
}

struct FeedInstagramView_Previews: PreviewProvider {
	static var previews: some View {
		NavigationView {
			ScrollView {
				VStack {
					FeedInstagramView(post: .fixture1)
						.elevationLevel2()

					FeedInstagramView(post: .fixture1)
						.elevationLevel2()
				}
			}
			.padding([.top, .bottom])
			.background(Color(.systemGroupedBackground))
			.navigationBarTitleDisplayMode(.inline)
		}
		.previewLayout(.sizeThatFits)
		.injectPreviewsEnvironment()
	}
}
