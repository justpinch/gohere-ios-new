import SwiftUI

struct FeedSavedPlaceView: View {
	let user: User
	let place: Place

	private let contentInsets = EdgeInsets(top: 0, leading: 16, bottom: 0, trailing: 16)

	var body: some View {
		VStack(spacing: 0) {
			NavigationLink(
				destination: UserProfileView(user),
				label: {
					FeedUserActionHeaderView(avatar: user.profile.avatar,
					                         initials: user.initials,
					                         name: user.fullName,
					                         actionDescription: "feed_saved_a_place")
						.padding(contentInsets)
						.padding([.top, .bottom])
				}
			)

			Divider()

			NavigationLink(
				destination: PlaceDetailsView(place: place),
				label: {
					PlaceInlineView(place: place)
						.fixedSize(horizontal: false, vertical: true)
						.padding(contentInsets)
						.padding([.top, .bottom])
				}
			)
		}
		.background(Color.gh(.backgroundPrimary))
		.compositingGroup()
	}
}

struct FeedSavedPlace_Previews: PreviewProvider {
	static var previews: some View {
		VStack {
			FeedSavedPlaceView(user: .fixture1, place: .fixture1)
				.elevationLevel2()

			FeedSavedPlaceView(user: .fixture2, place: .fixture2)
				.elevationLevel2()
		}
		.padding([.top, .bottom])
		.background(Color(.systemGroupedBackground))
		.previewLayout(.sizeThatFits)
		.injectPreviewsEnvironment()
	}
}
