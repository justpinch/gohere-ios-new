import SwiftUI

struct FeedReviewView: View {
	@Environment(\.appSettings) var appSettings

	let review: Review

	private let contentInsets = EdgeInsets(top: 0, leading: 16, bottom: 0, trailing: 16)

	var body: some View {
		VStack(spacing: 0) {
			if review.user.id != appSettings.mothershipId {
				NavigationLink(
					destination: UserProfileView(review.user),
					label: {
						FeedUserActionHeaderView(avatar: review.user.profile.avatar,
						                         initials: review.user.initials,
						                         name: review.user.fullName,
						                         actionDescription: "feed_placed_a_review")
					}
				)
				.padding(contentInsets)
				.padding([.top, .bottom])

				Divider()
			}

			NavigationLink(
				destination: PlaceDetailsView(place: review.place),
				label: {
					VStack(spacing: 0) {
						PlaceInlineView(place: review.place)
							.fixedSize(horizontal: false, vertical: true)
							.padding([.top, .bottom])

						ReviewSmallView(avatar: review.user.profile.avatar,
						                initials: review.user.initials,
						                rating: review.rating,
						                text: review.text,
						                images: review.images.map(\.path))
					}
					.padding(contentInsets)
					.padding(.bottom)
				}
			)
		}
		.background(Color.gh(.backgroundPrimary))
		.compositingGroup()
	}
}

struct FeedReview_Previews: PreviewProvider {
	static var previews: some View {
		VStack {
			FeedReviewView(review: .fixture1)
				.elevationLevel2()

			FeedReviewView(review: .fixture2)
				.elevationLevel2()
		}
		.padding([.top, .bottom])
		.background(Color(.systemGroupedBackground))
		.previewLayout(.sizeThatFits)
		.injectPreviewsEnvironment()
	}
}
