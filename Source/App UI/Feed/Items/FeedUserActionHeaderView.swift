import SwiftUI

struct FeedUserActionHeaderView: View {
	let avatar: ImagePath?
	let initials: String
	let name: String
	let actionDescription: LocalizedStringKey

	var body: some View {
		HStack {
			AvatarView(path: avatar, initials: initials)
				.frame(width: 44, height: 44)

			VStack(alignment: .leading) {
				Text(name)
					.font(.gh(.subhead))
					.foregroundColor(.gh(.labelPrimary))

				Text(actionDescription)
					.font(.gh(.caption1))
					.foregroundColor(.gh(.labelSecondary))
			}

			Spacer()
		}
	}
}

struct FeedUserActionHeaderView_Previews: PreviewProvider {
	static var previews: some View {
		FeedUserActionHeaderView(avatar: .fixture(.avatar1), initials: "IV", name: "Ivan Vasic", actionDescription: "feed_placed_a_review")
			.padding()
			.previewLayout(.sizeThatFits)
	}
}
