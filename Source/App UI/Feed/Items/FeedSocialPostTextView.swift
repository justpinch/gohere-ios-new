import SwiftUI

struct FeedSocialPostTextView: View {
	let text: String

	var body: some View {
		Text(text)
			.font(.gh(.footnote))
			.foregroundColor(.gh(.labelPrimary))
	}
}

struct FeedSocialPostTextView_Previews: PreviewProvider {
	static var previews: some View {
		FeedSocialPostTextView(text: FeedItem.InstagramPost.fixture1.text)
	}
}
