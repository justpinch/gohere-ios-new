import SwiftUI

struct FeedAdvertorialView: View {
	let advertorial: Advertorial

	@State var isAdVisible: Bool = false

	private let contentInsets = EdgeInsets(top: 0, leading: 16, bottom: 0, trailing: 16)

	var body: some View {
		if advertorial.ctaButton == nil, let url = advertorial.targetURL {
			Button(action: {
				isAdVisible = true
				Analytics.log(.advertorialCTA(slug: advertorial.slug, link: url))
			}, label: {
				contentView(allowsHitTesting: false)
			})
		} else {
			contentView()
		}
	}

	@ViewBuilder
	private func contentView(allowsHitTesting: Bool = true) -> some View {
		VStack(spacing: 0) {
			if let images = advertorial.images, !images.isEmpty {
				ImageGallery(images: images.map {
					ImageGallery.Image(url: $0.path.url(for: .screenWidth), fallback: $0.path.originalSizeURL())
				})
				.aspectRatio(CGFloat(max(1.0, images.first!.aspectRatio)), contentMode: .fill)
				.compositingGroup()
				.clipped()
				.allowsHitTesting(allowsHitTesting || images.count > 1)
			}

			if !advertorial.title.isEmpty ||
				!advertorial.subtitle.isEmpty ||
				(advertorial.ctaButton != nil &&
					advertorial.targetURL != nil)
			{
				HStack(spacing: 0) {
					VStack(alignment: .leading, spacing: 6) {
						if !advertorial.title.isEmpty {
							Text(advertorial.title)
								.font(.gh(.title3))
								.foregroundColor(.gh(.labelPrimary))
						}

						if !advertorial.subtitle.isEmpty {
							Text(advertorial.subtitle)
								.lineLimit(5)
								.font(.gh(.footnote))
								.foregroundColor(.gh(.labelPrimary))
						}

						if let cta = advertorial.ctaButton, let url = advertorial.targetURL {
							Button(cta.localizedTitle, action: {
								isAdVisible = true
								Analytics.log(.advertorialCTA(slug: advertorial.slug, link: url))
							})
							.primaryButtonStyle(size: .medium)
							.padding(.top)
						}
					}

					Spacer(minLength: 0)
				}
				.padding()
			}
		}
		.background(Color.gh(.backgroundPrimary))
		.compositingGroup()
		.sheet(isPresented: $isAdVisible, content: {
			if let url = advertorial.targetURL {
				targetDestinationView(.init(url, identity: \.absoluteString))
			}
		})
	}

	@ViewBuilder
	private func targetDestinationView(_ url: IdentifiableWrapper<URL, String>) -> some View {
		SafariView(url: url.wrapped)
	}
}

struct FeedAdvertorial_Previews: PreviewProvider {
	static var previews: some View {
		ScrollView {
			VStack {
				FeedAdvertorialView(advertorial: .fixture1)
					.elevationLevel2()

				FeedAdvertorialView(advertorial: .fixture2)
					.elevationLevel2()

				FeedAdvertorialView(advertorial: .fixture3)
					.elevationLevel2()
			}
		} 
		.padding([.top, .bottom])
		.background(Color(.systemGroupedBackground))
		.previewLayout(.sizeThatFits)
	}
}
