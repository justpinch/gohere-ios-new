import SwiftUI

struct FeedSocialActionHeaderView<Icon: View>: View {
	let avatar: URL?
	let name: String
	let screenName: String
	let icon: () -> Icon

	var body: some View {
		HStack {
			AvatarView(source: .url(avatar))
				.frame(width: 44, height: 44)

			VStack(alignment: .leading) {
				Text(name)
					.font(.gh(.subhead))
					.foregroundColor(.gh(.labelPrimary))

				Text("@" + screenName)
					.font(.gh(.caption1))
					.foregroundColor(.gh(.labelSecondary))
			}

			Spacer()

			icon()
		}
	}
}

struct FeedSocialActionHeaderView_Previews: PreviewProvider {
	static var previews: some View {
		VStack(spacing: 0) {
			VStack(spacing: 0) {
				FeedSocialActionHeaderView(avatar: nil, name: "GoHere", screenName: "gohereapp", icon: {
					Image("instagram-icon")
				})
				.padding()

				Divider()

				FeedSocialActionHeaderView(avatar: nil, name: "GoHere", screenName: "gohereapp", icon: {
					Image("twitter-icon")
				})
				.padding()
			}

			VStack(spacing: 0) {
				FeedSocialActionHeaderView(avatar: nil, name: "GoHere", screenName: "gohereapp", icon: {
					Image("instagram-icon")
				})
				.padding()

				Divider()

				FeedSocialActionHeaderView(avatar: nil, name: "GoHere", screenName: "gohereapp", icon: {
					Image("twitter-icon")
				})
				.padding()
			}
			.colorScheme(.dark)
			.background(Color.black)
		}
		.previewLayout(.sizeThatFits)
	}
}
