import SwiftUI

struct FeedSocialPhotosInline: View {
	enum Ratio {
		case square
		case landscape
	}

	let urls: [URL]
	let ratio: Ratio

	var gridSpacing: CGFloat = 4

	private typealias IdentifiableIndex = IdentifiableWrapper<Int, Int>
	@State private var fullScreenIndex: IdentifiableIndex? = nil

	var body: some View {
		VStack(spacing: gridSpacing) {
			row(index: 0)
			row(index: 2)
		}
		.sheet(item: $fullScreenIndex, content: imageGallery)
	}

	@ViewBuilder
	func row(index: Int) -> some View {
		HStack(spacing: gridSpacing) {
			if urls.count > index {
				image(at: index)
			}
			if urls.count > index + 1 {
				image(at: index + 1)
			}
		}
	}

	@ViewBuilder
	func image(at index: Int) -> some View {
		Button(action: {
			fullScreenIndex = .init(index)
		}, label: {
			Rectangle()
				.aspectRatio(ratio.floatValue, contentMode: .fit)
				.overlay(
					RemoteImage(url: urls[index])
						.placeholder { Color.gh(.backgroundTertiary) }
						.aspectRatio(contentMode: .fill)
				)
				.accentColor(Color.gh(.backgroundTertiary))
				.compositingGroup()
				.clipped()
		})
	}

	private func imageGallery(starting index: IdentifiableIndex) -> ImageGallery {
		let images = self.urls.map {
			ImageGallery.Image(url: $0)
		}

		return ImageGallery(images: images, isFullScreen: true, selectedIndex: index.wrapped, closeAction: {
			fullScreenIndex = nil
		})
	}
}

private extension FeedSocialPhotosInline.Ratio {
	var floatValue: CGFloat {
		switch self {
			case .square:
				return 1
			case .landscape:
				return 16.0 / 9.0
		}
	}
}

struct FeedSocialPhotosInline_Previews: PreviewProvider {
	static var urls: [URL] {
		[
			URL(string: "https://unsplash.com/photos/k5qfLbXM25s/download?force=true&w=400")!,
			URL(string: "https://unsplash.com/photos/8zsBofKrhP8/download?force=true&w=400")!,
			URL(string: "https://unsplash.com/photos/9-VxSBJz6F8/download?force=true&w=400")!,
			URL(string: "https://unsplash.com/photos/zAwhBlqWp6k/download?force=true&w=400")!,
		]
	}

	static var previews: some View {
		FeedSocialPhotosInline(urls: urls, ratio: .landscape)
	}
}
