import Combine
import Foundation
import SwiftUI

final class ActivityFeedModel: ObservableObject, ContentPagingModel, ContentRefreshableModel {
	typealias UpdateSavedStatusesAction = ([Place]) -> Void

	private let feedService: FeedService
	private let updateSavedStatusesAction: UpdateSavedStatusesAction?
	private let notificationCenter: NotificationCenterProtocol
	private let userChangedPublisher: AnyPublisher<Void, Never>

	@Published var content: ModelState<[FeedItem]> = .none

	var isPaginating: Bool {
		content.isLoading && content.modelOrPrevious != nil
	}

	private var nextPageToken: String?
	private let pageSize: UInt = 20

	private var fetchRequest: AnyCancellable?
	private var cancellables = Set<AnyCancellable>()

	init(feedService: FeedService,
	     userChangedPublisher: AnyPublisher<Void, Never>,
	     updateSavedStatusesAction: UpdateSavedStatusesAction?,
	     notificationCenter: NotificationCenterProtocol = NotificationCenter.default)
	{
		self.feedService = feedService
		self.updateSavedStatusesAction = updateSavedStatusesAction
		self.userChangedPublisher = userChangedPublisher
		self.notificationCenter = notificationCenter
	}

	func bindObservables() {
		guard cancellables.isEmpty else {
			return
		}

		// invalidate content on user change or when a review is posted
		notificationCenter
			.publisher(for: Review.Notification.posted, object: nil)
			.map { _ in }
			.merge(with: userChangedPublisher)
			.sink { [unowned self] _ in
				self.refresh()
			}
			.store(in: &cancellables)
	}

	func refresh() {
		nextPageToken = nil
		fetchNext()
	}

	func loadIfNeeded() {
		guard content == .none || content.error != nil else {
			return
		}

		fetchNext()
	}

	func itemAppeared(at index: Int) {
		guard nextPageToken != nil, !content.isLoading,
		      index > (content.model?.count ?? .max) - 3
		else {
			return
		}

		fetchNext()
	}

	private func fetchNext() {
		let replaceContent = nextPageToken == nil

		fetchRequest = feedService
			.get(pageToken: nextPageToken, limit: pageSize)
			.map { [unowned self] in
				self.nextPageToken = $0.nextPageToken

				let visibleItems = replaceContent ? [] : (self.content.modelOrPrevious ?? [])
				let pageResults = $0.results

				// eagerly batch update saved statuses
				self.updateSavedStatusesAction?(places(from: pageResults))

				let results = visibleItems + pageResults
				return results
			}
			.mapModelState(current: content.modelOrPrevious)
			.assign(to: \.content, onUnowned: self)
	}

	private func places(from items: [FeedItem]) -> [Place] {
		items.compactMap { item in
			switch item.kind {
				case let .review(review):
					return review.place
				case let .savedPlace(savedPlace):
					return savedPlace.place
				default:
					return nil
			}
		}
	}
}
