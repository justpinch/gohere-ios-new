import Combine
import Foundation
import GoogleMaps
import MapKit

final class ExploreModel: ObservableObject {
	@Published var region: MKCoordinateRegion
	@Published var locationLabel: String = ""
	@Published var filters = Filters()
	@Published var error: Error?

	@Published var isShowingMap: Bool = true {
		didSet {
			selectedMarker = nil
		}
	}

	@Published var markers: [MapPlaceMarker] = []

	@Published var shownPlaceDetails: AnyPlace?
	@Published var selectedMarker: ModelState<SelectedMapMarker>? {
		willSet {
			if let marker = (selectedMarker?.model?.marker() as? MapPlaceMarker), !markers.contains(marker) {
				// if marker is being deselected and it's not in the collection, we need to remove it from the map
				marker.map = nil
			}
		}
		didSet {
			updateSelectedMarkers()
			if selectedMarker != nil && selectedMarker != ModelState.none {
				bottomSheetPosition = .hidden
			} else if !(promotedPlaces?.places).isNullOrEmpty
						&& oldValue != nil
						&& oldValue != ModelState.none {
				bottomSheetPosition = previousBottomSheetPosition
			}
		}
	}

	@Published var userLocation: UserLocationManager.LocationState? = nil
	@Published var userLocationStatus = LocationServicesStatus.unknown
	@Published var bottomSheetPosition: PromotedBottomSheetPosition = .hidden {
		didSet {
			if oldValue != bottomSheetPosition {
				previousBottomSheetPosition = oldValue
			}
			if oldValue != bottomSheetPosition && bottomSheetPosition == .bottom {
				Analytics.log(.promotedPlacesDismissed)
			}
		}
	}
	
	@Published var isPresentingUrl: Bool = false
	@Published private(set) var promotedPlaces: PromotedPlaces? = nil

	var isUserLocationPossible: Bool {
		userLocationStatus != .denied && userLocationStatus != .disabled
	}

	var biasRegion: MKMapRect?
	let promotedPlacesZoomRadius: Int
	var shouldCenterOnUser = true
	var shouldSelectClosestPlace = true
	var externalUrl: URL? = nil

	private let responsePageSize: UInt = 35
	private let initialRegion: MKCoordinateRegion
	private var previousBottomSheetPosition: PromotedBottomSheetPosition = .hidden
	private var currentLocality: String?

	// MARK: - Dependencies

	private let placesSearchRegionCache: SearchRegionCache
	private let geocoder: Geocoder
	private let locationManager: UserLocationManager
	private let placeService: PlaceService
	private let placeExternalService: PlaceExternalService
	private let notificationCenter: NotificationCenterProtocol

	private let scheduler: DispatchQueue

	// MARK: - 🗑

	private var cancellables = Set<AnyCancellable>()
	private var lookupDetailsFetch: AnyCancellable?
	private var locateUserCancellable: AnyCancellable?
	private var searchPromotedPlacesCancellable: AnyCancellable?

	init(biasRegion: MKMapRect?,
		 promotedPlacesSearchRadius: Int,
	     placeService: PlaceService,
	     placeExternalService: PlaceExternalService,
	     locationManager: UserLocationManager = UserLocationManager(),
	     geocoder: Geocoder = CLGeocoder(),
	     notificationCenter: NotificationCenterProtocol = NotificationCenter.default,
	     scheduler: DispatchQueue = .main)
	{
		self.biasRegion = biasRegion
		self.placeService = placeService
		self.placeExternalService = placeExternalService
		self.locationManager = locationManager
		self.geocoder = geocoder
		self.notificationCenter = notificationCenter
		self.scheduler = scheduler
		self.promotedPlacesZoomRadius = promotedPlacesSearchRadius
		self.placesSearchRegionCache = SearchRegionCache(searchRadius: ExploreView.Const.PlaceSearch.radius,
		                                                 minimumSpanDistance: ExploreView.Const.PlaceSearch.minSearchAreaDiameter,
		                                                 maximumSpanDistance: ExploreView.Const.PlaceSearch.maxSearchAreaDiameter)

		self.initialRegion = biasRegion == nil ? MKCoordinateRegion(center: .approximateUserCoordinates, span: ExploreView.Const.ZoomSpan.level3) : MKCoordinateRegion(biasRegion!)
		self.region = initialRegion
	}

	// MARK: - Public interface

	/// Returns all places within the (50km extended) visible map region
	/// sorted by distance from the region center
	func listViewPlaces() -> [Place] {
		let span = region.spanInMeters()
		let extendedRegion = MKCoordinateRegion(center: region.center, latitudinalMeters: span.latitudeMeters + 50000, longitudinalMeters: span.longitudeMeters + 50000)

		let placeSet = Set(
			markers
				.filter { extendedRegion.contains($0.position) }
				.map(\.place)
		)

		return placeSet.sorted { p1, p2 -> Bool in
			p1.location.coordinate.distance(from: region.center) < p2.location.coordinate.distance(from: region.center)
		}
	}

	func bindObservables() {
		// only subscribe once
		guard cancellables.isEmpty else { return }

		$region
			.removeDuplicates()
			.debounce(for: .milliseconds(500), scheduler: DispatchQueue.main)
			.map { [unowned self] region -> AnyPublisher<CLPlacemark?, Error> in
				if region.span.latitudeDelta > ExploreView.Const.locationLabelThreshold.latitudeDelta, region.span.longitudeDelta > ExploreView.Const.locationLabelThreshold.longitudeDelta {
					return Just(nil).setFailureType(to: Error.self).eraseToAnyPublisher()
				} else {
					let location = CLLocation(latitude: region.center.latitude, longitude: region.center.longitude)
					return geocoder.reverseGeocode(location: location).map(\.first).eraseToAnyPublisher()
				}
			}
			.switchToLatest()
			.replaceError(with: nil)
			.sink { [unowned self] (placeMark: CLPlacemark?) in
				let newLocationLabel = [placeMark?.locality, placeMark?.country].compactMap { $0 }.joined(separator: ", ")
				if self.locationLabel != newLocationLabel {
					self.locationLabel = newLocationLabel
				}
				if placeMark?.locality == nil || self.currentLocality != placeMark?.locality {
					self.currentLocality = placeMark?.locality
					self.searchPromotedPlaces(location: self.region.center)
				}
			}
			.store(in: &cancellables)

		$region
			.removeDuplicates()
			.throttle(for: .milliseconds(200), scheduler: DispatchQueue.main, latest: true)
			.compactMap { [unowned self] in
				guard let markerPosition = self.selectedMarker?.model?.position else {
					return nil
				}
				return $0.center.distance(from: markerPosition)
			}
			.sink(receiveValue: { [unowned self] (distance: CLLocationDistance) in
				// If the map is moved more than 300m from selected marker, deselect it
				if distance > 300 {
					self.selectedMarker = nil
				}
			})
			.store(in: &cancellables)

		// Search for places on map move & filter change
		$region
			.debounce(for: .milliseconds(300), scheduler: scheduler)
			.removeDuplicates()
			.combineLatest(
				$filters
					.handleEvents(receiveOutput: { [unowned self] filters in
						self.prefilterLocalMarkers(with: filters)
					})
			)
			.filter { [unowned self] region, filters in
				guard !filters.hasChanges else { return true } // if filters are applied, don't consult cache
				return self.placesSearchRegionCache.shouldSearch(in: region)
			}
			.map { [unowned self] in
				self.search(region: $0.0, filters: $0.1)
			}
			.switchToLatest()
			.sink()
			.store(in: &cancellables)

		// Observe location authorization status
		locationManager
			.authorizationStatus()
			.assign(to: \.userLocationStatus, onUnowned: self)
			.store(in: &cancellables)

		// Observe review create/update
		notificationCenter
			.publisher(for: Review.Notification.posted, object: nil)
			.compactMap { $0.userInfo?[Review.Notification.Key.review] as? Review }
			.sink { [unowned self] in
				self.onReviewCreated($0)
			}
			.store(in: &cancellables)

		// Observe review delete
		notificationCenter
			.publisher(for: Review.Notification.deleted, object: nil)
			.compactMap { $0.userInfo?[Review.Notification.Key.review] as? Review }
			.sink { [unowned self] in
				self.onReviewDeleted($0)
			}
			.store(in: &cancellables)
	}

	func centerOnUserLocationIfNeeded() {
		centerOnUserLocation(force: false)
	}

	func centerOnUserLocation(force: Bool) {
		guard force || shouldCenterOnUser else { return }
		shouldCenterOnUser = false

		let initialRegion = region

		locateUserCancellable = locationManager
			.updateLocation()
			.sink(receiveValue: { [unowned self] state in
				self.userLocation = state

				guard let location = state?.value else {
					return
				}

				// if there's is a bias region and user's location is not inside it, don't zoom to user
				guard force || (self.biasRegion == nil || self.biasRegion!.contains(MKMapPoint(location.coordinate))) else { return }

				// if the initial region changed, don't zoom to location
				// (user may have dragged the map themself, we don't want to interrupt)
				guard force || (self.region == initialRegion) else { return }

				let region = MKCoordinateRegion(center: location.coordinate, span: ExploreView.Const.ZoomSpan.level1)
				self.region = region
			})
	}

	func selectGoogleMarker(for id: PlaceLookup.ID) {
		lookupDetailsFetch?.cancel()

		// first check locally, maybe we have it already
		if let marker = markers.first(where: { $0.place.idGoogle == id }) {
			select(marker: .model(SelectedMapMarker(marker: marker)))
		} else {
			lookupDetailsFetch = placeExternalService
				.import(lookupId: id, providerId: 1) // TODO: un-hardcode 1 for Google provider ID
				.map({ SelectedMapMarker(for: $0) })
				.mapModelState()
				.sink(receiveValue: { [unowned self] modelState in
					self.select(marker: modelState)
				})
		}
	}

	func setSelected(placeAutocomplete: PlaceAutocomplete) {
		lookupDetailsFetch?.cancel()

		if !isShowingMap, placeAutocomplete.isPOI {
			// when map is not shown, go straight to place details if place is POI
			shownPlaceDetails = .autocomplete(placeAutocomplete)
			return
		}

		// first check locally, maybe we have it already
		if let marker = markers.first(where: { $0.place.idGoogle == placeAutocomplete.lookupId }) {
			select(marker: .model(SelectedMapMarker(marker: marker)))
		} else {
			lookupDetailsFetch = placeExternalService
				.import(lookupId: placeAutocomplete.lookupId, providerId: placeAutocomplete.providerId)
				.map({ SelectedMapMarker(for: $0) })
				.mapModelState()
				.sink(receiveValue: { [unowned self] modelState in
					self.selectedMarker = placeAutocomplete.isPOI ? modelState : .none

					if let marker = modelState.model {
						let span = placeAutocomplete.isPOI ? ExploreView.Const.ZoomSpan.level1 : ExploreView.Const.ZoomSpan.level3
						let region = MKCoordinateRegion(center: marker.position, span: span)
						self.region = region
					}
				})
		}
	}

	func showPlaceDetails(for place: Place) {
		lookupDetailsFetch?.cancel()
		shownPlaceDetails = .place(place)
	}

	func showPlaceDetails(for lookup: PlaceLookup) {
		lookupDetailsFetch?.cancel()
		shownPlaceDetails = .lookup(lookup)
	}

	func removeAllPlaces() {
		/// Map **needs** to be cleared from each marker
		/// otherwise they stay on the map...
		markers.forEach { marker in
			marker.map = nil
		}

		selectedMarker?.model?.marker().map = nil

		markers.removeAll()
		placesSearchRegionCache.clear()
		selectedMarker = nil
	}
	
	func dismissCard() {
		selectedMarker = nil
	}
	
	func selectPromotedPlace(place: Place) {
		Analytics.log(.promotedPlaceSelected(place: place))
		if let marker = markers.first(where: { $0.place.id == place.id }) {
			select(marker: .model(SelectedMapMarker(marker: marker)))
		} else {
			let marker = MapPlaceMarker.init(place: place, isPromoted: true)
			markers.append(marker)
			select(marker: .model(SelectedMapMarker(marker: marker)))
		}
	}
	
	func openAffiliateInfo(place: Place) {
		if let affiliateInfo = place.affiliate {
			Analytics.log(.promotedPlaceAdvertorialCTA(type: affiliateInfo.kind, link: affiliateInfo.actionURL))
			externalUrl = affiliateInfo.actionURL
			isPresentingUrl = true
		}
	}
	
	func onUserChanged(_: User.ID?) {
		// remove everything
		removeAllPlaces()
		search(region: self.region, filters: self.filters).sink().store(in: &cancellables)
		searchPromotedPlaces(location: self.region.center)
	}

	// MARK: - Private

	private func updateSelectedMarkers() {
		markers.forEach { $0.isSelected = false }
		(selectedMarker?.model?.marker() as? MapPlaceMarker)?.isSelected = true
	}

	private func prefilterLocalMarkers(with filters: Filters) {
		var markers = self.markers

		// scope
		if filters.scope == .friends {
			// places with last review are from friends scope
			markers = markers.filter { marker in
				marker.place.lastReview != nil
			}
		}

		// categories
		markers = markers.filter { marker in
			filters.categories.contains(marker.place.category)
		}
		let promotedMarkers = self.apply(filters: filters, to: Array(self.promotedPlaces?.places ?? []))
		markers.append(contentsOf: promotedMarkers.map({ MapPlaceMarker.init(place: $0, isPromoted: true) }))

		self.removeAllPlaces()
		self.markers = markers
	}

	private func search(region: MKCoordinateRegion, filters: Filters) -> AnyPublisher<Void, Never> {
		let span = region.spanInMeters()
		let radius = Int(max(span.latitudeMeters, span.longitudeMeters))

		return placeService
			.search(area: (location: region.center, radius: radius),
			        scoped: filters.scope == .friends,
			        categories: filters.categories.map(\.id),
			        excludePlaceUUIDs: markers.map(\.place).map(\.id),
			        offset: 0,
			        limit: responsePageSize)
			.handleEvents(receiveOutput: { [unowned self] response in
				// if filters are applied, don't cache region results
				if !filters.hasChanges {
					let isFullySearched = response.results.count >= response.count // means that there's no more results available
					self.placesSearchRegionCache.setSearched(region: region, isFullySearched: isFullySearched)
				}

				self.addAnnotations(for: response.results)
			})
			.map { _ in }
			.catch { [unowned self] error -> Just<Void> in
				self.error = error
				return Just(())
			}
			.eraseToAnyPublisher()
	}

	private func addAnnotations(for places: [Place], arePromotedPlaces: Bool = false) {
		// de-dupe
		let existing = Set(markers.map(\.place))
		let newPlaces: [Place]
		if arePromotedPlaces {
			let toDelete = existing.intersection(Set(places))
			for index in (0..<markers.count).reversed() {
				if(toDelete.contains(markers[index].place)) {
					markers[index].map = nil
					markers.remove(at: index)
				}
			}
			newPlaces = places
		} else {
			newPlaces = places.filter { !existing.contains($0) }
		}
		markers.append(contentsOf: newPlaces.map({ MapPlaceMarker.init(place: $0, isPromoted: arePromotedPlaces) }))
	}

	private func selectClosestPlace() {
		guard biasRegion == nil else {
			// don't select closest place if we're zooming initially to the bias region
			return
		}

		guard shouldSelectClosestPlace, isShowingMap, selectedMarker == nil, !markers.isEmpty else {
			return
		}
		shouldSelectClosestPlace = false

		let referencePoint = userLocation?.value?.coordinate ?? region.center

		let sortedMarkers = markers.sorted(by: {
			let lDist = $0.place.location.coordinate.distance(from: referencePoint)
			let rDist = $1.place.location.coordinate.distance(from: referencePoint)
			return lDist < rDist
		})

		// look for the closest place with a review inside visible bounds
		let marker = sortedMarkers.first { marker in
			marker.place.lastReview != nil && region.contains(marker.place.location.coordinate)
		}

		if let marker = marker {
			select(marker: .model(SelectedMapMarker(marker: marker)))
		} else {
			if let anyMarker = sortedMarkers.first {
				select(marker: .model(SelectedMapMarker(marker: anyMarker)))
			}
		}
	}

	private func select(marker modelState: ModelState<SelectedMapMarker>) {
		selectedMarker = modelState
		if let marker = modelState.model {
			let markerRegion = MKCoordinateRegion(center: marker.position, span: ExploreView.Const.ZoomSpan.level1)
			// zoom to marker only if current region is larger than marker region
			if region.span.latitudeDelta > markerRegion.span.latitudeDelta || region.span.longitudeDelta > markerRegion.span.longitudeDelta {
				region = markerRegion
			} else {
				// else only center position to the marker leaving the old zoom level
				region = MKCoordinateRegion(center: marker.position, span: region.span)
			}
		}
	}

	private func onReviewCreated(_ review: Review) {
		var place = review.place
		place.lastReview = ReviewInline(id: review.id, user: review.user, rating: review.rating, text: review.text)
		let marker = MapPlaceMarker(place: place, isPromoted: false)

		if let index = markers.firstIndex(where: { $0.place.id == place.id }) {
			// if place marker already exists, replace it
			markers[index] = marker
		} else {
			// otherwise add the new one
			markers.append(marker)
		}
	}

	private func onReviewDeleted(_ review: Review) {
		if let index = markers.firstIndex(where: { $0.place.lastReview?.id == review.id }) {
			var place = markers[index].place
			place.lastReview = nil
			markers[index] = MapPlaceMarker(place: place, isPromoted: false)
		}

		if case let .place(marker) = selectedMarker?.model, marker.place.lastReview?.id == review.id {
			var place = marker.place
			place.lastReview = nil
			selectedMarker = .model(.place(.init(place: place, isPromoted: false)))
		}
	}
	
	private func searchPromotedPlaces(location: CLLocationCoordinate2D) {
		self.searchPromotedPlacesCancellable?.cancel()
		self.searchPromotedPlacesCancellable = placeService
			.searchPromotedPlaces(location: location, radius: promotedPlacesZoomRadius)
			.filter { [unowned self] (newPromotedPlaces: PromotedPlaces) in
				newPromotedPlaces.places.isEmpty || !newPromotedPlaces.places.isSubset(of: self.promotedPlaces?.places ?? [])
			}
			.catch { error -> Just<PromotedPlaces> in
				//We emit an empty list of places so the promoted places get hidden
				return Just((.empty()))
			}
			.sink { [unowned self] newPromotedPlaces in
				let currentMarker = (self.selectedMarker?.model?.marker() as? MapPlaceMarker)
				if newPromotedPlaces.places.isEmpty {
					self.bottomSheetPosition = .hidden
					if shouldSelectClosestPlace {
						selectClosestPlace()
					}
				} else {
					let promotedPlaces = self.apply(filters: self.filters, to: Array(newPromotedPlaces.places))
					self.addAnnotations(for: promotedPlaces, arePromotedPlaces: true)
					if currentMarker == nil || currentMarker?.isSelected == false {
						self.bottomSheetPosition = .middle
					}
					shouldSelectClosestPlace = false
				}
				self.promotedPlaces = newPromotedPlaces
			}
	}
	
	private func apply(filters: Filters, to places: [Place]) -> [Place] {
		var result = places
		let currentMarker = (selectedMarker?.model?.marker() as? MapPlaceMarker)

		// scope
		if filters.scope == .friends {
			// places with last review are from friends scope
			result = result.filter { place in
				place.lastReview != nil
			}
		}

		// categories
		if !filters.categories.isEmpty {
			result = result.filter { place in
				filters.categories.contains(place.category)
				|| (place == currentMarker?.place && currentMarker?.isSelected == true)
			}
		}
		return result
	}
}

// MARK: - Types

extension ExploreModel {
	struct Filters: Equatable {
		enum Scope: Equatable {
			case friends
			case everyone
		}

		private static let defaultScope = Scope.friends

		var scope: Scope = defaultScope
		var categories: Set<PlaceCategory> = []

		var hasChanges: Bool {
			hasCategoryChanges || scope != Self.defaultScope
		}

		var hasCategoryChanges: Bool {
			!categories.isEmpty
		}

		mutating func reset() {
			scope = Self.defaultScope
			categories = []
		}
	}
}
