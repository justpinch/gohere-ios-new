import Combine
import Foundation
import GoogleMaps
import Nuke

class MapPlaceMarker: GoHereMarker {
	let place: Place

	override var iconPath: ImagePath? {
		place.category.icon
	}

	init(place: Place, isPromoted: Bool) {
		self.place = place
		super.init()
		self.isPromoted = isPromoted

		self.position = place.location.coordinate
	}
}

class MapLookupMarker: GoHereMarker {
	let lookup: PlaceLookup

	override fileprivate var markerColors: MarkerColors {
		(fill: .gh(.backgroundPrimary),
		 stroke: .gh(.labelQuaternary),
		 selectedFill: .gh(.labelPrimary),
		 selectedStroke: .gh(.backgroundPrimary))
	}

	override var iconPath: ImagePath? {
		lookup.category?.icon
	}

	init(lookup: PlaceLookup) {
		self.lookup = lookup
		super.init()

		self.position = lookup.location.coordinate
	}
}

enum SelectedMapMarker: Equatable {
	case place(MapPlaceMarker)
	case lookup(MapLookupMarker)

	var position: CLLocationCoordinate2D {
		//Move the marker down preventing the card to hide it
		marker().position.movedBy(bearing: 180, distanceMeters: ExploreView.Const.Card.imageHeight * 1.5)
	}

	init?(marker: GMSMarker) {
		switch marker {
			case let placeMarker as MapPlaceMarker:
				self = .place(placeMarker)
			case let lookupMarker as MapLookupMarker:
				self = .lookup(lookupMarker)
			default:
				return nil
		}
	}

	init(marker: MapPlaceMarker) {
		self = .place(marker)
	}

	init(for place: Place, isPromoted: Bool = false) {
		self = .place(MapPlaceMarker(place: place, isPromoted: isPromoted))
	}

	init(marker: MapLookupMarker) {
		self = .lookup(marker)
	}

	init(for lookup: PlaceLookup) {
		self = .lookup(MapLookupMarker(lookup: lookup))
	}

	func marker() -> GMSMarker {
		switch self {
			case let .place(marker):
				return marker
			case let .lookup(marker):
				return marker
		}
	}
}

// MARK: - Base

private typealias MarkerColors = (fill: UIColor, stroke: UIColor, selectedFill: UIColor, selectedStroke: UIColor)
private typealias IconColors = (fill: UIColor, stroke: UIColor)

private let categoryIconWidth: CGFloat = 26
private let tooltipWidth: CGFloat = 48
private let tooltipHeight: CGFloat = 32
private let topWidth: CGFloat = 37
private let topHeight: CGFloat = 14

private let borderWidth: CGFloat = 2

class GoHereMarker: GMSMarker {
	var isSelected: Bool = false {
		didSet {
			if isSelected != oldValue {
				updateIconImage()
			}
		}
	}

	var isDarkMode: Bool = false {
		didSet {
			if isDarkMode != oldValue {
				updateIconImage()
			}
		}
	}
	
	var isPromoted: Bool = false {
		didSet {
			if isPromoted != oldValue {
				updateIconImage()
			}
		}
	}

	fileprivate var iconPath: ImagePath? {
		nil
	}

	fileprivate var markerColors: MarkerColors {
		(fill: .gh(.pin),
		 stroke: .gh(.onPin),
		 selectedFill: .gh(.onPin),
		 selectedStroke: .gh(.pin))
	}

	fileprivate var iconColors: IconColors {
		if isSelected {
			return (fill: markerColors.selectedFill, stroke: markerColors.selectedStroke)
		} else {
			return (fill: markerColors.fill, stroke: markerColors.stroke)
		}
	}

	fileprivate var lightIconCacheKey: String {
		"map-pin-\(self.classForCoder)"
	}

	fileprivate var darkIconCacheKey: String {
		"\(lightIconCacheKey)-dark"
	}

	fileprivate var processorCacheKey: String {
		var cacheKey = isDarkMode ? darkIconCacheKey : lightIconCacheKey
		if isSelected {
			cacheKey = "\(cacheKey)-selected"
		}
		if isPromoted {
			cacheKey = "\(cacheKey)-promoted"
		}
		return cacheKey
	}

	override init() {
		super.init()

		// TODO: Optimization: don't recreate the same image every time marker is created...
		self.icon = UIImage(systemName: "mappin")?.wrappedMapPinImage(colors: iconColors, showAsPromoted: isPromoted)

		self.updateIconImage()

		NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
	}

	func updateIconImage() {
		guard let iconPath = iconPath else {
			return
		}

		/// Skip updates if the app is not active
		guard UIApplication.shared.applicationState == .active else {
			return
		}

		let colors = self.iconColors
		let cacheKey = processorCacheKey
		let isPromoted = self.isPromoted

		let processor = ImageProcessors.Anonymous(id: cacheKey) {
			$0.wrappedMapPinImage(colors: colors, showAsPromoted: isPromoted)
		}

		ImagePipeline.shared.loadImage(with: ImageRequest(url: iconPath.originalSizeURL(), processors: [processor])) { [weak self] result in
			guard let self = self else {
				return
			}

			if let image = (try? result.get())?.image {
				self.icon = image
			}
		}
	}

	@objc func applicationDidBecomeActive() {
		updateIconImage()
	}
}

private extension UIImage {
	func wrappedMapPinImage(colors: IconColors, showAsPromoted: Bool) -> UIImage {
		let markerSize: CGSize
		if showAsPromoted {
			markerSize = CGSize(width: tooltipWidth, height: tooltipHeight + categoryIconWidth)
		} else {
			markerSize = CGSize(width: categoryIconWidth, height: categoryIconWidth)
		}
		let categoryIconSize = CGSize(width: categoryIconWidth, height: categoryIconWidth)
		
		UIGraphicsBeginImageContextWithOptions(markerSize, false, 0)

		let circleX = ((markerSize.width - categoryIconSize.width) / 2) + borderWidth
		let circleY = showAsPromoted ? tooltipHeight - (borderWidth * 2) : borderWidth
		let circlePath = UIBezierPath(ovalIn: CGRect(
			x: circleX,
			y: circleY,
			width: categoryIconSize.width - borderWidth * 2,
			height: categoryIconSize.height - borderWidth * 2)
		)
		circlePath.lineWidth = CGFloat(borderWidth)

		colors.fill.setFill()
		colors.stroke.setStroke()
		circlePath.stroke()
		circlePath.fill()

		let iconSize = categoryIconSize.width * sqrt(2) / 2 - 2 * borderWidth
		let icon = self.withTintColor(colors.stroke, renderingMode: .alwaysOriginal)
		let iconX = (markerSize.width - iconSize) / 2
		let iconY = showAsPromoted ? tooltipHeight : (markerSize.width - iconSize) / 2
		icon.draw(in: CGRect(origin: CGPoint(x: iconX, y: iconY), size: CGSize(width: iconSize, height: iconSize)))

		if showAsPromoted {
			//Tooltip
			let tooltip = UIImage(named: "tooltip")?.withTintColor(colors.stroke, renderingMode: .alwaysOriginal)
			tooltip?.draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: tooltipWidth, height: tooltipHeight)))
			
			//Top
			let top = UIImage(named: "top")?.withTintColor(colors.fill, renderingMode: .alwaysOriginal)
			let topX = (tooltipWidth - topWidth) / 2
			let topY = ((tooltipHeight * 0.8) - topHeight) / 2
			top?.draw(in: CGRect(origin: CGPoint(x: topX, y: topY), size: CGSize(width: topWidth, height: topHeight)))
		}
		
		let newImage = UIGraphicsGetImageFromCurrentImageContext()!
		UIGraphicsEndImageContext()
		return newImage
	}
}
