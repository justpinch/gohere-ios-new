import Combine
import CoreLocation
import SwiftUI

struct ExploreSearchView: View {
	@Binding var isMap: Bool
	@Binding var filters: ExploreModel.Filters
	@State var searchQuery: String = ""
	@State var isTyping: Bool = false

	let locationLabel: String
	let location: CLLocationCoordinate2D

	let selectionChanged: (PlaceAutocomplete) -> Void

	@StateObject var searchModel: AutocompleteSearchModel
	@StateObject var searchHistory = AutocompleteSearchHistory()

	var body: some View {
		VStack(spacing: 0) {
			ExploreSearchHeaderView(isMap: $isMap,
			                        searchQuery: $searchQuery,
			                        isTyping: $isTyping,
			                        filters: $filters,
			                        locationLabel: locationLabel)
				.padding(8)
				.background(headerBackgroundView())
				.background(GeometryReader(content: { geometry in
					Color.clear.preference(
						key: ExploreSearchHeaderHeightPreferenceKey.self,
						value: geometry.size.height
					)
				}))

			if isTyping {
				ExploreSearchResultsView(
					state: searchResultsState(),
					query: searchQuery,
					selectionChanged: { sel in
						selectionChanged(sel)
						searchHistory.add(query: sel.title)
					},
					querySuggestionSelected: { query in
						searchQuery = query
					}
				)
				.transition(.opacity)
			} else {
				Spacer()
			}
		}
		.onChange(of: searchQuery, perform: onQueryChanged)
	}

	func onQueryChanged(query: String) {
		guard isTyping else { return }
		searchModel.searchParams = (query, location)
	}

	func searchResultsState() -> ExploreSearchResultsView.State {
		switch searchModel.results {
			case .none:
				// TODO: suggestions
				return .start(history: searchHistory.queries, suggestions: [])
			case .loading:
				return .loading
			case let .model(model):
				return .results(model)
			case let .error(error):
				return .error(error)
		}
	}

	@ViewBuilder func headerBackgroundView() -> some View {
		if isMap, !isTyping {
			EmptyView()
		} else {
			Rectangle()
				.foregroundColor(.gh(.navbar))
				.background(
					Rectangle()
						.foregroundColor(.gh(.navbar))
						.padding(-3)
						.blur(radius: 3)
				)
				.animation(.easeInOut)
				.edgesIgnoringSafeArea(.top)
		}
	}
}

struct ExploreSearchHeaderHeightPreferenceKey: PreferenceKey {
	static let defaultValue: CGFloat = 0

	static func reduce(value: inout CGFloat, nextValue: () -> CGFloat) {
		value = max(value, nextValue())
	}
}

struct ExploreSearchView_Previews: PreviewProvider {
	static var previews: some View {
		VStack {
			ExploreSearchView(isMap: .constant(false),
			                  filters: .constant(.init()),
			                  locationLabel: "Amsterdam",
			                  location: .init(latitude: 54, longitude: 5),
			                  selectionChanged: { _ in },
			                  searchModel: .init(loader: { _ in
			                  	Just([.fixture1, .fixture2])
			                  		.setFailureType(to: API.Error.self)
			                  		.eraseToAnyPublisher()
			                  }))
			                  .colorScheme(.dark)

			ExploreSearchView(isMap: .constant(false),
			                  filters: .constant(.init()),
			                  isTyping: true,
			                  locationLabel: "Amsterdam",
			                  location: .init(latitude: 54, longitude: 5),
			                  selectionChanged: { _ in },
			                  searchModel: .init(loader: { _ in
			                  	Just([.fixture1, .fixture2])
			                  		.setFailureType(to: API.Error.self)
			                  		.eraseToAnyPublisher()
			                  }))

			ExploreSearchView(isMap: .constant(true),
			                  filters: .constant(.init()),
			                  locationLabel: "Amsterdam",
			                  location: .init(latitude: 54, longitude: 4),
			                  selectionChanged: { _ in },
			                  searchModel: .init(loader: { _ in
			                  	Just([.fixture1, .fixture2])
			                  		.setFailureType(to: API.Error.self)
			                  		.eraseToAnyPublisher()
			                  }))
		}
	}
}
