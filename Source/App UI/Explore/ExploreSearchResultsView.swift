import SwiftUI

struct ExploreSearchResultsView: View {
	enum State {
		case start(history: [String], suggestions: [String])
		case loading
		case results([PlaceAutocomplete])
		case error(Error)
	}

	let state: State
	let query: String
	let selectionChanged: (PlaceAutocomplete) -> Void
	let querySuggestionSelected: (String) -> Void

	var body: some View {
		VStack(alignment: .leading) {
			switch state {
				case let .start(history, suggestions):
					Group {
						if !history.isEmpty {
							Text("search_history")
								.font(.gh(.subhead))
								.bold()
								.foregroundColor(.gh(.tintPrimary))
								.padding(.top)

							VStack(alignment: .leading) {
								ForEach(history, id: \.self, content: suggestionTermView)
							}
							.padding(.bottom)
						}

						if !suggestions.isEmpty {
							Text("suggestions")
								.font(.gh(.subhead))
								.bold()
								.foregroundColor(.gh(.tintPrimary))
								.padding(.top)

							VStack(alignment: .leading) {
								ForEach(suggestions, id: \.self, content: suggestionTermView)
							}
						}
					}
					.padding([.leading, .trailing], 20)

				case .loading:
					List {
						PlaceLookupResultView
							.loadingPlaceholder()
							.padding([.top, .bottom])
					}
				case let .results(results):
					if results.isEmpty {
						HStack {
							Spacer()
							VStack {
								Image(systemName: "magnifyingglass")
									.font(.system(size: 38))
									.foregroundColor(.gh(.labelTertiary))
									.padding(.bottom)

								Text("No results for \(query)")
									.font(.gh(.title2))
									.foregroundColor(.gh(.labelPrimary))
									.lineLimit(2)

								Text("did_you_spell_it_correctly")
									.font(.gh(.callout))
									.foregroundColor(.gh(.labelSecondary))
							}
							Spacer()
						}
						.padding(.top, 30)
					} else {
						List {
							ForEach(results, id: \.lookupId, content: { place in
								Button(action: {
									withAnimation {
										hideKeyboard()
										selectionChanged(place)
									}
								}, label: {
									PlaceLookupResultView(title: place.title,
									                      subtitle: place.vicinity,
									                      icon: place.category?.icon)
										.contentShape(Rectangle())
								})
								.buttonStyle(HighlightButtonStyle(.opacity(0.2)))
							})
						}
					}
				case let .error(error):
					HStack {
						Spacer()
						VStack {
							Image(systemName: "xmark.octagon")
								.font(.system(size: 38))
								.foregroundColor(.gh(.utilityError))
								.padding(.bottom)

							Text(error.localizedDescription)
								.font(.gh(.callout))
								.foregroundColor(.gh(.labelSecondary))
								.multilineTextAlignment(.center)
								.fixedSize(horizontal: false, vertical: true)
								.lineLimit(5)
						}
						Spacer()
					}
					.padding(.top, 30)
					.padding([.leading, .trailing], 20)
			}

			Spacer()
		}
		.frame(maxWidth: .infinity, alignment: .topLeading)
		.background(Color(.systemBackground))
	}

	@ViewBuilder
	func suggestionTermView(term: String) -> some View {
		Button(action: {
			querySuggestionSelected(term)
		}, label: {
			Text(term)
				.font(.gh(.body))
				.foregroundColor(.gh(.labelPrimary))
				.padding(.top, 1)
		})
	}
}

struct ExploreSearchResultsView_Previews: PreviewProvider {
	static var previews: some View {
		VStack {
			ExploreSearchResultsView(state: .start(history: ["Bar Spek", "Utrecht", "Niš, Serbia"], suggestions: ["Maastricht", "Paris", "Barcelona", "Lissabon", "Berlin"]), query: "query", selectionChanged: { _ in }, querySuggestionSelected: { _ in })

			ExploreSearchResultsView(state: .loading, query: "query", selectionChanged: { _ in }, querySuggestionSelected: { _ in })

			ExploreSearchResultsView(state: .results([.fixture1, .fixture2, .fixture3]), query: "query", selectionChanged: { _ in }, querySuggestionSelected: { _ in })

			ExploreSearchResultsView(state: .results([]), query: "query", selectionChanged: { _ in }, querySuggestionSelected: { _ in })

			ExploreSearchResultsView(state: .error(API.Error.fixture1), query: "query", selectionChanged: { _ in }, querySuggestionSelected: { _ in })
		}
		.previewLayout(.sizeThatFits)
	}
}
