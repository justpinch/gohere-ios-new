import SwiftUI

struct ExploreFiltersView: View {
	@Environment(\.appSettings) var appSettings
	@Environment(\.presentationMode) var presentationMode

	@Binding var original: ExploreModel.Filters
	@State var draft: ExploreModel.Filters

	init(filters: Binding<ExploreModel.Filters>) {
		self._original = filters
		self._draft = State(initialValue: filters.wrappedValue)
	}

	var body: some View {
		NavigationView {
			VStack(spacing: 0) {
				ScrollView {
					HStack {
						Text("show_tips_from")
							.font(.gh(.title2))
							.foregroundColor(.gh(.labelPrimary))
						Spacer()
					}
					.padding(.top)
					.padding([.leading, .trailing], 30)

					ExploreFiltersScopePicker(selectedScope: $draft.scope)
						.padding([.top, .bottom])
						.padding([.leading, .trailing], 30)

					Divider()

					HStack {
						Text(categoriesTitle)
							.font(.gh(.title2))
							.foregroundColor(.gh(.labelPrimary))

						Spacer()

						Button("select_all", action: selectAllCategories)
							.accentColor(.gh(.tintPrimary))
							.font(.gh(.subhead))
							.disabled(!draft.hasCategoryChanges)
					}
					.padding(.top)
					.padding([.leading, .trailing], 30)

					LazyVStack {
						ForEach(sortedCategories()) { category in
							Button(action: {
								select(category)
							}, label: {
								ExploreFiltersCategoryView(icon: category.icon, title: category.localizedName, isHighlighted: isSelected(category))
							})
						}
					}
					.padding([.leading, .trailing], 30)
					.padding(.bottom)
				}

				FixedFooterBar(buttonTitle: "show_results", action: commit)
			}
			.navigationTitle("filter")
			.navigationBarTitleDisplayMode(.inline)
			.navigationBarItems(leading: closeButton(), trailing: resetButton())
		}.accentColor(.gh(.onNavbar))
	}

	private var categoriesTitle: String {
		let title = NSLocalizedString("categories", comment: "")
		if draft.hasCategoryChanges {
			return title + " (\(draft.categories.count))"
		} else {
			return title
		}
	}

	func isSelected(_ category: PlaceCategory) -> Bool {
		draft.categories.isEmpty || draft.categories.contains(category)
	}

	func sortedCategories() -> [PlaceCategory] {
		appSettings.categories.sorted { c1, c2 in
			c1.localizedName.localizedStandardCompare(c2.localizedName) == .orderedAscending
		}
	}

	func commit() {
		defer {
			dismiss()
		}

		guard draft != original else {
			return
		}

		original = draft
	}

	func dismiss() {
		presentationMode.wrappedValue.dismiss()
	}

	func select(_ category: PlaceCategory) {
		if draft.categories.contains(category) {
			draft.categories.remove(category)
		} else {
			draft.categories.insert(category)
		}

		if draft.categories == Set(appSettings.categories) {
			draft.categories.removeAll()
		}
	}

	func selectAllCategories() {
		draft.categories.removeAll()
	}

	@ViewBuilder
	func closeButton() -> some View {
		Button(action: dismiss, label: {
			Image(systemName: "xmark")
				.font(.system(size: 14, weight: .semibold))
				.foregroundColor(.gh(.labelPrimary))
				.padding(8)
				.background(Circle().foregroundColor(.gh(.tintQuaternary)))
		})
	}

	@ViewBuilder
	func resetButton() -> some View {
		Button("reset", action: {
			draft.reset()
		})
		.font(.gh(.subhead))
		.accentColor(.gh(.onTintPrimary))
		.disabled(!draft.hasChanges)
	}
}

struct ExploreFiltersView_Previews: PreviewProvider {
	static var previews: some View {
		ExploreFiltersView(filters: .constant(.init(categories: [.fixture1])))
			.injectPreviewsEnvironment()
	}
}
