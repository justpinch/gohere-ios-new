import SwiftUI

struct ExploreFiltersScopePicker: View {
	@Binding var selectedScope: ExploreModel.Filters.Scope

	var body: some View {
		VStack {
			ScopeRow(text: "my_friends", scope: .friends, selectedScope: $selectedScope)
			ScopeRow(text: "everyone", scope: .everyone, selectedScope: $selectedScope)
		}
	}
}

private struct ScopeRow: View {
	let text: LocalizedStringKey
	let scope: ExploreModel.Filters.Scope
	@Binding var selectedScope: ExploreModel.Filters.Scope

	var isSelected: Bool {
		selectedScope == scope
	}

	var body: some View {
		Button(action: {
			selectedScope = scope
		}, label: {
			HStack {
				Text(text)
					.font(.gh(.subhead))
					.foregroundColor(isSelected ? .gh(.labelPrimary) : .gh(.labelQuaternary))

				Spacer()

				if isSelected {
					Image(systemName: "checkmark")
						.foregroundColor(.gh(.tintPrimary))
						.font(.system(size: 18, weight: .bold, design: .default))
				}
			}
			.padding([.top, .bottom], 4)
		})
	}
}

struct ExploreFiltersScopePicker_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			ExploreFiltersScopePicker(selectedScope: .constant(.everyone))
				.padding()
				.previewLayout(.sizeThatFits)

			ExploreFiltersScopePicker(selectedScope: .constant(.everyone))
				.previewDarkTheme()
		}
	}
}
