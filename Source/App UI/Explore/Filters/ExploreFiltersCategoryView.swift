import SwiftUI

struct ExploreFiltersCategoryView: View {
	let icon: ImagePath
	let title: String
	var isHighlighted = true

	@Environment(\.colorScheme) private var colorScheme

	var body: some View {
		HStack {
			PlaceCategoryImage(icon: icon, tintColor: UIColor.gh(isHighlighted ? .filterIcon : .labelQuaternary))
				.frame(width: 24, height: 24)

			Text(title)
				.font(.gh(.subhead))
				.foregroundColor(isHighlighted ? .gh(.labelPrimary) : .gh(.labelQuaternary))

			Spacer()
		}
	}
}

struct ExploreFiltersCategoryView_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			ExploreFiltersCategoryView(icon: PlaceCategory.fixture1.icon, title: PlaceCategory.fixture1.localizedName)
				.padding()
				.previewLayout(.sizeThatFits)

			ExploreFiltersCategoryView(icon: PlaceCategory.fixture1.icon, title: PlaceCategory.fixture1.localizedName, isHighlighted: false)
				.padding()
				.previewLayout(.sizeThatFits)

			ExploreFiltersCategoryView(icon: PlaceCategory.fixture2.icon, title: PlaceCategory.fixture2.localizedName)
				.previewDarkTheme()
		}
	}
}
