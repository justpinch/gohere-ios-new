//
//  BottomSheetState.swift
//  GoHere
//
//  Created by jorisfavier on 21/03/2022.
//

import Foundation
import CoreGraphics

public enum PromotedBottomSheetPosition: CGFloat, CaseIterable {
	case hidden = 0, middle = 400, bottom = 150
}

extension PromotedBottomSheetPosition {
	//Workaround due to the fact that the bottom sheet lib
	//is using the bottom sheet position value + some offset value depending on the position
	func getValue() -> CGFloat {
		switch self {
			case .middle:
			return rawValue + 30
			case .bottom:
				return rawValue
			case .hidden:
				return rawValue
		}
	}
}
