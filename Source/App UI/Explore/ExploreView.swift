import Combine
import MapKit
import SwiftUI
import BottomSheet

struct ExploreView: View {
	@StateObject var model: ExploreModel

	@State private var searchHeaderHeight: CGFloat = 0
	@State private var isPresentedStartReview: Bool = false

	@State private var isPresentingSignIn: Bool = false
	@State private var ratingSheet: AnyPlaceReviewParams?

	/// workaround: see below
	@State private var isBarHidden: Bool = true

	@EnvironmentObject var userState: UserState
	@Environment(\.apiClient.services) var services

	var body: some View {
		NavigationView {
			ZStack {
				GeometryReader { geometry in
					let buttonOffset = model.bottomSheetPosition != .hidden ? (-model.bottomSheetPosition.getValue()) + geometry.safeAreaInsets.bottom : 0
					GMap(markers: model.markers,
						 onGooglePlaceSelected: model.selectGoogleMarker(for:),
						 selectedMarker: selectedMarkerBinding(),
						 region: $model.region)
						.edgesIgnoringSafeArea(.top)
						.bottomSheet(
							bottomSheetPosition: $model.bottomSheetPosition,
							options: [
								.background(AnyView(Color.gh(.bottomSheet))),
								.dragIndicatorColor(Color.gh(.labelQuaternary)),
								.absolutePositionValue
							],
							headerContent: {
								Text(model.promotedPlaces?.title ?? "")
									.font(.gh(.title2))
									.padding([.top], 10)
							}
						) {
							if let promotedPlaces = model.promotedPlaces {
								PromotedPlacesPagerView(
									places: promotedPlaces.places,
									onPromotedPlaceTapped: model.selectPromotedPlace,
									onAffiliateButtonTapped: model.openAffiliateInfo
								)
							}
						}
					if !model.isShowingMap {
						ExploreListView(places: model.listViewPlaces(), selectedPlace: $model.shownPlaceDetails)
							.padding(.top, searchHeaderHeight)
					}
					
					VStack(spacing: 0) {
						Spacer()
						HStack {
							Spacer()
							VStack {
								if model.isShowingMap {
									locationButton()
								}
								
								addReviewButton()
							}
						}
						.padding(.bottom)
						
						if model.isShowingMap, let marker = model.selectedMarker {
							switch marker {
							case .none:
								EmptyView()
							case .loading:
								loadingCard()
							case let .model(marker):
								switch marker {
								case let .place(placeMarker):
									card(for: placeMarker.place)
								case let .lookup(lookupMarker):
									card(for: lookupMarker.lookup)
								}
							case let .error(error):
								Text(error.localizedDescription)
									.foregroundColor(.gh(.utilityError))
									.font(.gh(.subhead))
									.padding()
									.background(Color.gh(.backgroundPrimary))
									.cornerRadius(10)
							}
						}
					}
					.padding([.leading, .trailing, .bottom], 20)
					.offset(x: 0, y: buttonOffset)
					.transition(.opacity)
					.animation(.default)
					
					ExploreSearchView(isMap: $model.isShowingMap,
									  filters: $model.filters,
									  locationLabel: model.locationLabel,
									  location: model.region.center,
									  selectionChanged: model.setSelected,
									  searchModel: .init(loader: services.placesExternal.autocomplete))
						.onPreferenceChange(ExploreSearchHeaderHeightPreferenceKey.self, perform: { value in
							searchHeaderHeight = value
						})
				}

			}
			.navigationBarHidden(isBarHidden)
			.onAppear(perform: {
				isBarHidden = true
			})
			.navigate(using: $model.shownPlaceDetails, destination: placeDetailsView)
			.sheet(item: $ratingSheet, content: anyPlaceReviewView)
		}
		.navigationViewStyle(StackNavigationViewStyle())
		.accentColor(.gh(.onNavbar))
		.onAppear(perform: model.bindObservables)
		.onAppear(perform: model.centerOnUserLocationIfNeeded)
		.onChange(of: userState.userID, perform: model.onUserChanged)
		.signInPrompt(isPresented: $isPresentingSignIn)
		.fullScreenCover(isPresented: $model.isPresentingUrl, content: {
			if let url = model.externalUrl {
				SafariView(url: url)
			}
		})
	}

	@ViewBuilder
	private func locationButton() -> some View {
		Button(action: {
			if model.isUserLocationPossible {
				model.centerOnUserLocation(force: true)
			} else {
				if let url = URL(string: UIApplication.openSettingsURLString) {
					UIApplication.shared.open(url, options: [:], completionHandler: nil)
				}
			}
		}, label: {
			if model.isUserLocationPossible {
				if model.userLocation?.isLocating ?? false {
					ProgressView()
				} else {
					Image(systemName: "location.fill")
						.foregroundColor(Color(.init(rgb: 0x2C98E6)))
				}
			} else {
				Image(systemName: "location.slash.fill")
					.foregroundColor(.gh(.labelSecondary))
			}
		})
		.floatingActionButtonStyle()
		.disabled(model.userLocation?.isLocating ?? false)
	}

	@ViewBuilder
	private func addReviewButton() -> some View {
		Button(action: {
			isPresentedStartReview = true
		}, label: {
			HStack(spacing: 0) {
				Image(systemName: "plus")
					.font(Font.body.weight(.bold))
					.imageScale(.large)
				if !model.isShowingMap {
					Text("place_review")
						.font(.gh(.callout))
						.padding(.leading, 6)
				}
			}
			.foregroundColor(.gh(.onPin))
		})
		.floatingActionButtonStyle(width: model.isShowingMap ? .compact : .expanded, backgroundColor: .gh(.pin))
		.fullScreenCoverIfSignedIn(isPresented: $isPresentedStartReview) {
			NavigationView {
				StartReviewView()
			}
			.environment(\.isModalPresented, $isPresentedStartReview)
			.accentColor(.gh(.onNavbar))
		}
	}

	@ViewBuilder
	private func card(for place: Place) -> some View {
		Button(action: {
			model.showPlaceDetails(for: place)
		}, label: {
			ExploreCardView(image: place.photo,
			                name: place.name,
			                category: place.category,
			                distance: place.location.prettyDistanceFromUser(),
			                saveButton: SavePlaceButton(place: place)) {
				if let review = place.lastReview {
					ReviewSmallView(avatar: review.user.profile.avatar,
					                initials: review.user.initials,
					                name: review.user.fullName,
					                rating: review.rating,
					                text: review.text,
					                images: [])
				} else {
					addReviewContent(selectedPlace: .place(place))
				}
			}
		})
		.buttonStyle(HighlightButtonStyle())
	}

	@ViewBuilder
	private func card(for lookup: PlaceLookup) -> some View {
		Button(action: {
			model.showPlaceDetails(for: lookup)
		}, label: {
			ExploreCardView(image: nil, name: lookup.title, category: lookup.category, content: {
				addReviewContent(selectedPlace: .lookup(lookup))
			})
		})
		.buttonStyle(HighlightButtonStyle())
	}

	@ViewBuilder
	private func loadingCard() -> some View {
		ExploreCardView(image: nil, name: "**********", category: PlaceCategory(id: 0, slug: "category_museum", icon: .init(path: "")), content: {
			addReviewContent(selectedPlace: .placeID("1"))
		})
		.redacted(reason: .placeholder)
		.disabled(true)
	}

	@ViewBuilder
	private func addReviewContent(selectedPlace: AnyPlace) -> some View {
		if Config.isRatingAllowed() {
			HStack {
				Text("place_first_review")
					.font(.caption)
					.foregroundColor(.gh(.tintPrimary))
				
				Spacer(minLength: 16)
				
				RatingInputSelectionView(onRatingSelected: { rating in
					if userState.isAuthenticated {
						ratingSheet = .init(selection: selectedPlace, rating: rating)
						model.dismissCard()
					} else {
						isPresentingSignIn = true
					}
				})
				.frame(height: 22)
				.fixedSize(horizontal: false, vertical: true)
			}
			.padding()
			.background(Color.gh(.backgroundTertiary))
			.cornerRadius(8)
		} else {
			EmptyView()
		}
	}

	@ViewBuilder
	private func anyPlaceReviewView(params: AnyPlaceReviewParams) -> some View {
		NavigationView {
			AnyPlaceReviewAddView(model: AnyPlaceReviewAddModel(source: params.place,
			                                                    initialRating: params.rating,
			                                                    placeService: services.places,
			                                                    placeExternalService: services.placesExternal))
		}
		.accentColor(.gh(.onNavbar))
		.provideEnvironmentObjects()
	}

	private func selectedMarkerBinding() -> Binding<SelectedMapMarker?> {
		.init(get: { model.selectedMarker?.modelOrPrevious },
		      set: { marker in
		      	if let marker = marker {
		      		model.selectedMarker = .model(marker)
		      	} else {
		      		model.selectedMarker = nil
		      	}
		      })
	}

	private func placeDetailsView(for selectedPlace: AnyPlace) -> some View {
		let view: PlaceDetailsView

		switch selectedPlace {
			case let .placeID(id):
				view = PlaceDetailsView(id: id)
			case let .place(place):
				view = PlaceDetailsView(place: place)
			case let .lookup(lookup):
				view = PlaceDetailsView(lookup: lookup)
			case let .autocomplete(autocomplete):
				view = PlaceDetailsView(autocomplete: autocomplete)
		}

		return view.modifier(NavBarWorkaround(isBarHidden: $isBarHidden))
	}
}

extension ExploreView {
	private struct AnyPlaceReviewParams: Identifiable {
		var id: String {
			switch place {
				case let .lookup(p):
					return "lookup" + p.lookupId + "\(rating)"
				case let .place(p):
					return "place" + p.id + "\(rating)"
				case let .placeID(id):
					return "id" + id + "\(rating)"
				case let .autocomplete(a):
					return "autocomplete" + a.lookupId + "\(rating)"
			}
		}

		let place: AnyPlace
		let rating: Int

		init(selection: AnyPlace, rating: Int) {
			self.place = selection
			self.rating = rating
		}
	}

	enum Const {
		enum ZoomSpan {
			static let level1 = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
			static let level2 = MKCoordinateSpan(latitudeDelta: 0.08, longitudeDelta: 0.08)
			static var level3 = MKCoordinateSpan(latitudeDelta: 0.12, longitudeDelta: 0.12)
			static var level4 = MKCoordinateSpan(latitudeDelta: 0.20, longitudeDelta: 0.20)
		}

		/// Zoom levels broader than this will cause current location label to be removed
		static let locationLabelThreshold = MKCoordinateSpan(latitudeDelta: 0.30, longitudeDelta: 0.30)

		enum PlaceSearch {
			/// Default search radius for GoHere places
			static let radius: CLLocationDistance = 10000 /* m */
			/// Minimum diameter for search area for GoHere places. Areas smaller than this will not be searched
			static let minSearchAreaDiameter: CLLocationDistance = 500 /* m */
			/// Maximum diameter for search area for GoHere places. Areas bigger than this will not be searched (e.g. map zoomed out a lot)
			static let maxSearchAreaDiameter: CLLocationDistance = 20000 /* m */
		}
		
		enum Card {
			static let imageHeight: CGFloat = 160
		}
	}
}

// MARK: - WORKAROUND

/// ⚠️ WORKAROUND
/// There are problems when explore view hides the navbar and the subsequently pushed
/// view shows it. The bar will sometimes disappear or be otherwise jerky. The only
/// workaround that fixed that behavior is by using the binding for showing/hiding the bar.
/// See: https://stackoverflow.com/a/60526662
private struct NavBarWorkaround: ViewModifier {
	@Binding var isBarHidden: Bool

	func body(content: Content) -> some View {
		content
			.onAppear {
				isBarHidden = false
			}
	}
}

// MARK: - Previews

struct ExploreView_Previews: PreviewProvider {
	static var previews: some View {
		{ () -> ExploreView in
			let model = ExploreModel(biasRegion: nil, promotedPlacesSearchRadius: 0, placeService: PreviewPlaceService(), placeExternalService: PreviewPlaceExternalService())
			model.selectedMarker = .model(SelectedMapMarker(marker: MapPlaceMarker(place: .fixture2, isPromoted: false)))
			let view = ExploreView(model: model)
			return view
		}()
			.injectPreviewsEnvironment()
	}
}
