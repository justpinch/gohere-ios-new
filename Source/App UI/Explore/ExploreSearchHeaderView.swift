import SwiftUI

struct ExploreSearchHeaderView: View {
	@Binding var isMap: Bool
	@Binding var searchQuery: String
	@Binding var isTyping: Bool
	@Binding var filters: ExploreModel.Filters

	@State var isShowingFilters: Bool = false

	let locationLabel: String

	private var isFlat: Bool { !isMap || isTyping }

	var body: some View {
		HStack(alignment: .center) {
			HStack {
				TextField(LocalizedStringKey("search_placeholder"), text: textBinding(), onEditingChanged: { value in isTyping = value })
					.font(.gh(.subhead))
					.foregroundColor(.gh(.labelPrimary))

				if !isTyping {
					ZStack {
						Button(action: { isShowingFilters = true }, label: {
							Image(systemName: "slider.horizontal.3")
						})
						.foregroundColor(.gh(.labelSecondary))
						.padding(.leading, 8)

						if filters.hasChanges {
							Circle()
								.foregroundColor(.gh(.utilityError))
								.frame(width: 10, height: 10)
								.offset(x: 12, y: -8)
						}
					}
				} else if !searchQuery.isEmpty {
					Button(action: {
						searchQuery = ""
					}, label: {
						Image(systemName: "xmark.circle.fill")
					})
					.foregroundColor(.gh(.labelSecondary))
					.padding(.leading, 8)
				}
			}
			.padding(10)
			.background(
				Color(isFlat ? .systemGray5 : .systemBackground)
					.cornerRadius(8)
					.shadow(color: isFlat ? .clear : Color(.sRGBLinear, white: 0, opacity: 0.15), radius: 8, x: 0, y: 4)
			)

			if isTyping {
				Button(action: {
					withAnimation {
						hideKeyboard()
					}
				}, label: {
					Text("cancel")
						.font(.gh(.subhead))
						.foregroundColor(.gh(isFlat ? .onNavbar : .labelSecondary))
				})
			} else {
				Button(action: {
					withAnimation { isMap.toggle() }
				}, label: {
					Image(systemName: isMap ? "list.bullet" : "map")
				})
				.accentColor(.gh(.onPin))
				.padding(16)
				.background(
					Color(.systemBackground)
						.clipShape(Circle())
						.shadow(color: isFlat ? .clear : Color(.sRGBLinear, white: 0, opacity: 0.15), radius: 8, x: 0, y: 4)
				)
			}
		}
		.padding(8)
		.sheet(isPresented: $isShowingFilters, content: filtersView)
	}

	private func textBinding() -> Binding<String> {
		Binding { isTyping ? searchQuery : locationLabel }
			set: { string in
				if isTyping {
					searchQuery = string
				}
			}
	}

	@ViewBuilder
	func filtersView() -> some View {
		ExploreFiltersView(filters: $filters)
	}
}

struct ExploreSearchHeaderView_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			ZStack {
				ExploreSearchHeaderView(isMap: .constant(true), searchQuery: .constant("Amsterdam"), isTyping: .constant(false), filters: .constant(.init(scope: .friends, categories: [])), locationLabel: "Amsterdam, Netherlands")
			}
			.preferredColorScheme(.light)
			.padding(50)
			.background(Color.white)
			.previewLayout(.sizeThatFits)
			ZStack {
				ExploreSearchHeaderView(isMap: .constant(true), searchQuery: .constant("Amsterdam"), isTyping: .constant(false), filters: .constant(.init()), locationLabel: "Amsterdam, Netherlands")
			}
			.preferredColorScheme(.light)
			.padding(50)
			.background(Color.white)
			.previewLayout(.sizeThatFits)

			ZStack {
				ExploreSearchHeaderView(isMap: .constant(false), searchQuery: .constant("Amsterdam"), isTyping: .constant(false), filters: .constant(.init()), locationLabel: "Amsterdam, Netherlands")
			}
			.preferredColorScheme(.dark)
			//			.preferredColorScheme(.dark)
			.environment(\.sizeCategory, .extraLarge)
			.padding(50)
			.background(Color.white)
			.previewLayout(.sizeThatFits)

			ZStack {
				ExploreSearchHeaderView(isMap: .constant(false), searchQuery: .constant("Amsterdam"), isTyping: .constant(true), filters: .constant(.init()), locationLabel: "Amsterdam, Netherlands")
			}
			//			.preferredColorScheme(.dark)
			.environment(\.sizeCategory, .extraLarge)
			.padding(50)
			.background(Color.white)
			.previewLayout(.sizeThatFits)
			ZStack {
				ExploreSearchHeaderView(isMap: .constant(false), searchQuery: .constant("Amsterdam"), isTyping: .constant(true), filters: .constant(.init()), locationLabel: "Amsterdam, Netherlands")
			}
			.preferredColorScheme(.dark)
			//			.preferredColorScheme(.dark)
			.environment(\.sizeCategory, .extraLarge)
			.padding(50)
			.background(Color.white)
			.previewLayout(.sizeThatFits)
		}
	}
}
