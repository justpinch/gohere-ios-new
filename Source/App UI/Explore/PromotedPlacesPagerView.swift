//
//  PromotedPlacesPagerView.swift
//  GoHere
//
//  Created by jorisfavier on 22/03/2022.
//

import SwiftUI
import SwiftUIPager

struct PromotedPlacesPagerView: View {
	let places: Set<Place>
	let onPromotedPlaceTapped: (Place) -> ()
	let onAffiliateButtonTapped: (Place) -> ()
	
	@State private var page: Page = .first()
	
    var body: some View {
		Pager(page: page, data: Array(places), id: \.id) { place in
			ExploreCardView(
				image: place.photo,
				name: place.name,
				category: place.category,
				distance: place.location.prettyDistanceFromUser(),
				saveButton: nil,
				imageHeight: 140
			) {
				if let affiliateInfo = place.affiliate {
					Button(action: { onAffiliateButtonTapped(place) }) {
						Text(affiliateInfo.callToAction)
					}
					.affiliateButtonStyle(size: .small, width: .infinity)
				} else {
					EmptyView()
				}
			}.onTapGesture {
				onPromotedPlaceTapped(place)
			}
		}
		.itemSpacing(10)
		.padding(20)
    }
}

struct PromotedPlacesPagerView_Previews: PreviewProvider {
    static var previews: some View {
		PromotedPlacesPagerView(
			places: [.fixture1, .fixture2],
			onPromotedPlaceTapped: {_ in },
			onAffiliateButtonTapped: {_ in }
		)
    }
}
