import SwiftUI

struct ExploreListView: View {
	let places: [Place]
	@Binding var selectedPlace: AnyPlace?

	var body: some View {
		ScrollView {
			LazyVStack {
				ForEach(places) { place in
					Button(action: {
						selectedPlace = .place(place)
					}, label: {
						PlaceListItemView(place: place)
					})
					.buttonStyle(HighlightButtonStyle())
				}
			}
		}
		.background(Color.gh(.backgroundPrimary))
	}
}

struct ExploreListView_Previews: PreviewProvider {
	static var previews: some View {
		ExploreListView(places: [.fixture1, .fixture2], selectedPlace: .constant(nil))
			.injectPreviewsEnvironment()
	}
}
