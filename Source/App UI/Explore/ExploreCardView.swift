import SwiftUI
import Nuke

struct ExploreCardView<Content: View>: View {
	let image: ImagePath?
	let name: String
	let category: PlaceCategory?
	let distance: String?
	let saveButton: SavePlaceButton?
	let content: Content
	let imageHeight: CGFloat

	@Environment(\.colorScheme) private var colorScheme

	init(
		image: ImagePath?,
		name: String,
		category: PlaceCategory?,
		distance: String? = nil,
		saveButton: SavePlaceButton? = nil,
		imageHeight: CGFloat = ExploreView.Const.Card.imageHeight,
		@ViewBuilder content: () -> Content
	) {
		self.image = image
		self.name = name
		self.category = category
		self.distance = distance
		self.saveButton = saveButton
		self.imageHeight = imageHeight
		self.content = content()
	}

	var body: some View {
		VStack(alignment: .leading, spacing: 0) {
			if let image = image {
				GeometryReader { geometry in
					RemoteImage(
						path: image,
						size: .screenWidth,
						processors: [
							ImageProcessors.Resize(
								size: CGSize(width: geometry.size.width, height: imageHeight),
								crop: true
							)
						]
					)
					.placeholder { Color.gh(.backgroundTertiary) }
					.id(image.path)
				}
				.frame(height: imageHeight)
			}

			HStack(alignment: .top) {
				VStack(alignment: .leading, spacing: 4) {
					Text(name)
						.lineLimit(2)
						.minimumScaleFactor(0.85)
						.font(.gh(.title3))
						.foregroundColor(.gh(.labelPrimary))

					if let category = category {
						HStack {
							PlaceCategoryImage(icon: category.icon)
								.frame(width: 24, height: 24)

							Text(category.localizedName)
								.font(.gh(.caption1))
								.foregroundColor(.gh(.labelPrimary))

							if let distance = distance {
								Text("• \(distance)")
									.font(.gh(.caption1))
									.foregroundColor(.gh(.labelTertiary))
							}
						}
					}
				}
				Spacer()
				saveButton
			}
			.padding()

			content
				.padding([.leading, .trailing, .bottom])
		}
		.background(Color(.systemBackground))
		.cornerRadius(8)
		.clipped()
		.elevationLevel2()
	}
}

struct ExploreCardView_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			ExploreCardView(image: .fixture(.place2),
			                name: Place.fixture2.name,
			                category: Place.fixture2.category,
			                distance: "500 m",
			                saveButton: SavePlaceButton(place: .fixture2),
			                content: {
			                	Rectangle()
			                		.frame(height: 40)
			                		.foregroundColor(.pink)
			                })
		}
		.padding()
		.previewLayout(.sizeThatFits)
		.injectPreviewsEnvironment()
	}
}
