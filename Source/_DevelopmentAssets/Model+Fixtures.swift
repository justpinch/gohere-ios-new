import Foundation
import UIKit

extension API.Error {
	static var fixture1: Self {
		API.Error(reason: NSError(domain: "app.gohere", code: 123, userInfo: [NSLocalizedDescriptionKey: "GoHere: (500) An unknown error in occured."]))
	}
}

extension ImagePath {
	var isFixture: Bool { self.path.hasPrefix("previews-") }

	enum Fixture: String {
		case avatar1
		case avatar2
		case avatar3
		case avatar4
		case avatar5

		case place1
		case place2
		case place3
		case place4

		case category1
		case category2
		case category3
	}

	static func fixture(_ fixture: Fixture) -> Self { ImagePath(path: "previews-\(fixture.rawValue)") }
}

extension Bootstrap {
	static var fixture1: Self {
		Bootstrap(
			mothershipId: User.fixture1.id,
			categories: [.fixture1, .fixture2],
			defaultRegion: nil,
			biasLocation: nil,
			bannerId: nil,
			uberClientId: nil,
			promotedPlacesSearchRadius: 0
		)
	}
}

extension AuthToken {
	static var fixture1: Self {
		AuthToken(userId: User.fixture1.id, access: "access_token1", refresh: "refresh_token1")
	}
}

extension Advertorial {
	static var fixture1: Self {
		Advertorial(id: "adv-1",
		            slug: "promo-1",
		            title: "New promotion available",
		            subtitle: "Get yours right now by clicking on the button below",
		            publishDate: Date(),
		            canHide: true,
		            ctaButton: .learnMore,
		            location: .approximateUserCoordinates,
		            images: [.init(path: .fixture(.place1), aspectRatio: 16.0 / 9.0), .init(path: .fixture(.place2), aspectRatio: 16.0 / 9.0)],
		            targetURL: URL(string: "https://www.gohere.app/"))
	}

	static var fixture2: Self {
		Advertorial(id: "adv-2",
		            slug: "promo-2",
		            title: "",
		            subtitle: "",
		            publishDate: Date(),
		            canHide: true,
		            ctaButton: nil,
		            location: nil,
		            images: [.init(path: .fixture(.place2), aspectRatio: 16.0 / 9.0)],
		            targetURL: URL(string: "https://www.gohere.app/"))
	}

	static var fixture3: Self {
		Advertorial(id: "adv-3",
		            slug: "promo-2",
		            title: "",
		            subtitle: "Only description text in this promo.",
		            publishDate: Date(),
		            canHide: true,
		            ctaButton: .buyNow,
		            location: nil,
		            images: [],
		            targetURL: nil)
	}
}

extension FeedResponse {
	static var fixture1: Self {
		FeedResponse(results: [.fixtureAdvertorial1, .fixtureReview1, .fixtureSavedPlace1, .fixtureTweet1, .fixtureInstagram1], nextPageToken: nil)
	}
}

extension FeedItem {
	static var fixtureReview1: Self {
		FeedItem(kind: .review(.fixture1), timestamp: Date().addingTimeInterval(-1))
	}

	static var fixtureSavedPlace1: Self {
		FeedItem(kind: .savedPlace(.init(user: .fixture1, place: .fixture1)), timestamp: Date().addingTimeInterval(-2))
	}

	static var fixtureAdvertorial1: Self {
		FeedItem(kind: .advertorial(.fixture1), timestamp: Date().addingTimeInterval(-2))
	}

	static var fixtureTweet1: Self {
		FeedItem(kind: .tweet(.fixture1), timestamp: Date().addingTimeInterval(-3))
	}

	static var fixtureInstagram1: Self {
		FeedItem(kind: .instagram(.fixture1), timestamp: Date().addingTimeInterval(-4))
	}
}

extension FeedItem.Tweet {
	static var fixture1: Self {
		.init(id: 112,
		      createdAt: Date(),
		      text: "Great weather may not be guaranteed, but one thing you can be sure of is otherworldly vistas, whether the sun comes out or not. Get to know Ireland's 6 national parks. https://t.co/YkDTuUk9sF https://t.co/r8Iy5hXBHe",
		      user: .init(id: 1121, name: "John Dobrivoj", screenName: "johnny", avatar: URL(string: "https://pbs.twimg.com/profile_images/659349744532246528/oJDWTI75_normal.png")!),
		      media: [
		      	FeedItem.Tweet.Media(ratio: 16 / 9, kind: .photo,
		      	                     thumbnailUrl: URL(string: "https://pbs.twimg.com/media/EzAbfaaWEAIdsbF.jpg")!,
		      	                     url: URL(string: "https://pbs.twimg.com/media/EzAbfaaWEAIdsbF.jpg")!),
		      ],
		      entities: [
		      	FeedItem.Tweet.Entity(startIndex: 192, endIndex: 215, kind: .link, text: "https://t.co/r8Iy5hXBHe"),
		      	FeedItem.Tweet.Entity(startIndex: 168, endIndex: 191, kind: .link, text: "https://t.co/YkDTuUk9sF"),
		      ])
	}
}

extension FeedItem.InstagramPost {
	static var fixture1: Self {
		.init(id: "insta-1",
		      createdAt: Date(),
		      text: "Ook Alkmaar heeft de smaak te pakken! 🤤",
		      user: .init(id: "insta-user-1", fullName: "Peter Green", username: "perazeleni", avatar: URL(string: "https://placehold.it/100x100/")!),
		      media: [
		      	FeedItem.InstagramPost.Media(height: 1080, width: 1080, kind: .photo, url: URL(string: "https://scontent-frt3-1.cdninstagram.com/v/t51.2885-15/e35/168826025_258018399393438_5020454969803009937_n.jpg?tp=1&_nc_ht=scontent-frt3-1.cdninstagram.com&_nc_cat=106&_nc_ohc=hutxFZ2iwHIAX_uoqc7&edm=ABfd0MgAAAAA&ccb=7-4&oh=607d9ac398eb24ce4181105649f5ea3c&oe=609C3A23&_nc_sid=7bff83")!),
		      ])
	}
}

extension User {
	static var fixture1: User {
		User(id: "user-1",
		     email: "ivan@gohere.app",
		     phoneNumber: "31234567890",
		     firstName: "Ivan",
		     lastName: "Vasic",
		     profile: Profile(avatar: .fixture(.avatar5), hometown: "Niš", isPrivate: false, biography: "Lorem ipsum dolor samet"))
	}

	static var fixture2: User {
		User(id: "user-2",
		     email: "anne@gohere.app",
		     phoneNumber: "31234567890",
		     firstName: "Anne",
		     lastName: "Hoevens",
		     profile: Profile(avatar: .fixture(.avatar3), hometown: "Haarlem", isPrivate: false, biography: "Lorem ipsum dolor samet"))
	}

	static var fixture3: User {
		User(id: "user-3",
		     email: "menno@gohere.app",
		     phoneNumber: "31234567890",
		     firstName: "Menno",
		     lastName: "Biesiot",
		     profile: Profile(avatar: .fixture(.avatar4), hometown: "Leiden", isPrivate: false, biography: "Lorem ipsum dolor samet"))
	}
}

extension UserExtended {
	static var fixtureExtended1: UserExtended {
		UserExtended(base: .fixture1, followersCount: 1, followingCount: 2, reviewCount: 3, favoriteCount: 4)
	}

	static var fixtureExtended2: UserExtended {
		UserExtended(base: .fixture2, followersCount: 12, followingCount: 22, reviewCount: 32, favoriteCount: 42)
	}

	static var fixtureExtended3: UserExtended {
		UserExtended(base: .fixture3, followersCount: 123, followingCount: 223, reviewCount: 323, favoriteCount: 423)
	}
}

extension UserConnection: CustomDebugStringConvertible {
	var debugDescription: String {
		"UserConnection(\(user.fullName), \(kind), \(status))"
	}

	static var fixture1: UserConnection {
		UserConnection(user: .fixture1, kind: .following, status: .pending)
	}

	static var fixture2: UserConnection {
		UserConnection(user: .fixture2, kind: .follower, status: .pending)
	}
}

extension Place.AffiliateInfo {
	static var fixture1: Self {
		Place.AffiliateInfo(kind: "booking", actionURL: URL(string: "https://www.booking.com/")!, ctaButton: "book_now")
	}

	static var fixture2: Self {
		Place.AffiliateInfo(kind: "locals", actionURL: URL(string: "https://www.withlocals.com/")!, ctaButton: "order_now")
	}
}

extension PlaceCategory {
	static var fixture1: Self {
		PlaceCategory(id: 101, slug: "museum", icon: .fixture(.category1))
	}

	static var fixture2: Self {
		PlaceCategory(id: 102, slug: "travel", icon: .fixture(.category2))
	}
}

extension Place {
	static var fixture1: Self {
		Place(id: "place-1",
		      idGoogle: nil,
		      name: "Van Gogh Museum",
		      address: "Museumplein 555",
		      city: "Amsterdam",
		      country: "NL",
		      location: GPSLocation(latitude: 54, longitude: 4),
		      category: PlaceCategory(id: 101, slug: "museum", icon: .fixture(.category1)),
		      website: nil,
		      phone: "",
		      affiliate: .fixture1,
		      photo: .fixture(.place1),
		      info: nil,
		      averageRating: 8,
		      labels: nil,
		      isFavorite: true,
		      lastReview: .fixture1)
	}

	static var fixture2: Self {
		Place(id: "place-2",
		      idGoogle: nil,
		      name: "Barcelona Museum of Contemporary Arts",
		      address: "Museumplein 555",
		      city: "Barcelona City",
		      country: "ES",
		      location: GPSLocation(latitude: 41.380714, longitude: 2.186015),
		      category: PlaceCategory(id: 101, slug: "museum", icon: .fixture(.category2)),
		      website: nil,
		      phone: "",
		      affiliate: nil,
		      photo: .fixture(.place2),
		      info: nil,
		      averageRating: 8,
		      labels: nil,
		      isFavorite: nil,
		      lastReview: nil)
	}
}

extension Review {
	static var fixture1: Self {
		Review(id: "review-1", place: .fixture1, user: .fixture1, labels: [], images: [.init(id: "1", path: .fixture(.place1)), .init(id: "2", path: .fixture(.place2))], createdAt: Date(), rating: 9, text: "Extraordinary place, highly recommended!", language: "nl")
	}

	static var fixture2: Self {
		Review(id: "review-2", place: .fixture2, user: .fixture2, labels: [], images: [], createdAt: Date(), rating: 7, text: "", language: "nl")
	}

	static var fixture3: Self {
		Review(id: "review-3", place: .fixture2, user: .fixture3, labels: [], images: [], createdAt: Date(), rating: 8, text: "The kaassoufflé came with satésaus. Lekker hoor.", language: "nl")
	}
}

extension ReviewInline {
	static var fixture1: Self {
		ReviewInline(id: "review-inline-1", user: .fixture1, rating: 7, text: "The kaassoufflé came with satésaus. Lekker hoor.")
	}

	static var fixture2: Self {
		ReviewInline(id: "review-inline-2", user: .fixture2, rating: 9, text: "Extraordinary place, highly recommended!")
	}

	static var fixture3: Self {
		ReviewInline(id: "review-inline-3", user: .fixture3, rating: 7, text: "")
	}
}

extension Review.Image {
	static var fixture1: Self {
		.init(id: "r-image-1", path: .fixture(.place1))
	}

	static var fixture2: Self {
		.init(id: "r-image-2", path: .fixture(.place2))
	}

	static var fixture3: Self {
		.init(id: "r-image-3", path: .fixture(.place3))
	}
}

extension PlaceLookup {
	static var fixture1: Self {
		PlaceLookup(lookupId: "lookup-1",
		            providerId: 1,
		            title: "Oslo Beers",
		            vicinity: "Balistraat, Amsterdam",
		            location: .init(latitude: 52.3578014, longitude: 4.8543473),
		            category: .init(id: 100, slug: "other", icon: .init(path: "/place/00-34e292e8610944c581e104b332c18312.jpg")))
	}

	static var fixture2: Self {
		PlaceLookup(lookupId: "lookup-2",
		            providerId: 1,
		            title: "Al ghazala",
		            vicinity: "Rue Palestine, Al Hoceïma",
		            location: .init(latitude: 35.2388016, longitude: 3.9294996),
		            category: .init(id: 100, slug: "other", icon: .init(path: "/place/00-34e292e8610944c581e104b332c18312.jpg")))
	}

	static var fixture3: Self {
		PlaceLookup(lookupId: "lookup-3",
		            providerId: 1,
		            title: "Kafana Mrak",
		            vicinity: "Bulevar 12. februara 110, Niš, Serbia",
		            location: .init(latitude: 43.342701, longitude: 21.8704281),
		            category: .init(id: 100, slug: "other", icon: .init(path: "/place/00-34e292e8610944c581e104b332c18312.jpg")))
	}
}

extension PlaceAutocomplete {
	static var fixture1: Self {
		PlaceAutocomplete(lookupId: "autocomp-1",
		                  providerId: 1,
		                  title: "Oslo Beers",
		                  vicinity: "Balistraat, Amsterdam",
		                  category: .init(id: 100, slug: "other", icon: .init(path: "/place/00-34e292e8610944c581e104b332c18312.jpg")))
	}

	static var fixture2: Self {
		PlaceAutocomplete(lookupId: "autocomp-2",
		                  providerId: 1,
		                  title: "Al ghazala",
		                  vicinity: "Rue Palestine, Al Hoceïma",
		                  category: .init(id: 100, slug: "other", icon: .init(path: "/place/00-34e292e8610944c581e104b332c18312.jpg")))
	}

	static var fixture3: Self {
		PlaceAutocomplete(lookupId: "autocomp-3",
		                  providerId: 1,
		                  title: "Kafana Mrak",
		                  vicinity: "Bulevar 12. februara 110, Niš, Serbia",
		                  category: .init(id: 100, slug: "other", icon: .init(path: "/place/00-34e292e8610944c581e104b332c18312.jpg")))
	}
}

extension Contact {
	static var fixture1: Self {
		Contact(id: "contact-1", initials: "IV", fullName: "Ivan Vasic", emails: ["ivan@gohere.app", "ivan.vasic@justpinch.com"], phoneNumbers: ["3112345678"], avatar: UIImage(named: "avatar1"))
	}

	static var fixture2: Self {
		Contact(id: "contact-2", initials: "MB", fullName: "Menno Biesiot", emails: ["menno@gohere.app", "menno@pinch.nl"], phoneNumbers: ["3112345678", "12014445556"], avatar: nil)
	}

	static var fixture3: Self {
		Contact(id: "contact-3", initials: "AH", fullName: "Anne Hoevens", emails: ["anne@gohere.app"], phoneNumbers: [], avatar: nil)
	}
}

extension PromotedPlaces {
	static var fixture1: Self {
		PromotedPlaces(id: 1, title: "Our recommandations", places: [.fixture1, .fixture2])
	}
}

extension TranslationDto {
	static var fixture1: Self {
		TranslationDto(detectedLanguage: "nl", text: "lorem ipsum dolor samet")
	}
}

extension ReviewItem {
	static var fixture1: Self {
		ReviewItem(review: .fixture1, translation: nil, isTranslationVisible: false)
	}
	static var fixture2: Self {
		ReviewItem(review: .fixture2, translation: nil, isTranslationVisible: false)
	}
	static var fixture3: Self {
		ReviewItem(review: .fixture3, translation: nil, isTranslationVisible: false)
	}
}
