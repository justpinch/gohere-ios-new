import SwiftUI

extension Locale {
	static let all = Bundle.main.localizations
		.map(Locale.init)
		.filter { $0.identifier != "base" }
}

struct LocalePreview<Preview: View>: View {
	private let preview: Preview

	var body: some View {
		ForEach(Locale.all, id: \.self) { locale in
			preview
				.previewLayout(PreviewLayout.sizeThatFits)
				.environment(\.locale, locale)
				.previewDisplayName("Locale: \(locale.identifier)")
		}
	}

	init(@ViewBuilder builder: @escaping () -> Preview) {
		preview = builder()
	}
}

struct DisabledPreview<Preview: View>: View {
	private let preview: Preview

	var body: some View {
		preview
			.disabled(true)
			.previewLayout(PreviewLayout.sizeThatFits)
			.previewDisplayName("Disabled")
	}

	init(@ViewBuilder builder: @escaping () -> Preview) {
		preview = builder()
	}
}

struct DarkThemePreview<Preview: View>: View {
	private let preview: Preview

	var body: some View {
		preview
			.padding()
			.previewLayout(PreviewLayout.sizeThatFits)
			.background(Color(.systemBackground))
			.environment(\.colorScheme, .dark)
			.previewDisplayName("Dark Theme")
	}

	init(@ViewBuilder builder: @escaping () -> Preview) {
		preview = builder()
	}
}

private let sizeCategories: [ContentSizeCategory] = [.extraSmall, .medium, .extraExtraExtraLarge]

struct ContentSizeCategoryPreview<Preview: View>: View {
	private let preview: Preview
	private let sizeCategory: ContentSizeCategory

	var body: some View {
		preview
			.previewLayout(PreviewLayout.sizeThatFits)
			.environment(\.sizeCategory, sizeCategory)
			.previewDisplayName("Content Size Category: \(sizeCategory)")
	}

	init(_ sizeCategory: ContentSizeCategory, @ViewBuilder builder: @escaping () -> Preview) {
		self.sizeCategory = sizeCategory
		preview = builder()
	}
}

extension View {
	func previewSupportedLocales() -> some View {
		LocalePreview { self }
	}

	func previewDarkTheme() -> some View {
		DarkThemePreview { self }
	}

	func previewContentSize(_ sizeCategory: ContentSizeCategory) -> some View {
		ContentSizeCategoryPreview(sizeCategory) { self }
	}

	func previewDisabled() -> some View {
		DisabledPreview { self }
	}

	func previewButtonPreset() -> some View {
		let content = self.padding()

		return Group {
			content.previewSupportedLocales()
			content.previewDisabled()
			content.previewDarkTheme()
			content.previewDarkTheme().previewDisabled()
			content.previewContentSize(.extraSmall)
			content.previewContentSize(.medium)
			content.previewContentSize(.extraExtraExtraLarge)
		}
	}
}
