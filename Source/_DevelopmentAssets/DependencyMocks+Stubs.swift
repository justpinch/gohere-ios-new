import Combine
import Contacts
import CoreLocation
import Foundation
import SwiftUI

class PreviewLocationManager: LocationManager {
	static func locationServicesEnabled() -> Bool { true }
	var authorizationStatus: CLAuthorizationStatus = .authorizedWhenInUse
	var delegate: CLLocationManagerDelegate?
	var location: CLLocation?

	func requestWhenInUseAuthorization() {}
	func requestLocation() {}
}

class PreviewAppleSignInCoordinator: AppleSignInCoordinator {
	func signIn() -> ApiResponsePublisher<AuthToken> {
		Just(.fixture1).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}
}

class PreviewUserInteractor: UserInteractor {
	var connections: UserConnectionsInteractor?
	var savedPlaces: UserSavedPlacesInteractor?

	func refreshProfile() {}

	func authenticate(email _: String, password _: String) -> ApiResponsePublisher<Void> {
		Just(()).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func authenticate(phoneNumber _: String, tvpToken _: String) -> ApiResponsePublisher<Void> {
		Just(()).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func authenticateWithApple() -> ApiResponsePublisher<Void> {
		Just(()).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func authenticateWithFacebook() -> ApiResponsePublisher<Void> {
		Just(()).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func authenticate(with _: ApiResponsePublisher<AuthToken>) -> ApiResponsePublisher<Void> {
		Just(()).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func signOut() {}
	
	func delete() -> ApiResponsePublisher<Void> {
		Just(()).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}
}

class PreviewUserConnectionsInteractor: UserConnectionsInteractor {
	func follow(id _: User.ID, isPrivate _: Bool) {}
	func unfollow(id _: User.ID) {}
	func updateStatus(for _: User.ID) {}
	func updateStatuses(for _: [User.ID]) {}
	func updateStatuses(for _: [User.ID], force _: Bool) {}
	func refreshConnectionRequests() {}
	func acceptFollowRequest(for _: UserConnection) {}
	func blockFollowRequest(for _: UserConnection) {}

	func ignoreFollowRequest(for _: UserConnection) {}
}

class PreviewUserSavedPlacesInteractor: UserSavedPlacesInteractor {
	func add(place _: Place) {}
	func remove(place _: Place) {}
	func refreshStatus(for _: Place) {}
	func refreshStatus(for _: [Place]) {}
	func setStatus(for _: Place.ID, isSaved _: Bool) {}
}

extension FriendSuggestionsModel {
	static var stub: FriendSuggestionsModel {
		FriendSuggestionsModel(userChanged: Just(()).eraseToAnyPublisher(), userService: PreviewUserService(), contactStore: PreviewContactStore())
	}
}

extension UserOwnConnectionsModel {
	static var stubFollowers: UserOwnConnectionsModel {
		UserOwnConnectionsModel(kind: .follower, userID: User.fixture1.id, connectionsPublisher: PreviewUserService().connections, updateConnectionStatuses: { _ in })
	}

	static var stubFollowing: UserOwnConnectionsModel {
		UserOwnConnectionsModel(kind: .following, userID: User.fixture1.id, connectionsPublisher: PreviewUserService().connections, updateConnectionStatuses: { _ in })
	}
}

// MARK: - AuthFlowInteractor

extension AuthFlowInteractor {
	static func stub() -> AuthFlowInteractor {
		AuthFlowInteractor(finishFlow: {},
		                   userInteractor: PreviewUserInteractor(),
		                   authService: PreviewAuthService(),
		                   userService: PreviewUserService(),
		                   verificationsService: PreviewVerificationsService())
	}
}

// MARK: - ContactStore

class PreviewContactStore: ContactStore {
	var authorizationStatus: CNAuthorizationStatus = .authorized

	required init() {}

	func requestAccess() -> Future<Bool, Error> {
		Future { $0(.success(true)) }
	}

	func getContacts() -> Future<[Contact], Error> {
		Future { $0(.success([.fixture1, .fixture2, .fixture3])) }
	}

	func getEmails() -> Future<[String], Error> {
		Future { $0(.success(["arno@schwarzie.de", "mowgli@jungle.com"])) }
	}

	func getPhoneNumbers() -> Future<[String], Error> {
		Future { $0(.success(["3112345678", "12345678910"])) }
	}
}

class PreviewCloudMessaging: CloudMessaging {
	func setAPNSToken(token _: Data) {}

	func getCurrentDevice() -> UserDevice? {
		UserDevice(id: "123", name: "Someone's iPhone", model: "iPhone 11,2", os: .ios, osVersion: "14.5", fcmToken: "fcm token", firebaseProjectId: "gohere-1259")
	}
}

// MARK: - Credentials Storage

class PreviewCredentialsStorage: CredentialsStorage {
	var authToken: CurrentValueSubject<AuthToken?, Never> = .init(nil)
}

// MARK: - Networking Session

class PreviewUrlSession: NetworkingSession {
	func publisher(for request: URLRequest) -> AnyPublisher<NetworkDataOutput, Error> {
		let result = (data: Data(), response: HTTPURLResponse(url: request.url!, statusCode: 200, httpVersion: nil, headerFields: nil)!)
		return Just(result).setFailureType(to: Error.self).eraseToAnyPublisher()
	}
}

// MARK: - API Services

class PreviewBootstrapService: BootstrapService {
	func get() -> ApiResponsePublisher<Bootstrap> {
		Just(.fixture1).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}
}

class PreviewAuthService: AuthService {
	func register(firstName _: String, lastName _: String, email _: String, password _: String) -> ApiResponsePublisher<AuthToken> {
		Just(.fixture1).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func register(firstName _: String, lastName _: String, phoneNumber _: String, tvpToken _: String) -> ApiResponsePublisher<AuthToken> {
		Just(.fixture1).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func authenticate(email _: String, password _: String) -> ApiResponsePublisher<AuthToken> {
		Just(.fixture1).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func authenticate(phoneNumber _: String, tvpToken _: String) -> ApiResponsePublisher<AuthToken> {
		Just(.fixture1).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func authenticate(facebookToken _: String) -> ApiResponsePublisher<AuthToken> {
		Just(.fixture1).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func authenticate(appleToken _: String, firstName _: String?, lastName _: String?) -> ApiResponsePublisher<AuthToken> {
		Just(.fixture1).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func refresh(token _: AuthToken) -> ApiResponsePublisher<AuthToken> {
		Just(.fixture1).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}
}

class PreviewFeedService: FeedService {
	func get(pageToken _: String?, limit _: UInt) -> ApiResponsePublisher<FeedResponse> {
		Just(.fixture1).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}
}

class PreviewModerationService: ModerationService {
	func reportReview(id _: Review.ID, reason _: ReviewReportReason) -> ApiResponsePublisher<Void> {
		Just(()).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}
}

class PreviewUserService: UserService {
	func deleteProfile(id: User.ID) -> ApiResponsePublisher<Void> {
		Just(()).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}
	
	func profile(id _: User.ID) -> ApiResponsePublisher<UserExtended> {
		Fail(error: API.Error(reason: NSError(domain: "", code: 2, userInfo: nil))).eraseToAnyPublisher()
	}

	func isEmailTaken(_: String) -> ApiResponsePublisher<Bool> {
		Fail(error: API.Error(reason: NSError(domain: "", code: 2, userInfo: nil))).eraseToAnyPublisher()
	}

	func isPhoneTaken(_: String) -> ApiResponsePublisher<Bool> {
		Fail(error: API.Error(reason: NSError(domain: "", code: 2, userInfo: nil))).eraseToAnyPublisher()
	}

	func resetPassword(for _: String) -> ApiResponsePublisher<Void> {
		Just(()).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func update(isPrivate _: Bool, avatar _: UIImage?, for _: User.ID) -> ApiResponsePublisher<User> {
		Just(.fixture1).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func update(user _: UpdateUser) -> ApiResponsePublisher<User> {
		Just(.fixture1).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func search(query _: String) -> ApiResponsePublisher<PaginatedResponse<User>> {
		Just(PaginatedResponse(count: 1, results: [.fixture1])).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func connections(for _: User.ID, kind _: UserConnection.Kind?, status _: UserConnection.Status?, offset _: UInt, limit _: UInt) -> ApiResponsePublisher<PaginatedResponse<UserConnection>> {
		Just(.init(count: 2, results: [.fixture1, .fixture2])).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func connectionStatus(from _: User.ID, to _: User.ID) -> ApiResponsePublisher<UserConnection?> {
		Just(nil).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func connectionStatuses(for _: [String], from _: String) -> ApiResponsePublisher<[BatchUserConnectionStatus]> {
		Just([]).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func follow(userId _: String, followerId _: String) -> ApiResponsePublisher<UserConnection> {
		Just(.fixture1).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func unfollow(userId _: String, followerId _: String) -> ApiResponsePublisher<Void> {
		Just(()).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func setConnectionStatus(status _: UserConnection.Status, from _: String, to _: String) -> ApiResponsePublisher<UserConnection> {
		Just(.fixture1).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func suggestFriends(facebookToken _: String?, emailHashes _: [String]?, phoneNumberHashes _: [String]?) -> ApiResponsePublisher<[User]> {
		Just([.fixture1]).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}
}

class PreviewUserDeviceService: UserDeviceService {
	func register(device: UserDeviceRegisterParams) -> ApiResponsePublisher<UserDevice> {
		Just(UserDevice(id: UUID().uuidString, name: device.name, model: device.model, os: .ios, osVersion: device.osVersion, fcmToken: device.fcmToken, firebaseProjectId: device.firebaseProjectId))
			.setFailureType(to: API.Error.self)
			.eraseToAnyPublisher()
	}

	func delete(id _: UserDevice.ID, token _: AuthToken) -> ApiResponsePublisher<Void> {
		Just(())
			.setFailureType(to: API.Error.self)
			.eraseToAnyPublisher()
	}

	func getFCMToken(apnsToken _: String, firebaseProjectID _: String) -> ApiResponsePublisher<String> {
		Just(UUID().uuidString)
			.setFailureType(to: API.Error.self)
			.eraseToAnyPublisher()
	}
}

class PreviewVerificationsService: VerificationsService {
	func createCode(phoneNumber _: String) -> ApiResponsePublisher<Void> {
		Just(()).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func verifyCode(_: String, for _: String) -> ApiResponsePublisher<String> {
		Just("tvp_token").setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}
}

class PreviewPlaceService: PlaceService {
	func get(id _: Place.ID) -> ApiResponsePublisher<Place> {
		Just(.fixture1).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func search(area _: (location: CLLocationCoordinate2D, radius: Int)?, scoped _: Bool, categories _: [PlaceCategory.ID], excludePlaceUUIDs _: [String], offset _: UInt, limit _: UInt) -> ApiResponsePublisher<PaginatedResponse<Place>> {
		Fail(error: API.Error(reason: NSError(domain: "", code: 2, userInfo: nil))).eraseToAnyPublisher()
	}

	func favorites(offset _: UInt, limit _: UInt) -> ApiResponsePublisher<PaginatedResponse<Place>> {
		Just(PaginatedResponse(count: 2, results: [.fixture1, .fixture2])).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func favorite(id _: Place.ID) -> ApiResponsePublisher<Void> {
		Just(()).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func unfavorite(id _: Place.ID) -> ApiResponsePublisher<Void> {
		Just(()).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func favoritesCheck(ids _: [Place.ID]) -> ApiResponsePublisher<[Place.ID]> {
		Just([Place.fixture1, .fixture2].map(\.id)).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}
	
	func searchPromotedPlaces(location: CLLocationCoordinate2D, radius: Int) -> ApiResponsePublisher<PromotedPlaces> {
		Just(PromotedPlaces.fixture1).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}
}

class PreviewPlaceExternalService: PlaceExternalService {
	func `import`(lookupId _: String, providerId _: Int) -> ApiResponsePublisher<Place> {
		Fail(error: API.Error(reason: NSError(domain: "", code: 2, userInfo: nil))).eraseToAnyPublisher()
	}

	func explore(query _: String?, location _: CLLocationCoordinate2D) -> ApiResponsePublisher<[PlaceLookup]> {
		Fail(error: API.Error(reason: NSError(domain: "", code: 2, userInfo: nil))).eraseToAnyPublisher()
	}

	func autocomplete(query _: String, location _: CLLocationCoordinate2D) -> ApiResponsePublisher<[PlaceAutocomplete]> {
		Fail(error: API.Error(reason: NSError(domain: "", code: 2, userInfo: nil))).eraseToAnyPublisher()
	}

	func geocode(query _: String) -> ApiResponsePublisher<[PlaceLookup]> {
		Fail(error: API.Error(reason: NSError(domain: "", code: 2, userInfo: nil))).eraseToAnyPublisher()
	}
}

class PreviewReviewService: ReviewService {
	func get(id _: String) -> ApiResponsePublisher<Review> {
		Just(.fixture1).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func create(place _: Place.ID, imageIds _: [String], rating _: Int, text _: String) -> ApiResponsePublisher<Review> {
		Just(.fixture1).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func update(id _: Review.ID, imageIds _: [String], rating _: Int, text _: String) -> ApiResponsePublisher<Review> {
		Just(.fixture1).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func list(params _: ReviewServiceListParameters) -> ApiResponsePublisher<PaginatedResponse<Review>> {
		Just(PaginatedResponse(count: 2, results: [.fixture1, .fixture2])).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func delete(id _: String) -> ApiResponsePublisher<Void> {
		Just(()).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}

	func upload(images _: [UIImage]) -> ApiResponsePublisher<[Review.Image]> {
		Just([.fixture1, .fixture1]).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}
}

class PreviewTranslatorService: TranslatorService {
	func translateReview(reviewId: String, to: String) -> ApiResponsePublisher<TranslationDto> {
		Just(TranslationDto.fixture1).setFailureType(to: API.Error.self).eraseToAnyPublisher()
	}
	
	
}
