import Foundation
import SwiftUI

extension View {
	func injectPreviewsEnvironment() -> some View {
		let dependencyContainer = DIContainer()

		// AppSettings
		let settings = AppSettings()
		settings.categories = [.fixture1, .fixture2]
		dependencyContainer.register(service: settings)

		// Services
		let services = PreviewServiceContainer(
			translator: PreviewTranslatorService(),
			bootstrap: PreviewBootstrapService(),
			auth: PreviewAuthService(),
			feed: PreviewFeedService(),
			moderation: PreviewModerationService(),
			places: PreviewPlaceService(),
			placesExternal: PreviewPlaceExternalService(),
			reviews: PreviewReviewService(),
			users: PreviewUserService(),
			userDevices: PreviewUserDeviceService(),
			verifications: PreviewVerificationsService()
		)

		// API Client
		dependencyContainer.register(service: API.Client(session: PreviewUrlSession(), credentialsStorage: PreviewCredentialsStorage(), services: services))

		// CloudMessaging
		dependencyContainer.register(service: PreviewCloudMessaging() as CloudMessaging)

		// Global State
		let userState = UserState(authToken: .init(.fixture1))
		userState.user = .model(.fixtureExtended1)
		userState.savedPlaceStatuses = [Place.fixture2.id: true]
		userState.connectionRequests = [.fixture1, .fixture2]

		// Interactors
		let userInteractor = PreviewUserInteractor()
		userInteractor.connections = PreviewUserConnectionsInteractor()
		userInteractor.savedPlaces = PreviewUserSavedPlacesInteractor()
		dependencyContainer.register(service: userInteractor as UserInteractor)

		// Environment Objects
		let provider = EnvironmentObjectsProvider(userState: userState)
		dependencyContainer.register(service: provider)

		return self
			.environment(\.dependencyContainer, dependencyContainer)
			.environmentObject(userState)
	}
}

private struct PreviewServiceContainer: API.Client.Services {
	var translator: TranslatorService
	var bootstrap: BootstrapService
	var auth: AuthService
	var feed: FeedService
	var moderation: ModerationService
	var places: PlaceService
	var placesExternal: PlaceExternalService
	var reviews: ReviewService
	var users: UserService
	var userDevices: UserDeviceService
	var verifications: VerificationsService
}
