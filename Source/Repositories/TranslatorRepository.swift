//
//  TranslatorRepository.swift
//  GoHere
//
//  Created by jorisfavier on 27/07/2022.
//

import Foundation
import Combine

protocol TranslatorRepository {
	func translateReview(reviewId: String, to: String) -> AnyPublisher<Translation, Swift.Error>
}

class TranslatorRepositoryImpl: TranslatorRepository {
	
	private let translatorService: TranslatorService
	
	init(service: TranslatorService) {
		self.translatorService = service
	}
	
	func translateReview(reviewId: String, to: String) -> AnyPublisher<Translation, Swift.Error> {
		translatorService.translateReview(reviewId: reviewId, to: to)
			.mapError({$0 as Swift.Error})
			.map { dto in
				Translation(text: dto.text, originalLanguage: dto.detectedLanguage)
			}
			.eraseToAnyPublisher()
	}
	
	
}
