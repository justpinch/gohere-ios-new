import Combine
import Foundation

extension FileCache.Keys {
	static let bootstrap = "bootstrap"
}

struct BootstrapRepository {
	let service: BootstrapService
	let cache: FileCacheProtocol

	init(service: BootstrapService, cache: FileCacheProtocol? = nil) {
		self.service = service
		self.cache = cache ?? FileCache(fileManager: FileManager.default)
	}

	/// Returns cached Bootstrap, if any
	func bootstrap() -> Bootstrap? {
		loadFromDisk()
	}

	/// Returns a publisher that fetches the latest Bootstrap from the API and emits it
	func bootstrapLoader() -> AnyPublisher<Bootstrap, Swift.Error> {
		service.get()
			.mapError { $0 as Swift.Error }
			.handleEvents(receiveOutput: saveToDisk) // cache new version on receive
			.eraseToAnyPublisher()
	}

	private func loadFromDisk() -> Bootstrap? {
		do {
			return try cache.value(for: FileCache.Keys.bootstrap)
		} catch {
			error.report()
			return nil
		}
	}

	private func saveToDisk(_ bootstrap: Bootstrap) {
		do {
			try cache.cache(bootstrap, key: FileCache.Keys.bootstrap)
		} catch {
			error.report()
		}
	}
}
