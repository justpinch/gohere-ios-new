import Combine
import Foundation

// MARK: - Service Provider & Service Container

extension API.Client {
	struct ServicesContainer {
		let url: URL
		let authenticator: RequestAuthenticator
		let responseProvider: ResponseProvider
	}

	// All available services
	typealias Services =
		BootstrapServiceContainer &
		AuthServiceContainer &
		FeedServiceContainer &
		ModerationServiceContainer &
		PlaceServiceContainer &
		PlaceExternalServiceContainer &
		ReviewServiceContainer &
		UserServiceContainer &
		UserDeviceServiceContainer &
		VerificationsServiceContainer &
		TranslatorServiceContainer
}

// MARK: - API Services

protocol ApiService {
	var url: URL { get }
	var responseProvider: ResponseProvider { get }
}

extension ApiService {
	func url(appending path: String) -> URL {
		url.appendingPathComponent(path)
	}

	func model<Model: Decodable>(for request: RequestPublisherConvertible) -> AnyPublisher<Model, API.Error> {
		responseProvider
			.response(for: request)
			.map(\.data)
			.eraseToAnyPublisher()
	}

	func response(for request: RequestPublisherConvertible) -> AnyPublisher<NetworkDataOutput, API.Error> {
		responseProvider.response(for: request)
	}

	func execute(request: RequestPublisherConvertible) -> AnyPublisher<Void, API.Error> {
		responseProvider.response(for: request).map { _ in }.eraseToAnyPublisher()
	}
}
