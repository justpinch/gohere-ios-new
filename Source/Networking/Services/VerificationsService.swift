import Combine
import Foundation

protocol VerificationsServiceContainer {
	var verifications: VerificationsService { get }
}

extension API.Client.ServicesContainer: VerificationsServiceContainer {
	var verifications: VerificationsService {
		DefaultVerificationsService(url: url, responseProvider: responseProvider)
	}
}

protocol VerificationsService {
	func createCode(phoneNumber: String) -> ApiResponsePublisher<Void>
	func verifyCode(_ code: String, for phoneNumber: String) -> ApiResponsePublisher<String>
}

struct DefaultVerificationsService: VerificationsService, ApiService {
	let url: URL
	let responseProvider: ResponseProvider

	func createCode(phoneNumber: String) -> ApiResponsePublisher<Void> {
		let request = Request.build(with: url(appending: "verification/phone/"))
			.method(.post)
			.parameters(["phone_number": phoneNumber], encoding: JSONEncoding.default)

		return execute(request: request)
	}

	func verifyCode(_ code: String, for phoneNumber: String) -> ApiResponsePublisher<String> {
		let params = ["code": code, "phone_number": phoneNumber]

		let request = Request.build(with: url(appending: "verification/phone/verify/"))
			.method(.post)
			.parameters(params, encoding: JSONEncoding.default)

		struct Response: Decodable {
			let tvpToken: String
		}

		return model(for: request).map(\Response.tvpToken).eraseToAnyPublisher()
	}
}
