import Combine
import Foundation
import OSLog
import UIKit

protocol UserServiceContainer {
	var users: UserService { get }
}

extension API.Client.ServicesContainer: UserServiceContainer {
	var users: UserService {
		DefaultUserService(url: url, authenticator: authenticator, responseProvider: responseProvider)
	}
}

/// @mockable
protocol UserService {
	func isEmailTaken(_ email: String) -> ApiResponsePublisher<Bool>
	func isPhoneTaken(_ phone: String) -> ApiResponsePublisher<Bool>

	func resetPassword(for email: String) -> ApiResponsePublisher<Void>

	func profile(id: User.ID) -> ApiResponsePublisher<UserExtended>
	func update(isPrivate: Bool, avatar: UIImage?, for userId: User.ID) -> ApiResponsePublisher<User>
	func update(user: UpdateUser) -> ApiResponsePublisher<User>

	func search(query: String) -> ApiResponsePublisher<PaginatedResponse<User>>

	func connections(for id: User.ID, kind: UserConnection.Kind?, status: UserConnection.Status?, offset: UInt, limit: UInt) -> ApiResponsePublisher<PaginatedResponse<UserConnection>>
	func connectionStatus(from sourceUserId: User.ID, to destinationUserId: User.ID) -> ApiResponsePublisher<UserConnection?>
	func connectionStatuses(for ids: [User.ID], from userId: User.ID) -> ApiResponsePublisher<[BatchUserConnectionStatus]>
	func setConnectionStatus(status: UserConnection.Status, from sourceUserId: String, to destinationUserId: String) -> ApiResponsePublisher<UserConnection>
	func follow(userId: String, followerId: String) -> ApiResponsePublisher<UserConnection>
	func unfollow(userId: String, followerId: String) -> ApiResponsePublisher<Void>
	func suggestFriends(facebookToken: String?, emailHashes: [String]?, phoneNumberHashes: [String]?) -> ApiResponsePublisher<[User]>
	func deleteProfile(id: User.ID) -> ApiResponsePublisher<Void>
}

struct DefaultUserService: UserService, ApiService {
	let url: URL
	var authenticator: RequestAuthenticator
	let responseProvider: ResponseProvider

	func profile(id: User.ID) -> ApiResponsePublisher<UserExtended> {
		let request = Request.build(with: url(appending: "users/\(id)/"))
			.authenticated(with: authenticator, optional: true)

		return model(for: request)
	}

	func isEmailTaken(_ email: String) -> ApiResponsePublisher<Bool> {
		userExists(with: ["email": email])
	}

	func isPhoneTaken(_ phone: String) -> ApiResponsePublisher<Bool> {
		userExists(with: ["phone_number": phone])
	}

	private func userExists(with params: [String: Any]) -> ApiResponsePublisher<Bool> {
		let request = Request.build(with: url(appending: "users/exist/"))
			.method(.post)
			.parameters(params, encoding: JSONEncoding.default)

		return execute(request: request)
			.map { true }
			.replaceError(with: false, if: { $0.statusCode == 404 })
			.eraseToAnyPublisher()
	}

	func resetPassword(for email: String) -> ApiResponsePublisher<Void> {
		let request = Request.build(with: url(appending: "users/reset-password/"))
			.method(.post)
			.parameters(["email": email], encoding: JSONEncoding.default)

		return execute(request: request)
	}

	func update(isPrivate: Bool, avatar: UIImage?, for userId: User.ID) -> ApiResponsePublisher<User> {
		let request = Request.build(with: url(appending: "users/\(userId)/"))
			.method(.patch)
			.multipartFormData { data in
				if let image = avatar?.resizedForUpload(maxSize: 1024).jpegData(compressionQuality: 0.85) {
					data.append(image, withName: "profile.avatar", fileName: "avatar.jpg", mimeType: "image/jpeg")
				}

				if let isPrivateData = (isPrivate ? "true" : "false").data(using: .utf8) {
					data.append(isPrivateData, withName: "profile.is_private")
				}
			}
			.authenticated(with: authenticator)

		return model(for: request)
	}

	func update(user: UpdateUser) -> ApiResponsePublisher<User> {
		let request = Request.build(with: url(appending: "users/\(user.id)/"))
			.method(.patch)
			.multipartFormData { data in
				if let image = user.avatar?.resizedForUpload(maxSize: 1024).jpegData(compressionQuality: 0.85) {
					data.append(image, withName: "profile.avatar", fileName: "avatar.jpg", mimeType: "image/jpeg")
				}

				if let firstName = user.firstName?.data(using: .utf8) {
					data.append(firstName, withName: "first_name")
				}

				if let lastName = user.lastName?.data(using: .utf8) {
					data.append(lastName, withName: "last_name")
				}

				if let hometown = user.hometown?.data(using: .utf8) {
					data.append(hometown, withName: "profile.hometown")
				}

				if let email = user.email?.data(using: .utf8) {
					data.append(email, withName: "email")
				}

				if let phone = user.phoneNumber?.data(using: .utf8) {
					data.append(phone, withName: "phone_number")
				}

				if let tvpToken = user.tvpToken?.data(using: .utf8) {
					data.append(tvpToken, withName: "tvp_token")
				}

				if let isPrivate = user.isPrivate, let isPrivateData = (isPrivate ? "true" : "false").data(using: .utf8) {
					data.append(isPrivateData, withName: "profile.is_private")
				}
				
				if let bio = user.biography?.data(using: .utf8) {
					data.append(bio, withName: "profile.biography")
				}
			}
			.authenticated(with: authenticator)

		return model(for: request)
	}

	func search(query: String) -> ApiResponsePublisher<PaginatedResponse<User>> {
		let request = Request.build(with: url(appending: "users/search/"))
			.method(.get)
			.parameters(["q": query], encoding: URLEncoding.default)
			.authenticated(with: authenticator)

		return model(for: request)
	}

	// MARK: - Connections

	func connections(for id: User.ID, kind: UserConnection.Kind?, status: UserConnection.Status?, offset: UInt, limit: UInt) -> ApiResponsePublisher<PaginatedResponse<UserConnection>> {
		var params: [String: Any] = ["offset": offset, "limit": limit]
		if let kind = kind {
			params["kind"] = kind.rawValue
		}
		if let status = status {
			params["status"] = status.rawValue
		}

		let request = Request.build(with: url(appending: "users/\(id)/connections/"))
			.method(.get)
			.parameters(params, encoding: URLEncoding.default)
			.authenticated(with: authenticator)

		return model(for: request)
	}

	func connectionStatus(from sourceUserId: User.ID, to destinationUserId: User.ID) -> ApiResponsePublisher<UserConnection?> {
		let request = Request.build(with: url(appending: "users/\(sourceUserId)/connections/\(destinationUserId)/"))
			.method(.get)
			.authenticated(with: authenticator)

		return model(for: request)
			.replaceError(with: nil, if: { $0.statusCode == 404 })
			.eraseToAnyPublisher()
	}

	func connectionStatuses(for ids: [User.ID], from userId: User.ID) -> ApiResponsePublisher<[BatchUserConnectionStatus]> {
		let request = Request.build(with: url(appending: "users/\(userId)/connections/statuses/"))
			.method(.post)
			.parameters(["user_ids": ids], encoding: JSONEncoding.default)
			.authenticated(with: authenticator)

		return model(for: request)
	}

	func setConnectionStatus(status: UserConnection.Status, from sourceUserId: String, to destinationUserId: String) -> ApiResponsePublisher<UserConnection> {
		let request = Request.build(with: url(appending: "users/\(sourceUserId)/connections/\(destinationUserId)/"))
			.method(.put)
			.parameters(["status": status.rawValue], encoding: JSONEncoding.default)
			.authenticated(with: authenticator)

		return model(for: request)
	}

	func follow(userId: String, followerId: String) -> ApiResponsePublisher<UserConnection> {
		let request = Request.build(with: url(appending: "users/\(followerId)/connections/\(userId)/follow/"))
			.method(.post)
			.authenticated(with: authenticator)

		return model(for: request)
	}

	func unfollow(userId: String, followerId: String) -> ApiResponsePublisher<Void> {
		let request = Request.build(with: url(appending: "users/\(followerId)/connections/\(userId)/"))
			.method(.delete)
			.authenticated(with: authenticator)

		return execute(request: request)
			.replaceError(with: (), if: { $0.statusCode == 404 })
			.eraseToAnyPublisher()
	}

	func suggestFriends(facebookToken: String?, emailHashes: [String]?, phoneNumberHashes: [String]?) -> ApiResponsePublisher<[User]> {
		var params: [String: Any] = [:]
		if let token = facebookToken {
			params["facebook_token"] = token
		}
		if let emails = emailHashes, !emails.isEmpty {
			params["email_hashes"] = emails
		}
		if let phoneNumbers = phoneNumberHashes, !phoneNumbers.isEmpty {
			params["phone_hashes"] = phoneNumbers
		}

		let request = Request.build(with: url(appending: "users/suggest-friends/"))
			.method(.post)
			.parameters(params, encoding: JSONEncoding.default)
			.authenticated(with: authenticator, optional: true)

		struct Response: Decodable {
			let suggestions: [User]
		}

		return model(for: request).map(\Response.suggestions).eraseToAnyPublisher()
	}
	
	func deleteProfile(id: User.ID) -> ApiResponsePublisher<Void> {
		let request = Request.build(with: url(appending: "users/\(id)/"))
			.method(.delete)
			.authenticated(with: authenticator)

		return execute(request: request)
	}
}
