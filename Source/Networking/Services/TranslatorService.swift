//
//  TranslatorService.swift
//  GoHere
//
//  Created by jorisfavier on 27/07/2022.
//

import Foundation
import SwiftUI

protocol TranslatorServiceContainer {
	var translator: TranslatorService { get }
}

extension API.Client.ServicesContainer: TranslatorServiceContainer {
	var translator: TranslatorService {
		DefaultTranslatorService(url: url, authenticator: authenticator, responseProvider: responseProvider)
	}
}

/// @mockable
protocol TranslatorService {
	func translateReview(reviewId: String, to: String) -> ApiResponsePublisher<TranslationDto>
}

struct DefaultTranslatorService: TranslatorService, ApiService {
	let url: URL
	var authenticator: RequestAuthenticator
	let responseProvider: ResponseProvider

	var baseURL: URL {
		url.appendingPathComponent("translator/")
	}

	func translateReview(reviewId: String, to: String) -> ApiResponsePublisher<TranslationDto> {
		let url = baseURL.appendingPathComponent("review/").appendingPathComponent(reviewId).appendingPathComponent("/")
		let request = Request.build(with: url)
			.parameters(["destination_language": to], encoding: JSONEncoding.default)
			.method(.post)

		return model(for: request)
	}

}
