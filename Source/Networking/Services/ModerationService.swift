import Combine
import Foundation

protocol ModerationServiceContainer {
	var moderation: ModerationService { get }
}

extension API.Client.ServicesContainer: ModerationServiceContainer {
	var moderation: ModerationService {
		DefaultModerationService(url: url, responseProvider: responseProvider, authenticator: authenticator)
	}
}

protocol ModerationService {
	func reportReview(id: Review.ID, reason: ReviewReportReason) -> ApiResponsePublisher<Void>
}

struct DefaultModerationService: ModerationService, ApiService {
	let url: URL
	let responseProvider: ResponseProvider
	let authenticator: RequestAuthenticator

	func reportReview(id: Review.ID, reason: ReviewReportReason) -> ApiResponsePublisher<Void> {
		let request = Request.build(with: url(appending: "report/review/"))
			.method(.post)
			.parameters(["review": id, "reason": reason.rawValue], encoding: JSONEncoding.default)
			.authenticated(with: authenticator)

		return execute(request: request)
	}
}
