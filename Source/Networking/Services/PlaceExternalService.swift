import Combine
import CoreLocation
import Foundation

protocol PlaceExternalServiceContainer {
	var placesExternal: PlaceExternalService { get }
}

extension API.Client.ServicesContainer: PlaceExternalServiceContainer {
	var placesExternal: PlaceExternalService {
		DefaultPlaceExternalService(url: url, authenticator: authenticator, responseProvider: responseProvider)
	}
}

protocol PlaceExternalService {
	func `import`(lookupId: String, providerId: Int) -> ApiResponsePublisher<Place>
	func explore(query: String?, location: CLLocationCoordinate2D) -> ApiResponsePublisher<[PlaceLookup]>
	func autocomplete(query: String, location: CLLocationCoordinate2D) -> ApiResponsePublisher<[PlaceAutocomplete]>
	func geocode(query: String) -> ApiResponsePublisher<[PlaceLookup]>
}

struct DefaultPlaceExternalService: PlaceExternalService, ApiService {
	let url: URL
	var authenticator: RequestAuthenticator
	let responseProvider: ResponseProvider

	func `import`(lookupId: String, providerId: Int) -> ApiResponsePublisher<Place> {
		let request = Request.build(with: url(appending: "places/external/import/"))
			.method(.post)
			.parameters(["lookup_id": lookupId, "provider_id": providerId], encoding: JSONEncoding.default)
			.authenticated(with: authenticator, optional: true)

		return model(for: request)
	}

	func explore(query: String?, location: CLLocationCoordinate2D) -> ApiResponsePublisher<[PlaceLookup]> {
		var params: [String: Any] = [
			"latitude": location.latitude,
			"longitude": location.longitude,
		]

		if let query = query, !query.isEmpty {
			params["query"] = query
		}

		let request = Request.build(with: url(appending: "places/external/explore/"))
			.method(.post)
			.parameters(params, encoding: JSONEncoding.default)
			.authenticated(with: authenticator, optional: true)

		return model(for: request)
	}

	func autocomplete(query: String, location: CLLocationCoordinate2D) -> ApiResponsePublisher<[PlaceAutocomplete]> {
		struct Response: Decodable {
			let results: [PlaceAutocomplete]
		}

		let params = [
			"query": query,
			"latitude": location.latitude,
			"longitude": location.longitude,
		] as [String: Any]

		let request = Request.build(with: url(appending: "places/external/autocomplete/"))
			.parameters(params, encoding: URLEncoding.default)
			.authenticated(with: authenticator, optional: true)

		return model(for: request).map(\Response.results).eraseToAnyPublisher()
	}

	func geocode(query: String) -> ApiResponsePublisher<[PlaceLookup]> {
		struct Response: Decodable {
			let results: [PlaceLookup]
		}

		let request = Request.build(with: url(appending: "places/external/geocode/"))
			.parameters(["query": query], encoding: URLEncoding.default)
			.authenticated(with: authenticator, optional: true)

		return model(for: request).map(\Response.results).eraseToAnyPublisher()
	}
}
