import Combine
import Foundation
import OSLog
import UIKit

protocol ReviewServiceContainer {
	var reviews: ReviewService { get }
}

extension API.Client.ServicesContainer: ReviewServiceContainer {
	var reviews: ReviewService {
		DefaultReviewService(url: url, authenticator: authenticator, responseProvider: responseProvider)
	}
}

/// @mockable
protocol ReviewService {
	func create(place: Place.ID, imageIds: [String], rating: Int, text: String) -> ApiResponsePublisher<Review>
	func update(id: Review.ID, imageIds: [String], rating: Int, text: String) -> ApiResponsePublisher<Review>
	func list(params: ReviewServiceListParameters) -> ApiResponsePublisher<PaginatedResponse<Review>>
	func delete(id: String) -> ApiResponsePublisher<Void>

	func upload(images: [UIImage]) -> ApiResponsePublisher<[Review.Image]>
}

struct ReviewServiceListParameters {
	var placeID: Place.ID?
	var userID: User.ID?
	var userScope: Bool = false
	var offset: UInt = 0
	var limit: UInt = 20
	var sort: String = Config.getReviewSorting()

	func toDictionary() -> [String: Any] {
		var params: [String: Any] = ["offset": offset, "limit": limit, "sort": sort]
		if let placeID = placeID {
			params["place"] = placeID
		}
		if let userID = userID {
			params["user"] = userID
		}
		if userScope {
			params["scope"] = "user"
		}

		return params
	}
}

struct DefaultReviewService: ReviewService, ApiService {
	let url: URL
	var authenticator: RequestAuthenticator
	let responseProvider: ResponseProvider

	var baseURL: URL {
		url.appendingPathComponent("reviews/")
	}

	func create(place: Place.ID, imageIds: [String], rating: Int, text: String) -> ApiResponsePublisher<Review> {
		let params = [
			"place_id": place,
			"label_ids": [],
			"image_ids": imageIds,
			"rating": rating,
			"text": text,
		] as [String: Any]

		let request = Request.build(with: baseURL)
			.method(.post)
			.parameters(params, encoding: JSONEncoding.default)
			.authenticated(with: authenticator)

		return model(for: request)
	}

	func update(id: Review.ID, imageIds: [String], rating: Int, text: String) -> ApiResponsePublisher<Review> {
		let params = [
			"image_ids": imageIds,
			"rating": rating,
			"text": text,
		] as [String: Any]

		let request = Request.build(with: baseURL.appendingPathComponent(id).appendingPathComponent("/"))
			.method(.patch)
			.parameters(params, encoding: JSONEncoding.default)
			.authenticated(with: authenticator)

		return model(for: request)
	}

	func list(params: ReviewServiceListParameters) -> ApiResponsePublisher<PaginatedResponse<Review>> {
		let request = Request.build(with: baseURL)
			.method(.get)
			.parameters(params.toDictionary(), encoding: URLEncoding.default)
			.authenticated(with: authenticator, optional: true)

		return model(for: request)
	}

	func upload(images: [UIImage]) -> ApiResponsePublisher<[Review.Image]> {
		let request = Request.build(with: baseURL.appendingPathComponent("images/"))
			.method(.post)
			.multipartFormData { formData in
				for (idx, image) in images.enumerated() {
					guard let imageData = image.jpegData(compressionQuality: 0.85) else {
						continue
					}
					formData.append(imageData, withName: "image", fileName: "image-\(idx).jpg", mimeType: "image/jpeg")
				}
			}
			.authenticated(with: authenticator)

		struct Response: Decodable {
			let images: [Review.Image]
		}

		return model(for: request).map(\Response.images).eraseToAnyPublisher()
	}

	func delete(id: String) -> ApiResponsePublisher<Void> {
		let request = Request.build(with: baseURL.appendingPathComponent(id).appendingPathComponent("/"))
			.method(.delete)
			.authenticated(with: authenticator)

		return execute(request: request)
	}
}
