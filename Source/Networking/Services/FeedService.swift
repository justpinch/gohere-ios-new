import Combine
import Foundation

protocol FeedServiceContainer {
	var feed: FeedService { get }
}

extension API.Client.ServicesContainer: FeedServiceContainer {
	var feed: FeedService {
		DefaultFeedService(url: url, authenticator: authenticator, responseProvider: responseProvider)
	}
}

/// @mockable
protocol FeedService {
	func get(pageToken: String?, limit: UInt) -> ApiResponsePublisher<FeedResponse>
}

struct DefaultFeedService: FeedService, ApiService {
	let url: URL
	var authenticator: RequestAuthenticator
	let responseProvider: ResponseProvider

	func get(pageToken: String?, limit: UInt) -> ApiResponsePublisher<FeedResponse> {
		var params: [String: Any] = ["limit": limit]
		if let pageToken = pageToken {
			params["page_token"] = pageToken
		}

		let request = Request.build(with: url(appending: "feed/"))
			.parameters(params, encoding: URLEncoding.default)
			.authenticated(with: authenticator, optional: true)

		return model(for: request)
	}
}
