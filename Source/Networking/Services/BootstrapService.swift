import Combine
import Foundation

protocol BootstrapServiceContainer {
	var bootstrap: BootstrapService { get }
}

extension API.Client.ServicesContainer: BootstrapServiceContainer {
	var bootstrap: BootstrapService {
		DefaultBootstrapService(url: url, responseProvider: responseProvider)
	}
}

/// @mockable
protocol BootstrapService {
	func get() -> ApiResponsePublisher<Bootstrap>
}

struct DefaultBootstrapService: BootstrapService, ApiService {
	let url: URL
	let responseProvider: ResponseProvider

	func get() -> ApiResponsePublisher<Bootstrap> {
		let request = Request.build(with: url(appending: "bootstrap/"))
		return model(for: request)
	}
}
