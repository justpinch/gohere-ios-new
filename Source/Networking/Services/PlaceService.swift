import Combine
import CoreLocation
import Foundation

protocol PlaceServiceContainer {
	var places: PlaceService { get }
}

extension API.Client.ServicesContainer: PlaceServiceContainer {
	var places: PlaceService {
		DefaultPlaceService(url: url, authenticator: authenticator, responseProvider: responseProvider)
	}
}

/// @mockable
protocol PlaceService {
	func get(id: Place.ID) -> ApiResponsePublisher<Place>

	func search(area: (location: CLLocationCoordinate2D, radius: Int)?,
	            scoped: Bool,
	            categories: [PlaceCategory.ID],
	            excludePlaceUUIDs: [String],
	            offset: UInt, limit: UInt) -> ApiResponsePublisher<PaginatedResponse<Place>>

	func favorites(offset: UInt, limit: UInt) -> ApiResponsePublisher<PaginatedResponse<Place>>
	func favorite(id: Place.ID) -> ApiResponsePublisher<Void>
	func unfavorite(id: Place.ID) -> ApiResponsePublisher<Void>
	func favoritesCheck(ids: [Place.ID]) -> ApiResponsePublisher<[Place.ID]>
	func searchPromotedPlaces(location: CLLocationCoordinate2D, radius: Int) -> ApiResponsePublisher<PromotedPlaces>
}

struct DefaultPlaceService: PlaceService, ApiService {
	let url: URL
	var authenticator: RequestAuthenticator
	let responseProvider: ResponseProvider

	func get(id: Place.ID) -> ApiResponsePublisher<Place> {
		let request = Request.build(with: url(appending: "places/\(id)/"))
		return model(for: request)
	}

	func search(area: (location: CLLocationCoordinate2D, radius: Int)?,
	            scoped: Bool,
	            categories: [PlaceCategory.ID],
	            excludePlaceUUIDs: [String],
	            offset: UInt, limit: UInt) -> ApiResponsePublisher<PaginatedResponse<Place>>
	{
		var params: [String: Any] = ["offset": offset, "limit": limit]

		if let area = area {
			params["latitude"] = area.location.latitude
			params["longitude"] = area.location.longitude
			params["distance"] = area.radius
		}

		if !categories.isEmpty {
			params["categories"] = categories
		}

		if !excludePlaceUUIDs.isEmpty {
			params["excluded_place_ids"] = excludePlaceUUIDs
		}

		let path = scoped ? "places/reviewed/search/" : "places/search/"

		let request = Request.build(with: url(appending: path))
			.method(.post)
			.parameters(params, encoding: JSONEncoding.default)
			.authenticated(with: authenticator, optional: true)

		return model(for: request)
	}

	func favorite(id: Place.ID) -> ApiResponsePublisher<Void> {
		favorite(id: id, remove: false)
	}

	func unfavorite(id: Place.ID) -> ApiResponsePublisher<Void> {
		favorite(id: id, remove: true)
	}

	func favorite(id: Place.ID, remove: Bool) -> ApiResponsePublisher<Void> {
		let request = Request.build(with: url(appending: "places/\(id)/favorite/"))
			.method(remove ? .delete : .post)
			.authenticated(with: authenticator)

		return execute(request: request)
	}

	func favorites(offset: UInt, limit: UInt) -> ApiResponsePublisher<PaginatedResponse<Place>> {
		let params = ["offset": offset, "limit": limit]

		let request = Request.build(with: url(appending: "places/favorites/"))
			.method(.get)
			.parameters(params, encoding: URLEncoding.default)
			.authenticated(with: authenticator)

		return model(for: request)
	}

	func favoritesCheck(ids: [Place.ID]) -> ApiResponsePublisher<[Place.ID]> {
		let params = ["place_ids": ids]

		let request = Request.build(with: url(appending: "places/favorites/check/"))
			.method(.post)
			.parameters(params, encoding: JSONEncoding.default)
			.authenticated(with: authenticator)

		struct Response: Decodable {
			let favorites: [Place.ID]
		}

		return model(for: request).map(\Response.favorites).eraseToAnyPublisher()
	}
	
	func searchPromotedPlaces(location: CLLocationCoordinate2D, radius: Int) -> ApiResponsePublisher<PromotedPlaces> {
		var params: [String: Any] = [:]

		params["latitude"] = location.latitude
		params["longitude"] = location.longitude
		params["distance"] = radius

		let path = "places/promoted/"

		let request = Request.build(with: url(appending: path))
			.method(.post)
			.parameters(params, encoding: JSONEncoding.default)
			.authenticated(with: authenticator, optional: true)

		return model(for: request).map { (promotedPlacesList: [PromotedPlaces]) in
			promotedPlacesList.first ?? .empty()
		}.eraseToAnyPublisher()
	}
}
