import Combine
import Foundation
import OSLog
import UIKit

protocol UserDeviceServiceContainer {
	var userDevices: UserDeviceService { get }
}

extension API.Client.ServicesContainer: UserDeviceServiceContainer {
	var userDevices: UserDeviceService {
		DefaultUserDeviceService(url: url, authenticator: authenticator, responseProvider: responseProvider)
	}
}

/// @mockable
protocol UserDeviceService {
	func register(device: UserDeviceRegisterParams) -> ApiResponsePublisher<UserDevice>
	func delete(id: UserDevice.ID, token: AuthToken) -> ApiResponsePublisher<Void>
	func getFCMToken(apnsToken: String, firebaseProjectID: String) -> ApiResponsePublisher<String>
}

struct DefaultUserDeviceService: UserDeviceService, ApiService {
	let url: URL
	var authenticator: RequestAuthenticator
	let responseProvider: ResponseProvider

	func register(device: UserDeviceRegisterParams) -> ApiResponsePublisher<UserDevice> {
		let url: URL
		let method: HTTPMethod

		if let id = device.id {
			url = self.url(appending: "users/devices/\(id)/")
			method = .patch
		} else {
			url = self.url(appending: "users/devices/")
			method = .post
		}

		var params: [String: Any] = [
			"name": device.name,
			"os": "ios",
			"fcm_token": device.fcmToken,
			"firebase_project_id": device.firebaseProjectId,
		]

		if let model = device.model {
			params["model"] = model
		}
		if let version = device.osVersion {
			params["os_version"] = version
		}

		let request = Request.build(with: url)
			.method(method)
			.parameters(params, encoding: JSONEncoding.default)
			.authenticated(with: authenticator)

		return model(for: request)
	}

	func delete(id: UserDevice.ID, token: AuthToken) -> ApiResponsePublisher<Void> {
		let request = Request.build(with: url(appending: "users/devices/\(id)/"))
			.method(.delete)
			// force explicit auth since the global token may already be gone
			.header(name: "Authorization", value: "Bearer \(token.access)")

		return execute(request: request)
			.replaceError(with: (), if: { $0.statusCode == 404 })
			.eraseToAnyPublisher()
	}

	func getFCMToken(apnsToken: String, firebaseProjectID: String) -> ApiResponsePublisher<String> {
		let params = [
			"apns_token": apnsToken,
			"project_id": firebaseProjectID,
		]

		let request = Request.build(with: url.appendingPathComponent("users/devices/fcm-token/"))
			.method(.post)
			.parameters(params, encoding: JSONEncoding.default)
			.authenticated(with: authenticator)

		struct Response: Decodable {
			let fcmToken: String
		}

		return model(for: request)
			.map(\Response.fcmToken)
			.eraseToAnyPublisher()
	}
}
