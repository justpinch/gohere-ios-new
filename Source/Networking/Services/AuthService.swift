import Combine
import Foundation

protocol AuthServiceContainer {
	var auth: AuthService { get }
}

extension API.Client.ServicesContainer: AuthServiceContainer {
	var auth: AuthService {
		DefaultAuthService(url: url, responseProvider: responseProvider)
	}
}

/// @mockable
protocol AuthService {
	func register(firstName: String, lastName: String, email: String, password: String) -> ApiResponsePublisher<AuthToken>
	func register(firstName: String, lastName: String, phoneNumber: String, tvpToken: String) -> ApiResponsePublisher<AuthToken>

	func authenticate(email: String, password: String) -> ApiResponsePublisher<AuthToken>
	func authenticate(phoneNumber: String, tvpToken: String) -> ApiResponsePublisher<AuthToken>
	func authenticate(facebookToken: String) -> ApiResponsePublisher<AuthToken>
	func authenticate(appleToken: String, firstName: String?, lastName: String?) -> ApiResponsePublisher<AuthToken>

	func refresh(token: AuthToken) -> ApiResponsePublisher<AuthToken>
}

struct DefaultAuthService: AuthService, ApiService {
	let url: URL
	let responseProvider: ResponseProvider

	func register(firstName: String, lastName: String, email: String, password: String) -> ApiResponsePublisher<AuthToken> {
		let params = [
			"first_name": firstName,
			"last_name": lastName,
			"email": email,
			"password": password,
		]

		let request = Request.build(with: url.appendingPathComponent("users/register/"))
			.method(.post)
			.parameters(params, encoding: JSONEncoding.default)

		return model(for: request)
	}

	func register(firstName: String, lastName: String, phoneNumber: String, tvpToken: String) -> ApiResponsePublisher<AuthToken> {
		let params = [
			"first_name": firstName,
			"last_name": lastName,
			"phone_number": phoneNumber,
			"tvp_token": tvpToken,
		]

		let request = Request.build(with: url.appendingPathComponent("users/register/"))
			.method(.post)
			.parameters(params, encoding: JSONEncoding.default)

		return model(for: request)
	}

	func authenticate(email: String, password: String) -> ApiResponsePublisher<AuthToken> {
		let request = Request.build(with: url(appending: "users/auth/token/"))
			.method(.post)
			.parameters(["email": email, "password": password], encoding: JSONEncoding.default)

		return model(for: request)
	}

	func authenticate(phoneNumber: String, tvpToken: String) -> ApiResponsePublisher<AuthToken> {
		let request = Request.build(with: url(appending: "users/auth/token/tvp/"))
			.method(.post)
			.parameters(["phone_number": phoneNumber, "tvp_token": tvpToken], encoding: JSONEncoding.default)

		return model(for: request)
	}

	func authenticate(facebookToken: String) -> ApiResponsePublisher<AuthToken> {
		let request = Request.build(with: url(appending: "users/facebook/"))
			.method(.post)
			.parameters(["token": facebookToken], encoding: JSONEncoding.default)

		return model(for: request)
	}

	func authenticate(appleToken: String, firstName: String?, lastName: String?) -> ApiResponsePublisher<AuthToken> {
		var params = ["token": appleToken]
		if let firstName = firstName {
			params["first_name"] = firstName
		}
		if let lastName = lastName {
			params["last_name"] = lastName
		}

		let request = Request.build(with: url(appending: "users/apple/"))
			.method(.post)
			.parameters(params, encoding: JSONEncoding.default)

		return model(for: request)
	}

	func refresh(token: AuthToken) -> ApiResponsePublisher<AuthToken> {
		let request = Request.build(with: url(appending: "users/auth/token/refresh/"))
			.method(.post)
			.parameters(["refresh": token.refresh], encoding: JSONEncoding.default)

		struct Response: Decodable {
			let access: String
		}

		return model(for: request)
			.map(\Response.access)
			.map { token.with(access: $0) }
			.eraseToAnyPublisher()
	}
}
