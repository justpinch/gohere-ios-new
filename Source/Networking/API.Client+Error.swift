import Foundation

extension API {
	struct Error: Swift.Error, LocalizedError {
		let reason: Swift.Error

		var errorDescription: String? { reason.localizedDescription }

		var statusCode: Int? {
			(reason as? HttpResponseStatusCodeContainer)?.statusCode
		}
	}

	enum AuthenticationError: Swift.Error {
		case missingToken
		case refreshError(Error)
	}
}

// MARK: - Errors

protocol HttpResponseStatusCodeContainer {
	var statusCode: Int { get }
}

extension API {
	struct DomainError: LocalizedError, Decodable, HttpResponseStatusCodeContainer {
		var requestId: String? = nil
		var statusCode: Int = 0

		let codeValue: String?
		let detail: String?

		enum CodingKeys: String, CodingKey {
			case codeValue = "code"
			case detail
		}

		enum Code {
			case invalidToken
			case userNotFound
			case upgradeRequired
			case userInactive
			case unknown
		}

		var code: Code {
			switch codeValue {
				case "token_not_valid":
					return .invalidToken
				case "user_not_found":
					return .userNotFound
				case "upgrade_required":
					return .upgradeRequired
				case "user_inactive":
					return .userInactive
				default:
					return .unknown
			}
		}

		var errorDescription: String? {
			var parts: [String] = []

			// TODO: handle any specific server code?
			//                switch code {
			//                }

			if statusCode != 0 {
				parts.append("(\(statusCode))")
			}

			if let id = requestId {
				parts.append("\(id)")
			}

			return parts.joined(separator: " ")
		}

		fileprivate func debuggingInfo() -> String {
			var messages = ["HTTP status code: \(statusCode)"]

			if let code = codeValue {
				messages.append("Code: \(code)")
			}

			if let detail = detail {
				messages.append("Detail: \(detail)")
			}

			return messages.joined(separator: ". ")
		}
	}

	struct FormValidationError: LocalizedError, Decodable, HttpResponseStatusCodeContainer {
		var requestId: String? = nil
		var statusCode: Int = 0

		var nonFieldErrors: [String] = []
		var fieldErrors: [String: [String]] = [:]

		struct InnerError: Decodable {
			var nested: [String: InnerError]
			var list: [String]

			init(from decoder: Decoder) throws {
				let container = try decoder.singleValueContainer()

				if let errs = try? container.decode([String].self) {
					list = errs
					nested = [:]
				} else {
					list = []
					nested = try container.decode([String: InnerError].self)
				}
			}
		}

		init(from decoder: Decoder) throws {
			func recurse(parent: String?, inner: InnerError) {
				inner.nested.forEach { key, err in
					guard key != "non_field_errors" else {
						nonFieldErrors.append(contentsOf: err.list)
						return
					}

					let finalKey = (parent?.appending(".") ?? "").appending(key)
					err.list.forEach { error in
						var errors = fieldErrors[finalKey] ?? []
						errors.append(error)
						fieldErrors[finalKey] = errors
					}

					recurse(parent: finalKey, inner: err)
				}
			}

			let inner = try InnerError(from: decoder)
			recurse(parent: nil, inner: inner)
		}

		var errorDescription: String? {
			var parts: [String] = []

			if statusCode != 0 {
				parts.append("(\(statusCode))")
			}

			if let id = requestId {
				parts.append("\(id)")
			}

			return parts.joined(separator: " ")
		}

		fileprivate func debuggingInfo() -> String {
			var messages = ["HTTP status code: \(statusCode)"]

			if !nonFieldErrors.isEmpty {
				var nonFieldMessages = ["Non field errors: \n"]
				nonFieldErrors.forEach { nonFieldMessages.append("\t" + $0) }
				messages.append(nonFieldMessages.joined(separator: "\n"))
			}

			return messages.joined(separator: ". ")
		}
	}

	/// Throttle error
	struct RateLimitError: LocalizedError, Decodable, HttpResponseStatusCodeContainer {
		var requestId: String? = nil
		var statusCode: Int = 0
		let detail: String?

		enum CodingKeys: String, CodingKey {
			case detail
		}

		init(from decoder: Decoder) throws {
			let container = try decoder.container(keyedBy: CodingKeys.self)
			detail = try container.decodeIfPresent(String.self, forKey: .detail)
		}

		var errorDescription: String? {
			var parts: [String] = [
				NSLocalizedString("too_many_request", comment: ""),
			]

			if statusCode != 0 {
				parts.append("\n(\(statusCode))")
			}

			if let id = requestId {
				parts.append("\(id)")
			}

			return parts.joined(separator: " ")
		}

		fileprivate func debuggingInfo() -> String {
			var messages = ["HTTP status code: \(statusCode)"]

			if let detail = detail {
				messages.append("\n\(detail)")
			}

			return messages.joined(separator: ". ")
		}
	}
}
