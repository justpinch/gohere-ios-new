import Combine
import Foundation

extension URLRequest {
	func authenticated(with token: AuthToken) -> URLRequest {
		var request = self
		request.addValue("Bearer \(token.access)", forHTTPHeaderField: "Authorization")
		return request
	}
}

class RequestAuthenticator {
	private var token: CurrentValueSubject<AuthToken?, Never>
	private let queue = DispatchQueue(label: "RequestAuthenticator.\(UUID().uuidString)")

	private let refreshTokenURL: URL
	private let responseProvider: ResponseProvider

	private var publisher: AnyPublisher<AuthToken, API.AuthenticationError>?

	var hasToken: Bool { token.value != nil }

	init(token: CurrentValueSubject<AuthToken?, Never>, baseURL: URL, responseProvider: ResponseProvider) {
		self.token = token
		self.refreshTokenURL = baseURL.appendingPathComponent("users/auth/token/refresh/")
		self.responseProvider = responseProvider
	}

	func token(forceRefresh: Bool = false) -> AnyPublisher<AuthToken, API.AuthenticationError> {
		queue.sync { [weak self] in
			// we're already loading a new token
			if let publisher = self?.publisher {
				return publisher
			}

			// we don't have a token at all, the user should probably log in
			guard let token = self?.token.value else {
				return Fail(error: API.AuthenticationError.missingToken)
					.eraseToAnyPublisher()
			}

			// we already have a valid token and don't want to force a refresh
			if !forceRefresh {
				return Just(token)
					.setFailureType(to: API.AuthenticationError.self)
					.eraseToAnyPublisher()
			}

			// now `self` has to be there
			guard let self = self else {
				return Fail(error: API.AuthenticationError.missingToken)
					.eraseToAnyPublisher()
			}

			// and we need a new token
			let publisher = self.refresh(token)
				.share()
				.handleEvents(receiveOutput: { [weak self] token in
					self?.token.value = token
				}, receiveCompletion: { [weak self] result in
					self?.queue.sync {
						self?.publisher = nil
					}

					if case let .failure(error) = result, error.isTokenInvalidError() {
						// if invalid token, remove it (sign the user out)
						self?.token.value = nil
					}
				})
				.mapError { API.AuthenticationError.refreshError($0) }
				.eraseToAnyPublisher()

			self.publisher = publisher
			return publisher
		}
	}

	private func refresh(_ token: AuthToken) -> ApiResponsePublisher<AuthToken> {
		let request = Request.build(with: refreshTokenURL)
			.method(.post)
			.parameters(["refresh": token.refresh], encoding: JSONEncoding.default)

		return responseProvider
			.response(for: request)
			.map(\.data)
			.map(\RefreshTokenResponse.access)
			.map { token.with(access: $0) }
			.eraseToAnyPublisher()
	}
}

private extension API.Error {
	func isTokenInvalidError() -> Bool {
		guard let domainError = self.reason as? API.DomainError else {
			return false
		}

		return domainError.code == .invalidToken
	}
}
