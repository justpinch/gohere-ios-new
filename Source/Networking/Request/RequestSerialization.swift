import Combine
import Foundation
import OSLog

struct Request {
	static func build(with url: URL) -> RequestBuilderAuthenticatable {
		AuthenticatableRequestBuilder(url: url)
	}
}

protocol RequestBuilder: RequestPublisherConvertible {
	func method(_ method: HTTPMethod) -> Self
	func parameters(_ parameters: [String: Any], encoding: ParameterEncoding) -> Self
	func multipartFormData(_ data: @escaping (MultipartFormData) -> Void) -> Self
	func header(name: String, value: String) -> Self
	func publisher() -> AnyPublisher<URLRequest, Error>
}

protocol RequestBuilderAuthenticatable: RequestBuilder {
	func authenticated(with authenticator: RequestAuthenticator) -> RequestBuilder
	func authenticated(with authenticator: RequestAuthenticator, optional: Bool) -> RequestBuilder
}

protocol RequestPublisherConvertible {
	func publisher() -> AnyPublisher<URLRequest, Error>
}

protocol RequestAuthenticatedPublisherConvertible: RequestPublisherConvertible {
	func publisher(refreshToken: Bool) -> AnyPublisher<URLRequest, Error>
}

// MARK: - Implementation

private class BaseRequestBuilder: RequestBuilder {
	private let url: URL
	private var method: HTTPMethod = .get
	private var parameters: [(pairs: [String: Any], encoding: ParameterEncoding)] = []
	private var multipartFormData: ((MultipartFormData) -> Void)?
	private var headers: [String: String] = [:]

	init(url: URL) {
		self.url = url
	}

	init(builder: BaseRequestBuilder) {
		url = builder.url
		method = builder.method
		parameters = builder.parameters
		multipartFormData = builder.multipartFormData
		headers = builder.headers
	}

	func method(_ method: HTTPMethod) -> Self {
		self.method = method
		return self
	}

	func parameters(_ parameters: [String: Any], encoding: ParameterEncoding) -> Self {
		self.parameters.append((pairs: parameters, encoding: encoding))
		return self
	}
	
	func multipartFormData(_ data: @escaping (MultipartFormData) -> Void) -> Self {
		self.multipartFormData = data
		return self
	}

	func header(name: String, value: String) -> Self {
		headers[name] = value
		return self
	}

	fileprivate func request() throws -> URLRequest {
		var request = URLRequest(url: url)
		request.httpMethod = method.rawValue

		// gotcha helper
		if !url.absoluteString.hasSuffix("/"), url.absoluteString.hasPrefix(API.Environment.current.apiBaseURL.absoluteString) {
			let urlString = url.absoluteString
			Logger.general.warning("API URL doesn't have trailing slash: \(urlString)")
			assertionFailure("API URL doesn't have trailing slash: \(urlString)")
		}

		headers.forEach { pair in
			request.addValue(pair.value, forHTTPHeaderField: pair.key)
		}

		try parameters.forEach { pairs, encoding in
			request = try encoding.encode(request, with: pairs)
		}
		
		if let formData = multipartFormData {
			let data = MultipartFormData()
			formData(data)

			request.addValue(data.contentType, forHTTPHeaderField: "Content-Type")
			request.httpBodyStream = try InputStream(data: data.encode())
		}

		return request
	}

	func publisher() -> AnyPublisher<URLRequest, Error> {
		Future { promise in
			do {
				try promise(.success(self.request()))
			} catch {
				promise(.failure(error))
			}
		}
		.eraseToAnyPublisher()
	}
}

private class AuthenticatableRequestBuilder: BaseRequestBuilder, RequestBuilderAuthenticatable {
	func authenticated(with authenticator: RequestAuthenticator, optional: Bool) -> RequestBuilder {
		AuthenticatedRequestBuilder(base: self, authenticator: authenticator, optional: optional)
	}

	func authenticated(with authenticator: RequestAuthenticator) -> RequestBuilder {
		authenticated(with: authenticator, optional: false)
	}
}

private class AuthenticatedRequestBuilder: BaseRequestBuilder, RequestAuthenticatedPublisherConvertible {
	private let authenticator: RequestAuthenticator
	private let optional: Bool

	fileprivate init(base: BaseRequestBuilder, authenticator: RequestAuthenticator, optional: Bool) {
		self.authenticator = authenticator
		self.optional = optional
		super.init(builder: base)
	}

	override func publisher() -> AnyPublisher<URLRequest, Error> {
		publisher(refreshToken: false)
	}

	func publisher(refreshToken: Bool) -> AnyPublisher<URLRequest, Error> {
		let publisher = super.publisher()

		if !authenticator.hasToken, optional {
			return publisher
		}

		return authenticator.token(forceRefresh: refreshToken)
			.mapError { $0 as Error }
			.flatMap { token in
				publisher.map { request in request.authenticated(with: token) }
			}
			.eraseToAnyPublisher()
	}
}
