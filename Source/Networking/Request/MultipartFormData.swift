//
//  MultipartFormData.swift
//
//  Copyright (c) 2014 Alamofire Software Foundation (http://alamofire.org/)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

import Foundation

#if os(iOS) || os(watchOS) || os(tvOS)
	import MobileCoreServices
#elseif os(macOS)
	import CoreServices
#endif

/// Constructs `multipart/form-data` for uploads within an HTTP or HTTPS body. There are currently two ways to encode
/// multipart form data. The first way is to encode the data directly in memory. This is very efficient, but can lead
/// to memory issues if the dataset is too large. The second way is designed for larger datasets and will write all the
/// data to a single file on disk with all the proper boundary segmentation. The second approach MUST be used for
/// larger datasets such as video content, otherwise your app may run out of memory when trying to encode the dataset.
///
/// For more information on `multipart/form-data` in general, please refer to the RFC-2388 and RFC-2045 specs as well
/// and the w3 form documentation.
///
/// - https://www.ietf.org/rfc/rfc2388.txt
/// - https://www.ietf.org/rfc/rfc2045.txt
/// - https://www.w3.org/TR/html401/interact/forms.html#h-17.13
open class MultipartFormData {
	// MARK: - Helper Types

	/// A dictionary of headers to apply to a `URLRequest`.
	typealias HTTPHeaders = [String: String]

	enum Error: Swift.Error {
		case inputStreamReadFailed(error: Swift.Error)
	}

	enum EncodingCharacters {
		static let crlf = "\r\n"
	}

	enum BoundaryGenerator {
		enum BoundaryType {
			case initial, encapsulated, final
		}

		static func randomBoundary() -> String {
			String(format: "gohere.boundary.%08x%08x", arc4random(), arc4random())
		}

		static func boundaryData(forBoundaryType boundaryType: BoundaryType, boundary: String) -> Data {
			let boundaryText: String

			switch boundaryType {
				case .initial:
					boundaryText = "--\(boundary)\(EncodingCharacters.crlf)"
				case .encapsulated:
					boundaryText = "\(EncodingCharacters.crlf)--\(boundary)\(EncodingCharacters.crlf)"
				case .final:
					boundaryText = "\(EncodingCharacters.crlf)--\(boundary)--\(EncodingCharacters.crlf)"
			}

			return boundaryText.data(using: String.Encoding.utf8, allowLossyConversion: false)!
		}
	}

	class BodyPart {
		let headers: HTTPHeaders
		let bodyStream: InputStream
		let bodyContentLength: UInt64
		var hasInitialBoundary = false
		var hasFinalBoundary = false

		init(headers: HTTPHeaders, bodyStream: InputStream, bodyContentLength: UInt64) {
			self.headers = headers
			self.bodyStream = bodyStream
			self.bodyContentLength = bodyContentLength
		}
	}

	// MARK: - Properties

	/// The `Content-Type` header value containing the boundary used to generate the `multipart/form-data`.
	open lazy var contentType: String = "multipart/form-data; boundary=\(self.boundary)"

	/// The content length of all body parts used to generate the `multipart/form-data` not including the boundaries.
	var contentLength: UInt64 { bodyParts.reduce(0) { $0 + $1.bodyContentLength } }

	/// The boundary used to separate the body parts in the encoded form data.
	var boundary: String

	private var bodyParts: [BodyPart]
	private let streamBufferSize: Int

	// MARK: - Lifecycle

	/// Creates a multipart form data object.
	///
	/// - returns: The multipart form data object.
	init() {
		self.boundary = BoundaryGenerator.randomBoundary()
		self.bodyParts = []

		///
		/// The optimal read/write buffer size in bytes for input and output streams is 1024 (1KB). For more
		/// information, please refer to the following article:
		///   - https://developer.apple.com/library/mac/documentation/Cocoa/Conceptual/Streams/Articles/ReadingInputStreams.html
		///

		self.streamBufferSize = 1024
	}

	// MARK: - Body Parts

	/// Creates a body part from the data and appends it to the multipart form data object.
	///
	/// The body part data will be encoded using the following format:
	///
	/// - `Content-Disposition: form-data; name=#{name}` (HTTP Header)
	/// - Encoded data
	/// - Multipart form boundary
	///
	/// - parameter data: The data to encode into the multipart form data.
	/// - parameter name: The name to associate with the data in the `Content-Disposition` HTTP header.
	func append(_ data: Data, withName name: String) {
		let headers = contentHeaders(withName: name)
		let stream = InputStream(data: data)
		let length = UInt64(data.count)

		append(stream, withLength: length, headers: headers)
	}

	/// Creates a body part from the data and appends it to the multipart form data object.
	///
	/// The body part data will be encoded using the following format:
	///
	/// - `Content-Disposition: form-data; name=#{name}` (HTTP Header)
	/// - `Content-Type: #{generated mimeType}` (HTTP Header)
	/// - Encoded data
	/// - Multipart form boundary
	///
	/// - parameter data:     The data to encode into the multipart form data.
	/// - parameter name:     The name to associate with the data in the `Content-Disposition` HTTP header.
	/// - parameter mimeType: The MIME type to associate with the data content type in the `Content-Type` HTTP header.
	func append(_ data: Data, withName name: String, mimeType: String) {
		let headers = contentHeaders(withName: name, mimeType: mimeType)
		let stream = InputStream(data: data)
		let length = UInt64(data.count)

		append(stream, withLength: length, headers: headers)
	}

	/// Creates a body part from the data and appends it to the multipart form data object.
	///
	/// The body part data will be encoded using the following format:
	///
	/// - `Content-Disposition: form-data; name=#{name}; filename=#{filename}` (HTTP Header)
	/// - `Content-Type: #{mimeType}` (HTTP Header)
	/// - Encoded file data
	/// - Multipart form boundary
	///
	/// - parameter data:     The data to encode into the multipart form data.
	/// - parameter name:     The name to associate with the data in the `Content-Disposition` HTTP header.
	/// - parameter fileName: The filename to associate with the data in the `Content-Disposition` HTTP header.
	/// - parameter mimeType: The MIME type to associate with the data in the `Content-Type` HTTP header.
	func append(_ data: Data, withName name: String, fileName: String, mimeType: String) {
		let headers = contentHeaders(withName: name, fileName: fileName, mimeType: mimeType)
		let stream = InputStream(data: data)
		let length = UInt64(data.count)

		append(stream, withLength: length, headers: headers)
	}

	/// Creates a body part from the stream and appends it to the multipart form data object.
	///
	/// The body part data will be encoded using the following format:
	///
	/// - `Content-Disposition: form-data; name=#{name}; filename=#{filename}` (HTTP Header)
	/// - `Content-Type: #{mimeType}` (HTTP Header)
	/// - Encoded stream data
	/// - Multipart form boundary
	///
	/// - parameter stream:   The input stream to encode in the multipart form data.
	/// - parameter length:   The content length of the stream.
	/// - parameter name:     The name to associate with the stream content in the `Content-Disposition` HTTP header.
	/// - parameter fileName: The filename to associate with the stream content in the `Content-Disposition` HTTP header.
	/// - parameter mimeType: The MIME type to associate with the stream content in the `Content-Type` HTTP header.
	func append(
		_ stream: InputStream,
		withLength length: UInt64,
		name: String,
		fileName: String,
		mimeType: String
	) {
		let headers = contentHeaders(withName: name, fileName: fileName, mimeType: mimeType)
		append(stream, withLength: length, headers: headers)
	}

	/// Creates a body part with the headers, stream and length and appends it to the multipart form data object.
	///
	/// The body part data will be encoded using the following format:
	///
	/// - HTTP headers
	/// - Encoded stream data
	/// - Multipart form boundary
	///
	/// - parameter stream:  The input stream to encode in the multipart form data.
	/// - parameter length:  The content length of the stream.
	/// - parameter headers: The HTTP headers for the body part.
	func append(_ stream: InputStream, withLength length: UInt64, headers: HTTPHeaders) {
		let bodyPart = BodyPart(headers: headers, bodyStream: stream, bodyContentLength: length)
		bodyParts.append(bodyPart)
	}

	// MARK: - Data Encoding

	/// Encodes all the appended body parts into a single `Data` value.
	///
	/// It is important to note that this method will load all the appended body parts into memory all at the same
	/// time. This method should only be used when the encoded data will have a small memory footprint.
	///
	/// - returns: The encoded `Data` if encoding is successful.
	func encode() throws -> Data {
		var encoded = Data()

		bodyParts.first?.hasInitialBoundary = true
		bodyParts.last?.hasFinalBoundary = true

		for bodyPart in bodyParts {
			let encodedData = try encode(bodyPart)
			encoded.append(encodedData)
		}

		return encoded
	}

	// MARK: - Private - Body Part Encoding

	private func encode(_ bodyPart: BodyPart) throws -> Data {
		var encoded = Data()

		let initialData = bodyPart.hasInitialBoundary ? initialBoundaryData() : encapsulatedBoundaryData()
		encoded.append(initialData)

		let headerData = encodeHeaders(for: bodyPart)
		encoded.append(headerData)

		let bodyStreamData = try encodeBodyStream(for: bodyPart)
		encoded.append(bodyStreamData)

		if bodyPart.hasFinalBoundary {
			encoded.append(finalBoundaryData())
		}

		return encoded
	}

	private func encodeHeaders(for bodyPart: BodyPart) -> Data {
		var headerText = ""

		for (key, value) in bodyPart.headers {
			headerText += "\(key): \(value)\(EncodingCharacters.crlf)"
		}
		headerText += EncodingCharacters.crlf

		return headerText.data(using: String.Encoding.utf8, allowLossyConversion: false)!
	}

	private func encodeBodyStream(for bodyPart: BodyPart) throws -> Data {
		let inputStream = bodyPart.bodyStream
		inputStream.open()
		defer { inputStream.close() }

		var encoded = Data()

		while inputStream.hasBytesAvailable {
			var buffer = [UInt8](repeating: 0, count: streamBufferSize)
			let bytesRead = inputStream.read(&buffer, maxLength: streamBufferSize)

			if let error = inputStream.streamError {
				throw Error.inputStreamReadFailed(error: error)
			}

			if bytesRead > 0 {
				encoded.append(buffer, count: bytesRead)
			} else {
				break
			}
		}

		return encoded
	}

	// MARK: - Private - Mime Type

	private func mimeType(forPathExtension pathExtension: String) -> String {
		if
			let id = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as CFString, nil)?.takeRetainedValue(),
			let contentType = UTTypeCopyPreferredTagWithClass(id, kUTTagClassMIMEType)?.takeRetainedValue()
		{
			return contentType as String
		}

		return "application/octet-stream"
	}

	// MARK: - Private - Content Headers

	private func contentHeaders(withName name: String, fileName: String? = nil, mimeType: String? = nil) -> [String: String] {
		var disposition = "form-data; name=\"\(name)\""
		if let fileName = fileName { disposition += "; filename=\"\(fileName)\"" }

		var headers = ["Content-Disposition": disposition]
		if let mimeType = mimeType { headers["Content-Type"] = mimeType }

		return headers
	}

	// MARK: - Private - Boundary Encoding

	private func initialBoundaryData() -> Data {
		BoundaryGenerator.boundaryData(forBoundaryType: .initial, boundary: boundary)
	}

	private func encapsulatedBoundaryData() -> Data {
		BoundaryGenerator.boundaryData(forBoundaryType: .encapsulated, boundary: boundary)
	}

	private func finalBoundaryData() -> Data {
		BoundaryGenerator.boundaryData(forBoundaryType: .final, boundary: boundary)
	}
}
