import Foundation

// MARK: - Environment

extension API {
	enum Environment: String {
		case local
		case staging
		case production

		static let current: Environment = {
			#if LOCAL
				return .local
			#elseif STAGING
				return .staging
			#else
				return .production
			#endif
		}()

		var apiBaseURL: URL {
			baseURL.appendingPathComponent("api/v1/")
		}

		var baseURL: URL {
			switch self {
				case .local:
					return URL(string: "http://localhost:8000")!
				case .staging:
					return URL(string: "https://app-staging.gohere.app")!
				case .production:
					return URL(string: "https://app.gohere.app")!
			}
		}

		var baseShareURL: URL {
			baseURL
				.appendingPathComponent("share")
				.appendingPathComponent(Config.appRealm)
		}

		var CDNURL: URL {
			switch self {
				case .local:
					return URL(string: "http://localhost:8000")!
				case .staging:
					return URL(string: "https://cdn-staging.gohere.app")!
				case .production:
					return URL(string: "https://cdn.gohere.app")!
			}
		}

		var mixpanelToken: String? {
			switch self {
				case .local:
					return nil
				case .staging:
					return "0e4bb00ed399ae727a83ae851f2993e7"
				case .production:
					return "91e105d98fc94e1fa9178dec46bd1f64"
			}
		}
	}
}

extension API.Environment {
	func createInviteLink(for id: User.ID?) -> URL {
		let url = baseShareURL.appendingPathComponent("invite")
		guard let id = id else {
			return url
		}

		var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)!
		urlComponents.queryItems = [URLQueryItem(name: "from", value: id)]
		return urlComponents.url!
	}
}
