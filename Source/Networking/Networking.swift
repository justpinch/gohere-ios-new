import Combine
import Foundation

typealias NetworkDataOutput = (data: Data, response: URLResponse)
typealias NetworkDecodedOutput<D: Decodable> = (data: D, response: URLResponse)

protocol NetworkingSession {
	func publisher(for request: URLRequest) -> AnyPublisher<NetworkDataOutput, Error>
}

extension URLSession: NetworkingSession {
	func publisher(for request: URLRequest) -> AnyPublisher<NetworkDataOutput, Error> {
		dataTaskPublisher(for: request)
			.mapError { $0 }
			.eraseToAnyPublisher()
	}
}

// MARK: - Publishers

// convenience
typealias ApiResponsePublisher<T> = AnyPublisher<T, API.Error>

extension Publisher where Self.Output == NetworkDataOutput {
	/// Decodes the output from network data upstream using a specified `TopLevelDecoder`.
	/// For example, use `JSONDecoder`.
	func decode<Item, Coder>(type: Item.Type, decoder: Coder) -> Publishers.TryMap<Self, NetworkDecodedOutput<Item>> where Item: Decodable, Coder: TopLevelDecoder, Coder.Input == Data {
		tryMap {
			do {
				return try (data: decoder.decode(type, from: $0.data), response: $0.response)
			} catch {
				assertionFailure("Unexpected decoding error: \(error)")
				error.report(context: ["url": $0.response.url?.absoluteString ?? ""])
				throw error
			}
		}
	}
}

extension Publisher where Self.Output == URLRequest, Self.Failure == Error {
	func authenticate(with authenticator: RequestAuthenticator, optional: Bool = false) -> Publishers.FlatMap<AnyPublisher<URLRequest, Error>, Self> {
		flatMap { request in
			authenticator.token()
				.mapError { $0 as Error }
				.map(request.authenticated(with:))
				.catch { error in
					optional ?
						Just(request).setFailureType(to: Error.self).eraseToAnyPublisher() :
						Fail(error: error).eraseToAnyPublisher()
				}
				.eraseToAnyPublisher()
		}
	}

	func response(with session: NetworkingSession) -> Publishers.FlatMap<Publishers.HandleEvents<AnyPublisher<NetworkDataOutput, Error>>, Publishers.HandleEvents<Self>> {
		self.addSentryBreadcrumb()
			.flatMap { session.publisher(for: $0).addSentryBreadcrumb() }
	}
}
