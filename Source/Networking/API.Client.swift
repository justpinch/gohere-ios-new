import Combine
import Foundation
import SwiftUI

struct API {
	struct Client {
		// Service Provider
		let services: Services

		init(credentialsStorage: CredentialsStorage) {
			let configuration = URLSessionConfiguration.default
			configuration.httpAdditionalHeaders = API.Client.defaultHTTPHeaders
			let session = URLSession(configuration: configuration)
			self.init(session: session, credentialsStorage: credentialsStorage)
		}

		init(session: NetworkingSession, credentialsStorage: CredentialsStorage, services: Services? = nil, decoder: JSONDecoder? = nil) {
			let decoder = decoder ?? {
				let decoder = JSONDecoder()
				decoder.keyDecodingStrategy = .convertFromSnakeCase
				return decoder
			}()

			let baseURL = Environment.current.apiBaseURL

			let responseProvider = DefaultResponseProvider(baseURL: baseURL, decoder: decoder, session: session)
			let authenticator = RequestAuthenticator(token: credentialsStorage.authToken, baseURL: baseURL, responseProvider: responseProvider)
			let services = services ?? ServicesContainer(url: baseURL, authenticator: authenticator, responseProvider: responseProvider)

			self.services = services
		}
	}

	struct DefaultResponseProvider {
		let baseURL: URL
		let decoder: JSONDecoder
		let session: NetworkingSession
	}
}

// MARK: - ResponseProvider

protocol ResponseProvider {
	func response(for request: RequestPublisherConvertible) -> ApiResponsePublisher<NetworkDataOutput>
	func response<Model: Decodable>(for request: RequestPublisherConvertible) -> ApiResponsePublisher<NetworkDecodedOutput<Model>>
}

extension API.DefaultResponseProvider: ResponseProvider {
	private func validateResponse(_ output: NetworkDataOutput) throws -> NetworkDataOutput {
		guard let response = output.response as? HTTPURLResponse else {
			throw API.DomainError(codeValue: nil, detail: "Invalid URL response")
		}

		// Validate status code
		guard !(200 ..< 300).contains(response.statusCode) else {
			return output
		}

		// If status code fails validation, we need to parse the error if possible

		// Request ID sent by the server

		let requestId = response.value(forHTTPHeaderField: "x-request-id")

		// ValidationError
		if response.statusCode == 400, !output.data.isEmpty, var validationError = try? decoder.decode(API.FormValidationError.self, from: output.data) {
			validationError.requestId = requestId
			validationError.statusCode = response.statusCode
			throw validationError
		}

		// RateLimitError
		if response.statusCode == 429, !output.data.isEmpty, var rateLimitError = try? decoder.decode(API.RateLimitError.self, from: output.data) {
			rateLimitError.requestId = requestId
			rateLimitError.statusCode = response.statusCode
			throw rateLimitError
		}

		// DomainError
		if !output.data.isEmpty, var domainError = try? decoder.decode(API.DomainError.self, from: output.data) {
			domainError.requestId = requestId
			domainError.statusCode = response.statusCode
			throw domainError
		}

		// Fallback, just in case
		throw API.DomainError(requestId: requestId, statusCode: response.statusCode, codeValue: nil, detail: nil)
	}

	private func dataResponse(for publisher: AnyPublisher<URLRequest, Error>) -> AnyPublisher<NetworkDataOutput, Error> {
		publisher.response(with: session).tryMap(validateResponse).eraseToAnyPublisher()
	}

	private func dataResponse(for request: RequestAuthenticatedPublisherConvertible, refresh: Bool) -> AnyPublisher<NetworkDataOutput, Error> {
		request.publisher(refreshToken: refresh).response(with: session).tryMap(validateResponse).eraseToAnyPublisher()
	}

	private func refreshableRequestResponse(for request: RequestPublisherConvertible) -> Publishers.Catch<AnyPublisher<NetworkDataOutput, Error>, AnyPublisher<NetworkDataOutput, Error>> {
		dataResponse(for: request.publisher())
			.catch { error -> AnyPublisher<NetworkDataOutput, Error> in
				guard let refreshableRequestPublisher = request as? RequestAuthenticatedPublisherConvertible,
				      let domainError = error as? API.DomainError, domainError.code == .invalidToken
				else {
					return Fail(error: error).eraseToAnyPublisher()
				}

				let publisher = refreshableRequestPublisher.publisher(refreshToken: true)
				return self.dataResponse(for: publisher)
			}
	}

	func response(for request: RequestPublisherConvertible) -> ApiResponsePublisher<NetworkDataOutput> {
		refreshableRequestResponse(for: request)
			.mapError { API.Error(reason: $0) }
			.receive(on: RunLoop.main)
			.eraseToAnyPublisher()
	}

	func response<Model: Decodable>(for request: RequestPublisherConvertible) -> ApiResponsePublisher<NetworkDecodedOutput<Model>> {
		refreshableRequestResponse(for: request)
			.decode(type: Model.self, decoder: decoder)
			.mapError { API.Error(reason: $0) }
			.receive(on: RunLoop.main)
			.eraseToAnyPublisher()
	}
}

// MARK: - API.Client Default HTTP Headers

extension API.Client {
	/// Creates default values for the "Accept-Encoding", "Accept-Language" and "User-Agent" headers.
	private static let defaultHTTPHeaders: [String: String] = {
		// ApiKey
		let apiKey = Config.apiKey

		// Accept-Encoding HTTP Header; see https://tools.ietf.org/html/rfc7230#section-4.2.3
		let acceptEncoding: String = "gzip;q=1.0, compress;q=0.5"

		// Accept-Language HTTP Header; see https://tools.ietf.org/html/rfc7231#section-5.3.5
		let acceptLanguage = Locale.preferredLanguages.prefix(6).enumerated().map { index, languageCode in
			let quality = 1.0 - (Double(index) * 0.1)
			return "\(languageCode);q=\(quality)"
		}.joined(separator: ", ")

		// User-Agent Header; see https://tools.ietf.org/html/rfc7231#section-5.5.3
		// Example: `iOS Example/1.0 (org.alamofire.iOS-Example; build:1; iOS 10.0.0) Alamofire/4.0.0`
		let userAgent: String = {
			if let info = Bundle.main.infoDictionary {
				let executable = info[kCFBundleExecutableKey as String] as? String ?? "Unknown"
				let bundle = info[kCFBundleIdentifierKey as String] as? String ?? "Unknown"
				let appVersion = info["CFBundleShortVersionString"] as? String ?? "Unknown"
				let internalVersion = (Bundle.main.infoDictionary?["GoHere-InternalVersion"] as? String) ?? "Unknown"
				let appBuild = info[kCFBundleVersionKey as String] as? String ?? "Unknown"

				let osNameVersion: String = {
					let version = ProcessInfo.processInfo.operatingSystemVersion
					let versionString = "\(version.majorVersion).\(version.minorVersion).\(version.patchVersion)"

					let osName: String = {
						#if os(iOS)
							return "iOS"
						#elseif os(watchOS)
							return "watchOS"
						#elseif os(tvOS)
							return "tvOS"
						#elseif os(macOS)
							return "OS X"
						#elseif os(Linux)
							return "Linux"
						#else
							return "Unknown"
						#endif
					}()

					return "\(osName) \(versionString)"
				}()

				return "\(executable)/\(appVersion) (\(bundle); build:\(internalVersion).\(appBuild); \(osNameVersion))"
			}

			return "GoHere"
		}()

		return [
			"Accept-Encoding": acceptEncoding,
			"Accept-Language": acceptLanguage,
			"User-Agent": userAgent,
			"X-Api-Key": apiKey,
		]
	}()
}
