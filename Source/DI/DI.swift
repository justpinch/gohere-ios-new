import Foundation
import SwiftUI

typealias RegistrationFactory<S> = (DependencyResolver) -> S

protocol DependencyContainer {
	func register<Service>(service: Service)
	func register<Service>(service: Service, name: String)
	func register<Service>(factory: @escaping RegistrationFactory<Service>)
	func register<Service>(name: String, factory: @escaping RegistrationFactory<Service>)
}

protocol DependencyResolver {
	func resolve<Service>(_ type: Service.Type, name: String?) -> Service
	func resolve<Service>(_ type: Service.Type) -> Service
}

class DIContainer: DependencyContainer, DependencyResolver {
	private var services: [ObjectIdentifier: [String: Any]] = [:]
	private let defaultServiceName = "default"

	func register<Service>(service: Service) {
		register(service: service, name: defaultServiceName)
	}

	func register<Service>(service: Service, name: String) {
		register(name: name, factory: { _ in service })
	}

	func register<Service>(factory: @escaping RegistrationFactory<Service>) {
		register(name: defaultServiceName, factory: factory)
	}

	func register<Service>(name: String, factory: @escaping RegistrationFactory<Service>) {
		let type = Service.self
		let key = self.key(for: type)
		let registrationName = name
		var serviceRegistrations = services[key] ?? [:]
		serviceRegistrations[registrationName] = factory(self)
		services[key] = serviceRegistrations
	}

	func resolve<Service>(_ type: Service.Type, name: String?) -> Service {
		let key = self.key(for: type)
		let registrationName = name ?? defaultServiceName
		guard let service = services[key]?[registrationName] as? Service else {
			fatalError("Could not resolve service of type `\(type)` with name `\(registrationName)`")
		}

		return service
	}

	func resolve<Service>(_ type: Service.Type) -> Service {
		resolve(type, name: nil)
	}

	private func key<T>(for _: T.Type) -> ObjectIdentifier {
		ObjectIdentifier(T.self)
	}
}

struct DependencyContainerKey: EnvironmentKey {
	static var defaultValue: DependencyContainer & DependencyResolver = DIContainer()
}

extension EnvironmentValues {
	var dependencyContainer: DependencyContainer & DependencyResolver {
		get { self[DependencyContainerKey.self] }
		set { self[DependencyContainerKey.self] = newValue }
	}

	var appSettings: AppSettings { self[DependencyContainerKey].self.resolve(AppSettings.self) }

	var apiClient: API.Client { self[DependencyContainerKey.self].resolve(API.Client.self) }

	var appleSignInCoordinator: AppleSignInCoordinator { self[DependencyContainerKey.self].resolve(AppleSignInCoordinator.self) }

	// Interactors
	var userInteractor: UserInteractor { self[DependencyContainerKey.self].resolve(UserInteractor.self) }
	
	var translatorRepository: TranslatorRepository { self[DependencyContainerKey.self].resolve(TranslatorRepository.self) }
}
