import CoreLocation
import Foundation

struct Advertorial: Decodable, Hashable {
	let id: String
	let slug: String
	let title: String
	let subtitle: String
	let publishDate: Date
	let canHide: Bool
	let ctaButton: CallToActionButton?
	let location: CLLocationCoordinate2D?
	let images: [Image]
	let targetURL: URL?

	enum CallToActionButton: String, Decodable {
		case getStarted = "get_started"
		case learnMore = "learn_more"
		case tryNow = "try_now"
		case bookNow = "book_now"
		case orderNow = "order_now"
		case visitNow = "visit_now"
		case buyNow = "buy_now"
		case readMore = "read_more"
		case moreTips = "more_tips"

		var localizedTitle: String {
			let key = rawValue.lowercased()
			return NSLocalizedString(key, comment: "")
		}
	}

	struct Image: Decodable, Hashable {
		let path: ImagePath
		let aspectRatio: Double

		init(path: ImagePath, aspectRatio: Double) {
			self.path = path
			self.aspectRatio = aspectRatio
		}
	}

	enum CodingKeys: String, CodingKey {
		case id
		case slug
		case title
		case subtitle
		case publishDate
		case canHide
		case ctaButton
		case location
		case images
		case targetURL = "targetUrl"
	}

	enum LocationKeys: String, CodingKey {
		case latitude
		case longitude
	}

	init(id: String, slug: String, title: String, subtitle: String, publishDate: Date, canHide: Bool, ctaButton: Advertorial.CallToActionButton?, location: CLLocationCoordinate2D?, images: [Advertorial.Image], targetURL: URL?) {
		self.id = id
		self.slug = slug
		self.title = title
		self.subtitle = subtitle
		self.publishDate = publishDate
		self.canHide = canHide
		self.ctaButton = ctaButton
		self.location = location
		self.images = images
		self.targetURL = targetURL
	}

	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		id = try container.decode(String.self, forKey: .id)
		slug = try container.decode(String.self, forKey: .slug)
		title = try container.decode(String.self, forKey: .title)
		subtitle = try container.decode(String.self, forKey: .subtitle)
		publishDate = try container.decode(Date.self, forKey: .publishDate)
		canHide = try container.decode(Bool.self, forKey: .canHide)
		ctaButton = try container.decodeIfPresent(CallToActionButton.self, forKey: .ctaButton)
		images = try container.decodeIfPresent([Image].self, forKey: .images) ?? []
		targetURL = try container.decodeIfPresent(URL.self, forKey: .targetURL)

		if let locationContainer = try? container.nestedContainer(keyedBy: LocationKeys.self, forKey: .location) {
			let lat = try locationContainer.decode(Double.self, forKey: .latitude)
			let lng = try locationContainer.decode(Double.self, forKey: .longitude)
			location = CLLocationCoordinate2D(latitude: lat, longitude: lng)
		} else {
			location = nil
		}
	}
}
