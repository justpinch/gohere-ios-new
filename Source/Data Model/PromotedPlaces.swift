//
//  PromotedPlacesList.swift
//  GoHere
//
//  Created by jorisfavier on 22/03/2022.
//

import Foundation

struct PromotedPlaces: Decodable {
	let id: Int
	let title: String
	let places: Set<Place>
	
	static func empty() -> PromotedPlaces {
		return PromotedPlaces(id: 1, title: "", places: [])
	}
}
