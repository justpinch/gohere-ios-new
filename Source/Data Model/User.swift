import Foundation
import UIKit

class User: Identifiable, Codable, Hashable {
	let id: String
	let email: String?
	let phoneNumber: String?
	let firstName: String
	let lastName: String
	let profile: Profile

	var fullName: String {
		"\(firstName) \(lastName)"
	}

	lazy var initials: String = {
		var components = PersonNameComponents()
		components.givenName = firstName
		components.familyName = lastName
		return PersonNameComponentsFormatter.localizedString(from: components, style: .abbreviated, options: .init())
	}()

	struct Profile: Codable, Hashable {
		let avatar: ImagePath?
		let hometown: String?
		let isPrivate: Bool
		let biography: String
	}

	init(id: String, email: String?, phoneNumber: String?, firstName: String, lastName: String, profile: User.Profile) {
		self.id = id
		self.email = email
		self.phoneNumber = phoneNumber
		self.firstName = firstName
		self.lastName = lastName
		self.profile = profile
	}

	func hash(into hasher: inout Hasher) {
		hasher.combine(id)
	}

	static func == (lhs: User, rhs: User) -> Bool {
		lhs.id == rhs.id &&
			lhs.email == rhs.email &&
			lhs.phoneNumber == rhs.phoneNumber &&
			lhs.firstName == rhs.firstName &&
			lhs.lastName == rhs.lastName &&
			lhs.profile == rhs.profile
	}
}

class UserExtended: User {
	var followersCount: Int?
	var followingCount: Int?
	let reviewCount: Int
	let favoriteCount: Int

	private enum CodingKeys: String, CodingKey {
		case followersCount
		case followingCount
		case reviewCount
		case favoriteCount
	}

	var isMothership: Bool {
		/// NOTE: (ugly hack)
		/// historically, the API returns NULL for following/follower counts for mothership users
		/// so we (ab)use it here
		followingCount == nil
	}

	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		followersCount = try container.decodeIfPresent(Int.self, forKey: .followersCount)
		followingCount = try container.decodeIfPresent(Int.self, forKey: .followingCount)
		reviewCount = try container.decode(Int.self, forKey: .reviewCount)
		favoriteCount = try container.decode(Int.self, forKey: .favoriteCount)

		try super.init(from: decoder)
	}

	init(base: User, followersCount: Int?, followingCount: Int?, reviewCount: Int, favoriteCount: Int) {
		self.followersCount = followersCount
		self.followingCount = followingCount
		self.reviewCount = reviewCount
		self.favoriteCount = favoriteCount

		super.init(id: base.id, email: base.email, phoneNumber: base.phoneNumber, firstName: base.firstName, lastName: base.lastName, profile: base.profile)
	}
}

struct UpdateUser {
	let id: User.ID
	var firstName: String?
	var lastName: String?
	var email: String?
	var phoneNumber: String?
	var tvpToken: String?
	var avatar: UIImage?
	var hometown: String?
	var isPrivate: Bool?
	var biography: String?
}

struct UserConnection: Decodable, Hashable {
	enum Status: String, Decodable, CaseIterable {
		case active
		case pending
		case ignored
		case blocked
		case none
	}

	enum Kind: String, Decodable {
		case following
		case follower
	}

	let user: User
	var kind: Kind
	let status: Status
}

struct BatchUserConnectionStatus: Decodable {
	let userId: User.ID
	let followingStatus: UserConnection.Status
	let followerStatus: UserConnection.Status

	enum CodingKeys: String, CodingKey {
		case userId = "user"
		case followingStatus
		case followerStatus
	}

	init(userID: User.ID, followingStatus: UserConnection.Status, followerStatus: UserConnection.Status) {
		self.userId = userID
		self.followingStatus = followingStatus
		self.followerStatus = followerStatus
	}

	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		userId = try container.decode(String.self, forKey: .userId)
		followingStatus = try container.decode(UserConnection.Status?.self, forKey: .followingStatus) ?? .none
		followerStatus = try container.decode(UserConnection.Status?.self, forKey: .followerStatus) ?? .none
	}
}

struct UserDevice: Identifiable, Codable, Equatable {
	enum OS: String, Codable {
		case ios
		case android
	}

	let id: String
	var name: String
	var model: String?
	let os: OS
	var osVersion: String?
	var fcmToken: String
	var firebaseProjectId: String
}

struct UserDeviceRegisterParams {
	let id: UserDevice.ID?
	let name: String
	let model: String?
	let osVersion: String?
	let fcmToken: String
	let firebaseProjectId: String
}
