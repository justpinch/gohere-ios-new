//
//  ReviewItem.swift
//  GoHere
//
//  Created by jorisfavier on 28/07/2022.
//

import Foundation

struct ReviewItem: Identifiable {
	var id: String {
		review.id
	}
	let review: Review
	let translation: Translation?
	let isTranslationVisible: Bool
}
