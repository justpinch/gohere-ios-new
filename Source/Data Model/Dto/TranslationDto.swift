//
//  TranslationResultDto.swift
//  GoHere
//
//  Created by jorisfavier on 27/07/2022.
//

import Foundation
import SwiftUI

struct TranslationDto: Codable {
	let detectedLanguage: String
	let text: String
}
