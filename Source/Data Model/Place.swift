import CoreLocation
import Foundation

struct Place: Codable, Hashable, Identifiable {
	typealias ID = String

	let id: ID
	let idGoogle: String?
	let name: String
	let address: String
	let city: String
	let country: String
	let location: GPSLocation
	let category: PlaceCategory
	let website: URL?
	let phone: String
	let affiliate: AffiliateInfo?
	let photo: ImagePath?
	let info: Info?

	// only if extended
	var averageRating: Int?
	let labels: [String]?
	let isFavorite: Bool?
	var lastReview: ReviewInline?

	struct Info: Codable, Hashable {
		let openingHours: OpeningHours?
	}

	struct AffiliateInfo: Codable, Hashable {
		let kind: String
		let actionURL: URL
		private let ctaButton: String?

		var callToAction: String {
			if let key = ctaButton?.lowercased() {
				let trans = NSLocalizedString(key, comment: "")
				if trans != key {
					return trans
				}
			}
			return NSLocalizedString("order_now", comment: "")
		}

		init(kind: String, actionURL: URL, ctaButton: String?) {
			self.kind = kind
			self.actionURL = actionURL
			self.ctaButton = ctaButton
		}

		enum CodingKeys: String, CodingKey {
			case kind
			case actionURL = "actionUrl"
			case ctaButton
		}
	}

	/// ⚠️ This can be a serious gotcha since the equality is enforced only through the ID.
	/// The reason is that 99% of the time that's what we want when we put the place in a set or so...
	/// For the next sane person that decides this was a terrible idea (which I agree) and goes on to change it,
	/// I would personally:
	/// 	1. temporarily remove Hashable from Place so the compiler gives you errors where Hashable is expected
	///		2. Inspect all those code paths and make necessary changes & re-conform Place to Hashable

	static func == (lhs: Self, rhs: Self) -> Bool {
		lhs.id == rhs.id
	}

	func hash(into hasher: inout Hasher) {
		hasher.combine(id)
	}

	/** ----------------------- -----------------------  -----------------------     */
}

struct PlaceCategory: Identifiable, Codable, Hashable {
	typealias ID = Int

	let id: ID
	let slug: String
	let icon: ImagePath

	var localizedName: String {
		let key = "category_\(slug)"
		let trans = NSLocalizedString("category_\(slug)", comment: "")
		return trans != key ? trans : slug.capitalized
	}
}

struct OpeningHours: Codable, Hashable {
	let periods: [Period]

	var isAlwaysOpen: Bool {
		if let period = periods.first, periods.count == 1, period.isAlwaysOpen {
			return true
		}
		return false
	}

	struct Period: Codable, Hashable {
		let open: Slot
		let close: Slot?

		struct Slot: Codable, Hashable {
			let day: Int
			let time: String
		}

		var isAlwaysOpen: Bool {
			guard close == nil else { return false }
			return open.day == 0 && open.time == "0000"
		}
	}
}

struct PlaceLookup: Codable, Hashable {
	typealias ID = String

	let lookupId: ID
	let providerId: Int
	let title: String
	let vicinity: String
	let location: GPSLocation
	let category: PlaceCategory?

	enum CodingKeys: String, CodingKey {
		case lookupId
		case providerId
		case title
		case vicinity
		case category
		case latitude
		case longitude
	}

	init(lookupId: String, providerId: Int, title: String, vicinity: String, location: GPSLocation, category: PlaceCategory?) {
		self.lookupId = lookupId
		self.providerId = providerId
		self.title = title
		self.vicinity = vicinity
		self.location = location
		self.category = category
	}

	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		lookupId = try container.decode(String.self, forKey: .lookupId)
		providerId = try container.decode(Int.self, forKey: .providerId)
		title = try container.decode(String.self, forKey: .title)
		vicinity = try container.decode(String.self, forKey: .vicinity)
		let lat = try container.decode(Double.self, forKey: .latitude)
		let lng = try container.decode(Double.self, forKey: .longitude)
		location = GPSLocation(latitude: lat, longitude: lng)
		category = try container.decodeIfPresent(PlaceCategory.self, forKey: .category)
	}

	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(lookupId, forKey: .lookupId)
		try container.encode(providerId, forKey: .providerId)
		try container.encode(title, forKey: .title)
		try container.encode(vicinity, forKey: .vicinity)
		try container.encode(location.latitude, forKey: .latitude)
		try container.encode(location.longitude, forKey: .longitude)
		try container.encodeIfPresent(category, forKey: .category)
	}
}

struct PlaceAutocomplete: Decodable, Hashable {
	let lookupId: String
	let providerId: Int
	let title: String
	let vicinity: String?
	let category: PlaceCategory?

	var isPOI: Bool { category != nil }
}

enum AnyPlace: Equatable {
	case placeID(Place.ID)
	case place(Place)
	case lookup(PlaceLookup)
	case autocomplete(PlaceAutocomplete)

	var location: CLLocationCoordinate2D? {
		switch self {
			case .placeID:
				return nil
			case let .place(place):
				return place.location.coordinate
			case let .lookup(lookup):
				return lookup.location.coordinate
			case .autocomplete:
				return nil
		}
	}
}
