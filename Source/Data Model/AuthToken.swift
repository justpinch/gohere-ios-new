import Foundation

struct AuthToken: Codable, Equatable {
	let userId: String
	let access: String
	let refresh: String

	func with(access: String) -> AuthToken {
		AuthToken(userId: userId, access: access, refresh: refresh)
	}
}

struct RefreshTokenResponse: Codable {
	let access: String
}
