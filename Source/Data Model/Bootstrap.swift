import CoreLocation
import Foundation
import MapKit

struct Bootstrap: Codable, Equatable {
	typealias RegionCoordinates = [CLLocationDegrees]
	typealias Region = [RegionCoordinates]
	typealias MultiRegion = [Region]

	let mothershipId: User.ID
	let categories: [PlaceCategory]
	let defaultRegion: MultiRegion?
	let biasLocation: GPSLocation?
	let bannerId: String?
	let uberClientId: String?
	let promotedPlacesSearchRadius: Int

	enum CodingKeys: String, CodingKey {
		case mothershipId = "mothershipAccountId"
		case categories
		case defaultRegion = "regions"
		case biasLocation
		case bannerId
		case uberClientId
		case promotedPlacesSearchRadius
	}
}

extension Bootstrap.MultiRegion {
	/// Combine all given regions into one single region (union)
	func union() -> MKMapRect? {
		let rects = map { region -> MKPolygon in
			let coords = region.map { CLLocationCoordinate2D(latitude: $0[1], longitude: $0[0]) }
			let poly = MKPolygon(coordinates: coords, count: coords.count)
			return poly
		}
		.map(\.boundingMapRect)

		guard let first = rects.first, rects.count > 1 else {
			return rects.first
		}

		return rects[1...].reduce(first) { result, rect -> MKMapRect in
			result.union(rect)
		}
	}
}
