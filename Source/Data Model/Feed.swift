
import Foundation
import UIKit

struct FeedResponse: Decodable {
	let results: [FeedItem]
	let nextPageToken: String?
}

struct FeedItem: Decodable, Hashable, Identifiable {
	let kind: Kind
	let timestamp: Date

	var id: String {
		"\(self.timestamp)\(self.kind.hashValue)"
	}

	enum Kind: Hashable {
		case review(Review)
		case savedPlace(SavedPlace)
		case advertorial(Advertorial)
		case tweet(Tweet)
		case facebook(FacebookPost)
		case instagram(InstagramPost)

		// local, temporary (until we force auth in app)
		case loginPrompt

		// unknown to prevent errors, should be ignored
		// if we send something unknown in the future
		case unknown
	}

	enum CodingKeys: String, CodingKey {
		case kind
		case timestamp
		case data
	}

	init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		timestamp = try container.decode(Date.self, forKey: .timestamp)

		let kindString = try container.decode(String.self, forKey: .kind)
		switch kindString {
			case "review":
				let review = try container.decode(Review.self, forKey: .data)
				kind = .review(review)
			case "favorite":
				let savedPlace = try container.decode(FeedItem.SavedPlace.self, forKey: .data)
				kind = .savedPlace(savedPlace)
			case "advertorial":
				let advertorial = try container.decode(Advertorial.self, forKey: .data)
				kind = .advertorial(advertorial)
			case "tweet":
				let tweet = try container.decode(FeedItem.Tweet.self, forKey: .data)
				kind = .tweet(tweet)
			case "facebook":
				let post = try container.decode(FeedItem.FacebookPost.self, forKey: .data)
				kind = .facebook(post)
			case "instagram":
				let post = try container.decode(FeedItem.InstagramPost.self, forKey: .data)

				// if there's at least 1 photo, we show it
				// since videos are not supported at the moment
				if post.media?.map(\.kind).contains(.photo) ?? false {
					kind = .instagram(post)
				} else {
					kind = .unknown
				}
			default:
				kind = .unknown
		}
	}

	init(kind: Kind, timestamp: Date) {
		self.kind = kind
		self.timestamp = timestamp
	}

	var isUnknown: Bool {
		if case .unknown = kind {
			return true
		}
		return false
	}
}

extension FeedItem {
	struct SavedPlace: Decodable, Hashable {
		let user: User
		let place: Place

		enum CodingKeys: String, CodingKey {
			case users
			case place
		}

		init(user: User, place: Place) {
			self.user = user
			self.place = place
		}

		init(from decoder: Decoder) throws {
			let container = try decoder.container(keyedBy: CodingKeys.self)
			user = try container.decode([User].self, forKey: .users).first!
			place = try container.decode(Place.self, forKey: .place)
		}
	}

	struct FacebookPost: Decodable, Hashable {
		let createdAt: Date
		let message: String?
		let url: URL?
		let user: User
		let attachments: [Attachment]?

		struct User: Decodable, Hashable {
			let name: String
			let username: String
			let avatarURL: URL
		}

		struct Attachment: Decodable, Hashable {
			let kind: Kind
			let title: String?
			let description: String?
			let url: URL?
			let imageUrl: URL?
			let videoUrl: URL?
			let ratio: Float

			enum Kind: String, Decodable, Hashable {
				case photo
				case video
				case share
			}
		}
	}

	struct Tweet: Decodable, Hashable {
		let id: Int
		let createdAt: Date
		let text: String
		let user: User
		let media: [Media]?
		let entities: [Entity]?

		struct User: Decodable, Hashable {
			let id: Int
			let name: String
			let screenName: String
			let avatar: URL
		}

		struct Media: Decodable, Hashable {
			enum Kind: String, Decodable, Hashable {
				case photo
				case video
			}

			let ratio: Float
			let kind: Kind
			let thumbnailUrl: URL
			let url: URL
		}

		struct Entity: Decodable, Hashable {
			let startIndex: Int
			let endIndex: Int
			let kind: Kind
			let text: String

			enum Kind: String, Decodable, Hashable {
				case link
				case mention
				case hashtag
				case cashtag
			}
		}
	}

	struct InstagramPost: Decodable, Hashable {
		let id: String
		let createdAt: Date
		let text: String
		let user: User
		let media: [Media]?

		struct User: Decodable, Hashable {
			let id: String
			let fullName: String
			let username: String
			let avatar: URL
		}

		struct Media: Decodable, Hashable {
			enum Kind: String, Decodable, Hashable {
				case photo
				case video
			}

			let height: Int
			let width: Int
			let kind: Kind
			let url: URL
		}
	}
}
