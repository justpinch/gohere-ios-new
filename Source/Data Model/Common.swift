import CoreLocation
import Foundation

struct GPSLocation: Codable, Hashable {
	let latitude: CLLocationDegrees
	let longitude: CLLocationDegrees

	var coordinate: CLLocationCoordinate2D {
		CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
	}

	func prettyDistanceFromUser() -> String? {
		UserLocationManager.lastKnownLocation?.coordinate.distance(from: self.coordinate).pretty()
	}
}

struct CategoryLabel: Codable, Hashable {
	let id: String
	let name: String
	let slug: String
	let icon: ImagePath

	var localizedName: String {
		let key = "category_\(slug.lowercased())"
		let localized = key.localizedFromTarget()
		if localized == key {
			return name
		} else {
			return localized
		}
	}
}

struct PaginatedResponse<T: Decodable>: Decodable {
	let count: Int
	let results: [T]
}
