//
//  Translation.swift
//  GoHere
//
//  Created by jorisfavier on 27/07/2022.
//

import Foundation

struct Translation {
	let text: String
	let originalLanguage: String
}
