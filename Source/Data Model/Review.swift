import Foundation

struct Review: Identifiable, Decodable, Hashable {
	typealias ID = String

	let id: ID
	let place: Place
	let user: User
	let labels: [CategoryLabel]
	let images: [Image]
	let createdAt: Date
	let rating: Int
	let text: String
	let language: String

	struct Image: Decodable, Hashable {
		let id: String
		let path: ImagePath
	}

	func mainImage() -> ImagePath? {
		images.first?.path ?? place.photo
	}
	
}

struct ReviewInline: Identifiable, Codable, Hashable {
	typealias ID = String

	let id: ID
	let user: User
	let rating: Int
	let text: String
}

enum ReviewReportReason: String, CaseIterable, Identifiable {
	case spam
	case offensive
	case other

	var id: String { rawValue }
}
