import Foundation
import UIKit

struct ImagePath: Codable, Hashable {
	let path: String

	struct Size {
		let value: Int

		static let availableSizes = [60, 120, 320, 640, 750, 1280]

		static var max: Size { .init(value: availableSizes.sorted().last!) }
		static var screenWidth: Size { .init(value: UIScreen.main.bounds.width) }
		static var screenHeight: Size { .init(value: UIScreen.main.bounds.height) }

		init(value: Int) {
			self.value = value
		}

		init(value: CGFloat) {
			self.value = Int(value)
		}
	}

	init(path: String) {
		self.path = path
	}

	init(from decoder: Decoder) throws {
		let container = try decoder.singleValueContainer()
		path = try container.decode(String.self)
	}

	func encode(to encoder: Encoder) throws {
		var container = encoder.singleValueContainer()
		try container.encode(path)
	}

	func url(for size: CGFloat) -> URL {
		url(for: Int(size))
	}

	func url(for size: Size) -> URL {
		url(for: size.value)
	}

	func url(for size: Int) -> URL {
		#if DEBUG
			if isFixture {
				let imageName = String(path.split(separator: "-")[1])
				return (Bundle.main.url(forResource: imageName, withExtension: "jpg") ?? Bundle.main.url(forResource: imageName, withExtension: "png"))!
			}
		#endif

		let selectedSize = Size.availableSizes
			.sorted(by: { l, r -> Bool in
				let d1 = abs(l - size * Int(UIScreen.main.scale))
				let d2 = abs(r - size * Int(UIScreen.main.scale))
				return d1 < d2
			})
			.first!

		return constructUrl(for: selectedSize)
	}

	private func constructUrl(for size: Int) -> URL {
		#if LOCAL
			let url = API.Environment.current.CDNURL
				.appendingPathComponent("/media/")
				.appendingPathComponent(path)
		#else
			let url = API.Environment.current.CDNURL
				.appendingPathComponent("/media/")
				.appendingPathComponent("\(size)")
				.appendingPathComponent("/")
				.appendingPathComponent(path)
		#endif

		return url
	}

	func originalSizeURL() -> URL {
		#if DEBUG
			if isFixture {
				let imageName = String(path.split(separator: "-")[1])
				return (Bundle.main.url(forResource: imageName, withExtension: "jpg") ?? Bundle.main.url(forResource: imageName, withExtension: "png"))!
			}
		#endif

		let url = API.Environment.current.CDNURL
			.appendingPathComponent("/media/")
			.appendingPathComponent("original/")
			.appendingPathComponent(path)
		return url
	}
}
