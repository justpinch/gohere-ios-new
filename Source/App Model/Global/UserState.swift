import Combine
import Foundation
import SwiftUI

final class UserState: ObservableObject {
	@ReadOnlyValue var userID: User.ID?
	@Published var user: ModelState<UserExtended> = .none

	@Published var savedPlaceStatuses: [Place.ID: Bool] = [:]

	@Published var connectionStatuses: [User.ID: ModelState<UserConnection.Status>] = [:]
	@Published var connectionRequests: [UserConnection] = []

	var isAuthenticated: Bool { userID != nil }

	private var tokenObserver: AnyCancellable?

	init(authToken: CurrentValueSubject<AuthToken?, Never>) {
		self._userID = ReadOnlyValue(subject: authToken, keyPath: \.userId)

		self.tokenObserver = authToken
			.removeDuplicates()
			.sink(receiveValue: { [unowned self] _ in
				self.objectWillChange.send()
			})
	}
}
