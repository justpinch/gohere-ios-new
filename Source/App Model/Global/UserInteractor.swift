import Combine
import Foundation

protocol UserInteractor {
	var connections: UserConnectionsInteractor? { get }
	var savedPlaces: UserSavedPlacesInteractor? { get }

	func refreshProfile()

	func authenticate(email: String, password: String) -> ApiResponsePublisher<Void>
	func authenticate(phoneNumber: String, tvpToken: String) -> ApiResponsePublisher<Void>
	func authenticateWithApple() -> ApiResponsePublisher<Void>
	func authenticateWithFacebook() -> ApiResponsePublisher<Void>
	func authenticate(with publisher: ApiResponsePublisher<AuthToken>) -> ApiResponsePublisher<Void>
	func delete() -> ApiResponsePublisher<Void>
	func signOut()
}

final class DefaultUserInteractor: UserInteractor {
	typealias FacebookAuth = (AuthService) -> ApiResponsePublisher<AuthToken>

	private(set) var connections: UserConnectionsInteractor?
	private(set) var savedPlaces: UserSavedPlacesInteractor?

	private let state: UserState

	private let authToken: CurrentValueSubject<AuthToken?, Never>
	private let authService: AuthService
	private let userService: UserService
	private let placeService: PlaceService
	private let appleSignInCoordinator: AppleSignInCoordinator
	private let facebookAuth: FacebookAuth

	private var refreshProfileCanceller: AnyCancellable?
	private var cancellables = Set<AnyCancellable>()

	init(state: UserState,
	     authToken: CurrentValueSubject<AuthToken?, Never>,
	     authService: AuthService,
	     userService: UserService,
	     placeService: PlaceService,
	     appleSignInCoordinator: AppleSignInCoordinator,
	     facebookAuth: @escaping FacebookAuth)
	{
		self.state = state

		self.authToken = authToken
		self.authService = authService
		self.userService = userService
		self.placeService = placeService
		self.appleSignInCoordinator = appleSignInCoordinator
		self.facebookAuth = facebookAuth

		self.bindObservables()
	}

	private func bindObservables() {
		authToken
			.removeDuplicates()
			.sink(receiveValue: { [unowned self] token in self.onTokenChange(token) })
			.store(in: &cancellables)

		authToken
			.removeDuplicates()
			.sink(receiveValue: { [unowned self] _ in
				self.refreshProfile()
			})
			.store(in: &cancellables)
	}

	private func onTokenChange(_ token: AuthToken?) {
		guard let token = token else {
			connections = nil
			savedPlaces = nil
			state.user = .none
			state.connectionStatuses.removeAll()
			state.savedPlaceStatuses.removeAll()

			Analytics.User.reset()
			return
		}

		connections = DefaultUserConnectionsInteractor(state: state, user: token.userId, userService: userService)
		savedPlaces = DefaultUserSavedPlacesInteractor(user: token.userId, state: state, placeService: placeService)

		// update connections
		connections?.refreshConnectionRequests()

		Analytics.User.identify(id: token.userId)
		Analytics.User.registerUserProperties()
	}

	func refreshProfile() {
		refreshProfileCanceller = fetchProfile(with: authToken.value)
			.handleEvents(receiveOutput: { modelState in
				if let model = modelState.model {
					Analytics.User.set(.privateProfile, value: model.profile.isPrivate)
					Analytics.User.set(.ownReviewCount, value: model.reviewCount)
					Analytics.User.set(.favoritesCount, value: model.favoriteCount)
					if let count = model.followersCount {
						Analytics.User.set(.followerCount, value: count)
					}
					if let count = model.followingCount {
						Analytics.User.set(.followingCount, value: count)
					}
				}
			})
			.assign(to: \.state.user, onUnowned: self)
	}

	private func fetchProfile(with token: AuthToken?) -> AnyPublisher<ModelState<UserExtended>, Never> {
		guard let token = token else {
			return Just(.none).eraseToAnyPublisher()
		}

		var initialPublisher = Empty(outputType: ModelState<UserExtended>.self, failureType: API.Error.self).eraseToAnyPublisher()

		if token.userId != state.userID {
			// in case a different user is being loaded, starting with the loading state
			initialPublisher = Just(.loading).setFailureType(to: API.Error.self).eraseToAnyPublisher()
		}

		let userService = self.userService
		return initialPublisher
			.merge(with: userService.profile(id: token.userId).map(ModelState.model))
			.catch { Just(.error($0)) }
			.eraseToAnyPublisher()
	}

	func authenticate(email: String, password: String) -> ApiResponsePublisher<Void> {
		Analytics.log(.login(kind: .credentials))
		return authenticate(with: authService.authenticate(email: email, password: password))
	}

	func authenticate(phoneNumber: String, tvpToken: String) -> ApiResponsePublisher<Void> {
		Analytics.log(.login(kind: .phone))
		return authenticate(with: authService.authenticate(phoneNumber: phoneNumber, tvpToken: tvpToken))
	}

	func authenticateWithApple() -> ApiResponsePublisher<Void> {
		Analytics.log(.login(kind: .apple))
		return authenticate(with: appleSignInCoordinator.signIn())
	}

	func authenticateWithFacebook() -> ApiResponsePublisher<Void> {
		Analytics.log(.login(kind: .facebook))
		return authenticate(with: facebookAuth(authService))
	}

	func signOut() {
		authToken.value = nil
	}

	func authenticate(with publisher: ApiResponsePublisher<AuthToken>) -> ApiResponsePublisher<Void> {
		publisher
			.handleEvents(receiveOutput: { [weak self] token in
				self?.authToken.value = token
			})
			.map { _ in }
			.eraseToAnyPublisher()
	}
	
	func delete() -> ApiResponsePublisher<Void> {
		guard let userId = authToken.value?.userId else {
			return Fail(error: API.Error(reason: API.AuthenticationError.missingToken)).eraseToAnyPublisher()
		}
		return userService.deleteProfile(id: userId)
			.eraseToAnyPublisher()
		
	}
}
