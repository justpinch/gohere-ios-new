import Combine
import Foundation

protocol UserConnectionsInteractor {
	func follow(id: User.ID, isPrivate: Bool)
	func unfollow(id: User.ID)
	func updateStatus(for id: User.ID)
	func updateStatuses(for ids: [User.ID])
	func updateStatuses(for ids: [User.ID], force: Bool)

	func refreshConnectionRequests()

	func acceptFollowRequest(for connection: UserConnection)
	func blockFollowRequest(for connection: UserConnection)
	func ignoreFollowRequest(for connection: UserConnection)
}

final class DefaultUserConnectionsInteractor: UserConnectionsInteractor {
	private let state: UserState
	private let user: User.ID
	private let userService: UserService

	init(state: UserState, user: User.ID, userService: UserService) {
		self.state = state
		self.user = user
		self.userService = userService
	}

	// MARK: - Connection actions

	func follow(id: User.ID, isPrivate: Bool = false) {
		// assume happy flow
		increase(\.followingCount)
		state.connectionStatuses[id] = .model(isPrivate ? .pending : .active)
		Analytics.User.increment(.followingCount, value: 1)

		userService
			.follow(userId: id, followerId: user)
			.map(\.status)
			.replaceError(with: .active, if: { $0.statusCode == 409 })
			.handleCompletionError { [weak self] _ in
				// REVERT ALL THE THINGS
				self?.decrease(\.followingCount)
				Analytics.User.increment(.followingCount, value: -1)
				// also try fetching latest status
				self?.updateStatus(for: id)
			}
			.subscribeIgnoringEvents()
	}

	func unfollow(id: User.ID) {
		// assume happy flow
		decrease(\.followingCount)
		state.connectionStatuses[id] = .model(.none)
		Analytics.User.increment(.followingCount, value: -1)

		userService
			.unfollow(userId: id, followerId: user)
			.handleCompletionError { [weak self] _ in
				// REVERT ALL THE THINGS
				self?.increase(\.followingCount)
				Analytics.User.increment(.followingCount, value: 1)
				// also try fetching latest status
				self?.updateStatus(for: id)
			}
			.subscribeIgnoringEvents()
	}

	private func increase(_ keyPath: ReferenceWritableKeyPath<UserExtended, Int?>) {
		guard let user = state.user.model, let value = user[keyPath: keyPath] else {
			return
		}
		user[keyPath: keyPath] = value + 1
		state.user = .model(user)
	}

	private func decrease(_ keyPath: ReferenceWritableKeyPath<UserExtended, Int?>) {
		guard let user = state.user.model, let value = user[keyPath: keyPath] else {
			return
		}
		user[keyPath: keyPath] = value - 1
		state.user = .model(user)
	}

	// MARK: - Connection Status

	func updateStatus(for id: User.ID) {
		userService
			.connectionStatus(from: user, to: id)
			.map { $0?.status }
			.replaceNil(with: .none)
			.mapModelState()
			.subscribe(
				Subscribers.Sink(
					receiveCompletion: { _ in },
					receiveValue: { [weak self] status in
						self?.state.connectionStatuses[id] = status
					}
				)
			)
	}

	func updateStatuses(for ids: [User.ID]) {
		updateStatuses(for: ids, force: false)
	}

	func updateStatuses(for ids: [User.ID], force: Bool) {
		// if force == false, update only statuses that are missing
		let ids = force ? ids : ids.filter { state.connectionStatuses[$0] == nil }

		guard !ids.isEmpty else {
			return
		}

		userService
			.connectionStatuses(for: ids, from: user)
			.mapModelState()
			.map { batchStatus -> [User.ID: ModelState<UserConnection.Status>] in
				// unpack ModelState<BatchUserConnectionStatus> into dict with separate
				// model states per user id
				switch batchStatus {
					case .none:
						return ids.reduce(into: [:]) { acc, uid in acc[uid] = ModelState<UserConnection.Status>.none }
					case .loading:
						return ids.reduce(into: [:]) { acc, uid in acc[uid] = .loading }
					case let .model(statuses):
						let statusesDict = statuses.reduce(into: [:]) { $0[$1.userId] = $1.followingStatus }
						return ids.reduce(into: [:]) { acc, uid in acc[uid] = .model(statusesDict[uid] ?? .none) }
					case let .error(error):
						return ids.reduce(into: [:]) { acc, uid in acc[uid] = .error(error) }
				}
			}
			.subscribe(
				Subscribers.Sink(
					receiveCompletion: { _ in },
					receiveValue: { [weak self] statuses in
						self?.state.connectionStatuses.merge(statuses, uniquingKeysWith: { _, new in new })
					}
				)
			)
	}

	// MARK: - Connection Requests

	func refreshConnectionRequests() {
		let state = self.state

		userService
			.connections(for: user, kind: .follower, status: .pending, offset: 0, limit: 20)
			.map(\.results)
			.replaceError(with: state.connectionRequests)
			.subscribe(Subscribers.Sink(receiveCompletion: { _ in }, receiveValue: { connections in
				state.connectionRequests = connections
			}))
	}

	func acceptFollowRequest(for connection: UserConnection) {
		setIncomingFollowRequest(status: .active, for: connection)
	}

	func blockFollowRequest(for connection: UserConnection) {
		setIncomingFollowRequest(status: .blocked, for: connection)
	}

	func ignoreFollowRequest(for connection: UserConnection) {
		setIncomingFollowRequest(status: .ignored, for: connection)
	}

	private func setIncomingFollowRequest(status: UserConnection.Status, for connection: UserConnection) {
		// assume happy flow
		state.connectionRequests.removeAll(where: { $0 == connection })
		if status == .active {
			// we added a follower
			increase(\.followersCount)
			Analytics.User.increment(.followerCount, value: 1)
		}

		let sourceID = connection.user.id
		let targetID = user

		userService
			.setConnectionStatus(status: status, from: sourceID, to: targetID)
			.handleCompletionError { [weak self] _ in
				// REVERT ALL THE THINGS
				self?.state.connectionRequests.append(connection)
				if status == .active {
					self?.decrease(\.followersCount)
					Analytics.User.increment(.followerCount, value: -1)
				}
				// also try fetching latest state
				self?.refreshConnectionRequests()
			}
			.subscribeIgnoringEvents()
	}
}
