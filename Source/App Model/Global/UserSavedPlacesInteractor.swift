import Combine
import Foundation

protocol UserSavedPlacesInteractor {
	func add(place: Place)
	func remove(place: Place)
	func refreshStatus(for place: Place)
	func refreshStatus(for places: [Place])
	func setStatus(for id: Place.ID, isSaved: Bool)
}

final class DefaultUserSavedPlacesInteractor: UserSavedPlacesInteractor {
	private let user: User.ID
	private let state: UserState

	private let placeService: PlaceService
	private let notificationCenter: NotificationCenterProtocol

	private var refreshPublisher: ApiResponsePublisher<LoadingState>?

	init(user: User.ID, state: UserState, placeService: PlaceService, notificationCenter: NotificationCenterProtocol = NotificationCenter.default) {
		self.user = user
		self.state = state
		self.placeService = placeService
		self.notificationCenter = notificationCenter
	}

	func add(place: Place) {
		// assume happy flow
		state.savedPlaceStatuses[place.id] = true
		notificationCenter.post(name: Place.Notification.placeSaved, object: self, userInfo: [Place.Notification.Key.place: place])
		Analytics.User.increment(.favoritesCount, value: 1)
		Analytics.log(.favoritePlaceAdded(place: place))

		placeService
			.favorite(id: place.id)
			.replaceError(with: (), if: { $0.statusCode == 409 })
			.handleCompletionError { [weak self] _ in
				// no happy flow :( revert
				self?.state.savedPlaceStatuses.removeValue(forKey: place.id)
				self?.notificationCenter.post(name: Place.Notification.placeUnsaved, object: self, userInfo: [Place.Notification.Key.place: place])
				Analytics.User.increment(.favoritesCount, value: -1)
			}
			.subscribeIgnoringEvents()
	}

	func remove(place: Place) {
		// assume happy flow
		let wasSaved = state.savedPlaceStatuses[place.id] == true
		state.savedPlaceStatuses[place.id] = false
		notificationCenter.post(name: Place.Notification.placeUnsaved, object: self, userInfo: [Place.Notification.Key.place: place])

		placeService
			.unfavorite(id: place.id)
			.replaceError(with: (), if: { $0.statusCode == 409 })
			.handleCompletionError { [weak self] _ in
				// no happy flow :( revert
				if wasSaved {
					self?.state.savedPlaceStatuses[place.id] = true
					self?.notificationCenter.post(name: Place.Notification.placeSaved, object: self, userInfo: [Place.Notification.Key.place: place])
					Analytics.User.increment(.favoritesCount, value: 1)
				}
			}
			.subscribeIgnoringEvents()
	}

	func refreshStatus(for place: Place) {
		refreshStatus(for: [place])
	}

	func refreshStatus(for places: [Place]) {
		placeService
			.favoritesCheck(ids: places.map(\.id))
			.map { favoriteIDs in
				places.reduce(into: [:]) { result, place in
					result[place.id] = favoriteIDs.contains(place.id)
				}
			}
			.handleEvents(receiveOutput: { [weak self] dict in
				self?.setStatus(with: dict)
			})
			.subscribeIgnoringEvents()
	}

	func setStatus(for id: Place.ID, isSaved: Bool) {
		setStatus(with: [id: isSaved])
	}

	private func setStatus(with dictionary: [Place.ID: Bool]) {
		state.savedPlaceStatuses.merge(dictionary) { $1 }
	}
}

extension Place {
	enum Notification {
		static let placeSaved = NSNotification.Name(rawValue: "GoHere.Place.Saved")
		static let placeUnsaved = NSNotification.Name(rawValue: "GoHere.Place.Unsaved")

		enum Key {
			static let place = "place"
		}
	}
}
