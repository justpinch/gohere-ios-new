import Combine
import SwiftUI

class UserSearchModel: ObservableObject {
	typealias SearchAction = (String) -> ApiResponsePublisher<PaginatedResponse<User>>
	typealias UpdateStatusAction = ([User.ID]) -> Void
	typealias SearchPublisher = (String) -> AnyPublisher<LoadingState, Never>

	@Published var query: String = ""
	@Published var results: [User] = []
	@Published var searchState: LoadingState = .none

	private let updateStatusAction: UpdateStatusAction
	private var suggestions = Set<User>()
	private var cachedRemoteUsers: Set<User> = Set()
	private let searchLimit = 10

	private var cancellables = Set<AnyCancellable>()

	init(searchAction: @escaping SearchAction, updateStatusAction: @escaping UpdateStatusAction) {
		self.updateStatusAction = updateStatusAction
		bindObservables(with: searchPublisher(from: searchAction))
	}

	func setSearchSuggestions(_ suggestions: [User]) {
		self.suggestions.formUnion(suggestions)
		self.updateVisibleResults()
	}

	private func bindObservables(with searchPublisher: @escaping SearchPublisher) {
		// Search results on query change
		$query
			.removeDuplicates()
			.handleEvents(receiveOutput: { [unowned self] in self.updateVisibleResults(query: $0) })
			.debounce(for: .milliseconds(300), scheduler: DispatchQueue.main)
			.map(searchPublisher)
			.switchToLatest()
			.assign(to: \.searchState, onUnowned: self)
			.store(in: &cancellables)
	}

	private func searchPublisher(from search: @escaping SearchAction) -> SearchPublisher {
		let action = { [unowned self] (query: String) -> AnyPublisher<LoadingState, Never> in
			guard !query.isEmpty else {
				return Just(.none).eraseToAnyPublisher()
			}

			return search(query)
				.map(\.results)
				.mapModelState()
				.handleEvents(receiveOutput: { [unowned self] result in
					if case let .model(users) = result {
						// store remote results
						self.cachedRemoteUsers.formUnion(users)

						// refresh visible results
						self.updateVisibleResults()
					}
				})
				.map { LoadingState($0) }
				.eraseToAnyPublisher()
		}
		return action
	}

	private func updateVisibleResults(query: String? = nil) {
		let query = query ?? self.query

		let results: [User]

		if query.isEmpty {
			results = Array(suggestions)
		} else {
			let previouslyVisibleUsers = self.results

			let allUsers = Set(previouslyVisibleUsers)
				.union(suggestions)
				.union(cachedRemoteUsers)

			results = filter(users: Array(allUsers), by: query, previouslyVisibleUsers: previouslyVisibleUsers)
		}

		// proactively batch update user follow statuses
		updateFollowStatuses(for: results)

		self.results = Array(results.prefix(searchLimit))
	}

	private func filter(users: [User], by query: String, previouslyVisibleUsers: [User]) -> [User] {
		let prioritized = users.compactMap { user -> (priority: Int, user: User)? in
			// Users mostly start typing search string
			// with characters from the beginning of the first/last name
			//
			// this code below will prioritize results that match from the
			// beginning of either first or last name depending on which is closer

			let priority = query.searchScore(in: user.fullName)

			guard priority > 0 else {
				return nil
			}

			return (priority: priority, user: user)
		}.sorted(by: { lhs, rhs -> Bool in
			if lhs.priority == rhs.priority {
				// if priority identical, prioritize already displayed users
				switch (previouslyVisibleUsers.firstIndex(of: lhs.user), previouslyVisibleUsers.firstIndex(of: rhs.user)) {
					case (.some, .none):
						return true
					case (.none, .some):
						return false
					case let (.some(lhi), .some(rhi)):
						return lhi < rhi
					case (.none, .none):
						// default alphabetical order
						return lhs.user.fullName < rhs.user.fullName
				}
			}
			return lhs.priority > rhs.priority
		})

		return prioritized.map(\.user)
	}

	private func updateFollowStatuses(for users: [User]) {
		if !users.isEmpty {
			updateStatusAction(users.map(\.id))
		}
	}
}
