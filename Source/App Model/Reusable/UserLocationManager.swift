import Combine
import CoreLocation
import Foundation

final class UserLocationManager: NSObject {
	enum LocationState {
		case locating(previous: CLLocation?)
		case located(CLLocation?)
		case failed(error: Error, last: CLLocation?)
	}

	private let manager: LocationManager
	private let location: PassthroughSubject<LocationState, Never>
	private let status: PassthroughSubject<LocationServicesStatus, Never>

	static var lastKnownLocation: CLLocation?

	init(manager: LocationManager = CLLocationManager()) {
		self.manager = manager
		self.location = .init()
		self.status = .init()
		super.init()

		self.manager.delegate = self
	}

	func authorizationStatus() -> AnyPublisher<LocationServicesStatus, Never> {
		let initial: Just<LocationServicesStatus>

		if !type(of: manager).locationServicesEnabled() {
			initial = Just(.disabled)
		} else {
			initial = Just(.init(status: manager.authorizationStatus))
		}

		return initial
			.merge(with: status)
			.eraseToAnyPublisher()
	}

	func updateLocation(requestPermissions: Bool = true) -> AnyPublisher<LocationState?, Never> {
		if requestPermissions, manager.authorizationStatus == .notDetermined {
			manager.requestWhenInUseAuthorization()
		}

		return authorizationStatus()
			.filter { $0 == .authorized }
			.flatMap { [weak self] _ -> AnyPublisher<LocationState?, Never> in
				guard let self = self else {
					return Just(nil).eraseToAnyPublisher()
				}

				self.manager.requestLocation()
				return Just(.locating(previous: self.manager.location))
					.merge(with: self.location)
					.map { $0 }
					.eraseToAnyPublisher()
			}
			.eraseToAnyPublisher()
	}
}

extension UserLocationManager: CLLocationManagerDelegate {
	func locationManager(_: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
		self.status.send(.init(status: status))
	}

	func locationManager(_: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		location.send(.located(locations.last))
		Self.lastKnownLocation = locations.last
	}

	func locationManager(_: CLLocationManager, didFailWithError error: Error) {
		location.send(.failed(error: error, last: manager.location))
	}
}

extension UserLocationManager.LocationState {
	var value: CLLocation? {
		switch self {
			case let .locating(previous: location):
				return location
			case let .located(location):
				return location
			case let .failed(error: _, last: location):
				return location
		}
	}

	var isLocating: Bool {
		if case .locating = self {
			return true
		}
		return false
	}

	var isLocated: Bool {
		if case .located = self {
			return true
		}
		return false
	}
}

extension UserLocationManager.LocationState: Equatable {
	static func == (lhs: Self, rhs: Self) -> Bool {
		switch (lhs, rhs) {
			case let (.locating(previous: l), .locating(previous: r)):
				return l == r
			case let (.located(l), .located(r)):
				return l == r
			case let (.failed(error: lerr, last: l), .failed(error: rerr, last: r)):
				return lerr.reflectedString == rerr.reflectedString && l == r
			default:
				return false
		}
	}
}
