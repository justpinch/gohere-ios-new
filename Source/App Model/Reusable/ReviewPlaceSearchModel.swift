import Combine
import CoreLocation

final class ReviewPlaceSearchModel: ObservableObject {
	typealias SearchAction = (_ query: String, _ location: CLLocationCoordinate2D) -> ApiResponsePublisher<[PlaceAutocomplete]>

	@Published var query: String
	@Published var results: ModelState<[PlaceAutocomplete]>

	private var userLocation: CLLocation?

	private let search: SearchAction
	private let locationManager: UserLocationManager
	private var cancellables = Set<AnyCancellable>()

	init(search: @escaping SearchAction,
	     locationManager: UserLocationManager = UserLocationManager(),
	     query: String = "",
	     results: ModelState<[PlaceAutocomplete]> = .none)
	{
		self.search = search
		self.locationManager = locationManager
		self.query = query
		self.results = results
	}

	func startObservingQuery() {
		cancellables.removeAll()

		// don't ask for permissions but do try locating them if possible
		locationManager
			.updateLocation(requestPermissions: false)
			.prefix(1)
			.map { $0?.value }
			.assign(to: \.userLocation, onUnowned: self)
			.store(in: &cancellables)

		$query
			.removeDuplicates()
			.handleEvents(receiveOutput: { [unowned self] in if $0.isEmpty { self.results = .none }})
			.filter { !$0.isEmpty }
			.debounce(for: .milliseconds(300), scheduler: DispatchQueue.main)
			.map { [unowned self] in
				self.search($0, self.userLocationCoordinates())
					.map { $0.filter(\.isPOI) } // TODO: change API to return only POI
					.mapModelState()
			}
			.switchToLatest()
			.assign(to: \.results, onUnowned: self)
			.store(in: &cancellables)
	}

	private func userLocationCoordinates() -> CLLocationCoordinate2D {
		userLocation?.coordinate ?? .approximateUserCoordinates
	}
}
