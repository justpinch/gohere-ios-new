import Combine
import Contacts
import Foundation

import typealias CommonCrypto.CC_LONG
import func CommonCrypto.CC_SHA256
import var CommonCrypto.CC_SHA256_DIGEST_LENGTH

final class FriendSuggestionsModel: ObservableObject {
	@Published var isFacebookConnected: Bool = false
	@Published var isAddressBookConnected: Bool = false

	@Published var suggestions: ModelState<[User]> = .none
	@Published var contacts: [Contact] = []

	private let userChangedPublisher: AnyPublisher<Void, Never>
	private let contactStore: ContactStore
	private let facebook: Facebook
	private let userService: UserService

	private var cancellables = Set<AnyCancellable>()
	private var fetchFriendsCanceller: AnyCancellable?
	private var getContactsCanceller: AnyCancellable?

	init(userChanged: AnyPublisher<Void, Never>, userService: UserService, contactStore: ContactStore = CNContactStore(), facebook: Facebook = FacebookManager()) {
		self.userChangedPublisher = userChanged
		self.contactStore = contactStore
		self.facebook = facebook
		self.userService = userService

		self.isAddressBookConnected = contactStore.isAuthorized
		self.isFacebookConnected = facebook.isAuthenticated
	}

	func bindObservables() {
		guard cancellables.isEmpty else {
			return
		}

		userChangedPublisher
			.sink { [unowned self] in
				self.suggestions = .none
				self.fetchFriends()
				self.updateContacts()
			}
			.store(in: &cancellables)
	}

	func connectAddressBook(completion: @escaping (Swift.Error?) -> Void) {
		guard !isAddressBookConnected else {
			completion(nil); return
		}
		guard contactStore.authorizationStatus == .notDetermined else {
			completion(ContactStoreError.unauthorized); return
		}

		contactStore
			.requestAccess()
			.receive(on: RunLoop.main)
			.subscribe(
				Subscribers.Sink(
					receiveCompletion: { result in
						if case let .failure(error) = result {
							completion(error)
						}
					}, receiveValue: { [weak self] result in
						self?.isAddressBookConnected = result
						self?.fetchFriends()
						self?.updateContacts()
						completion(nil)
					}
				)
			)
	}

	func connectFacebook(completion: @escaping (Swift.Error?) -> Void) {
		updateFacebookStatus()

		guard !isFacebookConnected else {
			completion(nil); return
		}

		facebook
			.authenticate()
			.receive(on: RunLoop.main)
			.subscribe(
				Subscribers.Sink(
					receiveCompletion: { result in
						if case let .failure(error) = result {
							completion(error)
						}
					}, receiveValue: { [weak self] _ in
						self?.updateFacebookStatus()
						self?.fetchFriends()
						completion(nil)
					}
				)
			)
	}

	private func updateFacebookStatus() {
		isFacebookConnected = facebook.isAuthenticated
	}

	private func fetchFriends() {
		fetchFriendsCanceller?.cancel()

		let facebook: Just<String?> = .init(self.facebook.token)
		let emails = contactStore.getEmails().map(saltedSHA256).replaceError(with: [])
		let phoneNumbers = contactStore.getPhoneNumbers().map(saltedSHA256).replaceError(with: [])

		let userService = self.userService

		fetchFriendsCanceller = Publishers.Zip3(facebook, emails, phoneNumbers)
			.subscribe(on: DispatchQueue.global())
			.flatMap { result in
				userService.suggestFriends(facebookToken: result.0, emailHashes: result.1, phoneNumberHashes: result.2)
			}
			.mapModelState()
			.receive(on: RunLoop.main)
			.sink(receiveValue: { [unowned self] suggestions in
				self.suggestions = suggestions
			})
	}

	private func updateContacts() {
		getContactsCanceller = contactStore
			.getContacts()
			.replaceError(with: [])
			.receive(on: RunLoop.main)
			.assign(to: \.contacts, onUnowned: self)
	}
}

private func saltedSHA256(strings: [String]) -> [String] {
	let salt = "lCv8k2d9BpE7"

	return strings.map { string in
		let data = "\(salt)\(string)\(salt)".data(using: .utf8)!

		var hash = [UInt8](repeating: 0, count: Int(CC_SHA256_DIGEST_LENGTH))
		data.withUnsafeBytes {
			_ = CC_SHA256($0.baseAddress, CC_LONG(data.count), &hash)
		}

		return Data(hash).map { String(format: "%02hhx", $0) }.joined()
	}
}
