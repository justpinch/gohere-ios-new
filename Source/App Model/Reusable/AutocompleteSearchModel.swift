import Combine
import CoreLocation
import Foundation
import SwiftUI

class AutocompleteSearchModel: ObservableObject {
	typealias SearchParams = (String, CLLocationCoordinate2D)
	typealias Loader = (SearchParams) -> ApiResponsePublisher<[PlaceAutocomplete]>

	@Published var searchParams: SearchParams?
	@Published private(set) var results: ModelState<[PlaceAutocomplete]> = .none

	private let loader: Loader
	private var cancellables = Set<AnyCancellable>()

	init(loader: @escaping Loader) {
		self.loader = loader
		self.bindQuery()
	}

	private func bindQuery() {
		$searchParams
			.debounce(for: .milliseconds(300), scheduler: RunLoop.main)
			.map { [unowned self] params -> AnyPublisher<ModelState<[PlaceAutocomplete]>, Never> in
				if let params = params, !params.0.gh_trimmed().isEmpty {
					return self.loader(params).mapModelState()
				} else {
					return Just(.none).eraseToAnyPublisher()
				}
			}
			.switchToLatest()
			.assign(to: \.results, onUnowned: self)
			.store(in: &cancellables)
	}
}
