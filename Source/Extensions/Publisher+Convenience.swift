import Combine
import Foundation
import SwiftUI

extension Publisher {
	/// Subscribes to the Publisher using Sink and ignores all events
	func subscribeIgnoringEvents() {
		subscribe(Subscribers.Sink(receiveCompletion: { _ in }, receiveValue: { _ in }))
	}

	func handleCompletionSuccess(_ handler: @escaping () -> Void) -> Publishers.HandleEvents<Self> {
		handleEvents(receiveCompletion: { completion in
			if case .finished = completion {
				handler()
			}
		})
	}

	func handleCompletionError(_ handler: @escaping (Failure) -> Void) -> Publishers.HandleEvents<Self> {
		handleEvents(receiveCompletion: { completion in
			if case let .failure(error) = completion {
				handler(error)
			}
		})
	}

	/// Includes the current element as well as the previous element from the upstream publisher in a tuple where the previous element is optional.
	/// The first time the upstream publisher emits an element, the previous element will be `nil`.
	///
	///     let range = (1...5)
	///     cancellable = range.publisher
	///         .withPrevious()
	///         .sink { print ("(\($0.previous), \($0.current))", terminator: " ") }
	///      // Prints: "(nil, 1) (Optional(1), 2) (Optional(2), 3) (Optional(3), 4) (Optional(4), 5) ".
	///
	/// - Returns: A publisher of a tuple of the previous and current elements from the upstream publisher.
	func withPrevious() -> AnyPublisher<(previous: Output?, current: Output), Failure> {
		scan((Output?, Output)?.none) { ($0?.1, $1) }
			.compactMap { $0 }
			.eraseToAnyPublisher()
	}
}

extension Publisher where Failure == Never {
	/// WORKAROUND: For reference cycles caused by assign(to:on:) strongly capturing the target
	/// when the cancellable is also strongly referenced by that same target.
	/// SEE: https://forums.swift.org/t/does-assign-to-produce-memory-leaks/29546
	///
	func assign<Root: AnyObject>(to keyPath: ReferenceWritableKeyPath<Root, Output>, onUnowned root: Root) -> AnyCancellable {
		sink { [unowned root] in
			root[keyPath: keyPath] = $0
		}
	}
}

extension Publisher where Output == Void, Failure == Never {
	func sink() -> AnyCancellable {
		sink(receiveValue: { _ in })
	}
}

// MARK: - Error replacing

extension Publisher where Failure: Error {
	func replaceError(with output: Output, if condition: @escaping (Failure) -> Bool) -> Publishers.Catch<Self, AnyPublisher<Self.Output, Self.Failure>> {
		self.catch { error -> AnyPublisher<Output, Failure> in
			if condition(error) {
				return Just(output).setFailureType(to: Failure.self).eraseToAnyPublisher()
			} else {
				return Fail(error: error).eraseToAnyPublisher()
			}
		}
	}
}

// MARK: - Error logging

extension Publisher where Failure: Error {
	func logError() -> Publishers.HandleEvents<Self> {
		handleEvents(receiveCompletion: { completion in
			if case let .failure(error) = completion {
				error.log()
			}
		})
	}

	func reportError() -> Publishers.HandleEvents<Self> {
		handleEvents(receiveCompletion: { completion in
			if case let .failure(error) = completion {
				error.report()
			}
		})
	}
}

// MARK: - Loading State

class LoadCanceller {
	fileprivate var cancellable: AnyCancellable?

	func cancel() {
		cancellable?.cancel()
	}
}

extension AnyCancellable {
	func store(in canceller: LoadCanceller) {
		canceller.cancellable = self
	}
}

extension Publisher where Failure == API.Error {
	func assignLoadingState(binding: Binding<LoadingState>) {
		mapLoadingState()
			.subscribe(Subscribers.Sink(receiveCompletion: { _ in },
			                            receiveValue: { binding.wrappedValue = $0 }))
	}

	func mapLoadingState() -> AnyPublisher<LoadingState, Never> {
		Just(LoadingState.loading)
			.merge(with:
				self
					.map { _ in LoadingState.finished }
					.catch { Just(.error($0)) }
					.eraseToAnyPublisher()
			)
			.eraseToAnyPublisher()
	}

	func mapModelState(current: Output? = nil) -> AnyPublisher<ModelState<Output>, Never> {
		Just(ModelState.loading(previous: current))
			.merge(with:
				self
					.map { ModelState.model($0) }
					.catch { Just(.error($0)) }
					.eraseToAnyPublisher()
			)
			.eraseToAnyPublisher()
	}
}
