import Combine
import Foundation
import Sentry

extension URLResponse {
	func sentryContext() -> [String: String] {
		let response = (self as? HTTPURLResponse)

		return [
			"kind": "response",
			"request_id": (response?.allHeaderFields["x-request-id"] as? String) ?? (response?.allHeaderFields["X-Request-Id"] as? String) ?? "",
			"status_code": response?.statusCode.description ?? "unknown",
		]
	}
}

extension URLRequest {
	func sentryContext() -> [String: String] {
		var endpoint = (httpMethod ?? "?") + " " + (url?.path ?? "")

		// mangle Sentry sensitive keywords (we don't put sensitive info in urls..)
		endpoint = endpoint
			.replacingOccurrences(of: "auth", with: "4uth")
			.replacingOccurrences(of: "token", with: "t0k3n")
			.replacingOccurrences(of: "user", with: "us3r")

		return [
			"kind": "request",
			"endpoint": endpoint,
			"has_access_teken": allHTTPHeaderFields?["Authorization"] != nil ? "YES" : "NO",
		]
	}
}

extension Publisher where Self.Output == URLRequest {
	func addSentryBreadcrumb() -> Publishers.HandleEvents<Self> {
		handleEvents(receiveOutput: { output in
			let crumb = Breadcrumb(level: .info, category: "networking")
			crumb.type = "http"
			crumb.data = output.sentryContext()
			SentrySDK.addBreadcrumb(crumb: crumb)
		})
	}
}

extension Publisher where Self.Output == NetworkDataOutput {
	func addSentryBreadcrumb() -> Publishers.HandleEvents<Self> {
		handleEvents(receiveOutput: { output in
			let crumb = Breadcrumb(level: .info, category: "networking")
			crumb.type = "http"

			crumb.data = output.response.sentryContext()

			switch (output.response as? HTTPURLResponse)?.statusCode {
				case .some(200 ..< 300):
					crumb.level = .warning
				default:
					break
			}

			SentrySDK.addBreadcrumb(crumb: crumb)
		})
	}
}
