import Foundation

extension Error {
	var reflectedString: String {
		String(reflecting: self)
	}

	func isEqual(to: Self) -> Bool {
		self.reflectedString == to.reflectedString
	}
}

extension NSError {
	// prevents scenario where one would cast swift Error to NSError
	// whereby losing the associatedvalue in Obj-C realm.
	// (IntError.unknown as NSError("some")).(IntError.unknown as NSError)
	func isEqual(to: NSError) -> Bool {
		let lhs = self as Error
		let rhs = to as Error
		return self.isEqual(to) && lhs.reflectedString == rhs.reflectedString
	}
}
