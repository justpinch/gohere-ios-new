import CoreLocation
import Foundation

extension TimeZone {
	static let approximateCoordinates = getApproximateCoordinates(for: TimeZone.current)

	private struct Coords: Decodable {
		let lat: Double
		let lng: Double
	}

	private static func getApproximateCoordinates(for timeZone: TimeZone) -> CLLocationCoordinate2D? {
		guard let url = Bundle.main.url(forResource: "timezones", withExtension: "json"),
		      let data = try? Data(contentsOf: url),
		      let table = try? JSONDecoder().decode([String: Coords].self, from: data)
		else {
			assertionFailure()
			return nil
		}

		let id = timeZone.identifier
		guard let coords = table[id] else {
			return nil
		}

		return CLLocationCoordinate2D(latitude: coords.lat, longitude: coords.lng)
	}
}
