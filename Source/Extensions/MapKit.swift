import CoreLocation
import MapKit

private extension CLLocationDegrees {
	enum WrapingDimension: Double {
		case latitude = 180
		case longitude = 360
	}

	/// Standardises and angle to [-180 to 180] or [-90 to 90] degrees
	func wrapped(dimension: WrapingDimension) -> CLLocationDegrees {
		let length = dimension.rawValue
		let halfLenght = length / 2.0
		let angle = self.truncatingRemainder(dividingBy: length)
		switch dimension {
			case .longitude:
				//        return angle < -180.0 ? 360.0 + angle : angle > 180.0 ? -360.0 + angle : angle
				return angle < -halfLenght ? length + angle : angle > halfLenght ? -length + angle : angle
			case .latitude:
				//        return angle < -90.0 ? -180.0 - angle : angle > 90.0 ? 180.0 - angle : angle
				return angle < -halfLenght ? -length - angle : angle > halfLenght ? length - angle : angle
		}
	}
}

extension CLLocationDistance {
	static var prettyFormatter: MeasurementFormatter = {
		let formatter = MeasurementFormatter()
		formatter.unitStyle = .medium
		formatter.unitOptions = .naturalScale
		formatter.numberFormatter.usesSignificantDigits = true
		formatter.numberFormatter.maximumSignificantDigits = 3
		return formatter
	}()

	func pretty() -> String? {
		Self.prettyFormatter.string(for: Measurement<UnitLength>(value: self, unit: .meters))
	}
}

extension CLLocationCoordinate2D {
	/// Returns the distance
	func distance(from coordinate: CLLocationCoordinate2D) -> CLLocationDistance {
		let locationOther = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
		let locationSelf = CLLocation(latitude: latitude, longitude: longitude)
		return locationSelf.distance(from: locationOther)
	}
}

extension MKCoordinateRegion {
	/// Returns a distance from center coordinate of another region
	func centerDistance(from region: MKCoordinateRegion) -> CLLocationDistance {
		center.distance(from: region.center)
	}

	/// confirms that a region contains a location
	func contains(_ coordinate: CLLocationCoordinate2D) -> Bool {
		let deltaLat = abs((self.center.latitude - coordinate.latitude).wrapped(dimension: .latitude))
		let deltalong = abs((self.center.longitude - coordinate.longitude).wrapped(dimension: .longitude))
		return self.span.latitudeDelta / 2.0 >= deltaLat && self.span.longitudeDelta / 2.0 >= deltalong
	}

	/// confirms that a region contains another region
	func contains(_ region: MKCoordinateRegion) -> Bool {
		guard self.contains(region.center) else {
			return false
		}

		let latMin = (region.center.latitude - region.span.latitudeDelta / 2).wrapped(dimension: .latitude)
		let latMax = (region.center.latitude + region.span.latitudeDelta / 2).wrapped(dimension: .latitude)
		let lngMin = (region.center.longitude - region.span.longitudeDelta / 2).wrapped(dimension: .longitude)
		let lngMax = (region.center.longitude + region.span.longitudeDelta / 2).wrapped(dimension: .longitude)

		let coords = [
			CLLocationCoordinate2D(latitude: latMin, longitude: lngMin),
			CLLocationCoordinate2D(latitude: latMin, longitude: lngMax),
			CLLocationCoordinate2D(latitude: latMax, longitude: lngMin),
			CLLocationCoordinate2D(latitude: latMax, longitude: lngMax),
		]

		for c in coords {
			if !self.contains(c) {
				return false
			}
		}

		return true
	}

	/// Converts (approx.) span delta from degrees to meters
	func spanInMeters() -> (latitudeMeters: Double, longitudeMeters: Double) {
		let latitudeCircumference = 40_075_160 * cos(center.latitude * .pi / 180)
		return (span.latitudeDelta * 40_008_000 / 360,
		        span.longitudeDelta * latitudeCircumference / 360)
	}

	/// Calculates (approx.) change/movement of the new region in meters
	func spanDifferenceInMeters(to region: MKCoordinateRegion) -> (latMeters: Double, lngMeters: Double) {
		let lat1 = region.center.latitude + region.span.latitudeDelta
		let lng1 = region.center.longitude + region.span.longitudeDelta

		let lat2 = center.latitude + span.latitudeDelta
		let lng2 = center.longitude + span.longitudeDelta

		let latDelta = (lat1 - lat2).wrapped(dimension: .latitude)
		let lngDelta = (lng1 - lng2).wrapped(dimension: .longitude)

		let latitudeCircumference = 40_075_160 * cos(center.latitude * .pi / 180)
		return (latDelta * 40_008_000 / 360,
		        lngDelta * latitudeCircumference / 360)
	}
}

extension MKCoordinateRegion: Equatable {
	public static func == (lhs: Self, rhs: Self) -> Bool {
		lhs.center == rhs.center &&
			lhs.span.latitudeDelta == rhs.span.latitudeDelta &&
			lhs.span.longitudeDelta == rhs.span.longitudeDelta
	}
}
