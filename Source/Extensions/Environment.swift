import Foundation
import SwiftUI

struct CustomModalPresentationKey: EnvironmentKey {
	static let defaultValue = Binding<Bool>.constant(false)
}

extension EnvironmentValues {
	var isModalPresented: Binding<Bool> {
		get { self[CustomModalPresentationKey.self] }
		set { self[CustomModalPresentationKey.self] = newValue }
	}
}
