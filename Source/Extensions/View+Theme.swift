import Foundation
import SwiftUI

extension View {
	func elevationLevel1() -> some View {
		shadow(color: Color.gh(.labelPrimary).opacity(0.15), radius: 4, x: 0, y: 1)
	}

	func elevationLevel2() -> some View {
		shadow(color: Color.gh(.labelPrimary).opacity(0.2), radius: 2, x: 0, y: 1)
	}

	func elevationLevel3() -> some View {
		shadow(color: Color.gh(.labelPrimary).opacity(0.11), radius: 12, x: 0, y: 1)
	}
}

struct ElevationLevel_Previews: PreviewProvider {
	static var previews: some View {
		Group {
			Rectangle()
				.foregroundColor(.white)
				.elevationLevel2()
				.padding()

			Rectangle()
				.foregroundColor(.white)
				.elevationLevel3()
				.padding()
		}
		.previewLayout(.fixed(width: 380, height: 160))
	}
}
