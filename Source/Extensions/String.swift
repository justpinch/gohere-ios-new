import Foundation

extension String {
	func gh_trimmed() -> String {
		self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
	}

	func removeAllNonNumericValues() -> String {
		self.replacingOccurrences(of: "[^\\d+]", with: "", options: .regularExpression, range: self.startIndex ..< self.endIndex)
	}
}

// MARK: - Localization

protocol LocalizedStringConvertible {
	var localizableString: String { get }
}

extension String {
	func localizedFromTarget() -> String {
		NSLocalizedString(self, tableName: "LocalizableTarget", bundle: Bundle.main, comment: "")
	}

	func localizedOverrideable() -> String {
		let key = self + "_override"
		let override = key.localizedFromTarget()
		guard override != key else {
			return NSLocalizedString(self, comment: "")
		}
		return override
	}
}

// MARK: - Validation

extension String {
	/// Naive email validation
	func isValidEmail() -> Bool {
		guard !self.gh_trimmed().isEmpty else {
			return false
		}

		// naive validation..
		let emailRegex = "^[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+$"
		guard self.range(of: emailRegex, options: .regularExpression) != nil else {
			return false
		}

		return true
	}
}

// MARK: - Search Scoring

extension String {
	func searchScore(in string: String) -> Int {
		let queryBits = self.components(separatedBy: CharacterSet.whitespacesAndNewlines)
		let stringBits = string.components(separatedBy: CharacterSet.whitespacesAndNewlines)

		var score = 0

		queryBits.forEach { keyword in
			stringBits.forEach { word in
				if let idx = word.range(of: keyword, options: [.diacriticInsensitive, .caseInsensitive])?.lowerBound {
					// offset is scored highly (max 100, beginning of word)
					let distance = word.distance(from: word.startIndex, to: idx)
					let offset = Int(Double(1 / (1 + distance) * 100))

					let percentCovered = Int((Double(keyword.count) / Double(word.count)) * 100)

					let bitScore = offset + percentCovered
					score += bitScore
				}
			}
		}

		return score
	}
}

extension String {
	func firstOrNull(pattern: String) -> String? {
		let range = NSRange(location: 0, length: self.utf16.count)
		let regex = try! NSRegularExpression(pattern: pattern)
		if let match = regex.firstMatch(in: self, range: range) {
			return String(self[Range(match.range, in: self)!])
		}
		return nil
	}
	
	func split(with pattern: String) -> [String] {
		let current = self.utf16
		let range = NSRange(location: 0, length: current.count)
		let regex = try! NSRegularExpression(pattern: pattern)
		let matches = regex.matches(in: self, range: range)
		var res: [String] = []
		var lastUpperBound = current.startIndex
		for match in matches {
			if match.range.lowerBound > 0 {
				let ind = current.index(current.startIndex, offsetBy: match.range.lowerBound - 1)
				res.append(String(current[lastUpperBound...ind])!)
			}
			res.append(String(current[Range(match.range, in: self)!])!)
			lastUpperBound = current.index(current.startIndex, offsetBy: min(current.count, match.range.upperBound))
		}
		return res.isEmpty ? [self] : res
	}
	
	func stripHtmlTags() -> String {
		return self.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
	}
}
