import UIKit

extension UIApplication {
	var fullApplicationVersion: String {
		let marketingVersion = (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String) ?? "?"
		let internalVersion = (Bundle.main.infoDictionary?["GHInternalVersion"] as? String) ?? "?"
		let build = (Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as? String) ?? "?"
		var environment = ""

		#if !PRODUCTION
			environment = " \(API.Environment.current) - \(API.Environment.current.baseURL.host!)"
		#endif

		return "\(marketingVersion) (\(internalVersion).\(build))\(environment)"
	}
}
