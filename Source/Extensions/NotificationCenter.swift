import Combine
import Foundation

/// @mockable
protocol NotificationCenterProtocol {
	func post(name: NSNotification.Name, object: Any?, userInfo: [AnyHashable: Any]?)
	func publisher(for name: Notification.Name, object: AnyObject?) -> AnyPublisher<Notification, Never>
}

extension NotificationCenter: NotificationCenterProtocol {
	func publisher(for name: Notification.Name, object: AnyObject?) -> AnyPublisher<Notification, Never> {
		(publisher(for: name, object: object) as NotificationCenter.Publisher).eraseToAnyPublisher()
	}
}
