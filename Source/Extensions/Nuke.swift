import Nuke
import UIKit

public extension ImageProcessors {
	/// Processed an image using a specified closure.
	struct TintOverlay: ImageProcessing {
		public var identifier: String {
			"app.gohere.ios.imageprocessing.tintoverlay.\(color.description.trimmingCharacters(in: .whitespaces))"
		}

		let color: UIColor

		public func process(_ image: PlatformImage) -> PlatformImage? {
			image.tinted(with: color)
		}
	}
}
