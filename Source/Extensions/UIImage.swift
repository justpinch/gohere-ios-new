import func AVFoundation.AVMakeRect
import UIKit

extension UIImage {
	func resizedImage(for size: CGSize) -> UIImage? {
		let targetRect = CGRect(origin: .zero, size: size)
		let rect = AVMakeRect(aspectRatio: self.size, insideRect: targetRect)

		let renderer = UIGraphicsImageRenderer(size: rect.size)
		return renderer.image { _ in
			self.draw(in: .init(origin: .zero, size: rect.size))
		}
	}

	func resizedForUpload(maxSize: CGFloat) -> UIImage {
		var scaleFactor: Double = 1
		let maxSize = maxSize / self.scale

		guard !(self.size.width < maxSize && self.size.height < maxSize) else {
			return self
		}

		if self.size.width > self.size.height || self.size.width == self.size.height {
			scaleFactor = Double(maxSize) / Double(self.size.width)
		} else if self.size.width < self.size.height {
			scaleFactor = Double(maxSize) / Double(self.size.height)
		}

		let imageSize = self.size.applying(CGAffineTransform(scaleX: CGFloat(scaleFactor), y: CGFloat(scaleFactor)))
		let hasAlpha = false
		let scale: CGFloat = 1 // Force scale 1.0

		UIGraphicsBeginImageContextWithOptions(imageSize, !hasAlpha, scale)
		self.draw(in: CGRect(origin: CGPoint.zero, size: imageSize))

		let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()

		return scaledImage!
	}

	func tinted(with tintColor: UIColor) -> UIImage? {
		UIGraphicsBeginImageContextWithOptions(size, false, scale)

		let context = UIGraphicsGetCurrentContext()!
		context.translateBy(x: 0, y: size.height)
		context.scaleBy(x: 1.0, y: -1.0)
		context.setBlendMode(.normal)

		let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
		context.clip(to: rect, mask: cgImage!)

		tintColor.setFill()
		context.fill(rect)

		let newImage = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		return newImage
	}
}
