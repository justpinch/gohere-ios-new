import Foundation
import GoogleMaps

extension GMSServices {
	static var _lock: Bool = {
		GMSServices.provideAPIKey("AIzaSyBPlvAELpt9PXYQSujbw43c8VMMJ7n9hiU")
	}()

	@discardableResult
	static func initializeSDK() -> Bool {
		_lock
	}
}
