import SwiftUI

extension View {
	@ViewBuilder
	func navigate<Value, Destination: View>(
		using binding: Binding<Value?>,
		@ViewBuilder destination: @escaping (Value) -> Destination
	) -> some View {
		let isActive = Binding(
			get: { binding.wrappedValue != nil },
			set: { newValue in if !newValue { binding.wrappedValue = nil } }
		)

		background(
			NavigationLink(destination: LazyNavView(value: binding, build: destination), isActive: isActive, label: EmptyView.init))
	}
}

private struct LazyNavView<Value, Content>: View where Content: View {
	@Binding var value: Value?
	let build: (Value) -> Content

	var body: some View {
		if let value = value {
			build(value)
		} else {
			EmptyView()
		}
	}
}
