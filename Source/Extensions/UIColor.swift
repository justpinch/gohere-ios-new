import SwiftUI
import UIKit

extension UIColor {
	convenience init(red: Int, green: Int, blue: Int, alpha: CGFloat = 1) {
		assert(red >= 0 && red <= 255, "Invalid red component")
		assert(green >= 0 && green <= 255, "Invalid green component")
		assert(blue >= 0 && blue <= 255, "Invalid blue component")

		self.init(
			red: CGFloat(red) / 255.0,
			green: CGFloat(green) / 255.0,
			blue: CGFloat(blue) / 255.0,
			alpha: alpha
		)
	}

	convenience init(rgb: Int, alpha: CGFloat = 1.0) {
		self.init(
			red: (rgb >> 16) & 0xFF,
			green: (rgb >> 8) & 0xFF,
			blue: rgb & 0xFF,
			alpha: alpha
		)
	}

	/// Overlaying color for KFImage with dynamic color doesn't respect current color scheme
	/// so we need to force resolution with specific (current) color scheme, thus this category helper.
	func forceResolve(with colorScheme: ColorScheme) -> UIColor {
		switch colorScheme {
			case .light:
				return resolvedColor(with: .init(userInterfaceStyle: .light))
			case .dark:
				return resolvedColor(with: .init(userInterfaceStyle: .dark))
			@unknown default:
				return resolvedColor(with: .init(userInterfaceStyle: .light))
		}
	}
}
