//
//  Collection.swift
//  GoHere
//
//  Created by jorisfavier on 23/03/2022.
//

import Foundation
import SwiftUI

extension Optional where Wrapped : Collection {
	var isNullOrEmpty: Bool {
		return self?.isEmpty ?? true
	}
}
