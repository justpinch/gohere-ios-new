import Combine
import CoreLocation
import Foundation

enum LocationServicesStatus {
	case unknown
	case disabled
	case authorized
	case denied

	init(status: CLAuthorizationStatus) {
		switch status {
			case .notDetermined:
				self = .unknown
			case .restricted, .denied:
				self = .denied
			case .authorizedAlways, .authorizedWhenInUse:
				self = .authorized
			@unknown default:
				assertionFailure()
				self = .unknown
		}
	}
}

/// @mockable
protocol LocationManager: AnyObject {
	static func locationServicesEnabled() -> Bool
	var authorizationStatus: CLAuthorizationStatus { get }
	var delegate: CLLocationManagerDelegate? { get set }
	var location: CLLocation? { get }
	func requestWhenInUseAuthorization()

	func requestLocation()
}

extension CLLocationManager: LocationManager {}

/// @mockable
protocol Geocoder {
	func reverseGeocode(location: CLLocation) -> AnyPublisher<[CLPlacemark], Error>
}

extension CLGeocoder: Geocoder {
	func reverseGeocode(location: CLLocation) -> AnyPublisher<[CLPlacemark], Error> {
		Deferred { [weak self] in
			Future { [weak self] promise in
				self?.reverseGeocodeLocation(location) { placemarks, error in
					if let error = error {
						promise(.failure(error))
					} else {
						promise(.success(placemarks ?? []))
					}
				}
			}
		}
		.handleEvents(receiveCancel: { [weak self] in self?.cancelGeocode() })
		.eraseToAnyPublisher()
	}
}

extension CLLocation {
	convenience init(coordinate: CLLocationCoordinate2D) {
		self.init(latitude: coordinate.latitude, longitude: coordinate.longitude)
	}
}

extension CLLocationCoordinate2D {
	static let approximateUserCoordinates = TimeZone.approximateCoordinates ?? CLLocationCoordinate2D(latitude: 53.3724, longitude: 4.89973)
	
	func movedBy(bearing: Double, distanceMeters: Double) -> CLLocationCoordinate2D {
		let distRadians = distanceMeters / (6372797.6)

		let rbearing = bearing * .pi / 180.0

		let lat1 = self.latitude * .pi / 180
		let lon1 = self.longitude * .pi / 180

		let lat2 = asin(sin(lat1) * cos(distRadians) + cos(lat1) * sin(distRadians) * cos(rbearing))
		let lon2 = lon1 + atan2(sin(rbearing) * sin(distRadians) * cos(lat1), cos(distRadians) - sin(lat1) * sin(lat2))

		return CLLocationCoordinate2D(latitude: lat2 * 180 / .pi, longitude: lon2 * 180 / .pi)
	}
}

extension CLLocationCoordinate2D: Equatable, Hashable {
	public static func == (lhs: Self, rhs: Self) -> Bool {
		lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
	}

	public func hash(into hasher: inout Hasher) {
		hasher.combine(latitude)
		hasher.combine(longitude)
	}
}
