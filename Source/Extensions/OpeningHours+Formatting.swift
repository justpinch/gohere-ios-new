import Foundation

extension OpeningHours {
	typealias DayInfo = (day: String, times: String, isToday: Bool)

	func formattedToday(locale _: Locale = .current, today _: Date = Date()) -> String {
		guard !isAlwaysOpen else {
			return NSLocalizedString("open", comment: "")
		}

		guard let info = formattedDays().first(where: \.isToday) else {
			return "opening_hours"
		}

		return "\(info.day) \(info.times)"
	}

	func formattedDays(locale: Locale = .current, today: Date = Date()) -> [DayInfo] {
		var calendar = Calendar.current
		calendar.locale = locale

		let weekdays = calendar.weekdaySymbols
		let weekStartIndex = calendar.firstWeekday - 1
		let todayIndex: Int?

		let components = calendar.dateComponents([.weekday], from: today)
		if let weekday = components.weekday {
			todayIndex = weekday - 1
		} else {
			todayIndex = nil
		}

		var hours: [Int: String] = [:]
		periods.enumerated().forEach {
			let existing = hours[$0.element.open.day] ?? ""
			let new = self.hours(for: $0.element, weekdays: weekdays)
			hours[$0.element.open.day] = existing.isEmpty ? new : "\(existing)\n\(new)"
		}

		var periods: [DayInfo] = Array(0 ..< 7).map {
			let string = hours[$0] ?? NSLocalizedString("closed", comment: "")
			let day = weekdays[$0]
			return (day: day, times: string, isToday: todayIndex == $0)
		}

		let prefix = periods.prefix(weekStartIndex)
		periods.removeFirst(weekStartIndex)
		periods.append(contentsOf: prefix)

		return periods
	}

	private func hours(for period: OpeningHours.Period, weekdays: [String]) -> String {
		guard period.open.day < weekdays.count, period.open.time.count == 4 else {
			assertionFailure()
			return ""
		}

		let hour = period.open.time.prefix(2)
		let minutes = period.open.time.suffix(2)
		let timesFrom = "\(hour):\(minutes)"

		var timesTo = "23:59"
		if let close = period.close {
			let hour = close.time.prefix(2)
			let minutes = close.time.suffix(2)
			timesTo = "\(hour):\(minutes)"
		}

		let hours = "\(timesFrom) - \(timesTo)"
		return hours
	}
}
