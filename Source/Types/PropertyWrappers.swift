import Combine
import Foundation

@propertyWrapper struct ReadOnlyValue<Value> {
	/// The current value getter.
	var wrappedValue: Value { getter() }

	/// The ``Published/projectedValue`` is the property accessed with the `$` operator.
	let projectedValue: AnyPublisher<Value, Never>

	/// Value getter
	private let getter: () -> Value

	init<Input>(subject: CurrentValueSubject<Input, Never>, transform: @escaping (Input) -> (Value)) {
		self.getter = { transform(subject.value) }
		self.projectedValue = subject.map(transform).eraseToAnyPublisher()
	}

	init<Input>(subject: CurrentValueSubject<Input, Never>, keyPath: KeyPath<Input, Value>) {
		self.getter = { subject.value[keyPath: keyPath] }
		self.projectedValue = subject.map(keyPath).eraseToAnyPublisher()
	}

	init<Input, Output>(subject: CurrentValueSubject<Input?, Never>, keyPath: KeyPath<Input, Output>) where Value == Output? {
		let transform = { (_: Input?) -> Output? in
			guard let value = subject.value else { return nil }
			return value[keyPath: keyPath]
		}
		self.init(subject: subject, transform: transform)
	}

	init(subject: CurrentValueSubject<Value, Never>) {
		self.getter = { subject.value }
		self.projectedValue = subject.eraseToAnyPublisher()
	}
}
