import Combine
import CoreLocation
import Foundation
import MapKit

final class SearchRegionCache {
	let searchRadius: CLLocationDistance
	let minimumSpanDistance: CLLocationDistance
	let maximumSpanDistance: CLLocationDistance

	/// The last searched region
	private var lastSearchRegion: MKCoordinateRegion?

	/// Optimization:
	/// When the API returns all results it has for a region, we save it here
	/// so we know there aren't any more results within it and we can skip a search if needed
	private var fullySearchedRegions: [MKCoordinateRegion] = []

	init(searchRadius: CLLocationDistance,
	     minimumSpanDistance: CLLocationDistance,
	     maximumSpanDistance: CLLocationDistance)
	{
		self.searchRadius = searchRadius
		self.minimumSpanDistance = minimumSpanDistance
		self.maximumSpanDistance = maximumSpanDistance
	}

	func clear() {
		lastSearchRegion = nil
		fullySearchedRegions.removeAll()
	}

	func setSearched(region: MKCoordinateRegion, isFullySearched: Bool = false) {
		lastSearchRegion = region

		if isFullySearched {
			// remove any regions that might be contained within this new region
			var regions = fullySearchedRegions.filter { !region.contains($0) }
			regions.append(region)
			fullySearchedRegions = regions
		}
	}

	func shouldSearch(in region: MKCoordinateRegion) -> Bool {
		// dev config sanity check
		assert(searchRadius <= maximumSpanDistance, "Search radius should be within max span")

		// YES, if there's no previous search
		guard let previousRegion = lastSearchRegion else {
			return true
		}

		// NO, if new span is larger than max distance
		let newSpan = region.spanInMeters()
		if newSpan.latitudeMeters > maximumSpanDistance || newSpan.longitudeMeters > maximumSpanDistance {
			return false
		}

		// NO, if new span is too small < minimumSpanDistance
		if newSpan.latitudeMeters < minimumSpanDistance || newSpan.longitudeMeters < minimumSpanDistance {
			return false
		}

		// NO, if the region has already been searched fully
		if isFullySearched(region: region) {
			return false
		}

		// YES if center distance > searchRadius
		let distance = region.centerDistance(from: previousRegion)
		if distance > searchRadius {
			return true
		}

		// YES if region is changed by at least minimumSpanDistance meters
		let spanDiff = previousRegion.spanDifferenceInMeters(to: region)
		if abs(spanDiff.latMeters) > minimumSpanDistance || abs(spanDiff.lngMeters) > minimumSpanDistance {
			return true
		}

		return false
	}

	private func isFullySearched(region: MKCoordinateRegion) -> Bool {
		fullySearchedRegions.contains { $0.contains(region) }
	}
}
