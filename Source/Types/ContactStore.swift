import Combine
import Contacts
import Foundation
import UIKit

protocol ContactStore {
	var authorizationStatus: CNAuthorizationStatus { get }

	init()
	func requestAccess() -> Future<Bool, Error>
	func getContacts() -> Future<[Contact], Error>
	func getEmails() -> Future<[String], Error>
	func getPhoneNumbers() -> Future<[String], Error>
}

extension ContactStore {
	var isAuthorized: Bool {
		authorizationStatus == .authorized
	}
}

enum ContactStoreError: Error {
	case unauthorized
	case cancelled
}

struct Contact: Identifiable {
	let id: String
	let initials: String
	let fullName: String
	let emails: [String]
	let phoneNumbers: [String]
	let avatar: UIImage?
}

extension CNContactStore: ContactStore {
	var authorizationStatus: CNAuthorizationStatus {
		CNContactStore.authorizationStatus(for: .contacts)
	}

	func requestAccess() -> Future<Bool, Error> {
		Future { [weak self] promise in
			guard let self = self else {
				promise(.failure(ContactStoreError.cancelled))
				return
			}

			self.requestAccess(for: .contacts) { result, error in
				if let error = error {
					if (error as? CNError)?.errorCode == CNError.Code.authorizationDenied.rawValue {
						promise(.failure(ContactStoreError.unauthorized))
					} else {
						promise(.failure(error))
					}
				} else {
					promise(.success(result))
				}
			}
		}
	}

	func getContacts() -> Future<[Contact], Error> {
		Future { [weak self] promise in
			guard let self = self else {
				promise(.failure(ContactStoreError.cancelled))
				return
			}

			guard self.isAuthorized else {
				promise(.failure(ContactStoreError.unauthorized))
				return
			}

			let req = CNContactFetchRequest(keysToFetch: [
				CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
				CNContactEmailAddressesKey as CNKeyDescriptor,
				CNContactPhoneNumbersKey as CNKeyDescriptor,
				CNContactThumbnailImageDataKey as CNKeyDescriptor,
				CNContactImageDataKey as CNKeyDescriptor,
			])

			let initialsFormatter = PersonNameComponentsFormatter()
			initialsFormatter.style = .abbreviated

			let maxAvatarSize: CGFloat = 200 // px
			var contacts: [Contact] = []

			DispatchQueue.global().async {
				do {
					try self.enumerateContacts(with: req) { contact, _ in
						guard let fullName = CNContactFormatter.string(from: contact, style: .fullName), !fullName.isEmpty else {
							return
						}

						let initials: String = {
							guard let components = initialsFormatter.personNameComponents(from: fullName) else {
								return String(fullName.first!)
							}

							return initialsFormatter.string(from: components)
						}()

						let emails = contact.emailAddresses.map { entry -> String in
							entry.value as String
						}
						let phoneNumbers = contact.phoneNumbers.compactMap { entry -> String? in
							PhoneNumberFormatter.toE164(entry.value.stringValue)
						}

						guard !emails.isEmpty || !phoneNumbers.isEmpty else {
							return
						}

						let avatar: UIImage? = {
							guard let data = contact.thumbnailImageData ?? contact.imageData, let image = UIImage(data: data) else {
								return nil
							}
							return (image.size.width > maxAvatarSize || image.size.height > maxAvatarSize) ? image.resizedImage(for: CGSize(width: maxAvatarSize, height: maxAvatarSize)) : image
						}()

						contacts.append(Contact(id: "contact-\(contact.identifier)", initials: initials, fullName: fullName, emails: emails, phoneNumbers: phoneNumbers, avatar: avatar))
					}

					promise(.success(contacts))
				} catch {
					promise(.failure(error))
				}
			}
		}
	}

	func getEmails() -> Future<[String], Error> {
		let request = CNContactFetchRequest(keysToFetch: [CNContactEmailAddressesKey as CNKeyDescriptor])
		return reduceContacts(with: request) { contact -> [String] in
			contact.emailAddresses.map { entry -> String in
				entry.value as String
			}
		}
	}

	func getPhoneNumbers() -> Future<[String], Error> {
		let request = CNContactFetchRequest(keysToFetch: [CNContactPhoneNumbersKey as CNKeyDescriptor])
		return reduceContacts(with: request) { contact -> [String] in
			contact.phoneNumbers.compactMap { entry -> String? in
				PhoneNumberFormatter.toE164(entry.value.stringValue)
			}
		}
	}

	private func reduceContacts<T>(with request: CNContactFetchRequest, transform: @escaping (CNContact) -> [T]) -> Future<[T], Error> {
		Future { [weak self] promise in
			guard let self = self else {
				promise(.failure(ContactStoreError.cancelled))
				return
			}

			guard self.isAuthorized else {
				promise(.failure(ContactStoreError.unauthorized))
				return
			}

			DispatchQueue.global().async {
				var result: [T] = []
				do {
					try self.enumerateContacts(with: request) { contact, _ in
						let values = transform(contact)
						result.append(contentsOf: values)
					}

					promise(.success(result))
				} catch {
					promise(.failure(error))
				}
			}
		}
	}
}
