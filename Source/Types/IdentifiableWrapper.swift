import Foundation

struct IdentifiableWrapper<T, ID: Hashable>: Identifiable {
	var id: ID
	let wrapped: T

	init(_ wrapped: T, identity: ID) {
		self.id = identity
		self.wrapped = wrapped
	}

	init(_ wrapped: T, identity keyPath: KeyPath<T, ID>) {
		self.id = wrapped[keyPath: keyPath]
		self.wrapped = wrapped
	}

	init(_ wrapped: T) where T == ID {
		self.id = wrapped
		self.wrapped = wrapped
	}
}
