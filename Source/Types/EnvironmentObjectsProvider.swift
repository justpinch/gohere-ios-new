import Foundation
import SwiftUI

final class EnvironmentObjectsProvider {
	let userState: UserState

	init(userState: UserState) {
		self.userState = userState
	}

	func provideEnvironmentObjects<V: View>(to view: V) -> some View {
		view.environmentObject(userState)
	}
}

struct EnvironmentObjectsProviderModifier: ViewModifier {
	@Environment(\.dependencyContainer) var dependencies

	private var provider: EnvironmentObjectsProvider {
		dependencies.resolve(EnvironmentObjectsProvider.self)
	}

	@ViewBuilder
	func body(content: Content) -> some View {
		provider.provideEnvironmentObjects(to: content)
	}
}

extension View {
	func provideEnvironmentObjects() -> some View {
		modifier(EnvironmentObjectsProviderModifier())
	}
}
