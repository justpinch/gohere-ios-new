import AuthenticationServices
import Combine
import Foundation
import UIKit

/// @mockable
protocol AppleSignInCoordinator {
	func signIn() -> ApiResponsePublisher<AuthToken>
}

enum AppleAuthError: Error {
	case unknown
}

final class DefaultAppleSignInCoordinator: NSObject, AppleSignInCoordinator {
	private let window: UIWindow
	private let authService: AuthService

	init(window: UIWindow, authService: AuthService) {
		self.window = window
		self.authService = authService
	}

	func signIn() -> ApiResponsePublisher<AuthToken> {
		var handler: AppleSignInHandler? = AppleSignInHandler()

		return Future<ApiResponsePublisher<AuthToken>, Error> { [weak self] promise in
			guard let self = self else { return }

			let request = ASAuthorizationAppleIDProvider().createRequest()
			request.requestedScopes = [.fullName, .email]

			handler?.completion = { [weak self] credential in
				guard let self = self else { return }

				let authAction: ApiResponsePublisher<AuthToken>?

				if let credential = credential as? ASAuthorizationAppleIDCredential, let token = credential.identityToken {
					let stringToken = String(data: token, encoding: .utf8)!
					authAction = self.authService.authenticate(appleToken: stringToken,
					                                           firstName: credential.fullName?.givenName,
					                                           lastName: credential.fullName?.familyName)
				} else if let credential = credential as? ASPasswordCredential {
					authAction = self.authService.authenticate(email: credential.user, password: credential.password)
				} else {
					authAction = nil
					promise(.failure(AppleAuthError.unknown))
				}

				promise(.success(authAction!))
			}

			handler?.error = { error in
				promise(.failure(error))
			}

			let controller = ASAuthorizationController(authorizationRequests: [request])
			controller.delegate = handler!
			controller.presentationContextProvider = self
			controller.performRequests()
		}
		.mapError(API.Error.init)
		.flatMap { $0 }
		.handleEvents(receiveCompletion: { _ in
			handler = nil
		}, receiveCancel: {
			handler = nil
		})
		.eraseToAnyPublisher()
	}
}

extension DefaultAppleSignInCoordinator: ASAuthorizationControllerPresentationContextProviding {
	func presentationAnchor(for _: ASAuthorizationController) -> ASPresentationAnchor {
		self.window
	}
}

private final class AppleSignInHandler: NSObject, ASAuthorizationControllerDelegate {
	var error: ((Error) -> Void)?
	var completion: ((ASAuthorizationCredential) -> Void)?

	func authorizationController(controller _: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
		self.completion?(authorization.credential)
	}

	func authorizationController(controller _: ASAuthorizationController, didCompleteWithError error: Error) {
		self.error?(error)
	}
}
