import Combine
import FacebookLogin
import Foundation

typealias FacebookManager = LoginManager

protocol Facebook {
	var token: String? { get }
	var isAuthenticated: Bool { get }
	func authenticate() -> Future<Bool, Error>
}

enum FacebookError: Error {
	case cancelled
}

extension LoginManager: Facebook {
	var isAuthenticated: Bool { token != nil }

	var token: String? {
		guard let token = AccessToken.current else {
			return nil
		}
		return token.isExpired ? nil : token.tokenString
	}

	func authenticate() -> Future<Bool, Error> {
		Future { [weak self] promise in
			guard let self = self else {
				return
			}

			self.logIn(permissions: [.email, .publicProfile, .userFriends], viewController: nil) { result in
				switch result {
					case .success:
						promise(.success(true))
					case .cancelled:
						promise(.success(false))
					case let .failed(error):
						promise(.failure(error))
				}
			}
		}
	}

	static func authenticate(with service: AuthService) -> ApiResponsePublisher<AuthToken> {
		let manager = FacebookManager()

		if let token = manager.token {
			return service.authenticate(facebookToken: token)
		}

		return manager.authenticate()
			.tryMap { _ in
				if let token = manager.token {
					return token
				} else {
					throw FacebookError.cancelled
				}
			}
			.mapError(API.Error.init)
			.flatMap { service.authenticate(facebookToken: $0) }
			.eraseToAnyPublisher()
	}
}
