import Combine
import Foundation

final class CountDown: ObservableObject {
	@Published private(set) var count = 0
	var isCounting: Bool { count > 0 }

	private var timer: Timer?

	deinit {
		stop()
	}

	func start(from: Int, onFinished: @escaping () -> Void = {}) {
		timer?.invalidate()
		count = from

		timer = .scheduledTimer(withTimeInterval: 1, repeats: true, block: { [unowned self] _ in
			self.count = max(0, self.count - 1)
			if self.count == 0 {
				self.stop()
				onFinished()
			}
		})
	}

	func stop() {
		timer?.invalidate()
		timer = nil
	}
}
