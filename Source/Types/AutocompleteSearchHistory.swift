import Foundation

final class AutocompleteSearchHistory: ObservableObject {
	private let storage: UserDefaults
	private let storageKey = "AutocompleteSearchHistory.Queries"

	var maxQueries = 5

	var queries: [String] {
		get { storage.object(forKey: storageKey) as? [String] ?? [] }
		set { storage.set(newValue, forKey: storageKey) }
	}

	init(storage: UserDefaults = .standard) {
		self.storage = storage
	}

	func add(query: String) {
		// de-dupe
		queries.removeAll(where: { $0 == query })

		// add on top
		queries.insert(query, at: 0)

		if queries.count > maxQueries {
			queries.removeLast()
		}

		// TODO: objectWillUpdate.send()
	}
}
