import Combine
import Foundation
import OSLog
import Sentry
import SwiftUI
import UIKit

protocol CloudMessaging {
	func setAPNSToken(token: Data)
	func getCurrentDevice() -> UserDevice?
}

protocol UIApplicationProtocol {
	var fullApplicationVersion: String { get }
	func registerForRemoteNotifications()
}

extension UIApplication: UIApplicationProtocol {}

protocol UserNotificationCenterProtocol {
	func requestAuthorization(options: UNAuthorizationOptions, completionHandler: @escaping (Bool, Error?) -> Void)
}

extension UNUserNotificationCenter: UserNotificationCenterProtocol {}

final class DefaultCloudMessaging: CloudMessaging {
	@ReadOnlyValue private var authToken: AuthToken?
	private let service: UserDeviceService
	private let notificationCenter: UserNotificationCenterProtocol
	private let application: UIApplicationProtocol
	private let userDefaults: UserDefaults

	private static let storageKey = "CloudMessaging.deviceInfo"

	struct DeviceInfo: Codable {
		let userID: User.ID
		let device: UserDevice
	}

	private var deviceInfo: DeviceInfo? {
		didSet {
			do {
				try Self.storeDevice(deviceInfo, in: userDefaults)
			} catch {
				assertionFailure(error.localizedDescription)
				error.report()
			}
		}
	}

	private var registerRequest: AnyCancellable?
	private var cancellables: Set<AnyCancellable> = []

	init(authToken: CurrentValueSubject<AuthToken?, Never>,
	     service: UserDeviceService,
	     notificationCenter: UserNotificationCenterProtocol = UNUserNotificationCenter.current(),
	     application: UIApplicationProtocol = UIApplication.shared,
	     userDefaults: UserDefaults = .standard)
	{
		self._authToken = ReadOnlyValue(subject: authToken)
		self.service = service
		self.notificationCenter = notificationCenter
		self.application = application
		self.userDefaults = userDefaults

		self.deviceInfo = Self.loadStoredDevice(storage: userDefaults)

		bindObservables()
	}

	private func bindObservables() {
		$authToken
			.removeDuplicates(by: { $0?.userId == $1?.userId })
			.withPrevious()
			.sink { [unowned self] previous, current in
				if let current = current, current.userId != previous??.userId {
					self.registerForPushNotifications()
				} else if current == nil {
					self.deleteDevice(token: previous ?? nil)
				}
			}
			.store(in: &cancellables)
	}

	private static func loadStoredDevice(storage: UserDefaults) -> DeviceInfo? {
		guard let data = storage.data(forKey: storageKey) else { return nil }

		do {
			return try JSONDecoder().decode(DeviceInfo.self, from: data)
		} catch {
			assertionFailure(error.localizedDescription)
			error.report()
		}

		return nil
	}

	private static func storeDevice(_ device: DeviceInfo?, in storage: UserDefaults) throws {
		guard let device = device else {
			storage.removeObject(forKey: storageKey)
			return
		}

		let data = try JSONEncoder().encode(device)
		storage.set(data, forKey: storageKey)
	}

	func setAPNSToken(token: Data) {
		let apnsToken = token.map { String(format: "%.2hhx", $0) }.joined()
		Logger.general.info("Received APNS token: \(apnsToken)")
		registerDevice(token: apnsToken)

		Analytics.User.set(.pushNotificationsEnabled, value: true)
	}

	func getCurrentDevice() -> UserDevice? {
		deviceInfo?.device
	}

	func registerForPushNotifications() {
		let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
		let application = self.application
		notificationCenter.requestAuthorization(options: authOptions, completionHandler: { _, _ in
			DispatchQueue.main.async {
				application.registerForRemoteNotifications()
			}
		})
	}

	func registerDevice(token: String) {
		guard let userID = authToken?.userId else {
			return
		}

		guard let firebaseProjectID = (Bundle.main.infoDictionary?["FirebaseProjectID"] as? String), !firebaseProjectID.isEmpty else {
			return
		}

		guard shouldUpdateDeviceRegistration() else {
			return
		}

		registerRequest = service
			.getFCMToken(apnsToken: token, firebaseProjectID: firebaseProjectID)
			.map { [unowned self] in
				self.getDeviceParams(for: userID, with: $0, firebaseProjectID: firebaseProjectID)
			}
			.map { [unowned self] in
				self.service.register(device: $0)
					.map { Optional(DeviceInfo(userID: userID, device: $0)) }
			}
			.switchToLatest()
			.catch { error -> Just<DeviceInfo?> in
				error.log()
				return Just(nil)
			}
			.assign(to: \.deviceInfo, onUnowned: self)
	}

	func deleteDevice(token: AuthToken?) {
		guard let deviceInfo = deviceInfo else {
			return
		}

		if let token = token, token.userId == deviceInfo.userID {
			unregister(device: deviceInfo.device, token: token)
		}

		self.deviceInfo = nil
	}

	func unregister(device: UserDevice, token: AuthToken) {
		service
			.delete(id: device.id, token: token)
			.subscribeIgnoringEvents()
	}

	func shouldUpdateDeviceRegistration() -> Bool {
		guard let deviceInfo = deviceInfo else {
			return true
		}

		let newParams = getDeviceParams(for: deviceInfo.userID, with: deviceInfo.device.fcmToken, firebaseProjectID: deviceInfo.device.firebaseProjectId)
		return deviceInfo.device.hasDifferences(with: newParams)
	}

	func getDeviceParams(for userID: User.ID, with fcmToken: String, firebaseProjectID: String) -> UserDeviceRegisterParams {
		let uiDevice = UIDevice()
		let deviceName = uiDevice.name
		let deviceModel = uiDevice.deviceModelCode ?? uiDevice.model
		let systemVersion = uiDevice.systemVersion

		var deviceID: UserDevice.ID?

		if let deviceInfo = deviceInfo, deviceInfo.userID == userID {
			deviceID = deviceInfo.device.id
		}

		return UserDeviceRegisterParams(id: deviceID, name: deviceName, model: deviceModel, osVersion: systemVersion, fcmToken: fcmToken, firebaseProjectId: firebaseProjectID)
	}
}

private extension UserDevice {
	func hasDifferences(with params: UserDeviceRegisterParams) -> Bool {
		!(params.id == id
			&& params.name == name
			&& params.model == model
			&& params.osVersion == osVersion
			&& params.fcmToken == fcmToken
			&& params.firebaseProjectId == firebaseProjectId)
	}
}

extension UIDevice {
	var deviceModelCode: String? {
		var systemInfo = utsname()
		uname(&systemInfo)
		let modelCode = withUnsafePointer(to: &systemInfo.machine) {
			$0.withMemoryRebound(to: CChar.self, capacity: 1) {
				ptr in String(validatingUTF8: ptr)
			}
		}

		return modelCode
	}
}
