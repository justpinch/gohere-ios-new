import Combine
import Foundation

@dynamicMemberLookup
enum ModelState<Model> {
	case none
	case loading(previous: Model?)
	case model(Model)
	case error(API.Error)

	static var loading: Self { .loading(previous: nil) }

	var model: Model? {
		if case let .model(model) = self { return model }
		return nil
	}

	var modelOrPrevious: Model? {
		if let model = model {
			return model
		}

		if case let .loading(previous) = self {
			return previous
		}

		return nil
	}

	var error: API.Error? {
		if case let .error(error) = self { return error }
		return nil
	}

	var isLoading: Bool {
		if case .loading = self { return true }
		return false
	}

	subscript<T>(dynamicMember keyPath: KeyPath<Model, T>) -> T? {
		model?[keyPath: keyPath]
	}
	
	func map<T>(transform: (Model) -> T) -> ModelState<T> {
		switch self {
		case .none:
			return ModelState<T>.none
		case .error(error: let errorValue):
			return ModelState<T>.error(errorValue)
		case .loading(previous: let previous):
			return ModelState<T>.loading(previous: previous.map(transform))
		case .model(model: let modelValue):
			return ModelState<T>.model(transform(modelValue))
		}
	}
}

enum LoadingState {
	case none
	case loading
	case error(API.Error)
	case finished

	init<M>(modelState: ModelState<M>) {
		switch modelState {
			case .none:
				self = .none
			case .loading:
				self = .loading
			case .model:
				self = .finished
			case let .error(error):
				self = .error(error)
		}
	}

	var isLoading: Bool {
		if case .loading = self { return true }
		return false
	}

	var error: API.Error? {
		if case let .error(error) = self { return error }
		return nil
	}
}

extension ModelState: Equatable where Model: Equatable {
	static func == (lhs: ModelState<Model>, rhs: ModelState<Model>) -> Bool {
		switch (lhs, rhs) {
			case (.none, .none):
				return true
			case let (.loading(l), .loading(r)):
				return l == r
			case let (.model(l), .model(r)):
				return l == r
			case let (.error(l), .error(r)):
				return l.reflectedString == r.reflectedString
			default:
				return false
		}
	}
}

extension LoadingState: Equatable {
	static func == (lhs: LoadingState, rhs: LoadingState) -> Bool {
		switch (lhs, rhs) {
			case (.none, .none):
				return true
			case (.loading, .loading):
				return true
			case (.finished, .finished):
				return true
			case let (.error(l), .error(r)):
				return l.reflectedString == r.reflectedString
			default:
				return false
		}
	}
}

extension LoadingState {
	init<T>(_ modelState: ModelState<T>) {
		switch modelState {
			case .none:
				self = .none
			case .loading:
				self = .loading
			case .model:
				self = .finished
			case let .error(error):
				self = .error(error)
		}
	}
}
