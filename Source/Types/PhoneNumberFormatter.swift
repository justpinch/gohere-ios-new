import Foundation
import PhoneNumberKit

// MARK: - Phone Numbers

private var phoneNumberFormatter: PhoneNumberKit = {
	PhoneNumberKit()
}()

enum PhoneNumberFormatter {
	static func toE164(_ string: String) -> String? {
		guard let phoneNumber = try? phoneNumberFormatter.parse(string) else {
			return nil
		}

		return phoneNumberFormatter.format(phoneNumber, toType: .e164)
	}
}
