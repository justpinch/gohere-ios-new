# GoHere iOS

GoHere uses [`xcodegen`](https://github.com/yonaskolb/XcodeGen) to generate the project file from `project.yml`. To generate a fresh project/workspace, run:

```bash
make project
```

This will generate GoHere.xcworkspace which can be then used for development as usual.

To update any project settings **do not modify xcodeproj/xcworkspace directly**, modify `project.yml` and run:

```bash
make cleanproj && make project
```

Alternatively, you can simply run `xcodegen`.

### Localizations

To fetch localizations for shared strings as well as all target specific strings, Makefile rule `localizations` can be used:

```bash
make localizations
```

### Make a new whitelabel

0. Run `make whitelabel` and follow the instructions
0. Register bundle ID for Google Maps. Go to Google Cloud APIs & Services, Credentials (https://console.cloud.google.com/apis/credentials/) and choose "Google Maps - GoHere iOS", add bundle ID of this new whitelabel. Add main bundle ID but don't forget development and Bob builds as well (`app.gohere.whitelabel.development`, `com.weroel.gohere.whitelabel.staging` & `com.weroel.gohere.whitelabel.production`).
0. Register the app in Firebase project (check which project is the latest considering the 30 apps limit, at the moment that is "GoHere2" - `gohere2-6c47b`), add the correct push key from `Configuration/Push Keys/`
0. Fill in values in `Source/Config.swift` (apprealm, api key...)
0. Update `Source/Theme.swift`
0. Add images to target asset catalogs
0. Update onboarding animation json file
0. Check/update splash background color in `LaunchScreen.storyboard`
0. Update strings - `make localizations`

### Cleaning generated artifacts

To clean the workspace from (auto)generated artifacts, two Makefile rules are implemented:

```bash
# 1. To clean only xcodeproj/xcworkspace from xcodegen
make cleanproj

# 2. To clean everything (project files, localizations build artifacts, Pods...)
make clean
```
