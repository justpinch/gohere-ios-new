.PHONY: clean mocks format localizations localizeCLI whitelabel

LANGUAGES := en nl fr

package_resolved_dir = "GoHere.xcodeproj/project.xcworkspace/xcshareddata/swiftpm"
package_resolved = "$(package_resolved_dir)/Package.resolved"

GoHere.xodeproj:
	xcodegen

project: GoHere.xodeproj

format:
	@if which swiftformat >/dev/null; then break; else echo "ERROR: swiftformat not found. (brew install swiftformat)"; exit 1; fi
	swiftformat .

mocks:
	@if which mockolo >/dev/null; then break; else echo "ERROR: mockolo not found. (brew install mockolo)"; exit 1; fi
	mockolo --enable-args-history -i GoHere -s Source/ -d Tests/Source/TestHelpers/Mocks/GeneratedMocks.swift
	swiftformat Tests/Source/TestHelpers/Mocks/GeneratedMocks.swift

localizeCLI:
	$(info -- Downloading the localization CLI tool...)
	@if [ ! -d .localize ]; then git clone git@bitbucket.org:justpinch/gohere-localize.git .localize; else cd .localize && git reset --hard && git pull; fi
	$(info -- Installing npm dependencies...)
	@cd .localize/localize && npm install >/dev/null
	@cd .localize/localizeCLI && npm install >/dev/null

localizations: localizeCLI
	# -- Exporting Info.plist strings...
	@rm -f .localize/*.strings
	@node .localize/localizeCLI -s Info.plist! --ios -p .localize/ >/dev/null
	@for lang in $(LANGUAGES) ; do \
		cat .localize/strings-$$lang.strings > Resources/$$lang.lproj/InfoPlist.strings ; \
	done

	# -- Exporting common strings....
	@rm -f .localize/*.strings
	@node .localize/localizeCLI -s CommonNew! --ios -p .localize/ >/dev/null
	@for lang in $(LANGUAGES) ; do \
		if [ ! -f .localize/strings-$$lang.strings ]; then \
			echo "Main $$lang translations not found!" ; \
			exit 1 ; \
		else \
			cat .localize/strings-$$lang.strings > Resources/$$lang.lproj/Localizable.strings ; \
		fi \
	done

	# -- Exporting target specific strings...
	@for file in ./Targets/* ; do \
		target=$$(basename $$file) ; \
		rm -f .localize/*.strings ; \
		node .localize/localizeCLI -s $$target! --ios -p .localize/ >/dev/null ; \
		for lang in $(LANGUAGES) ; do \
			if [ ! -f .localize/strings-$$lang.strings ]; then \
				echo "[$$target] $$lang translations not found." ; \
			else \
				cat .localize/strings-$$lang.strings > Targets/$$target/Resources/Localization/$$lang.lproj/LocalizableTarget.strings ; \
			fi \
		done \
	done

whitelabel:
	@echo "Enter the app TARGET name: \n(e.g. GoHere or TransaviaTips)"; \
    read TARGET; \
    echo "Enter the app DISPLAY name: \n(e.g. Ciao tutti or Something tips)"; \
    read DISPLAY; \
    echo "Enter the app bundle suffix: \n(as in app.gohere.SUFFIX, e.g. transaviatips (makes -> app.gohere.transaviatips))"; \
    read BUNDLESUFFIX; \
    echo "Enter the app marketing version: \n(e.g. 1.0.0)"; \
    read VERSION_NUMBER; \
    echo "Enter the Firebase Project ID: \n(e.g. go-here-1259 or gohere2-6c47b)"; \
    read FIREBASE_PROJECT_ID; \
    sh Scripts/make_whitelabel.sh "$$TARGET" "$$DISPLAY" "$$BUNDLESUFFIX" "$$VERSION_NUMBER" "$$FIREBASE_PROJECT_ID"

clean:
	rm -rf .localize/
	@mv $(package_resolved) Package.tmp
	rm -rf GoHere.xcodeproj
	@mkdir -p $(package_resolved_dir)
	@mv ./Package.tmp $(package_resolved)
	

