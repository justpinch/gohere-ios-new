import Combine
@testable import GoHere
import MapKit
import XCTest

class ExploreModelTests: XCTestCase {
	var biasRegion: MKMapRect? = MKMapRect(x: 50, y: 60, width: 70, height: 80)

	var placeService = PlaceServiceMock()
	var placeExternalService = MockPlaceExternalService()
	var userLocationManager = UserLocationManager(manager: LocationManagerMock())
	var geocoder = GeocoderMock()
	var notificationCenter = NotificationCenterProtocolMock.withDefaults()

	override func setUp() {
		placeService = PlaceServiceMock()
		placeExternalService = MockPlaceExternalService()
		userLocationManager = UserLocationManager(manager: LocationManagerMock())
		geocoder = GeocoderMock()
		notificationCenter = .withDefaults()
	}

	func makeSUT() -> ExploreModel {
		ExploreModel(biasRegion: biasRegion,
		             placeService: placeService,
		             placeExternalService: placeExternalService,
		             locationManager: userLocationManager,
		             geocoder: geocoder,
		             notificationCenter: notificationCenter,
		             scheduler: DispatchQueue.main)
	}

	func testReferenceCycles() throws {
		weak var obj: ExploreModel?

		_ = {
			let instance = makeSUT()

			// call some methods..
			instance.bindObservables()
			// add more...

			obj = instance
		}()

		XCTAssertNil(obj, "Expected instance to be deallocated")
	}
}
