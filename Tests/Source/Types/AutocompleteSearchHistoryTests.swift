@testable import GoHere
import XCTest

class AutocompleteSearchHistoryTests: XCTestCase {
	let suiteName = "AutocompleteSearchHistoryTests"
	var storage: UserDefaults!

	override func setUp() {
		super.setUp()

		storage = UserDefaults(suiteName: suiteName)
		storage.removePersistentDomain(forName: suiteName)
	}

	override func tearDown() {
		super.tearDown()
		storage.removePersistentDomain(forName: suiteName)
	}

	func testEmpty() {
		let history = AutocompleteSearchHistory(storage: storage)
		XCTAssertEqual(history.queries, [])
	}

	func testAddQuery() {
		let history = AutocompleteSearchHistory(storage: storage)

		history.add(query: "string1")
		history.add(query: "string2")

		XCTAssertEqual(history.queries, ["string2", "string1"])
	}

	func testAddQueryOverLimit() {
		let history = AutocompleteSearchHistory(storage: storage)
		history.maxQueries = 3

		history.add(query: "string1")
		history.add(query: "string2")
		history.add(query: "string3")
		history.add(query: "string4")

		XCTAssertEqual(history.queries, ["string4", "string3", "string2"])
	}

	func testUniqueQueries() {
		let history = AutocompleteSearchHistory(storage: storage)

		history.add(query: "string1")
		history.add(query: "string2")
		history.add(query: "string1")

		XCTAssertEqual(history.queries, ["string1", "string2"])
	}
}
