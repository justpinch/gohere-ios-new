import Combine
import CoreLocation
@testable import GoHere
import XCTest

class AutocompleteSearchControllerTests: XCTestCase {
	func testSearch() throws {
		let result = try PlaceAutocomplete.fixture1()
		var searchParams: AutocompleteSearchModel.SearchParams?
		let loader = { (params: AutocompleteSearchModel.SearchParams) -> ApiResponsePublisher<[PlaceAutocomplete]> in
			searchParams = params
			return Just([result]).setFailureType(to: API.Error.self).eraseToAnyPublisher()
		}
		let sut = AutocompleteSearchModel(loader: loader)

		// TODO: We should use an injected scheduler in SUT and switch to
		// synchronous scheduler for tests but Combine does not make it easy/possible at the moment
		let expectation = self.expectation(description: "Wait for results (because of the debounce operator)")
		var results = [ModelState<[PlaceAutocomplete]>]()
		sut.$results
			.handleEvents(receiveOutput: {
				results.append($0)
				if $0.model != nil { expectation.fulfill() }
			}).subscribeIgnoringEvents()

		sut.searchParams = ("query", CLLocationCoordinate2D(latitude: 1, longitude: 2))

		waitForExpectations(timeout: 1, handler: nil)

		XCTAssertEqual(searchParams?.0, "query")
		XCTAssertEqual(searchParams?.1.latitude, 1)
		XCTAssertEqual(searchParams?.1.longitude, 2)
		XCTAssertEqual(sut.results, .model([result]))
		XCTAssertEqual(results, [.none, .loading, .model([result])])
	}

	func testSearchFailure() throws {
		let error = API.Error(reason: NSError(domain: "test", code: 123, userInfo: nil))
		let loader = { (_: AutocompleteSearchModel.SearchParams) -> ApiResponsePublisher<[PlaceAutocomplete]> in
			Fail(error: error).eraseToAnyPublisher()
		}
		let sut = AutocompleteSearchModel(loader: loader)

		// TODO: We should use an injected scheduler in SUT and switch to
		// synchronous scheduler for tests but Combine does not make it easy/possible at the moment
		let expectation = self.expectation(description: "Wait for results (because of the debounce operator)")
		var results = [ModelState<[PlaceAutocomplete]>]()
		sut.$results
			.handleEvents(receiveOutput: {
				results.append($0)
				switch $0 {
					case .error:
						expectation.fulfill()
					default: break
				}
			}).subscribeIgnoringEvents()

		sut.searchParams = ("query", CLLocationCoordinate2D(latitude: 1, longitude: 2))

		waitForExpectations(timeout: 1, handler: nil)

		XCTAssertEqual(sut.results, .error(error))
		XCTAssertEqual(results, [.none, .loading, .error(error)])
	}
}
