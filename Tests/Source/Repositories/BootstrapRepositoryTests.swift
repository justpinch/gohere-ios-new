import Combine
@testable import GoHere
import XCTest

class BootstrapRepositoryTests: XCTestCase {
	let cache = MockFileCache()
	let model = Bootstrap.fixture1
	var service = BootstrapServiceMock()

	var repo: BootstrapRepository!

	override func setUp() {
		cache.reset()
		service = BootstrapServiceMock()
		repo = BootstrapRepository(service: service, cache: cache)
	}

	/// Test cached bootstrap when it's present
	func testCachedBootstrap() throws {
		cache.storage[FileCache.Keys.bootstrap] = model

		let bootstrap = repo.bootstrap()

		XCTAssertEqual(bootstrap, model)
	}

	/// Test cached bootstrap when not in cache
	func testCachedBootstrapEmpty() throws {
		XCTAssertNil(repo.bootstrap())
	}

	/// Test bootstrap publisher for  network fetched object
	func testBootstrapLoader() throws {
		service.getHandler = { [unowned self] in Just(model).mapAPIError().eraseToAnyPublisher() }

		let bootstrap = try repo.bootstrapLoader().waitForCompletion()

		XCTAssertEqual(bootstrap, [model])
	}

	/// Test bootstrap publisher when network fetch raises an error
	func testBootstrapLoaderNetworkErrorOnly() throws {
		let innerError = NSError(domain: "1", code: 2, userInfo: nil)
		let error = API.Error(reason: innerError)
		service.getHandler = { Fail(error: error).eraseToAnyPublisher() }

		var thrownError: Error?
		try XCTAssertThrowsError(repo.bootstrapLoader().waitForCompletion()) { error in
			thrownError = error
		}

		let apiError = try XCTUnwrap(thrownError as? API.Error)
		XCTAssertEqual(apiError.reason as NSError, innerError)
	}

	/// Ensure the getting bootstrap will also cache fetched network result
	func testBootstrapLoaderCachesNewVersion() throws {
		service.getHandler = { [unowned self] in Just(model).mapAPIError().eraseToAnyPublisher() }

		_ = try repo.bootstrapLoader().waitForCompletion()

		let cachedValue = try XCTUnwrap(cache.storage[FileCache.Keys.bootstrap] as? Bootstrap)
		XCTAssertEqual(cachedValue, model)
	}
}
