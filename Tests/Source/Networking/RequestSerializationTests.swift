import Combine
@testable import GoHere
import XCTest

class RequestSerializationTests: XCTestCase {
	var cancellables: Set<AnyCancellable> = Set()

	override func setUp() {
		cancellables = []
	}

	override func tearDown() {
		cancellables = []
	}

	func testRequestWithOnlyURL() throws {
		let url = URL(string: "https://www.gohere.app/")!

		let request = Request.build(with: url)

		try XCTAssertEqual(request.build(), URLRequest(url: url))
	}

	func testRequestWithParams() throws {
		let url = URL(string: "https://www.gohere.app/")!
		let params: [String: Any] = ["p1": "v1", "p2": 2, "p3": [3, 30], "p4": true]

		let request = Request.build(with: url)
			.parameters(params, encoding: URLEncoding.default)

		let fullUrl = URL(string: "https://www.gohere.app/?p1=v1&p2=2&p3[]=3&p3[]=30&p4=1")!
		try XCTAssertEqual(request.build(), URLRequest(url: fullUrl))
	}

	func testRequestWithPOSTParams() throws {
		let url = URL(string: "https://www.gohere.app/")!
		let params: [String: Any] = ["p1": "v1", "p2": 2, "p3": [3, 30], "p4": true]

		let request = Request.build(with: url)
			.method(.post)
			.parameters(params, encoding: URLEncoding.default)

		var expectedRequest = URLRequest(url: url)
		expectedRequest.httpMethod = "POST"
		expectedRequest.httpBody = "p1=v1&p2=2&p3%5B%5D=3&p3%5B%5D=30&p4=1".data(using: .utf8)
		expectedRequest.addValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
		try XCTAssertEqual(request.build(), expectedRequest)
	}

	func testRequestHeaders() throws {
		let url = URL(string: "https://www.gohere.app/")!

		let request = try Request.build(with: url)
			.header(name: "Header", value: "Value")
			.build()

		var expectedRequest = URLRequest(url: url)
		expectedRequest.addValue("Value", forHTTPHeaderField: "Header")
		XCTAssertEqual(request, expectedRequest)
	}

	func testRequestPublisher() throws {
		let sink = CurrentValueSubject<URLRequest?, Error>(nil)
		let url = URL(string: "https://www.gohere.app/")!

		Request.build(with: url)
			.publisher()
			.compactMap { $0 }
			.subscribe(sink)
			.store(in: &cancellables)

		XCTAssertEqual(sink.value, URLRequest(url: url))
	}
}
