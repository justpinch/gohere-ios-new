import Combine
@testable import GoHere
import XCTest

class APIClientResponseProviderTests: XCTestCase {
	let url = URL(string: "base:api/v1/")!
	let session = MockNetworkingSession()

	lazy var responseProvider: API.DefaultResponseProvider = {
		API.DefaultResponseProvider(baseURL: url, decoder: JSONDecoder(), session: session)
	}()

	var requestPublisher: RequestPublisherConvertible {
		let request = URLRequest(url: url)
		let publisher = Just(request).setFailureType(to: Error.self).eraseToAnyPublisher()
		return MockRequestPublisherConvertible(result: publisher)
	}

	override func setUp() {
		session.reset()
	}

	func makeResponse(statusCode: Int, requestId: String) throws -> HTTPURLResponse {
		let response = HTTPURLResponse(url: url, statusCode: statusCode, httpVersion: nil, headerFields: ["X-Request-Id": requestId])
		return try XCTUnwrap(response)
	}
}

// MARK: - Response Validation

extension APIClientResponseProviderTests {
	func testResponseValidation_InvalidResponse() throws {
		let response = try makeResponse(statusCode: 200, requestId: "1")

		// test result data
		let model = ["value"]
		let modelData = try JSONEncoder().encode(model)
		session.defaultResult = .success((data: modelData, response))

		let result: [NetworkDecodedOutput<[String]>] = try responseProvider.response(for: requestPublisher).waitForCompletion()

		XCTAssertEqual(result.map(\.data), [["value"]])
	}

	func testResponseValidation_FormValidationError() throws {
		let modelData = try API.FormValidationError.fixture1Data()
		let response = try makeResponse(statusCode: 400, requestId: "1")
		session.defaultResult = .success((data: modelData, response))

		let result: (output: [NetworkDecodedOutput<[String]>], error: Error?) = try responseProvider.response(for: requestPublisher).waitForCompletionCollecting()

		let error = try XCTUnwrap((result.error as? API.Error)?.reason as? API.FormValidationError)

		XCTAssertTrue(result.output.isEmpty)
		XCTAssertEqual(error.statusCode, 400)
		XCTAssertEqual(error.requestId, "1")
		XCTAssertEqual(error.nonFieldErrors, ["err1", "err2"])
		XCTAssertEqual(error.fieldErrors, ["field1": ["error field1"], "field2": ["error field2"]])
	}

	func testResponseValidation_RateLimitError() throws {
		let modelData = try API.RateLimitError.fixture1Data()
		let response = try makeResponse(statusCode: 429, requestId: "1")
		session.defaultResult = .success((data: modelData, response))

		let result: (output: [NetworkDecodedOutput<[String]>], error: Error?) = try responseProvider.response(for: requestPublisher).waitForCompletionCollecting()

		let error = try XCTUnwrap((result.error as? API.Error)?.reason as? API.RateLimitError)

		XCTAssertTrue(result.output.isEmpty)
		XCTAssertEqual(error.statusCode, 429)
		XCTAssertEqual(error.requestId, "1")
		XCTAssertEqual(error.detail, "Rate limit exceeded")
	}

	func testResponseValidation_DomainError() throws {
		let modelData = try API.DomainError.fixture1Data()
		let response = try makeResponse(statusCode: 500, requestId: "1")
		session.defaultResult = .success((data: modelData, response))

		let result: (output: [NetworkDecodedOutput<[String]>], error: Error?) = try responseProvider.response(for: requestPublisher).waitForCompletionCollecting()

		let error = try XCTUnwrap((result.error as? API.Error)?.reason as? API.DomainError)

		XCTAssertTrue(result.output.isEmpty)
		XCTAssertEqual(error.statusCode, 500)
		XCTAssertEqual(error.requestId, "1")
		XCTAssertEqual(error.code, .invalidToken)
		XCTAssertEqual(error.detail, "Invalid token")
	}

	func testResponseValidation_DomainErrorOther() throws {
		// DomainError won't be able to be constructed from this JSON
		let modelData = try XCTUnwrap("{ \"code\": [] }".data(using: .utf8))
		let response = try makeResponse(statusCode: 500, requestId: "1")
		session.defaultResult = .success((data: modelData, response))

		let result: (output: [NetworkDecodedOutput<[String]>], error: Error?) = try responseProvider.response(for: requestPublisher).waitForCompletionCollecting()

		let error = try XCTUnwrap((result.error as? API.Error)?.reason as? API.DomainError)

		XCTAssertTrue(result.output.isEmpty)
		XCTAssertEqual(error.statusCode, 500)
		XCTAssertEqual(error.requestId, "1")
	}
}
