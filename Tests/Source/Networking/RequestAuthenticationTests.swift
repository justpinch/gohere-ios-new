import Combine
@testable import GoHere
import XCTest

class RequestAuthenticationTests: XCTestCase {
	let url = URL(string: "base:v1/")!
	let responseProvider = MockResponseProvider()
	var cancellables: Set<AnyCancellable> = Set()

	override func setUp() {
		cancellables = []
		responseProvider.reset()
	}

	func testMissingToken() {
		let authenticator = RequestAuthenticator(token: .init(nil), baseURL: url, responseProvider: responseProvider)
		let sink = CurrentValueSubject<Result<AuthToken, Error>?, Never>(nil)

		authenticator
			.token()
			.map { Result.success($0) }
			.catch { Just(Result.failure($0)) }
			.subscribe(sink)
			.store(in: &cancellables)

		switch sink.value {
			case let .some(.failure(error)):
				XCTAssertTrue(error is API.AuthenticationError)
				switch error as? API.AuthenticationError {
					case .missingToken:
						break
					default: XCTFail("Expected .missingToken error got \(error).")
				}
			default: XCTFail()
		}
	}

	func testValidToken() {
		let token = AuthToken(userId: "uid", access: "access", refresh: "refresh")
		let authenticator = RequestAuthenticator(token: .init(token), baseURL: url, responseProvider: responseProvider)
		let sink = CurrentValueSubject<Result<AuthToken, Error>?, Never>(nil)

		authenticator
			.token()
			.map { Result.success($0) }
			.catch { Just(Result.failure($0)) }
			.subscribe(sink)
			.store(in: &cancellables)

		try XCTAssertEqual(sink.value?.get(), token)
	}

	func testRefreshToken() throws {
		let token = AuthToken(userId: "uid", access: "access", refresh: "refresh")
		let model = RefreshTokenResponse(access: "access-new")

		responseProvider.results.append(.success((data: model, response: HTTPURLResponse())))
		let authenticator = RequestAuthenticator(token: .init(token), baseURL: url, responseProvider: responseProvider)

		let sink = CurrentValueSubject<AuthToken?, API.AuthenticationError>(nil)

		authenticator.token(forceRefresh: true)
			.compactMap { $0 }
			.subscribe(sink)
			.store(in: &cancellables)

		XCTAssertEqual(sink.value?.userId, "uid")
		XCTAssertEqual(sink.value?.access, "access-new")
		XCTAssertEqual(sink.value?.refresh, "refresh")
	}
}
