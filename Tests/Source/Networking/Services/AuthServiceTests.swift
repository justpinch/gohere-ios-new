import Combine
@testable import GoHere
import XCTest

class AuthServiceTests: XCTestCase {
	let url = URL(string: "base:url/v1")!
	var cancellables: Set<AnyCancellable> = Set()
	let token = AuthToken(userId: "uid", access: "access", refresh: "refresh")

	let responseProvider = MockResponseProvider()

	override func setUp() {
		cancellables = []
		responseProvider.reset()
	}

	override func tearDown() {
		cancellables = []
	}

	func testRequestAuthenticateWithEmailPassword() throws {
		let sink = CurrentValueSubject<AuthToken?, API.Error>(nil)

		let svc = DefaultAuthService(url: url, responseProvider: responseProvider)
		responseProvider.results.append(.success((data: token, response: URLResponse())))

		svc.authenticate(email: "email", password: "password")
			.compactMap { $0 }
			.subscribe(sink)
			.store(in: &cancellables)

		XCTAssertEqual(sink.value, token)

		let expectedRequest = Request.build(with: url.appendingPathComponent("/users/auth/token"))
			.method(.post)
			.parameters(["email": "email", "password": "password"], encoding: JSONEncoding.default)
		try XCTAssertEqual(responseProvider.requests.first?.build(), expectedRequest.build())
	}

	func testRequestAuthenticateWithPhoneNumber() throws {
		let sink = CurrentValueSubject<AuthToken?, API.Error>(nil)

		let svc = DefaultAuthService(url: url, responseProvider: responseProvider)
		responseProvider.results.append(.success((data: token, response: URLResponse())))

		svc.authenticate(phoneNumber: "012345", tvpToken: "token")
			.compactMap { $0 }
			.subscribe(sink)
			.store(in: &cancellables)

		XCTAssertEqual(sink.value, token)

		let expectedRequest = Request.build(with: url.appendingPathComponent("/users/auth/token/tvp/"))
			.method(.post)
			.parameters(["phone_number": "012345", "tvp_token": "token"], encoding: JSONEncoding.default)
		try XCTAssertEqual(responseProvider.requests.first?.build(), expectedRequest.build())
	}

	func testRequestAuthenticateFacebook() throws {
		let sink = CurrentValueSubject<AuthToken?, API.Error>(nil)

		let svc = DefaultAuthService(url: url, responseProvider: responseProvider)
		responseProvider.results.append(.success((data: token, response: URLResponse())))

		svc.authenticate(facebookToken: "fb_token")
			.compactMap { $0 }
			.subscribe(sink)
			.store(in: &cancellables)

		XCTAssertEqual(sink.value, token)

		let expectedRequest = Request.build(with: url.appendingPathComponent("/users/facebook/"))
			.method(.post)
			.parameters(["token": "fb_token"], encoding: JSONEncoding.default)
		try XCTAssertEqual(responseProvider.requests.first?.build(), expectedRequest.build())
	}

	func testRequestAuthenticateApple() throws {
		let sink = CurrentValueSubject<AuthToken?, API.Error>(nil)

		let svc = DefaultAuthService(url: url, responseProvider: responseProvider)
		responseProvider.results.append(.success((data: token, response: URLResponse())))

		svc.authenticate(appleToken: "token", firstName: "Name", lastName: "Surname")
			.compactMap { $0 }
			.subscribe(sink)
			.store(in: &cancellables)

		XCTAssertEqual(sink.value, token)

		let expectedRequest = Request.build(with: url.appendingPathComponent("/users/apple/"))
			.method(.post)
			.parameters(["token": "token", "first_name": "Name", "last_name": "Surname"],
			            encoding: JSONEncoding.default)
		try XCTAssertEqual(responseProvider.requests.first?.build(), expectedRequest.build())
	}
}
