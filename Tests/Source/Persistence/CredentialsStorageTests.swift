import Combine
@testable import GoHere
import XCTest

class CredentialsStorageTests: XCTestCase {
	let keychain = MockKeychain()

	override func setUp() {
		keychain.resetAllMocks()
	}

	lazy var encoder: JSONEncoder = {
		let encoder = JSONEncoder()
		encoder.keyEncodingStrategy = .convertToSnakeCase
		return encoder
	}()

	lazy var decoder: JSONDecoder = {
		let decoder = JSONDecoder()
		decoder.keyDecodingStrategy = .convertFromSnakeCase
		return decoder
	}()

	func testAuthTokenOnInit() throws {
		// token read from empty keychain means token is nil
		keychain.dataForKeyMocks.append { _ in throw Keychain.KeychainError(status: errSecItemNotFound) }
		var storage = DefaultCredentialsStorage(keychain: keychain)
		XCTAssertNil(storage.authToken.value)

		let token = AuthToken(userId: "uid", access: "access", refresh: "refresh")
		keychain.dataForKeyMocks.append { _ in try self.encoder.encode(token) }

		storage = DefaultCredentialsStorage(keychain: keychain)
		XCTAssertEqual(storage.authToken.value, token)
	}

	func testSetNilAuthToken() throws {
		let storage = DefaultCredentialsStorage(keychain: keychain)
		keychain.resetMocks.append {}

		storage.authToken.value = nil

		XCTAssertNil(storage.authToken.value)
	}

	func testSetAuthToken() throws {
		let storage = DefaultCredentialsStorage(keychain: keychain)
		let token = AuthToken(userId: "uid", access: "access", refresh: "refresh")
		var resetCalled = false
		keychain.resetMocks.append { resetCalled = true }
		var storedData: Data?
		var storedDataKey: String?
		keychain.setDataForKeyMocks.append { storedData = $0; storedDataKey = $1 }

		storage.authToken.value = token

		XCTAssertEqual(storage.authToken.value, token)
		XCTAssertTrue(resetCalled)
		XCTAssertEqual(storedDataKey, DefaultCredentialsStorage.Keys.authToken)
		try XCTAssertEqual(decoder.decode(AuthToken.self, from: XCTUnwrap(storedData)), token)
	}

	func testSetAuthTokenIgnoresDuplicates() throws {
		let storage = DefaultCredentialsStorage(keychain: keychain)
		let token = AuthToken(userId: "uid", access: "access", refresh: "refresh")
		var resetCallCount = 0
		keychain.resetMocks.append { resetCallCount += 1 }
		keychain.setDataForKeyMocks.append { _, _ in } // ignore set closure

		storage.authToken.value = token
		storage.authToken.value = token
		storage.authToken.value = token

		XCTAssertEqual(resetCallCount, 1)
	}

	func testSettingTokenOnInitIsNotCalledForTheInitialValue() throws {
		// this token is to be loaded from Keychain on init()
		let token = AuthToken(userId: "uid", access: "access", refresh: "refresh")
		keychain.dataForKeyMocks.append { _ in try self.encoder.encode(token) }

		// setData is called everytime token changes but it should drop first value
		// as we don't want to re-write the existing value we just read
		var setCallCount = 0
		keychain.setDataForKeyMocks.append { _, _ in setCallCount += 1 }

		_ = DefaultCredentialsStorage(keychain: keychain)
		XCTAssertEqual(setCallCount, 0)
	}
}
