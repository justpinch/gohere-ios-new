import Combine
@testable import GoHere
import XCTest

class FileManagerExtensionTests: XCTestCase {
	let fileManager = FileManager.default

	var fileURL = FileManager.default.temporaryDirectory.appendingPathComponent("FileManagerExtensionTests.file")

	override func tearDown() {
		try? fileManager.removeItem(at: fileURL)
	}

	func testWrite() throws {
		let data = try XCTUnwrap("string".data(using: .utf8))

		try fileManager.write(data: data, to: fileURL)

		try XCTAssertEqual(Data(contentsOf: fileURL), data)
	}

	func testContentsOfUrl() throws {
		let data = try XCTUnwrap("string".data(using: .utf8))
		try data.write(to: fileURL)

		let result = try fileManager.contents(of: fileURL)

		XCTAssertEqual(result, data)
	}

	func testContentsOfUrlWhenNoFile() throws {
		let result = try fileManager.contents(of: fileURL)
		XCTAssertNil(result)
	}

	func testContentsOfUrlException() throws {
		let url = URL(string: "invalid:file:url")!
		try XCTAssertThrowsError(fileManager.contents(of: url))
	}
}

class FileCacheTests: XCTestCase {
	let fileManager = MockFileManager()

	override func setUp() {
		fileManager.reset()
	}

	func testCacheWrite() throws {
		let cache = FileCache(fileManager: fileManager)
		let url = fileManager.url(for: "key")

		try cache.cache(["array item"], key: "key")

		let storedData = try XCTUnwrap(fileManager.storage[url])
		let object = try PropertyListDecoder().decode([String].self, from: storedData)
		XCTAssertEqual(object, ["array item"])
	}

	func testCacheRead() throws {
		let cache = FileCache(fileManager: fileManager)
		let url = fileManager.url(for: "key")
		let data = try PropertyListEncoder().encode(["pairKey": "pairValue"])
		fileManager.storage[url] = data

		let result: [String: String]? = try cache.value(for: "key")

		XCTAssertEqual(result, ["pairKey": "pairValue"])
	}

	func testCacheReadNoFile() throws {
		let cache = FileCache(fileManager: fileManager)

		let result: [String: String]? = try cache.value(for: "key")

		XCTAssertNil(result)
	}
}
