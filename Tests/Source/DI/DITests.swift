import Combine
@testable import GoHere
import XCTest

class DependencyContainerTests: XCTestCase {
	var sut: DIContainer!

	class ExampleService {
		weak var resolver: DIContainer?
	}

	override func setUp() {
		sut = DIContainer()
	}

	func testRegisterService() {
		let service = ExampleService()
		sut.register(service: service)
		XCTAssertTrue(sut.resolve(ExampleService.self) === service)
	}

	func testRegisterServiceWithName() {
		let service1 = ExampleService()
		let service2 = ExampleService()

		sut.register(service: service1)
		sut.register(service: service2, name: "second")

		XCTAssertTrue(sut.resolve(ExampleService.self, name: "second") === service2)
	}

	func testRegisterServiceWithFactory() {
		let service = ExampleService()

		sut.register { resolver -> ExampleService in
			service.resolver = (resolver as! DIContainer)
			return service
		}

		XCTAssertTrue(sut.resolve(ExampleService.self) === service)
		XCTAssertTrue(service.resolver === sut)
	}

	func testRegisterServiceWithFactoryAndName() {
		let service1 = ExampleService()
		let service2 = ExampleService()

		sut.register { _ -> ExampleService in
			service1
		}

		sut.register(name: "second") { resolver -> ExampleService in
			service2.resolver = (resolver as! DIContainer)
			return service2
		}

		XCTAssertTrue(sut.resolve(ExampleService.self, name: "second") === service2)
		XCTAssertTrue(service2.resolver === sut)
	}
}
