import Combine
import CoreLocation
@testable import GoHere
import XCTest

class UserLocationManagerTests: XCTestCase {
	var locationManager = LocationManagerMock()
	let coreLocationManager = CLLocationManager()

	func makeSUT(manager: LocationManager? = nil) -> UserLocationManager {
		UserLocationManager(manager: manager ?? locationManager)
	}

	override func setUp() {
		locationManager = LocationManagerMock(authorizationStatus: .notDetermined)
		type(of: locationManager).locationServicesEnabledHandler = { true }
	}

	func testReferenceCycles() throws {
		weak var obj: UserLocationManager?

		_ = {
			let instance = makeSUT(manager: coreLocationManager)

			_ = instance.authorizationStatus()
			_ = instance.updateLocation()

			obj = instance
		}()

		XCTAssertNil(obj, "Expected instance to be deallocated")
	}

	func testAuthorizationStatusServicesDisabled() throws {
		type(of: locationManager).locationServicesEnabledHandler = { false }
		let sut = makeSUT()

		let result = try sut.authorizationStatus()
			.prefix(1) // take first otherwise publisher never completes
			.waitForCompletionCollecting()

		XCTAssertEqual(result.output, [.disabled])
	}

	func testAuthorizationStatusPublisher() throws {
		let sut = makeSUT()
		locationManager.authorizationStatus = .denied
		var result: [LocationServicesStatus] = []

		sut.authorizationStatus()
			.subscribe(Subscribers.Sink(receiveCompletion: { _ in }, receiveValue: {
				result.append($0)
			}))

		sut.locationManager(coreLocationManager, didChangeAuthorization: .authorizedWhenInUse)

		XCTAssertEqual(result, [.denied, .authorized])
	}

	func testUpdateLocation() throws {
		var authorizationRequestCount = 0
		var locationRequestCount = 0
		let exampleLocation = CLLocation()
		let exampleError = NSError(domain: "", code: 1, userInfo: nil)

		locationManager.authorizationStatus = .notDetermined
		locationManager.requestWhenInUseAuthorizationHandler = { authorizationRequestCount += 1 }
		locationManager.requestLocationHandler = { locationRequestCount += 1 }

		let sut = makeSUT()
		var result: [UserLocationManager.LocationState?] = []

		sut.updateLocation()
			.subscribe(Subscribers.Sink(receiveCompletion: { _ in }, receiveValue: {
				result.append($0)
			}))

		sut.locationManager(coreLocationManager, didChangeAuthorization: .authorizedAlways)
		sut.locationManager(coreLocationManager, didUpdateLocations: [exampleLocation])

		locationManager.location = exampleLocation // (for previous)
		sut.locationManager(coreLocationManager, didFailWithError: exampleError)

		XCTAssertEqual(authorizationRequestCount, 1)
		XCTAssertEqual(locationRequestCount, 1)
		XCTAssertEqual(result, [.locating(previous: nil),
		                        .located(exampleLocation),
		                        .failed(error: exampleError, last: exampleLocation)])
	}

	func testUpdateLocationWithoutPermissions() throws {
		var authorizationRequestCount = 0
		var locationRequestCount = 0

		locationManager.authorizationStatus = .notDetermined
		locationManager.requestWhenInUseAuthorizationHandler = { authorizationRequestCount += 1 }
		locationManager.requestLocationHandler = { locationRequestCount += 1 }

		let sut = makeSUT()
		var result: [UserLocationManager.LocationState?] = []
		let exampleLocation = CLLocation()

		sut.updateLocation(requestPermissions: false)
			.subscribe(Subscribers.Sink(receiveCompletion: { _ in }, receiveValue: {
				result.append($0)
			}))

		sut.locationManager(coreLocationManager, didUpdateLocations: [exampleLocation])

		XCTAssertEqual(authorizationRequestCount, 0)
		XCTAssertEqual(locationRequestCount, 0)
		XCTAssertEqual(result, [])
	}
}
