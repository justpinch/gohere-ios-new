import Combine
@testable import GoHere
import XCTest

class UserSearchModelTests: XCTestCase {
	var searchAction: UserSearchModel.SearchAction = { _ in
		Just(PaginatedResponse<User>(count: 0, results: [.fixture1]))
			.mapAPIError()
			.eraseToAnyPublisher()
	}

	var updateStatusAction: UserSearchModel.UpdateStatusAction = { _ in }

	func makeSUT() -> UserSearchModel {
		UserSearchModel(searchAction: searchAction, updateStatusAction: updateStatusAction)
	}

	func testReferenceCycles() throws {
		weak var obj: UserSearchModel?

		_ = {
			let instance = makeSUT()
			obj = instance
		}()

		XCTAssertNil(obj, "Expected instance to be deallocated")
	}

	func testSetSearchSuggestionsIncludeSuggestionsInResults() {
		let sut = makeSUT()
		let wasEmpty = sut.results.isEmpty

		sut.setSearchSuggestions([.fixture3])

		XCTAssertTrue(wasEmpty)
		XCTAssertEqual(sut.results, [.fixture3])
	}
}
