import Combine
@testable import GoHere
import XCTest

class UserConnectionsInteractorTests: XCTestCase {
	var state: UserState!
	let tokenSubject = CurrentValueSubject<AuthToken?, Never>(nil)
	var authService = AuthServiceMock()

	var userService = UserServiceMock() {
		didSet { // default handlers
			userService.followHandler = { _, _ in
				Just(.fixture1).mapAPIError().eraseToAnyPublisher()
			}

			userService.unfollowHandler = { _, _ in
				Just(()).mapAPIError().eraseToAnyPublisher()
			}

			userService.setConnectionStatusHandler = { _, _, _ in
				Just(.fixture1).mapAPIError().eraseToAnyPublisher()
			}

			userService.connectionStatusHandler = { _, _ in
				Just(.fixture1).mapAPIError().eraseToAnyPublisher()
			}

			userService.connectionsHandler = { _, _, _, _, _ -> ApiResponsePublisher<PaginatedResponse<UserConnection>> in
				Just(PaginatedResponse<UserConnection>(count: 2, results: [.fixture1, .fixture2])).mapAPIError().eraseToAnyPublisher()
			}
		}
	}

	override func setUp() {
		tokenSubject.value = .fixture1
		userService = UserServiceMock()
		authService = AuthServiceMock()

		state = UserState(authToken: tokenSubject)
	}

	func makeSUT() -> DefaultUserConnectionsInteractor {
		DefaultUserConnectionsInteractor(state: state, user: state.userID!, userService: userService)
	}

	func testReferenceCycles() throws {
		weak var obj: DefaultUserConnectionsInteractor?

		_ = {
			let instance = makeSUT()

			// call some methods..
			instance.follow(id: "1")
			instance.unfollow(id: "1")
			instance.acceptFollowRequest(for: .fixture1)
			instance.blockFollowRequest(for: .fixture1)
			instance.ignoreFollowRequest(for: .fixture1)
			instance.refreshConnectionRequests()
			instance.updateStatus(for: "1")

			obj = instance
		}()

		XCTAssertNil(obj, "Expected instance to be deallocated")
	}

	func testFollow() {
		let sut = makeSUT()

		let user = UserExtended.fixtureExtended1
		user.followingCount = 0
		state.user = .model(user)

		sut.follow(id: "123")

		XCTAssertEqual(state.connectionStatuses["123"], .model(.active))
		XCTAssertEqual(state.user.model?.followingCount, 1)
	}

	func testFollowError() {
		let sut = makeSUT()

		let user = UserExtended.fixtureExtended1
		user.followingCount = 0
		state.user = .model(user)

		userService.followHandler = { _, _ in
			Fail(error: API.Error.fixture1).eraseToAnyPublisher()
		}

		sut.follow(id: "123")

		XCTAssertEqual(state.user.model?.followingCount, 0)

		// on error, status is updated
		XCTAssertEqual(userService.connectionStatusCallCount, 1)
		XCTAssertEqual(state.connectionStatuses["123"]?.model, .pending)
	}

	func testUnfollow() {
		let sut = makeSUT()

		let user = UserExtended.fixtureExtended1
		user.followingCount = 1
		state.user = .model(user)

		sut.unfollow(id: "123")

		XCTAssertEqual(state.connectionStatuses["123"], .model(.none))
		XCTAssertEqual(state.user.model?.followingCount, 0)
	}

	func testUnfollowError() {
		let sut = makeSUT()

		let user = UserExtended.fixtureExtended1
		user.followingCount = 1
		state.user = .model(user)

		userService.unfollowHandler = { _, _ in
			Fail(error: API.Error.fixture1).eraseToAnyPublisher()
		}

		sut.unfollow(id: "123")

		XCTAssertEqual(state.user.model?.followingCount, 1)

		// on error, status is updated
		XCTAssertEqual(userService.connectionStatusCallCount, 1)
		XCTAssertEqual(state.connectionStatuses["123"]?.model, .pending)
	}

	func testUpdateStatus() {
		let sut = makeSUT()

		sut.updateStatus(for: "123")

		XCTAssertEqual(state.connectionStatuses["123"], .model(.pending))
	}

	func testRefreshConnectionRequests() {
		let sut = makeSUT()

		sut.refreshConnectionRequests()

		XCTAssertEqual(state.connectionRequests, [.fixture1, .fixture2])
	}

	func testAcceptFollowRequest() {
		state.connectionRequests = [.fixture1]
		let connection = state.connectionRequests.first!

		let user = UserExtended.fixtureExtended2
		user.followersCount = 0
		state.user = .model(user)
		tokenSubject.value = .init(userId: user.id, access: "access", refresh: "refresh")

		let sut = makeSUT()

		sut.acceptFollowRequest(for: connection)

		XCTAssertTrue(state.connectionRequests.isEmpty)
		XCTAssertEqual(state.user.model?.followersCount, 1)
		XCTAssertEqual(userService.setConnectionStatusCallCount, 1)

		XCTAssertEqual(userService.setConnectionStatusArgValues.count, 1)
		XCTAssertEqual(userService.setConnectionStatusArgValues[0].0, .active)
		XCTAssertEqual(userService.setConnectionStatusArgValues[0].1, connection.user.id)
		XCTAssertEqual(userService.setConnectionStatusArgValues[0].2, user.id)
	}

	func testAcceptFollowRequestError() {
		state.connectionRequests = [.fixture1]
		let connection = state.connectionRequests.first!

		let user = UserExtended.fixtureExtended2
		user.followersCount = 0
		state.user = .model(user)
		tokenSubject.value = .init(userId: user.id, access: "access", refresh: "refresh")

		userService.setConnectionStatusHandler = { _, _, _ in
			Fail(error: API.Error.fixture1).eraseToAnyPublisher()
		}
		userService.connectionsHandler = { _, _, _, _, _ -> ApiResponsePublisher<PaginatedResponse<UserConnection>> in
			Just(PaginatedResponse<UserConnection>(count: 1, results: [.fixture1])).mapAPIError().eraseToAnyPublisher()
		}

		let sut = makeSUT()

		sut.acceptFollowRequest(for: connection)

		XCTAssertEqual(state.connectionRequests, [.fixture1])
		XCTAssertEqual(state.user.model?.followersCount, 0)
		XCTAssertEqual(userService.setConnectionStatusCallCount, 1)
		XCTAssertEqual(userService.connectionsCallCount, 1) // refreshes on error
	}

	func testUpdatingStatuses() {
		state.connectionStatuses = [:]
		let id1 = "usr-1"
		let id2 = "usr-2"

		userService.connectionStatusesHandler = { _, _ -> ApiResponsePublisher<[BatchUserConnectionStatus]> in
			Just([
				BatchUserConnectionStatus(userID: id1, followingStatus: .active, followerStatus: .blocked),
				BatchUserConnectionStatus(userID: id2, followingStatus: .pending, followerStatus: .blocked),
			])
			.mapAPIError()
			.eraseToAnyPublisher()
		}

		let sut = makeSUT()

		// when
		sut.updateStatuses(for: [id1, id2])

		// then
		XCTAssertEqual(state.connectionStatuses, [
			id1: .model(.active),
			id2: .model(.pending),
		])
	}

	/// Ensure only statuses not already known are updated
	func testUpdatingStatusesOnlyMissingOnes() {
		let id1 = "usr-1"
		let id2 = "usr-2"

		state.connectionStatuses = [id1: .model(.active)]

		userService.connectionStatusesHandler = { _, _ -> ApiResponsePublisher<[BatchUserConnectionStatus]> in
			Just([
				BatchUserConnectionStatus(userID: id1, followingStatus: .ignored, followerStatus: .blocked),
				BatchUserConnectionStatus(userID: id2, followingStatus: .pending, followerStatus: .blocked),
			])
			.mapAPIError()
			.eraseToAnyPublisher()
		}

		let sut = makeSUT()

		// when
		sut.updateStatuses(for: [id1, id2])

		// then
		XCTAssertEqual(state.connectionStatuses, [
			id1: .model(.active),
			id2: .model(.pending),
		])
	}

	/// Ensure all statuses updated if forced
	func testUpdatingStatusesForced() {
		let id1 = "usr-1"
		let id2 = "usr-2"

		state.connectionStatuses = [id1: .model(.active)]

		userService.connectionStatusesHandler = { _, _ -> ApiResponsePublisher<[BatchUserConnectionStatus]> in
			Just([
				BatchUserConnectionStatus(userID: id1, followingStatus: .ignored, followerStatus: .blocked),
				BatchUserConnectionStatus(userID: id2, followingStatus: .pending, followerStatus: .blocked),
			])
			.mapAPIError()
			.eraseToAnyPublisher()
		}

		let sut = makeSUT()

		// when
		sut.updateStatuses(for: [id1, id2], force: true)

		// then
		XCTAssertEqual(state.connectionStatuses, [
			id1: .model(.ignored),
			id2: .model(.pending),
		])
	}
}
