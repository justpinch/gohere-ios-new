import Combine
@testable import GoHere
import XCTest

class UserInteractorTests: XCTestCase {
	var userService = UserServiceMock() {
		didSet {
			// default handlers
			userService.profileHandler = { _ in
				Just.fixture1.mapAPIError().eraseToAnyPublisher()
			}

			userService.connectionsHandler = { _, _, _, _, _ -> ApiResponsePublisher<PaginatedResponse<UserConnection>> in
				Just(PaginatedResponse<UserConnection>(count: 0, results: [])).mapAPIError().eraseToAnyPublisher()
			}
		}
	}

	let tokenSubject = CurrentValueSubject<AuthToken?, Never>(nil)
	var authService = AuthServiceMock()
	var placeService = PlaceServiceMock()
	var appleSignIn = AppleSignInCoordinatorMock()

	var state: UserState!
	var fbAuth: (AuthService) -> ApiResponsePublisher<AuthToken> = { _ in Just.fixture1.mapAPIError().eraseToAnyPublisher() }

	func makeSUT() -> DefaultUserInteractor {
		DefaultUserInteractor(state: state,
		                      authToken: tokenSubject,
		                      authService: authService,
		                      userService: userService,
		                      placeService: placeService,
		                      appleSignInCoordinator: appleSignIn,
		                      facebookAuth: fbAuth)
	}

	override func setUp() {
		tokenSubject.value = nil
		userService = UserServiceMock()
		authService = AuthServiceMock()
		appleSignIn = AppleSignInCoordinatorMock()

		state = UserState(authToken: tokenSubject)
	}

	func testReferenceCycles() throws {
		weak var obj: DefaultUserInteractor?

		_ = {
			let instance = makeSUT()
			obj = instance
		}()

		XCTAssertNil(obj, "Expected instance to be deallocated")
	}

	func testChangeAuthToken() throws {
		let sut = makeSUT()

		// when token: nil -> userA
		tokenSubject.value = AuthToken(userId: "userA", access: "", refresh: "")
		// then
		let connectionsA = sut.connections
		XCTAssertNotNil(connectionsA)

		// when token: userA -> userB
		tokenSubject.value = AuthToken(userId: "userB", access: "", refresh: "")
		// then
		let connectionsB = sut.connections
		XCTAssertNotNil(connectionsB)
		XCTAssertFalse(connectionsA as AnyObject === connectionsB as AnyObject)
	}
}
