import Combine
@testable import GoHere
import XCTest

class UserStateTests: XCTestCase {
	let tokenSubject = CurrentValueSubject<AuthToken?, Never>(nil)

	func makeSUT() -> UserState {
		UserState(authToken: tokenSubject)
	}

	override func setUp() {
		tokenSubject.value = nil
	}

	func testUserID() throws {
		let sut = makeSUT()

		XCTAssertNil(sut.userID)

		tokenSubject.value = AuthToken(userId: "uid1", access: "", refresh: "")
		XCTAssertEqual(sut.userID, "uid1")
	}

	func testIsAuthenticated() throws {
		let sut = makeSUT()

		XCTAssertFalse(sut.isAuthenticated)

		tokenSubject.value = AuthToken(userId: "uid1", access: "", refresh: "")
		XCTAssertTrue(sut.isAuthenticated)
	}
}
