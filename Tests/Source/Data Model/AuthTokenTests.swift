@testable import GoHere
import XCTest

class AuthTokenTests: XCTestCase {
	func testCreateWithAccessToken() throws {
		let token = AuthToken(userId: "uid", access: "access", refresh: "refresh")

		let newToken = token.with(access: "new")

		XCTAssertEqual(newToken.userId, "uid")
		XCTAssertEqual(newToken.access, "new")
		XCTAssertEqual(newToken.refresh, "refresh")
	}
}
