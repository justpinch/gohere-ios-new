@testable import GoHere
import XCTest

class UserTests: XCTestCase {
	func testUserDecode() throws {
		let json = """
		{
		  "id": "b61ee335-39da-42cb-adc5-f59c37d276c6",
		  "email": "info@gohere.app",
		  "phone_number": "1234567890",
		  "first_name": "Go",
		  "last_name": "Here",
		  "profile": {
			"avatar": "avatars/1.jpg",
			"hometown": "Amsterdam",
			"is_private": true
		  }
		}
		"""
		let data = try XCTUnwrap(json.data(using: .utf8))

		let decoder = JSONDecoder()
		decoder.keyDecodingStrategy = .convertFromSnakeCase

		let user = try decoder.decode(User.self, from: data)

		XCTAssertEqual(user.id, "b61ee335-39da-42cb-adc5-f59c37d276c6")
		XCTAssertEqual(user.email, "info@gohere.app")
		XCTAssertEqual(user.phoneNumber, "1234567890")
		XCTAssertEqual(user.firstName, "Go")
		XCTAssertEqual(user.lastName, "Here")
		XCTAssertEqual(user.profile.avatar?.path, "avatars/1.jpg")
		XCTAssertEqual(user.profile.hometown, "Amsterdam")
		XCTAssertEqual(user.profile.isPrivate, true)
	}

	func testUserExtendedDecode() throws {
		let json = """
		{
		  "id": "b61ee335-39da-42cb-adc5-f59c37d276c6",
		  "email": "info@gohere.app",
		  "phone_number": "1234567890",
		  "first_name": "Go",
		  "last_name": "Here",
		  "profile": {
			"avatar": "avatars/1.jpg",
			"hometown": "Amsterdam",
			"is_private": true
		  },
		  "followers_count": 1,
		  "following_count": 2,
		  "review_count": 3,
		  "favorite_count": 4
		}
		"""
		let data = try XCTUnwrap(json.data(using: .utf8))

		let decoder = JSONDecoder()
		decoder.keyDecodingStrategy = .convertFromSnakeCase

		let user = try decoder.decode(UserExtended.self, from: data)

		XCTAssertEqual(user.id, "b61ee335-39da-42cb-adc5-f59c37d276c6")
		XCTAssertEqual(user.email, "info@gohere.app")
		XCTAssertEqual(user.phoneNumber, "1234567890")
		XCTAssertEqual(user.firstName, "Go")
		XCTAssertEqual(user.lastName, "Here")
		XCTAssertEqual(user.profile.avatar?.path, "avatars/1.jpg")
		XCTAssertEqual(user.profile.hometown, "Amsterdam")
		XCTAssertEqual(user.profile.isPrivate, true)
		XCTAssertEqual(user.followersCount, 1)
		XCTAssertEqual(user.followingCount, 2)
		XCTAssertEqual(user.reviewCount, 3)
		XCTAssertEqual(user.favoriteCount, 4)
	}
}
