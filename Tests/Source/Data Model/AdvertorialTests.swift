import CoreLocation
@testable import GoHere
import XCTest

class AdvertorialTests: XCTestCase {
	func testDecode() throws {
		let json = """
		{
			"id": "1",
			"location": {
			  "latitude": 51.57706953722564,
			  "longitude": 4.855957031249998
			},
			"images": [
			  {
				"path": "advertorials/bc847dfc02f447f0928d7eb403970b55.jpeg",
				"aspect_ratio": 1.9190283400809716
			  }
			],
			"publish_date": 1576596770,
			"title": "New MacBook Pro",
			"subtitle": "The best MacBook ever",
			"slug": "new-macbook-pro",
			"target_url": "https://www.apple.com/nl/macbook-pro-16/",
			"cta_button": "order_now",
			"can_hide": true
		}
		"""
		let data = try XCTUnwrap(json.data(using: .utf8))

		let decoder = JSONDecoder()
		decoder.keyDecodingStrategy = .convertFromSnakeCase
		decoder.dateDecodingStrategy = .secondsSince1970

		let advertorial = try decoder.decode(Advertorial.self, from: data)

		XCTAssertEqual(advertorial.id, "1")
		XCTAssertEqual(advertorial.slug, "new-macbook-pro")
		XCTAssertEqual(advertorial.title, "New MacBook Pro")
		XCTAssertEqual(advertorial.subtitle, "The best MacBook ever")
		XCTAssertEqual(advertorial.publishDate.timeIntervalSince1970, 1_576_596_770)
		XCTAssertTrue(advertorial.canHide)
		XCTAssertEqual(advertorial.ctaButton, Advertorial.CallToActionButton.orderNow)
		XCTAssertEqual(advertorial.location?.latitude, 51.57706953722564)
		XCTAssertEqual(advertorial.location?.longitude, 4.855957031249998)
		XCTAssertEqual(advertorial.images.count, 1)
		XCTAssertEqual(advertorial.images.first?.path.path, "advertorials/bc847dfc02f447f0928d7eb403970b55.jpeg")
		XCTAssertEqual(advertorial.images.first?.aspectRatio, 1.9190283400809716)
		XCTAssertEqual(advertorial.targetURL, URL(string: "https://www.apple.com/nl/macbook-pro-16/")!)
	}
}
