import CoreLocation
@testable import GoHere
import XCTest

class FeedTests: XCTestCase {
	func testResponseDecode() throws {
		let data = try FeedResponse.fixture1Data()

		let decoder = JSONDecoder()
		decoder.keyDecodingStrategy = .convertFromSnakeCase
		decoder.dateDecodingStrategy = .secondsSince1970

		let response = try decoder.decode(FeedResponse.self, from: data)

		XCTAssertEqual(response.nextPageToken, "1617784522")
		XCTAssertEqual(response.results.count, 5)

		var item = response.results[0]
		XCTAssertTrue(item.kind.isReview)
		XCTAssertEqual(item.timestamp.timeIntervalSince1970, 1_618_477_907)

		item = response.results[1]
		XCTAssertTrue(item.kind.isAdvertorial)
		XCTAssertEqual(item.timestamp.timeIntervalSince1970, 1_576_596_770)

		item = response.results[2]
		XCTAssertTrue(item.kind.isSavedPlace)
		XCTAssertEqual(item.timestamp.timeIntervalSince1970, 1_617_871_029)

		item = response.results[3]
		XCTAssertTrue(item.kind.isTweet)
		XCTAssertEqual(item.timestamp.timeIntervalSince1970, 1_618_480_921)

		item = response.results[4]
		XCTAssertTrue(item.kind.isInsta)
		XCTAssertEqual(item.timestamp.timeIntervalSince1970, 1_617_805_396)
	}

	func testTweetDecode() throws {
		let data = try FeedResponse.fixture1Data()

		let decoder = JSONDecoder()
		decoder.keyDecodingStrategy = .convertFromSnakeCase
		decoder.dateDecodingStrategy = .secondsSince1970

		let response = try decoder.decode(FeedResponse.self, from: data)

		let item = response.results[3]
		guard case let .tweet(tweet) = item.kind else {
			XCTFail("Expected tweet object")
			return
		}

		XCTAssertEqual(tweet.id, 1_382_635_313_714_106_374)
		XCTAssertEqual(tweet.createdAt.timeIntervalSince1970, 1_618_480_921)
		XCTAssertEqual(tweet.text, "Great weather may not be guaranteed, but one thing you can be sure of is otherworldly vistas, whether the sun comes out or not. Get to know Ireland's 6 national parks. https://t.co/YkDTuUk9sF https://t.co/r8Iy5hXBHe")

		XCTAssertEqual(tweet.user.id, 15_066_760)
		XCTAssertEqual(tweet.user.name, "Lonely Planet")
		XCTAssertEqual(tweet.user.screenName, "lonelyplanet")
		XCTAssertEqual(tweet.user.avatar, URL(string: "https://pbs.twimg.com/profile_images/659349744532246528/oJDWTI75_normal.png")!)

		XCTAssertEqual(tweet.media?.count, 1)
		XCTAssertEqual(tweet.media?.first?.ratio, 1.4938488576449913)
		XCTAssertEqual(tweet.media?.first?.kind, .photo)
		XCTAssertEqual(tweet.media?.first?.thumbnailUrl, URL(string: "https://pbs.twimg.com/media/EzAbfaaWEAIdsbF.jpg")!)
		XCTAssertEqual(tweet.media?.first?.thumbnailUrl, URL(string: "https://pbs.twimg.com/media/EzAbfaaWEAIdsbF.jpg")!)

		XCTAssertEqual(tweet.entities?.count, 2)
		XCTAssertEqual(tweet.entities?.first?.startIndex, 192)
		XCTAssertEqual(tweet.entities?.first?.endIndex, 215)
		XCTAssertEqual(tweet.entities?.first?.text, "https://t.co/r8Iy5hXBHe")
		XCTAssertEqual(tweet.entities?.last?.startIndex, 168)
		XCTAssertEqual(tweet.entities?.last?.endIndex, 191)
		XCTAssertEqual(tweet.entities?.last?.text, "https://t.co/YkDTuUk9sF")
	}

	func testInstagramDecode() throws {
		let data = try FeedResponse.fixture1Data()

		let decoder = JSONDecoder()
		decoder.keyDecodingStrategy = .convertFromSnakeCase
		decoder.dateDecodingStrategy = .secondsSince1970

		let response = try decoder.decode(FeedResponse.self, from: data)

		let item = response.results[4]
		guard case let .instagram(post) = item.kind else {
			XCTFail("Expected tweet object")
			return
		}

		XCTAssertEqual(post.id, "2546658703563943720")
		XCTAssertEqual(post.createdAt.timeIntervalSince1970, 1_617_805_396)
		XCTAssertTrue(post.text.hasPrefix("Ook Alkmaar heeft de smaak te pakken! 🤤"))

		XCTAssertEqual(post.user.id, "21848896265")
		XCTAssertEqual(post.user.username, "gohere.app")
		XCTAssertEqual(post.user.fullName, "GoHere App")
		XCTAssertEqual(post.user.avatar, URL(string: "https://scontent-frx5-1.cdninstagram.com/v/t51.2885-19/s320x320/70894027_402045657358306_6020938266545487872_n.jpg?tp=1&_nc_ht=scontent-frx5-1.cdninstagram.com&_nc_ohc=CWBrTmgG1ogAX8jowd7&edm=ABfd0MgAAAAA&ccb=7-4&oh=6f8d532b157e0244b366685fab5efae2&oe=609C0EE7&_nc_sid=7bff83")!)

		XCTAssertEqual(post.media?.count, 4)
		XCTAssertEqual(post.media?.first?.width, 1080)
		XCTAssertEqual(post.media?.first?.height, 1080)
		XCTAssertEqual(post.media?.first?.url, URL(string: "https://scontent-frx5-1.cdninstagram.com/v/t51.2885-15/e35/169984745_466725427774714_7204097411731471203_n.jpg?tp=1&_nc_ht=scontent-frx5-1.cdninstagram.com&_nc_cat=110&_nc_ohc=Fxe71ECcDkEAX9R90MW&edm=ABfd0MgAAAAA&ccb=7-4&oh=6c508bcf0fad8775283b83315aadecad&oe=609D5414&_nc_sid=7bff83")!)
	}
}

extension FeedItem.Kind {
	var isReview: Bool {
		if case .review = self {
			return true
		}
		return false
	}

	var isAdvertorial: Bool {
		if case .advertorial = self {
			return true
		}
		return false
	}

	var isSavedPlace: Bool {
		if case .savedPlace = self {
			return true
		}
		return false
	}

	var isTweet: Bool {
		if case .tweet = self {
			return true
		}
		return false
	}

	var isInsta: Bool {
		if case .instagram = self {
			return true
		}
		return false
	}
}
