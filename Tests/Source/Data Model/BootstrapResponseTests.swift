import CoreLocation
@testable import GoHere
import MapKit
import XCTest

class BootstrapTests: XCTestCase {
	func testBootstrapDecode() throws {
		let json = """
		{
		  "regions": [
			[
			  [
				5.160140991210937,
				53.407485286760206
			  ],
			  [
				5.113449096679686,
				53.361616391774874
			  ],
			  [
				5.18074035644531,
				53.32964278553432
			  ],
			  [
				5.296096801757811,
				53.3452226658659
			  ],
			  [
				5.421066284179685,
				53.36981089039779
			  ],
			  [
				5.585861206054686,
				53.415670961305345
			  ],
			  [
				5.588607788085937,
				53.45085141324726
			  ],
			  [
				5.522689819335936,
				53.474561324096115
			  ],
			  [
				5.160140991210937,
				53.407485286760206
			  ]
			]
		  ],
		  "bias_location": {
			"latitude": 53.40135460437008,
			"longitude": 5.353636549188749
		  },
		  "banner_id": "banner_id",
		  "uber_client_id": null,
		  "mothership_account_id": "12345",
		  "categories": [
			{
				"id": 100,
				"slug": "other",
				"icon": "place-categories/10-70562e1a5d394a3c87c575e713b0f2c1.png"
			},
			{
				"id": 134,
				"slug": "activity",
				"icon": "place-categories/7d2c2b38bc3f41a4a1bd0a90f18f29cd.png"
			},
		  ]
		}
		"""
		let data = try XCTUnwrap(json.data(using: .utf8))

		let decoder = JSONDecoder()
		decoder.keyDecodingStrategy = .convertFromSnakeCase

		let bootstrap = try decoder.decode(Bootstrap.self, from: data)

		XCTAssertEqual(bootstrap.biasLocation?.latitude ?? 0, 53.40135460437008, accuracy: 0.000001)
		XCTAssertEqual(bootstrap.biasLocation?.longitude ?? 0, 5.353636549188749, accuracy: 0.000001)
		XCTAssertEqual(bootstrap.bannerId, "banner_id")
		XCTAssertNil(bootstrap.uberClientId)

		XCTAssertEqual(bootstrap.defaultRegion?.count, 1)
		try XCTAssertEqual(XCTUnwrap(bootstrap.defaultRegion?.first).count, 9)
		try XCTAssertEqual(XCTUnwrap(bootstrap.defaultRegion?.first)[0].count, 2)
		try XCTAssertEqual(XCTUnwrap(bootstrap.defaultRegion?.first)[0][0], 5.160140991210937, accuracy: 0.00001)
		try XCTAssertEqual(XCTUnwrap(bootstrap.defaultRegion?.first)[0][1], 53.407485286760206, accuracy: 0.00001)

		try XCTAssertEqual(XCTUnwrap(bootstrap.defaultRegion?.first)[8].count, 2)
		try XCTAssertEqual(XCTUnwrap(bootstrap.defaultRegion?.first)[8], XCTUnwrap(bootstrap.defaultRegion?.first)[0])

		XCTAssertEqual(bootstrap.categories.first?.id, 100)
		XCTAssertEqual(bootstrap.categories.last?.id, 134)
	}

	func testDefaultRegionUnionSingle() throws {
		let json = """
			[
				[ [0, 0], [0, 1], [1, 1], [1, 0], [0, 0] ],
			]
		"""
		let data = try XCTUnwrap(json.data(using: .utf8))

		let multiRegion = try JSONDecoder().decode(Bootstrap.MultiRegion.self, from: data)
		let union = try XCTUnwrap(multiRegion.union())

		XCTAssertEqual(multiRegion.count, 1)

		let a = MKMapPoint(x: union.minX, y: union.minY).coordinate
		XCTAssertEqual(a.latitude, 1, accuracy: 0.01)
		XCTAssertEqual(a.longitude, 0, accuracy: 0.01)

		let b = MKMapPoint(x: union.minX, y: union.maxY).coordinate
		XCTAssertEqual(b.latitude, 0, accuracy: 0.01)
		XCTAssertEqual(b.longitude, 0, accuracy: 0.01)

		let c = MKMapPoint(x: union.maxX, y: union.minY).coordinate
		XCTAssertEqual(c.latitude, 1, accuracy: 0.01)
		XCTAssertEqual(c.longitude, 1, accuracy: 0.01)

		let d = MKMapPoint(x: union.maxX, y: union.maxY).coordinate
		XCTAssertEqual(d.latitude, 0, accuracy: 0.01)
		XCTAssertEqual(d.longitude, 1, accuracy: 0.01)
	}

	func testDefaultRegionUnionMulti() throws {
		let json = """
			[
				[ [0, 0], [0, 1], [1, 1], [1, 0], [0, 0] ],
				[ [1, 0], [1, 1], [2, 1], [2, 0], [1, 0] ]
			]
		"""
		let data = try XCTUnwrap(json.data(using: .utf8))

		let multiRegion = try JSONDecoder().decode(Bootstrap.MultiRegion.self, from: data)
		let union = try XCTUnwrap(multiRegion.union())

		XCTAssertEqual(multiRegion.count, 2)

		let a = MKMapPoint(x: union.minX, y: union.minY).coordinate
		XCTAssertEqual(a.latitude, 1, accuracy: 0.01)
		XCTAssertEqual(a.longitude, 0, accuracy: 0.01)

		let b = MKMapPoint(x: union.minX, y: union.maxY).coordinate
		XCTAssertEqual(b.latitude, 0, accuracy: 0.01)
		XCTAssertEqual(b.longitude, 0, accuracy: 0.01)

		let c = MKMapPoint(x: union.maxX, y: union.minY).coordinate
		XCTAssertEqual(c.latitude, 1, accuracy: 0.01)
		XCTAssertEqual(c.longitude, 2, accuracy: 0.01)

		let d = MKMapPoint(x: union.maxX, y: union.maxY).coordinate
		XCTAssertEqual(d.latitude, 0, accuracy: 0.01)
		XCTAssertEqual(d.longitude, 2, accuracy: 0.01)
	}

	func testDefaultRegionUnionEmpty() throws {
		let json = "[]"
		let data = try XCTUnwrap(json.data(using: .utf8))

		let multiRegion = try JSONDecoder().decode(Bootstrap.MultiRegion.self, from: data)
		XCTAssertNil(multiRegion.union())
	}

	func testBootstrapNilMembers() throws {
		let json = """
		{
		  "mothership_account_id": "12345",
		  "regions": null,
		  "bias_location": null,
		  "banner_id": null,
		  "uber_client_id": null,
		  "categories": [],
		}
		"""
		let data = try XCTUnwrap(json.data(using: .utf8))

		let decoder = JSONDecoder()
		decoder.keyDecodingStrategy = .convertFromSnakeCase

		let bootstrap = try decoder.decode(Bootstrap.self, from: data)

		XCTAssertNil(bootstrap.biasLocation)
		XCTAssertNil(bootstrap.bannerId)
		XCTAssertNil(bootstrap.uberClientId)
		XCTAssertNil(bootstrap.defaultRegion)
		XCTAssertEqual(bootstrap.categories, [])
	}
}
