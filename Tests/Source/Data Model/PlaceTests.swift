@testable import GoHere
import XCTest

class PlaceTests: XCTestCase {
	let encoder = JSONEncoder()
	let decoder = JSONDecoder()

	override func setUp() {
		encoder.keyEncodingStrategy = .convertToSnakeCase
		decoder.keyDecodingStrategy = .convertFromSnakeCase
	}

	func testPlaceDecode() throws {
		let data = try Place.fixture1Data()

		let place = try decoder.decode(Place.self, from: data)

		XCTAssertEqual(place.id, "410cbbb3-d4cf-4bd5-8117-88966c73deb3")
		XCTAssertEqual(place.idGoogle, "ChIJf5WgALkrx0cRBZ7csZMMNmk")
		XCTAssertEqual(place.name, "TravelXL - Riks de Jong")
		XCTAssertEqual(place.address, "Antoon Meurkensstraat 14, 6021 LC Budel, Netherlands")
		XCTAssertEqual(place.city, "Budel")
		XCTAssertEqual(place.country, "Netherlands")
		XCTAssertEqual(place.website, URL(string: "https://www.travelxl.nl/riks-de-jong")!)
		XCTAssertEqual(place.phone, "+31 495 494 908")
		XCTAssertEqual(place.photo?.path, "place/41-3664f3f590d84ba9a4a378f0b0317148")
		XCTAssertEqual(place.location.latitude, 51.2747322, accuracy: 0.001)
		XCTAssertEqual(place.location.longitude, 5.571525399999999, accuracy: 0.001)
		XCTAssertEqual(place.category.id, 119)
		XCTAssertEqual(place.category.slug, "bakery")
		XCTAssertEqual(place.category.icon.path, "place-categories/d85b1486e1fd40dd9cc04e5d82cd796d.png")
		XCTAssertEqual(place.affiliate?.kind, "tiqets")
		XCTAssertEqual(place.affiliate?.actionURL, URL(string: "https://www.tiqets.com/en/edinburgh-c21/john-knox-house-p974624?partner=gohere")!)
		XCTAssertEqual(place.affiliate?.callToAction, NSLocalizedString("order_now", comment: ""))
	}

	func testPlaceEncodeDecode() throws {
		let place = try Place(id: "1",
		                      idGoogle: "gid",
		                      name: "p1",
		                      address: "addr",
		                      city: "city",
		                      country: "country",
		                      location: .init(latitude: 1, longitude: 2),
		                      category: .init(id: 100, slug: "other", icon: .init(path: "path")),
		                      website: URL(string: "https://gohere.app/")!,
		                      phone: "1234567890",
		                      affiliate: Place.AffiliateInfo.fixture1(),
		                      photo: ImagePath(path: "path"),
		                      info: Place.Info(openingHours: OpeningHours(periods: [.init(open: .init(day: 1, time: "1"), close: .init(day: 2, time: "2"))])),
		                      averageRating: 4,
		                      labels: ["label1", "label2"],
		                      isFavorite: true,
		                      lastReview: nil)

		let data = try encoder.encode(place)
		let result = try decoder.decode(Place.self, from: data)

		XCTAssertEqual(result.id, place.id)
		XCTAssertEqual(result.idGoogle, place.idGoogle)
		XCTAssertEqual(result.name, place.name)
		XCTAssertEqual(result.address, place.address)
		XCTAssertEqual(result.city, place.city)
		XCTAssertEqual(result.country, place.country)
		XCTAssertEqual(result.location, place.location)
		XCTAssertEqual(result.category, place.category)
		XCTAssertEqual(result.website, place.website)
		XCTAssertEqual(result.phone, place.phone)
		XCTAssertEqual(result.affiliate, place.affiliate)
		XCTAssertEqual(result.photo, place.photo)
		XCTAssertEqual(result.info, place.info)
		XCTAssertEqual(result.averageRating, place.averageRating)
		XCTAssertEqual(result.labels, place.labels)
		XCTAssertEqual(result.isFavorite, place.isFavorite)
	}

	func testOpeningHoursPeriodAlwaysOpen() throws {
		let alwaysOpen = OpeningHours.Period(open: .init(day: 0, time: "0000"), close: nil)
		let notAlwaysOpen1 = OpeningHours.Period(open: .init(day: 0, time: "0000"), close: .init(day: 0, time: "0000"))
		let notAlwaysOpen2 = OpeningHours.Period(open: .init(day: 0, time: "0000"), close: .init(day: 1, time: "0000"))
		let notAlwaysOpen3 = OpeningHours.Period(open: .init(day: 0, time: "0001"), close: nil)

		XCTAssertTrue(alwaysOpen.isAlwaysOpen)
		XCTAssertFalse(notAlwaysOpen1.isAlwaysOpen)
		XCTAssertFalse(notAlwaysOpen2.isAlwaysOpen)
		XCTAssertFalse(notAlwaysOpen3.isAlwaysOpen)
	}
}

class PlaceLookupTests: XCTestCase {
	let encoder = JSONEncoder()
	let decoder = JSONDecoder()

	override func setUp() {
		encoder.keyEncodingStrategy = .convertToSnakeCase
		decoder.keyDecodingStrategy = .convertFromSnakeCase
	}

	func testDecode() throws {
		let data = try PlaceLookup.fixture1Data()

		let place = try decoder.decode(PlaceLookup.self, from: data)

		XCTAssertEqual(place.lookupId, "ChIJ5Z2mAfwzdA0R8FocaWWWnUg")
		XCTAssertEqual(place.providerId, 1)
		XCTAssertEqual(place.title, "Tchorro")
		XCTAssertEqual(place.vicinity, "El Hoceima")
		XCTAssertEqual(place.category?.id, 119)
		XCTAssertEqual(place.category?.slug, "bakery")
		XCTAssertEqual(place.category?.icon.path, "place-categories/d85b1486e1fd40dd9cc04e5d82cd796d.png")
		XCTAssertEqual(place.location.latitude, 35.237641, accuracy: 0.001)
		XCTAssertEqual(place.location.longitude, -3.934683199999999, accuracy: 0.001)
	}

	func testEncodeDecode() throws {
		let place = PlaceLookup(lookupId: "1",
		                        providerId: 1,
		                        title: "title",
		                        vicinity: "vicinity",
		                        location: .init(latitude: 1, longitude: 2),
		                        category: .init(id: 123, slug: "slug", icon: .init(path: "path")))

		let data = try encoder.encode(place)
		let result = try decoder.decode(PlaceLookup.self, from: data)

		XCTAssertEqual(result, place)
	}
}
