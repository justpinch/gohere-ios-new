import Foundation
import XCTest

class DataLoadingClass {}

extension Data {
	static func fromTestFile(name: String, extension ext: String) throws -> Data {
		let url = Bundle(for: DataLoadingClass.self).url(forResource: name, withExtension: ext)
		return try Data(contentsOf: XCTUnwrap(url))
	}
}
