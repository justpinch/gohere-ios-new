import Combine
import Foundation
@testable import GoHere
import XCTest

extension RequestPublisherConvertible {
	/// Turns asynchronous publisher into synchronous method
	/// which returns the URLRequest or raises an Error
	func build() throws -> URLRequest {
		let semaphore = DispatchSemaphore(value: 1)
		var result: Result<URLRequest, Error>?

		_ = publisher()
			.sink { completion in
				switch completion {
					case .finished:
						semaphore.signal()
					case let .failure(error):
						result = .failure(error)
				}
			} receiveValue: { request in
				result = .success(request)
			}

		semaphore.wait()
		switch try XCTUnwrap(result) {
			case let .success(request):
				return request
			case let .failure(error):
				throw error
		}
	}
}
