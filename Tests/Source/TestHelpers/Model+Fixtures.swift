import Combine
import Foundation
@testable import GoHere

protocol JSONTestDataLoadable: Decodable {
	static var filename: String { get }
}

let decoder: JSONDecoder = {
	let d = JSONDecoder()
	d.keyDecodingStrategy = .convertFromSnakeCase
	return d
}()

extension JSONTestDataLoadable {
	static var filename: String { String(describing: Self.self) }

	static func fixture1Data() throws -> Data {
		try Data.fromTestFile(name: filename, extension: "json")
	}

	static func fixture1() throws -> Self {
		let data = try fixture1Data()
		return try decoder.decode(Self.self, from: data)
	}
}

// MARK: - Combine helpers

extension Just where Output: JSONTestDataLoadable {
	static var fixture1: AnyPublisher<Output, Error> {
		do {
			let model = try Output.fixture1()
			return Just(model).setFailureType(to: Error.self).eraseToAnyPublisher()
		} catch {
			return Fail(error: error).eraseToAnyPublisher()
		}
	}
}

// MARK: - Models

extension Bootstrap {
	static var fixture1: Bootstrap {
		Bootstrap(mothershipId: User.fixture1.id, categories: [.fixture1, .fixture2], defaultRegion: nil, biasLocation: nil, bannerId: "banner1", uberClientId: "uber1")
	}

	static var fixture2: Bootstrap {
		Bootstrap(mothershipId: User.fixture2.id, categories: [.fixture1, .fixture2], defaultRegion: nil, biasLocation: nil, bannerId: "banner2", uberClientId: "uber2")
	}
}

extension AuthToken: JSONTestDataLoadable {}
extension Advertorial: JSONTestDataLoadable {}
extension FeedResponse: JSONTestDataLoadable {}
extension PlaceAutocomplete: JSONTestDataLoadable {}
extension PlaceLookup: JSONTestDataLoadable {}
extension Place: JSONTestDataLoadable {}
extension Place.AffiliateInfo: JSONTestDataLoadable {}
extension UserExtended: JSONTestDataLoadable {}

extension API.FormValidationError: JSONTestDataLoadable {}
extension API.RateLimitError: JSONTestDataLoadable {}
extension API.DomainError: JSONTestDataLoadable {}
