import Foundation
@testable import GoHere
import XCTest

import Combine
import CoreLocation

class MockPlaceExternalService: PlaceExternalService {
	enum MockError: Error {
		case valueDoesNotExist
	}

	var exploreStorage: [ApiResponsePublisher<[PlaceLookup]>] = []

	func reset() {
		exploreStorage.removeAll()
	}

	func explore(query _: String?, location _: CLLocationCoordinate2D) -> ApiResponsePublisher<[PlaceLookup]> {
		guard !exploreStorage.isEmpty else {
			return Fail(error: API.Error(reason: MockError.valueDoesNotExist)).eraseToAnyPublisher()
		}

		return exploreStorage.removeFirst()
	}

	func `import`(lookupId _: String, providerId _: Int) -> ApiResponsePublisher<Place> {
		fatalError("Not implemented")
	}

	func autocomplete(query _: String, location _: CLLocationCoordinate2D) -> ApiResponsePublisher<[PlaceAutocomplete]> {
		fatalError("Not implemented")
	}

	func geocode(query _: String) -> ApiResponsePublisher<[PlaceLookup]> {
		fatalError("Not implemented")
	}
}
