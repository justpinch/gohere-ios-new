import Foundation
@testable import GoHere

import Combine

// MARK: - CredentialsStorage

class MockCredentialsStorage: CredentialsStorage {
	var authToken = CurrentValueSubject<AuthToken?, Never>(nil)

	func reset() {
		authToken = CurrentValueSubject<AuthToken?, Never>(nil)
	}
}
