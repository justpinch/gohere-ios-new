import Foundation
@testable import GoHere

import Combine

// MARK: - NetworkingSession

class MockNetworkingSession: NetworkingSession {
	var results: [URLRequest: Result<NetworkDataOutput, Error>] = [:]
	var defaultResult: Result<NetworkDataOutput, Error>?

	var calls: [URLRequest] = []

	struct MockError: Swift.Error {}

	func reset() {
		defaultResult = nil
		results.removeAll()
		calls.removeAll()
	}

	func publisher(for request: URLRequest) -> AnyPublisher<NetworkDataOutput, Error> {
		calls.append(request)

		guard let result = results[request] ?? defaultResult else {
			return Fail(outputType: NetworkDataOutput.self, failure: MockError())
				.eraseToAnyPublisher()
		}

		switch result {
			case let .success(response):
				return Just(response)
					.setFailureType(to: Error.self)
					.eraseToAnyPublisher()
			case let .failure(error):
				return Fail(outputType: NetworkDataOutput.self, failure: error)
					.eraseToAnyPublisher()
		}
	}
}
