import Combine
import Foundation

extension NotificationCenterProtocolMock {
	static func withDefaults() -> NotificationCenterProtocolMock {
		let mock = NotificationCenterProtocolMock()

		mock.publisherHandler = { name, _ in
			Just(Notification(name: name)).eraseToAnyPublisher()
		}

		mock.postHandler = { _, _, _ in }

		return mock
	}
}
