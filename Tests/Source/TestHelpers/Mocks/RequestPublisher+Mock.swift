import Foundation
@testable import GoHere

import Combine

// MARK: - RequestPublisherConvertible

struct MockRequestPublisherConvertible: RequestPublisherConvertible {
	var result: AnyPublisher<URLRequest, Error>
	func publisher() -> AnyPublisher<URLRequest, Error> { result }
}
