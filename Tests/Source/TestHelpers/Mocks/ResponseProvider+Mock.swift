import Foundation
@testable import GoHere

import Combine

// MARK: - ResponseProvider

class MockResponseProvider: ResponseProvider {
	enum MockError: Error {
		case missingResult
	}

	var results: [Result<(data: Decodable, response: URLResponse), API.Error>] = []
	var requests: [RequestPublisherConvertible] = []

	func reset() {
		results.removeAll()
		requests.removeAll()
	}

	func response<M>(for request: RequestPublisherConvertible) -> ApiResponsePublisher<NetworkDecodedOutput<M>> where M: Decodable {
		self.requests.append(request)

		guard !results.isEmpty else {
			return Fail(error: API.Error(reason: MockError.missingResult)).eraseToAnyPublisher()
		}

		let result = results.removeFirst()

		switch result {
			case let .success(output):
				return Just((data: output.data as! M, response: output.response))
					.setFailureType(to: API.Error.self)
					.eraseToAnyPublisher()
			case let .failure(error):
				return Fail(error: error).eraseToAnyPublisher()
		}
	}
}
