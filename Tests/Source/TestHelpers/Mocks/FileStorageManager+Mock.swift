import Foundation
@testable import GoHere

import Combine

// MARK: - FileStorageManager

class MockFileManager: FileStorageManager {
	var cacheDirectoryURL = FileManager.default.temporaryDirectory
	var storage: [URL: Data] = [:]
	var contentsOfUrlSideEffects: [URL: Error] = [:]

	func reset() {
		storage.removeAll()
		contentsOfUrlSideEffects.removeAll()
	}

	func url(for key: String) -> URL {
		cacheDirectoryURL.appendingPathComponent(key)
	}

	func write(data: Data, to url: URL) throws {
		storage[url] = data
	}

	func contents(of url: URL) throws -> Data? {
		if let error = contentsOfUrlSideEffects[url] {
			throw error
		}

		return storage[url]
	}
}
