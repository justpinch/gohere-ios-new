import Foundation
@testable import GoHere

import Combine

// MARK: - FileCache

class MockFileCache: FileCacheProtocol {
	enum MockError: Error {
		case invalidValueType
	}

	var storage: [String: Any] = [:]

	func reset() {
		storage.removeAll()
	}

	func cache<Value>(_ value: Value, key: String) throws where Value: Encodable {
		storage[key] = value
	}

	func value<Value>(for key: String) throws -> Value? where Value: Decodable {
		guard let value = storage[key] else {
			return nil
		}

		if let typedValue = value as? Value {
			return typedValue
		}

		throw MockError.invalidValueType
	}
}
