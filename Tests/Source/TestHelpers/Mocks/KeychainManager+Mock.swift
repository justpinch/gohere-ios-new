import Foundation
@testable import GoHere

import Combine

// MARK: - KeychainManager

class MockKeychain: KeychainManager {
	enum MockError: Error {
		case missingMockData
	}

	var dataForKeyMocks: [(String) throws -> Data] = []
	var setDataForKeyMocks: [(Data, String) throws -> Void] = []
	var setStringForKeyMocks: [(String, String) throws -> Void] = []
	var deleteDataForKeyMocks: [(String) throws -> Void] = []
	var resetMocks: [() throws -> Void] = []

	func resetAllMocks() {
		dataForKeyMocks.removeAll()
		setDataForKeyMocks.removeAll()
		setStringForKeyMocks.removeAll()
		deleteDataForKeyMocks.removeAll()
		resetMocks.removeAll()
	}

	@objc func data(for key: String) throws -> Data {
		guard !dataForKeyMocks.isEmpty else { throw MockError.missingMockData }
		let next = dataForKeyMocks.removeFirst()
		return try next(key)
	}

	func set(_ data: Data, for key: String) throws {
		guard !setDataForKeyMocks.isEmpty else { return }
		let next = setDataForKeyMocks.removeFirst()
		try next(data, key)
	}

	func set(_ string: String, for key: String) throws {
		guard !setStringForKeyMocks.isEmpty else { return }
		let next = setStringForKeyMocks.removeFirst()
		try next(string, key)
	}

	func deleteData(for key: String) throws {
		guard !deleteDataForKeyMocks.isEmpty else { return }
		let next = deleteDataForKeyMocks.removeFirst()
		try next(key)
	}

	func reset() throws {
		guard !resetMocks.isEmpty else { return }
		let next = resetMocks.removeFirst()
		try next()
	}
}
